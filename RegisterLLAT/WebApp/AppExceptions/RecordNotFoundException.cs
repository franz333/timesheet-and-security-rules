﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.AppExceptions
{
	public class RecordNotFoundException : Exception
	{
		public RecordNotFoundException() : base()
		{

		}
		
		public RecordNotFoundException(string message) : base(message)
		{

		}

		public RecordNotFoundException(string message, Exception innerException) : base(message, innerException)
		{

		}
	}
}
