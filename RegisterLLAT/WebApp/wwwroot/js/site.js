﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.

//// Settings override for Jquery Unobstrusive
//var settings = {
//    errorElement: "div",
//    errorClass: "text-danger",
//    errorPlacement: function (error, element) {
//        var elm = $(element);

//        if (elm.parent('.input-group').length || elm.parent('.input-group-custom').length) {
//            error.insertAfter(elm.parent());
//        }
//        else if (elm.prop('type') === 'checkbox' || elm.prop('type') === 'radio') {
//            error.appendTo(elm.closest(':not(input, label, .checkbox, .radio)').first());
//        }
//        else if (elm.closest(".customTimeBlock")) {
//            elm.closest(".customTimeBlock").append(error);
//            //elm.closest(".customTimeBlock > div .text-danger").addClass("mb-3");
//            var elmId = "#" + elm.attr('id') + "-error";
//            $(elmId).addClass("mb-3");
//        } else {
//            error.insertAfter(elm);
//        }
//    },
//    submitHandler: function (form) {
//        showLoading();
//        form.submit();
//    }
//}
//$.validator.unobtrusive.options = settings;

//// Override Jquery validate messages for built in and custom rules
//jQuery.extend(jQuery.validator.messages, {
//    //required: "Ce champ est requis.",
//    //remote: "Please fix this field.",
//    //email: "Entrez une adresse e-mail valide.",
//    //url: "Entrez une URL valide.",
//    //date: "Entrez une date valide.",
//    //dateISO: "Please enter a valid date (ISO).",
//    //number: "Entrez un nombre valide.",
//    //digits: "Entrez des chiffres uniquement.",
//    //creditcard: "Entrez une carte de crédit valide.",
//    //equalTo: "Entrez la même valeur.",
//    //accept: "Please enter a value with a valid extension.",
//    //maxlength: jQuery.validator.format("Please enter no more than {0} characters."),
//    //minlength: jQuery.validator.format("Please enter at least {0} characters."),
//    //rangelength: jQuery.validator.format("Please enter a value between {0} and {1} characters long."),
//    //range: jQuery.validator.format("Please enter a value between {0} and {1}."),
//    //max: jQuery.validator.format("Please enter a value less than or equal to {0}."),
//    //min: jQuery.validator.format("Please enter a value greater than or equal to {0}."),
//    dateHTMLInput: "La date doit être supérieure au 01-01-2020.",
//    unique: "Cette date est un doublon."
//});

// Make sure a field be required if another related field's value is 0
$.validator.addMethod("required-if-otherfield-is-zero", function (value, element, params) {      

    // if the element is blank AND it's not required
    if (this.optional(element)) {  
        return true;
    }

    var otherfield = $("#" + params.otherfield).val();      

    var nullValue = false;

    if (isNullOrWhitespace(value) || value == null) {
        nullValue = true;
        console.log("passe dans le chack de la value");
    }

    // /^\s*$/.test(value)

    if (otherfield === "0" && nullValue == true) {
        console.log("passe dans le test en js côté validator method");
        return false;
    } else {
        return true;
    }
   
});


$.validator.unobtrusive.adapters.add("required-if-otherfield-is-zero", ["otherfield"], function (options) {

    // Identify the displayed dropdown list
    var par = $("select#" + options.params['otherfield']);

    if (par.length) {
        console.log("youpie");

        var otherField = par.attr("Id");

        var params = {
            otherfield: otherField,
        };

        options.rules["required-if-otherfield-is-zero"] = params;

        if (options.message) {
            options.messages["required-if-otherfield-is-zero"] = options.message;
        }        
    }
});

// Make sure an numeric value be either empty or positive
$.validator.addMethod("empty-or-positive", function (value, element, params) {

    if (this.optional(element)) {  
        return true;
    }

    var parsedValue = parseInt(value, 10);

    if (isNaN(parsedValue) == true || parsedValue < 0) {
        return false;
    }
    else {
        return true;
    }

});

$.validator.unobtrusive.adapters.add("empty-or-positive", function (options) {
    options.rules["empty-or-positive"] = {};
    options.messages["empty-or-positive"] = options.message;
});


function disableActiveTab() {
    for (i = 0; i < arguments.length; i++) {
        if (arguments[i].hasClass("active")) {
            arguments[i].removeClass("active");
            arguments[i].addClass("disabled");
        }
    };
};

function enableActiveTab(element) {
    element.removeClass("disabled");
    element.addClass("active");
}

function fadeOutCustom(selector, time) {
    selector.fadeOut(time);
}

function hideElements(...args) {
    for (let i = 0; i < args.length; i++) {
        if (!args[i].hasClass("d-none")) {
            args[i].addClass("d-none")
        }
    };
}

function showElements(...args) {
    for (let i = 0; i < args.length; i++) {

        if (args[i].hasClass("d-none")) {
            args[i].removeClass("d-none")
        }
    };
}

function isNullOrWhitespace(input) {
    if (typeof input === 'undefined' || input == null) return true;
    return input.replace(/\s/g, '').length < 1;
}


// Get tomorrow's date in yyyy-mm-dd
// chn.slice(indiceDebut[, indiceFin])
function DateTomorrow() {
    var today = new Date();
    // seconds * minutes * hours * milliseconds = 1 day
    var day = 60 * 60 * 24 * 1000;
    var tomorrow = new Date(today.getTime() + day);
    return tomorrow.toISOString().slice(0, 10);
}

//function GetTodaysDate() {
//    var date = new Date();
//    var day = date.getDate();
//    var month = date.getMonth() + 1;
//    var year = date.getFullYear();
//    if (month < 10) month = "0" + month;
//    if (day < 10) day = "0" + day;
//    var today = year + "-" + month + "-" + day;
//    return today;
//}

// Display or hide "Supprimer définitivement" button in Company/Edit
function ToggleBtnVisibilityOnCheckBox(checkBox, element) {
    if (checkBox.is(":checked")) {
        if (element.hasClass("d-none")) {
            element.removeClass("d-none");
            element.addClass("d-inline-block");
        }
    }
    else {
        if (!element.hasClass("d-none")) {
            element.removeClass("d-inline-block");
            element.addClass("d-none");
        }
    }
}

// Hide modal grey background if it remains after modal closing
// https://stackoverflow.com/a/37916303/9433438
function HideModalBackDrop() {
    $('.modal').on('hidden.bs.modal', function () {
        if ($('.modal-backdrop').length) {
            //remove the backdrop
            $('.modal-backdrop').remove();
        }
    })
};

// Hide modal grey background if it remains after modal closing && Reload page
function ReloadPageOnModalClose() {
    $('.modal').on('hidden.bs.modal', function () {
        if ($('.modal-backdrop').length) {
            //remove the backdrop
            $('.modal-backdrop').remove();
        }
        window.location.reload(false);
    });
}


function DisplayModal(title, message) {

    $.ajax({
        url: "/Modal/DisplayModal",
        data: { title, message },
        type: "GET",
        success: function (result) {
            $('#modal-placeholder').html(result);
            $('#modal-placeholder > .modal').modal('show');
        }
    });
}

// Filter list of companies and person
function ListFilter(controllerArg, containerArg) {

    let statusId = $("#StatusDropDown").val();
    let search = $("input[name='SearchString']").val();
    let isNew = $("input[name='IsNew']").is(":checked");

    let container = containerArg;
    let controller = controllerArg;

    $.ajax({
        url: "/" + controller + "/Filter",
        data: { statusId, search, isNew },
        type: "POST",
        success: function (result) {
            container.html(result);
        }
    });
}

// Display role description on dropdown roleId change
function DisplayRoleDescription() {
    
    let roleId = $("#RoleId").val();
    var t = $("input[name='__RequestVerificationToken']").val();

    $.ajax({
        url: "/Admin/GetRoleDescription",
        data: { roleId: roleId },
        type: "POST",
        header:
        {
            "RequestVerificationToken": t
        },
        success: function (result) {
            $("textarea#RoleDescription").val(result);
        }
    });
}
