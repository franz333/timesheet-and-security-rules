﻿using DAL.Services;
using DALE = DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Models.RegisterViewModels;
using WebApp.AppTools;
using WebApp.Models;

namespace WebApp.Facades.Register
{
	public class RegisterFacade
	{
		// RegisterPlanning : SetStatus

		/// <summary>
		/// Check whether the status is provided or existing. Returns null if check passes or a string with error message if check fails.
		/// </summary>
		/// <param name="session"></param>
		/// <param name="status"></param>
		/// <returns></returns>
		public string CheckStatus(User session, SetStatus status)
		{
			string result = null;

			// Reset session if things changed after navigation (Next && Previous)
			if (session.StatusId > 0 && (session.StatusId != status.Id))
			{
				session.StatusId = status.Id;
				session.CompanyId = 0;
				session.DepartmentId = null;
				session.PersonVisited = null;
			}

			if (status.Id == 0)
			{
				result = "Veuillez sélectionner un status.";
			}
			else
			{
				// Check that status exists
				CompanyStatusService serv = new CompanyStatusService();
				DALE.CompanyStatus statusExists = serv.Get(status.Id);

				if (statusExists != null)
				{
					session.StatusId = status.Id;

					// If the user is an intern, its company is SNCB
					if (status.Id == 1)
					{
						session.CompanyId = 1;
					}
				}
				else
				{
					session.StatusId = 0;
					status.Id = 0;
					result = "ce status n'existe pas";
				}
			}

			return result;
		}

		
		// RegisterPlanning : Company
		/// <summary>
		/// Populate SetCompany with lists of companies and departments for dropdown menus. Return a SetCompany.
		/// </summary>
		/// <param name="company"></param>
		/// <returns></returns>
		public SetCompany PrepareCompany(SetCompany company)
		{
			// If the user is part of SNCB, get departments only
			if (company.CompanyStatusId == 1)
			{
				company.Departments = DepartmentsDD(company.CompanyStatusId, (int)company.Id);
			}
			else
			{
				// Get companies for dropdown
				company.Companies = CompaniesDD(company.CompanyStatusId);

				if (company.Companies != null)
				{
					// Get departments for dropdown
					if (company.DepartmentId > 0)
					{
						company.Departments = DepartmentsDD(company.CompanyStatusId, (int)company.Id);
					}
				}
				else
				{
					// If no dropdown list with companies is provided, force user to enter the company's name
					company.Id = 0;
				}
			}
			return company;
		}

		/// <summary>
		/// Check whether the provided company and departments exist. Return a null string if check pass or a string error message if checks fail.
		/// </summary>
		/// <param name="session"></param>
		/// <param name="company"></param>
		/// <returns></returns>
		public string CheckCompany(SetCompany company)
		{
			string result = null;

			CompanyService servC = new CompanyService();
			DepartmentService servD = new DepartmentService();

			// User provides companyId
			if (company.Id > 0)
			{
				// Check whether the company exists
				DALE.Company companyExists = servC.Get((int)company.Id, company.CompanyStatusId);

				if (companyExists != null)
				{
					// Check wether the company requires a department
					IEnumerable<DALE.Department> departments = servD.GetAllDepartmentByCompany((int)company.Id)
						.ToList();

					// Company needs a department
					if (departments.Count() > 0)
					{
						if (company.DepartmentId > 0)
						{
							// Check that provided department is in DB
							bool isDepartmentValid = departments
								.Any(d => d.CompanyId == (int)company.Id && d.Id == company.DepartmentId);

							if (isDepartmentValid == false)
							{
								result = "Ce départment n'existe pas.";
							}
						}
						// department provided by input
						else if (company.DepartmentId == 0)
						{
							if (!String.IsNullOrWhiteSpace(company.DepartmentName))
							{
								// check if department does not already exist
								bool departmentNameExists = departments
									.Any(d => d.DepartmentName.ToLower() == company.DepartmentName.ToLower());

								if (departmentNameExists == true)
								{
									result = "Cette société se trouve déjà dans la liste, veuillez la sélectionner.";
								}
							}
							else
							{
								result = "Veuillez fournir un nom valide pour le département.";
							}
						}
						// No department was provided, return to view
						else
						{
							result = "Un département doit être renseigné pour cette société, veuillez recommencer.";
						}
					}
				}
				// the company does not exist, return view
				else
				{
					result = "Cette société n'existe pas";
				}
			}
			// User provides company Name
			else if (company.Id == 0)
			{
				if (!String.IsNullOrWhiteSpace(company.Name))
				{
					// Check that the company name is not already taken
					DALE.Company companyExist = servC.Get(company.Name.ToLower()).FirstOrDefault();

					if (companyExist != null)
					{
						// check that the company belongs to the same status
						if (companyExist.CompanyStatusId == company.CompanyStatusId)
						{
							result = "Cette société existe déjà, sélectionnez-là dans la liste.";
						}
						else
						{
							result = String.Format("Cette société existe déjà sous le status \"{0}\", veuillez revenir en arrière et sélectionnez le bon status", companyExist.StatusName);
						}
					}
				}
				else
				{
					result = "Veuillez spécifier un nom pour la société.";
				}
			}
			// User did not make any choice
			else
			{
				result = "Veuillez choisir une société.";
			}

			return result;
		}

		/// <summary>
		/// Returns a list of companies for a dropdown or null if no companies exist.
		/// </summary>
		/// <param name="statusId"></param>
		/// <returns></returns>
		private List<Company> CompaniesDD(int statusId)
		{
			CompanyService service = new CompanyService();
			List<Company> companies = service.GetAllCompanyByStatusWithoutDep(statusId)
										.Select(c => c.DalToCompanyApp())
										.ToList();

			if (companies.Count() > 0)
			{
				companies.Insert(0, new Company { Id = 0, CompanyName = "Autre" });
			}
			else
			{
				companies = null;
			}

			return companies;
		}

		/// <summary>
		/// Returns a list of departments for a dropdown. If the company is SNCB, remove the Atelier d'Arlon department. Returns null if no departments exist.
		/// </summary>
		/// <param name="statusId"></param>
		/// <param name="companyId"></param>
		/// <returns></returns>
		private List<Department> DepartmentsDD(int statusId, int companyId)
		{
			DepartmentService service = new DepartmentService();
			List<Department> departments = new List<Department>();

			if (statusId == 1)
			{
				departments = service.GetAllDepartmentByCompany(companyId)
					.Where(s => s.Id != 2)
					.Select(d => d.DalToDepartmentApp())
					.ToList();
			}
			else
			{
				departments = service.GetAllDepartmentByCompany(companyId)
					.Select(d => d.DalToDepartmentApp())
					.ToList();
			}

			if (departments.Count() > 0)
			{
				departments.Insert(0, new Department { Id = 0, Name = "Autre" });
			}
			else
			{
				departments = null;
			}						

			return departments;
		}

		/// <summary>
		/// Populate a SetCompany with session data.
		/// </summary>
		/// <param name="user"></param>
		/// <returns></returns>
		public SetCompany SessionToSetCompany(User user)
		{
			SetCompany company = new SetCompany();

			company.CompanyStatusId = user.StatusId;

			// Handel company's id null value to prevent 0 as a selected list item
			if (user.CompanyId == 0 && user.CompanyName == null)
			{
				company.Id = null;
			}
			else
			{
				company.Id = user.CompanyId;
				company.Name = user.CompanyName;
			}

			// Handel department's id null value to prevent 0 as a selected list item
			if (user.DepartmentId == 0 && user.DepartmentName == null)
			{
				company.DepartmentId = null;
			}
			else
			{
				company.DepartmentId = user.DepartmentId;
				company.DepartmentName = user.DepartmentName;
			}

			return company;
		}

		/// <summary>
		/// Save a new company. Return the id of the inserted element.
		/// </summary>
		/// <param name="company"></param>
		/// <returns></returns>
		public int SaveCompany(SetCompany company)
		{
			// TODO Try Catch with error handling
			CompanyService service = new CompanyService();

			int id = service.Insert(new DALE.Company
			{
				CompanyStatusId = company.CompanyStatusId,
				CompanyName = company.Name
			});

			return id;
		}

		public int SaveDepartment(SetCompany company)
		{
			// TODO Try Catch with error handling
			DepartmentService service = new DepartmentService();

			int id = service.Insert(new DALE.Department
			{
				CompanyId = (int)company.Id,
				DepartmentName = company.DepartmentName,
			});
			return id;
		}
	}
}
