﻿using Microsoft.AspNetCore.Mvc.DataAnnotations;
using Microsoft.Extensions.Localization;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using WebApp.AppTools.Attributes;

namespace WebApp.AppMiddleWare
{
	// An Attribute Adapter provider allows to register the Attribute Adapter with the DI in StartUp.
	// This adapter is 'generic' and will create the required Attribute Adapter accordingly.
	public class CustomAttributeAdapterProviderRegistration : IValidationAttributeAdapterProvider
	{
		private readonly IValidationAttributeAdapterProvider _baseProvider = new ValidationAttributeAdapterProvider();

		public IAttributeAdapter GetAttributeAdapter(ValidationAttribute attribute, IStringLocalizer stringLocalizer)
		{
			if (attribute is RequiredIfOtherFieldIsZeroAttribute)
			{
				return new RequiredIfOtherFieldIsZeroAttributeAdapter(attribute as RequiredIfOtherFieldIsZeroAttribute, stringLocalizer);
			}
			else if (attribute is EmptyOrPositiveAttribute)
			{
				return new EmptyOrPositiveAttributeAdapter(attribute as EmptyOrPositiveAttribute, stringLocalizer);
			}
			else if (attribute is CustomRangeAttribute)
			{
				return new CustomRangeAttributeAdapter(attribute as CustomRangeAttribute, stringLocalizer);
			}
			else
			{
				return _baseProvider.GetAttributeAdapter(attribute, stringLocalizer);
			}
			throw new NotImplementedException();
		}
	}
}
