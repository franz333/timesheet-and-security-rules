﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Data.Common;
using MySql.Data.MySqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.AppMiddleWare
{
	public class MysqlProviderRegistration
	{
		private readonly RequestDelegate _next;

		public MysqlProviderRegistration(RequestDelegate next)
		{
			_next = next;
		}

		public async Task Invoke(HttpContext httpContext)
		{
			DbProviderFactories.RegisterFactory("MySqlConnector", MySqlClientFactory.Instance);
			await _next(httpContext); // calling next middleware
		}
	}

	// Extension method used to add the middleware to the HTTP request pipeline.
	public static class MysqlProviderRegistrationExtensions
	{
		public static IApplicationBuilder UseMysqlProviderRegistration(this IApplicationBuilder builder)
		{
			return builder.UseMiddleware<MysqlProviderRegistration>();
		}
	}
}
