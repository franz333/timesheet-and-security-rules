﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models
{
	public class QcmItem
	{
		public int QcmItemId { get; set; }

		public string Item { get; set; }

		public bool IsAnswer { get; set; }
	}
}
