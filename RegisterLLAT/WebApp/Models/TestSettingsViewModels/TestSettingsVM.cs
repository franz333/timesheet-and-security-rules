﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models.TestSettingsViewModels
{
	public class TestSettingsVM
	{
		[Key]
		public int Id { get; set; }

		[DisplayName("Titre")]
		[Required(ErrorMessage = "Un titre est requis.")]
		public string Title { get; set; }

		[DisplayName("Seuil / 10")]
		[Required(ErrorMessage = "Ce champ est requis.")]
		[Range(5, 10, ErrorMessage = "Le seuil doit être compris entre {1} et {2}")]
		public float Treshold { get; set; }

		[DisplayName("Validité - réussite")]
		[Required(ErrorMessage = "Ce champ est requis.")]
		[Range(30, 360, ErrorMessage = "La période de validité en cas de réussite doit être comprise entre {1} et {2}")]
		public int ValidityPeriod { get; set; }

		[DisplayName("Validité - échec")]
		[Required(ErrorMessage = "Ce champ est requis.")]
		[Range(15, 45, ErrorMessage = "La période de validité en cas d'échec doit être comprise entre {1} et {2}")]
		public int FailurePeriod { get; set; }
	}
}
