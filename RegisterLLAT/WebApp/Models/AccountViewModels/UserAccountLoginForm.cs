﻿using DAL.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using WebApp.AppTools;

namespace WebApp.Models.AccountViewModels
{
	public class UserAccountLoginForm
	{
		[Required(ErrorMessage = "L'adresse e-mail n'est pas valide")]
		[EmailAddress(ErrorMessage = "Veuillez entrer une adresse e-mail valide")]
		[Display(Name = "Adresse e-mail")]
		public string Email { get; set; }

		[Required(ErrorMessage = "Le mot de passe n'est pas valide")]
		[DataType(DataType.Password)]
		[Display(Name = "Mot de passe")]
		public string Pwd { get; set; }

		[Display(Name = "Se souvenir de moi")]
		public bool IsPersistent { get; set; }

		public Admin LoginCheck()
		{
			AdminService serv = new AdminService();

			DAL.Entities.Admin toLog = new DAL.Entities.Admin()
			{
				Email = this.Email,
				Pwd = this.Pwd.ApplyHash()
			};

			Admin loggedAdmin = serv.Login(toLog).DalToAdminApp();

			return loggedAdmin;
		}
	}
}
