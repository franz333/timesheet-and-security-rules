﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using WebApp.AppTools;
using WebApp.AppTools.Attributes;

namespace WebApp.Models
{
	public class RoleType
	{
		public int Id { get; set; }

		public string Title { get; set; }

		public string RoleTypeDescription { get; set; }

	}
}
