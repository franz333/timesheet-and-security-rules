﻿using DAL.Services;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using WebApp.AppTools;

namespace WebApp.Models.RegisterPlanningViewModels
{
	public class SetStatus
	{
		[Key]
		[HiddenInput(DisplayValue = false)]
		public int Id { get; set; }

		private IEnumerable<CompanyStatus> _companyStatuses;

		public IEnumerable<CompanyStatus> CompanyStatus
		{
			get 
			{
				if (_companyStatuses == null)
				{
					CompanyStatusService service = new CompanyStatusService();
					_companyStatuses = service.GetAll().Select(cs => cs.DalToCompanyStatusApp());
				}
				return _companyStatuses; 
			}
			set { _companyStatuses = value; }
		}

		// public IEnumerable<CompanyStatus> CompanyStatus { get; set; }
	}
}
