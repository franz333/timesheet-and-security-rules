﻿using DAL.Services;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Interfaces;
using WebApp.Models.RegisterSharedViewModels;
using DALE = DAL.Entities;

namespace WebApp.Models.RegisterPlanningViewModels
{
	public class FormRegisterPlanningVM : AbstractRegisterVM, IRegistrable<FormRegisterPlanningVM>
	{
		public SetTime Time { get; set; }

		public List<SetTime> Times { get; set; }


		#region Methods

		public FormRegisterPlanningVM AvoidInternToTakeTest(FormRegisterPlanningVM entity)
		{
			throw new NotImplementedException();
		}

		public override int SaveNewCompany(AbstractRegisterVM register)
		{
			int result = (int)register.CompanyId;

			if (register.CompanyId == 0 && register.CompanyName != "")
			{
				CompanyService serviceC = new CompanyService();
				try
				{
					result = serviceC.Insert(new DALE.Company
					{
						CompanyStatusId = register.StatusId,
						CompanyName = register.CompanyName,
					});
				}
				catch (Exception e)
				{
					throw e;
				}
			}
			return result;
		}

		public override int? SaveNewDepartment(AbstractRegisterVM register)
		{
			int? result = null;

			// Insert potential new department
			if (register.StatusId == 1 && register.DepartmentId == 0 && register.DepartmentName != "")
			{
				DepartmentService serviceD = new DepartmentService();
				try
				{
					register.DepartmentId = serviceD.Insert(new DALE.Department
					{
						CompanyId = (int)register.CompanyId,
						DepartmentName = register.DepartmentName,
					});
				}
				catch (Exception e)
				{
					throw e;
				}
			}
			return result;
		}

		public override int SaveNewPerson(AbstractRegisterVM register)
		{
			int result = (int)register.PersonId;

			if (register.PersonId == 0 && register.FName != "" && register.LName != "")
			{
				PersonService serviceP = new PersonService();

				try
				{
					result = serviceP.Insert(new DALE.Person
					{
						FName = register.FName,
						LName = register.LName.ToUpper(),
						CompanyId = (int)register.CompanyId,
						DepartmentId = register.DepartmentId,
						AdministratorId = null
					});
				}
				catch (Exception e)
				{
					throw e;
				}
			}
			return result;
		}

		/// <summary>
		/// Save new created department, company and user if necessary
		/// </summary>
		/// <param name="register"></param>
		public void SaveNewUserData(AbstractRegisterVM register)
		{
			try
			{
				register.DepartmentId = register.SaveNewDepartment(register);
			}
			catch (Exception e)
			{
				throw e;
			}

			// Save potential new Company
			try
			{
				register.CompanyId = register.SaveNewCompany(register);
			}
			catch (Exception e)
			{
				throw e;
			}

			// Save potential new Person
			try
			{
				register.PersonId = register.SaveNewPerson(register);
			}
			catch (Exception e)
			{
				throw e;
			}
		}

		public FormRegisterPlanningVM SetCompany(FormRegisterPlanningVM register)
		{
			// If status is "Intern"
			if (register.StatusId == 1)
			{
				register.Companies = null;
				register.CompanyId = 1;

				// Get SNCB's departments
				register.Departments = GetDepartmentsForDropDown((int)register.CompanyId);

				if (register.Departments.Count() == 0)
				{
					register.DepartmentId = 0;
				}
			}
			else
			{
				// Get companies by status 
				register.Companies = GetCompaniesForDropDown(register.StatusId);

				// Provide the company's dropdownlist
				if (register.Companies.Count() == 0)
				{
					register.CompanyId = 0;
				}
			}
			return register;
		}

		public FormRegisterPlanningVM SetPerson(FormRegisterPlanningVM register)
		{
			// Prepare dropdown for a user intern (SNCB) or extern
			if (register.StatusId == 1 && register.CompanyId == 1)
			{
				register.Persons = register.GetPersonsForDropDown((int)register.CompanyId, (int)register.DepartmentId);
			}
			else
			{
				register.Persons = register.GetPersonsForDropDown((int)register.CompanyId);
			}

			// Set Person Name input in case the list is empty
			if (register.Persons.Count() == 0)
			{
				register.PersonId = 0;
			}

			// Prepare dropdown for the visited person
			if (register.DepartmentId != 2)
			{
				register.PersonsVisited = register.GetPersonsForDropDown(1, 2);

				if (register.PersonsVisited.Count() == 0)
				{
					register.PersonVisitedId = 0;
				}
			}
			//else
			//{
			//    register.PersonsVisited = null;
			//}
			return register;
		}

		public FormRegisterPlanningVM UserToSetCompanyProps(User user)
		{
			FormRegisterPlanningVM register = new FormRegisterPlanningVM();
			register.StatusId = user.StatusId;
			register.DepartmentId = user.DepartmentId;
			register.DepartmentName = user.DepartmentName;
			register.CompanyId = user.CompanyId;
			register.CompanyName = user.CompanyName;
			register.FName = user.FName;
			register.LName = user.LName;
			return register;
		}

		public override string ValidateSetCompany(AbstractRegisterVM register)
		{
			if (register.StatusId == 1 && register.CompanyId == 1)
			{
				if (register.DepartmentId == null)
				{
					return "Veuillez sélectionner un département ou l'ajouter en cliquant sur \"Autre\".";
				}
				else
				{
					return register.ValidateDepartment((int)register.CompanyId, (int)register.DepartmentId, register.DepartmentName);
				}
			}
			else
			{
				if (register.CompanyId == null)
				{
					return "Veuillez sélectionner une société ou l'ajouter en cliquant sur \"Autre\".";
				}
				else
				{
					return register.ValidateCompany((int)register.CompanyId, register.StatusId, register.CompanyName);
				}
			}
		}

		public override string ValidateSetPerson(AbstractRegisterVM register)
		{
			string result = "Cette personne n'existe pas.";

			// Check provided person
			if (register.PersonId == null)
			{
				return "Veuillez sélectionner un personne dans la liste ou l'ajouter en cliquant sur \"Autre\"";
			}

			// Check provided person
			if (register.PersonId > 0)
			{
				if (register.StatusId == 1 && register.CompanyId == 1)
				{
					return register.ValidatePerson((int)register.CompanyId, (int)register.DepartmentId, (int)register.PersonId);
				}
				else
				{
					return register.ValidatePerson((int)register.CompanyId, (int)register.PersonId);
				}
			}

			// Avoid duplicate FName and LName
			if (register.PersonId == 0)
			{
				if (register.StatusId == 1 && register.CompanyId == 1)
				{
					return register.AvoidDuplicatePerson(register.LName, register.FName, (int)register.CompanyId, (int)register.DepartmentId);
				}
				else
				{
					return register.AvoidDuplicatePerson(register.LName, register.FName, (int)register.CompanyId);
				}
			}

			// Validate the visited person
			if (register.CompanyId != 1 && register.DepartmentId != 2)
			{
				if (register.PersonVisitedId == null)
				{
					return "Veuillez sélectionner dans la liste la personne que vous venez visiter";
				}
				else
				{
					return register.ValidateVisitedPerson((int)register.PersonVisitedId);
				}
			}

			return result;
		}

		public string ValidateSetTime(IFormCollection form)
		{
			string result = "Veuillez remplir tous les champs correctement."; 

			foreach (var key in form.Keys)
			{
				if (key.Contains("Time_SameDayDate"))
				{
					if (form[key] == "")
					{
						return result;
					}
				}
				else if (key.Contains("Time_SameDayTimeIn"))
				{
					if (form[key] == "")
					{
						return result;
					}
				}
				else if (key.Contains("Time_SameDayTimeOut"))
				{
					if (form[key] == "")
					{
						return result;
					}
				}
			}
			return null;
		}
		
		/// <summary>
		/// Get times from the form
		/// </summary>
		/// <param name="form"></param>
		/// <param name="inValid"></param>
		/// <returns></returns>
		public FormRegisterPlanningVM FormToSetTime(IFormCollection form, out int inValid)
		{
			FormRegisterPlanningVM register = new FormRegisterPlanningVM();
			register.Times = new List<SetTime>();

			SetTime time = new SetTime();

			inValid = 0;
			
			int index = 0;			

			foreach (var key in form.Keys)
			{				
				if (key.Contains("Time.SameDayDate"))
				{
					time = new SetTime();
					time.SameDayDate = form[key];

					if (form[key] == "")
					{
						inValid++;
					}
				}
				else if (key.Contains("Time.SameDayTimeIn"))
				{
					time.SameDayTimeIn = form[key];

					if (form[key] == "")
					{
						inValid++;
					}
				}
				else if (key.Contains("Time.SameDayTimeOut"))
				{
					time.SameDayTimeOut = form[key];

					if (form[key] == "")
					{
						inValid++;
					}
				}

				if (key.Contains("Time"))
				{
					index++;

					// When the object if filled up, add it to the list
					if (index % 3 == 0)
					{
						register.Times.Add(time);
					}
				}				
			}
			return register;
		}

		public FormRegisterPlanningVM FormToSingleSetTime(IFormCollection form, out int inValid)
		{
			FormRegisterPlanningVM register = new FormRegisterPlanningVM();

			register.Time = new SetTime();

			inValid = 0;

			foreach (var key in form.Keys)
			{
				if (key.Contains("SameDayDate"))
				{
					register.Time.SameDayDate = form[key];

					if (form[key] == "")
					{
						inValid++;
					}
				}
				else if (key.Contains("SameDayTimeIn"))
				{
					register.Time.SameDayTimeIn = form[key];

					if (form[key] == "")
					{
						inValid++;
					}
				}
				else if (key.Contains("SameDayTimeOut"))
				{
					register.Time.SameDayTimeOut = form[key];

					if (form[key] == "")
					{
						inValid++;
					}
				}
			}
			return register;
		}

		public string ValidateTimeIntegrity(FormRegisterPlanningVM register)
		{
			string result = null;

			List<DateTime> dates = new List<DateTime>();

			for (int i = 0; i < register.Times.Count(); i++)
			{

				DateTime timeIn = new DateTime();
				DateTime timeOut = new DateTime();

				string date = register.Times[i].SameDayDate;

				string hourIn = register.Times[i].SameDayTimeIn + ":00";
				string dateTimeIn = date + " " + hourIn;
				timeIn = DateTime.Parse(dateTimeIn);

				string hourOut = register.Times[i].SameDayTimeOut + ":00";
				string dateTimeOut = date + " " + hourOut;
				timeOut = DateTime.Parse(dateTimeOut);

				if (timeIn > timeOut)
				{
					return "Vérifiez votre formulaire, l'horaire de début doit être inférieur à l'horaire de fin.";
				}

				if (register.Times.Count() > 1)
				{
					dates.Add(timeIn);
				}
			}

			if (dates.Count() > 1)
			{
				for (int j = 0; j < dates.Count(); j++)
				{
					for (int k = j+1; k < dates.Count()-j; k++)
					{
						if ((dates[j].Day == dates[k].Day))
						{
							return "Vérifiez votre formulaire, il existe des dates dont le jour est identique.";
						}
					}
				}
			}
			return result;
		}



		internal List<DALE.RegisterPlanning> PrepareRegisters(FormRegisterPlanningVM register, User user, int AdministratorId)
		{
			List<DALE.RegisterPlanning> list = new List<DALE.RegisterPlanning>();
			
			// Prepare registerPlanning for insert or update
			for (int i = 0; i < register.Times.Count(); i++)
			{
				string dateIn = register.Times[i].SameDayDate + " " + register.Times[i].SameDayTimeIn + ":00";
				DateTime dateTimeIn = DateTime.Parse(dateIn);

				string dateOut = register.Times[i].SameDayDate + " " + register.Times[i].SameDayTimeOut + ":00";
				DateTime dateTimeOut = DateTime.Parse(dateOut);

				DALE.RegisterPlanning dalRegister = new DALE.RegisterPlanning();
				dalRegister.TimeIn = dateTimeIn;
				dalRegister.TimeOut = dateTimeOut;
				dalRegister.Planned = true;
				dalRegister.PersonId = user.Id;
				dalRegister.PersonVisited = user.PersonVisited;
				dalRegister.AdministratorId = AdministratorId;
				list.Add(dalRegister);
			}

			return list;
		}

		#endregion
	}
}
