﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using WebApp.AppTools;

namespace WebApp.Models.RegisterPlanningViewModels
{
	public class RegisterPlanningVM 
	{
		public int Id { get; set; }

		[Display(Name = "Entrée")]
		[DataType(DataType.Date)]
		[DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = @"{0:dd'/'MM'/'yy HH\:mm}")]
		public DateTime TimeIn { get; set; }

		[Display(Name = "Sortie")]
		[DataType(DataType.Date)]
		[DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = @"{0:dd'/'MM'/'yy HH\:mm}", NullDisplayText = "Sur site")]
		public DateTime? TimeOut { get; set; }

		[Display(Name = "Oubli")]
		[DisplayFormat(NullDisplayText = "/")]
		public string Mistake { get; set; }

		public int PersonId { get; set; }

		[Display(Name = "Nom")]
		public string LName { get; set; }

		[Display(Name = "Prénom")]
		public string FName { get; set; }

		public int CompanyId { get; set; }

		[Display(Name = "Société")]
		public string CompanyName { get; set; }

		[Display(Name = "Status")]
		public string StatusName { get; set; }

		public int? PersonVisited { get; set; }

		public string VisitedLName { get; set; }

		public string VisitedFName { get; set; }

		private string _personVisitedName;

		[Display(Name = "Visité(e)")]
		public string PersonVisitedName
		{
			get
			{
				if (VisitedFName != null & VisitedLName != null)
				{
					_personVisitedName = StringCustomMethods.GetInitialsFromFirstName(VisitedFName) + " " + VisitedLName;
				}
				else if (VisitedFName != null & VisitedLName == null)
				{
					_personVisitedName = StringCustomMethods.GetInitialsFromFirstName(VisitedFName);
				}
				else
				{
					_personVisitedName = VisitedLName;
				}
				return _personVisitedName;
			}
			set { _personVisitedName = value; }
		}

		public int AdministratorId { get; set; }

		[Display(Name = "Etat")]
		public string State { get; set; }

		[DisplayName("Plannifié")]
		public bool Planned { get; set; }
	}
}
