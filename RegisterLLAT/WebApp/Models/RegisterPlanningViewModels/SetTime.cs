﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models.RegisterPlanningViewModels
{
	public class SetTime
	{
		public string SameDayDate { get; set; }

		public string SameDayTimeIn { get; set; }

		public string SameDayTimeOut { get; set; }
	}
}
