﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models.AdminViewModels
{
	public class ModifyPassword
	{
		public int UserId { get; set; }

		[Required(ErrorMessage = "Veuillez renseigner votre mot de passe actuel")]
		[DisplayName("Votre de passe actuel *")]
		[DataType(DataType.Password)]
		public string CurrentPassword { get; set; }

		[Required(ErrorMessage = "Veuillez renseigner votre nouveau mot de passe")]
		[DisplayName("Votre nouveau mot de passe *")]
		[DataType(DataType.Password)]
		public string Password { get; set; }

		[Required(ErrorMessage = "Veuillez confirmer votre nouveau mot de passe")]
		[DisplayName("Confirmation du nouveau mot de passe *")]
		[DataType(DataType.Password)]
		[Compare(nameof(Password), ErrorMessage = "Les mots de passe ne correspondent pas")]
		public string PasswordCheck { get; set; }
	}
}
