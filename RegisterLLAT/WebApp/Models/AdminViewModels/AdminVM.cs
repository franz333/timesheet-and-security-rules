﻿using DAL.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using WebApp.AppTools;

namespace WebApp.Models.AdminViewModels
{
	public class AdminVM
	{
		[Key]
		[HiddenInput]
		public int Id { get; set; }

		[Required(ErrorMessage = "Le prénom est requis.")]
		[DisplayName("Prénom")]
		public string FName { get; set; }

		private string _lName { get; set; }

		[Required(ErrorMessage = "Le nom de famille est requis.")]
		[DisplayName("Nom de famille")]
		public string LName 
		{
			get 
			{
				if (_lName != null)
				{
					return _lName.ToUpper();
				}
				return _lName;
			} 
			set { _lName = value.ToUpper(); }
		}

		[Required(ErrorMessage = "L'adresse e-mail est requise.")]
		[DisplayName("E-mail")]
		[DataType(DataType.EmailAddress, ErrorMessage ="L'adresse e-mail n'est pas valide.")]
		public string Email { get; set; }

		[DisplayName("Archivé")]
		public bool Deleted { get; set; } = false;

		[DisplayName("Actif")]
		public bool IsActive { get; set; } = true;

		[DisplayName("Role")]
		public int RoleId { get; set; }

		[DisplayName("Role")]
		public string Role { get; set; }

		private List<RoleType> _roles;

		public List<RoleType> Roles
		{
			get
			{
				if (_roles == null)
				{
					RoleTypeService serv = new RoleTypeService();
					_roles = serv.GetAll().Where(r => r.Id != 1).Select(r => r.DalToRoleTypeApp()).ToList();
				}
				return _roles;
			}
			set { _roles = value; }
		}

		[DisplayName("Description du rôle")]
		public string RoleDescription { get; set; }
	}
}
