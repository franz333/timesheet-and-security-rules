﻿using DAL.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using WebApp.AppTools;

namespace WebApp.Models.AdminViewModels
{
	public class AdminForm : AdminVM
	{
		[Required(ErrorMessage = "Le mot de passe est requis.")]
		[DisplayName("Mot de passe")]
		[DataType(DataType.Password)]
		public string Pwd { get; set; }

		[Required(ErrorMessage = "La confirmation du mot de passe est requise.")]
		[DisplayName("Confirmation du mot de passe")]
		[DataType(DataType.Password, ErrorMessage = "Veuillez entrer un e-mail valide")]
		[Compare(nameof(Pwd), ErrorMessage = "Les mots de passe ne correspondent pas")]
		public string PwdCheck { get; set; }
	}
}
