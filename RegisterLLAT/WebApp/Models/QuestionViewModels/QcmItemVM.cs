﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;


namespace WebApp.Models.QuestionViewModels
{
	public class QcmItemVM
	{
		public int Id { get; set; }

		[DisplayName("Proposition")]
		public string Item { get; set; }

		[DisplayName("Réponse")]
		public bool IsAnswer { get; set; }
	}
}
