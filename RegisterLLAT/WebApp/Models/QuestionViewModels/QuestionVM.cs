﻿using DAL.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using WebApp.AppTools;
using WebApp.Models.TestViewModels;

namespace WebApp.Models.QuestionViewModels
{
	public class QuestionVM
	{
		public int Id { get; set; }

		[Display(Name = "Titre")]
		public string Title { get; set; }

		[Display(Name = "Activée")]
		public bool IsActive { get; set; }

		//private string _tests;

		//[DisplayName("Test(s)")]
		//public string Tests
		//{
		//	get 
		//	{
		//		if (_tests == null)
		//		{
		//			TestService serviceT = new TestService();
		//			List<string> testNames = serviceT.GetAllTestNameByQuestion(Id).Select(t => t.Title).ToList();

		//			if (testNames == null)
		//			{
		//				_tests = "/";
		//			}

		//			if (testNames.Count() > 0)
		//			{
		//				for (int i = 0; i < testNames.Count(); i++)
		//				{
		//					_tests += testNames[i];
		//					if (i < testNames.Count()-1)
		//					{
		//						_tests += ", ";
		//					}
		//				}
		//			}
		//		}
		//		return _tests; 
		//	}
		//	set { _tests = value; }
		//}

		private List<Test> _tests;

		[Display(Name = "Test(s)")]
		public List<Test> Tests
		{
			get
			{
				if (_tests == null)
				{
					TestService serviceT = new TestService();
					_tests = serviceT.GetAll(Id).Select(t => t.DalToTestApp()).ToList();
				}
				return _tests;
			}
			set { _tests = value; }
		}

		private IEnumerable<QcmItem> _qcmItems;

		[Display(Name = "Qcm")]
		public IEnumerable<QcmItem> QcmItems
		{
			get
			{
				if (_qcmItems == null)
				{
					QcmItemService service = new QcmItemService();
					_qcmItems = service.GetAll(Id).Select(q => q.DalToQcmItemApp()).ToList();
				}
				return _qcmItems;
			}
			set { _qcmItems = value; }
		}

		[Display(Name = "Test(s)")]
		public List<TestQuestionVM> TestQuestion { get; set; }

	}
}
