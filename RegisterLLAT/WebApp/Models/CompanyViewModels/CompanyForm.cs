﻿using DAL.Services;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.NetworkInformation;
using System.Threading.Tasks;
using WebApp.AppTools;
using DALE = DAL.Entities;
using WebApp.AppTools.Attributes;

namespace WebApp.Models.CompanyViewModels
{
	public class CompanyForm
	{
		[Key]
		public int Id { get; set; }

		[Required(ErrorMessage = "Le nom de la société est requis")]
		[DisplayName("Nom")]
		public string CompanyName { get; set; }

		[Required(ErrorMessage = "Le statut de la société est requis")]
		[DisplayName("Statut")]
		public int CompanyStatusId { get; set; }

		private List<CompanyStatus> _companyStatuses;

		public List<CompanyStatus> CompanyStatuses
		{
			get
			{
				if (_companyStatuses == null)
				{
					CompanyStatusService serv = new CompanyStatusService();
					_companyStatuses = serv.GetAll().Select(c => c.DalToCompanyStatusApp()).ToList();
				}
				return _companyStatuses;
			}
			set { _companyStatuses = value; }
		}

		[DisplayName("Statut")]
		public string StatusName { get; set; }

		[DisplayName("Archivée")]
		public bool Deleted { get; set; } = false;

		[DisplayName("Encodeur")]
		public int? AdministratorId { get; set; }

		public string AdminLName { get; set; }

		public string AdminFName { get; set; }

		private string _adminName;

		[DisplayName("Encodeur")]
		public string AdminName
		{
			get
			{
				if (AdminLName == null && AdminFName == null)
				{
					_adminName = "un utilisateur à l'enregistrement";
				}
				else
				{
					_adminName = AdminLName.ToUpper() + " " + AdminFName;
				}
				return _adminName;
			}
			set { _adminName = value; }
		}

		[DisplayName("Test")]
		public int TestId { get; set; } = 1;

		[DisplayName("Test")]
		public string TestName { get; set; }

		private List<DALE.Test> _tests;

		public List<DALE.Test> Tests
		{
			get 
			{
				if (_tests == null)
				{
					TestService testService = new TestService();
					_tests = testService.GetAll().ToList();
				}
				return _tests; 
			}
			set { _tests = value; }
		}

		private List<DALE.Person> _persons;

		public List<DALE.Person> Persons
		{
			get 
			{
				if (_persons == null)
				{
					PersonService personService = new PersonService();
					if (Id == 1)
					{
						_persons = personService.GetPersonsByCompany(Id).OrderBy(p => p.DepartmentName).ThenBy(p => p.LName).ToList();
					}
					else
					{
						_persons = personService.GetPersonsByCompany(Id).OrderBy(p => p.LName).ToList();
					}										
				}
				return _persons;
			}
			set { _persons = value; }
		}

	}
}
