﻿using DAL.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using WebApp.AppTools;

namespace WebApp.Models.CompanyViewModels
{
	public class CompanyVM
	{
		[Key]
		public int Id { get; set; }

		[DisplayName("Nom")]
		public string CompanyName { get; set; }

		private List<Department> _departments;

		[DisplayName("Département(s)")]
		[DisplayFormat(NullDisplayText = "/")]
		public List<Department> Departments
		{
			get 
			{
				if (_departments == null)
				{
					DepartmentService servD = new DepartmentService();
					_departments = servD.GetAllDepartmentByCompany(this.Id).Select(d => d.DalToDepartmentApp()).ToList();
				}
				return _departments; 
			}
			set { _departments = value; }
		}

		[DisplayName("Département")]
		[DisplayFormat(NullDisplayText = "/")]
		public string DepartmentName { get; set; }

		[DisplayName("Archivée")]
		public bool Deleted { get; set; }

		[DisplayName("Statut")]
		public int CompanyStatusId { get; set; }

		[DisplayName("Statut")]
		public string StatusName { get; set; }
	}
}
