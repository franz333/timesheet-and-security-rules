﻿using DAL.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApp.AppTools;
using WebApp.Models.TestViewModels;

namespace WebApp.Models
{
	public class Question
	{
		public int Id { get; set; }

		public string Title { get; set; }

		public bool IsActive { get; set; }

		//private IEnumerable<QcmItem> _qcmItems;

		//public IEnumerable<QcmItem> QcmItems
		//{
		//	get 
		//	{
		//		if (_qcmItems == null)
		//		{
		//			QcmItemService service = new QcmItemService();
		//			_qcmItems = service.GetAll(Id).Select(q => q.ToApp()).ToList();
		//		}
		//		return _qcmItems;
		//	}
		//	set { _qcmItems = value; }
		//}
	}
}
