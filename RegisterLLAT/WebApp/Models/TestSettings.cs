﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models
{
	public class TestSettings
	{
		public int Id { get; set; }

		public string Title { get; set; }

		public float Treshold { get; set; }

		public int ValidityPeriod { get; set; }

		public int FailurePeriod { get; set; }
	}
}
