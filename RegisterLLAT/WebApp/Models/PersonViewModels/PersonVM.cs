﻿using DAL.Entities;
using DAL.Services;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using WebApp.AppTools;
using WebApp.Interfaces;
using DALE = DAL.Entities;

namespace WebApp.Models.PersonViewModels
{
	public class PersonVM 
	{
		public int Id { get; set; }

		private string _lName;

		[DisplayName("Nom de famille")]
		[Required(ErrorMessage = "Le nom de famille est requis.")]
		public string LName
		{
			get 
			{
				if (_lName != null)
				{
					return _lName.ToUpper();
				}
				else
				{
					return _lName;
				}				
			}
			set { _lName = value; }
		}

		[DisplayName("Prénom")]
		[Required(ErrorMessage = "Le prénom est requis.")]
		public string FName { get; set; }

		[DisplayName("Archivé")]
		public bool Deleted { get; set; }

		[DisplayName("Statut du test")]
		public int IsInOrder { get; set; }

		private string _isInOrderToString;

		[DisplayName("Dernier test")]
		public string IsInOrderToString
		{
			get 
			{
				switch (IsInOrder)
				{
					case 0:
						_isInOrderToString = "Aucun test passé";
						break;
					case 1:
						_isInOrderToString = "Dispensé";
						break;
					case 2:
						_isInOrderToString = "Périmé";
						break;
					case 3:
						_isInOrderToString = "Valide";
						break;
				}
				return _isInOrderToString; 
			}
			set { _isInOrderToString = value; }
		}

		[DisplayName("Société")]
		public int CompanyId { get; set; }

		[DisplayName("Société")]
		public string CompanyName { get; set; }

		public bool CompanyDeleted { get; set; }


		[DisplayName("Statut")]
		public int CompanyStatusId { get; set; }

		private IEnumerable<DALE.CompanyStatus> _companyStatuses;

		public IEnumerable<DALE.CompanyStatus> CompanyStatuses
		{
			get 
			{
				CompanyStatusService companyStatusService = new CompanyStatusService();
				_companyStatuses = companyStatusService.GetAll();
				return _companyStatuses; 
			}
			set { _companyStatuses = value; }
		}

		[DisplayName("Statut")]
		public string StatusType { get; set; }


		private IEnumerable<Company> _companies;

		public IEnumerable<Company> Companies
		{
			get 
			{
				CompanyService companyService = new CompanyService();
				_companies = companyService.GetAllCompanyByStatus(CompanyStatusId).Select(c => c.DalToCompanyApp());
				return _companies;
			}
			set { _companies = value; }
		}

		[DisplayName("Département")]
		public int? DepartmentId { get; set; }

		[DisplayName("Département")]
		public string DepartmentName { get; set; }

		private IEnumerable<Department> _departments;

		public IEnumerable<Department> Departments
		{
			get 
			{
				if (CompanyId == 1)
				{
					DepartmentService departmentService = new DepartmentService();
					_departments = departmentService.GetAllDepartmentByCompany(CompanyId).Select(d => d.DalToDepartmentApp());
				}
				return _departments; 
			}
			set { _departments = value; }
		}


		public int? AdminId { get; set; }

		public string AdminLName { get; set; }

		public string AdminFName { get; set; }

		private string _adminName;

		[DisplayName("Encodeur")]
		public string AdminName
		{
			get
			{
				if (AdminLName == null && AdminFName == null)
				{
					_adminName = "lui-même, à l'enregistrement";
				}
				else
				{
					_adminName = AdminLName.ToUpper() + " " + AdminFName;
				}
				return _adminName; 
			}
			set { _adminName = value; }
		}

		#region Methods
		/// <summary>
		/// Validate person checking that the status, company and department are valid.
		/// </summary>
		/// <param name="person"></param>
		/// <returns></returns>
		public string ValidatePerson(PersonVM person)
		{
			string result;

			// Check that status Exists
			result = ControllerMethods.CheckCompanyStatus(person.CompanyStatusId);
			if (result != null)
			{
				return result;
			}

			// Check that company exist with that status
			result = CheckCompany(person.CompanyStatusId, person.CompanyId);
			if (result != null)
			{
				return result;
			}

			// if company is SNCB, check that department exists
			if (person.CompanyId == 1)
			{
				result = CheckCompanyDepartment((int)person.DepartmentId, person.CompanyId);
				if (result != null)
				{
					return result;
				}
			}

			// Check for duplicate person's LastName and FirstName within the same company
			result = AvoidDuplicatePerson(person.LName, person.FName, person.CompanyId, person.Id);
			if (result != null)
			{
				return result;
			}

			return result;
		}

		/// <summary>
		/// Check that a company exist in DB under a specified statusId
		/// </summary>
		/// <param name="statusId"></param>
		/// <param name="companyId"></param>
		/// <returns></returns>
		public string CheckCompany(int statusId, int companyId)
		{
			string result = null;

			// statusId was checked before so check only companyId
			if (companyId == 0)
			{
				result = "Veuillez sélectionner une société dans la liste.";
			}
			else
			{
				CompanyService service = new CompanyService();
				DALE.Company company = service.Get(companyId, statusId);

				if (company == null)
				{
					result = "Cette société n'existe pas sous ce statut.";
				}
			}
			return result;
		}

		/// <summary>
		/// Check that the departement corresponds to the companyId
		/// </summary>
		/// <param name="companyId"></param>
		/// <param name="departmentId"></param>
		/// <returns></returns>
		public string CheckCompanyDepartment(int departmentId, int companyId)
		{
			string result = null;

			if (departmentId == 0)
			{
				result = "Veuillez sélectionner un département dans la liste.";
			}
			else
			{
				DepartmentService service = new DepartmentService();

				DALE.Department departement = service.Get(departmentId, companyId);

				if (departement == null)
				{
					result = "Ce département n'est pas valable pour cette société.";
				}
			}

			return result;
		}

		// TODO : bonus > take into account the departmentId.. a trigger would be needed for unicity constraint in DB
		/// <summary>
		/// Check that only one person with unique firstName and lastName exist for a company
		/// </summary>
		/// <param name="lName"></param>
		/// <param name="fName"></param>
		/// <param name="companyId"></param>
		/// <returns></returns>
		public string AvoidDuplicatePerson(string lName, string fName, int companyId, int personId = 0)
		{
			string result = null;

			PersonService service = new PersonService();

			DALE.Person person = null;

			if (personId != 0)
			{
				person = service.GetAll(lName, fName).Where(p => p.CompanyId == companyId && p.Id != personId).SingleOrDefault();
			}
			else
			{
				person = service.GetAll(lName, fName).Where(p => p.CompanyId == companyId).SingleOrDefault();
			}

			if (person != null)
			{
				result = "Une personne avec les mêmes nom et prénom existe déjà. Veuillez les différencier (ex: deuxième nom de famille).";
			}
			return result;
		}

		public PersonVM AvoidInternToTakeTest(PersonVM person)
		{
			// the person is an intern and belongs to SNCB
			if (person.CompanyStatusId == 1 && person.CompanyId == 1)
			{
				person.IsInOrder = 1;
			}
			return person;		
		}
		#endregion
	}
}
