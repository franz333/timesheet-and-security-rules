﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models.RegisterViewModels
{
	public class PersonListVM
	{
		public int Id { get; set; }

		public string LName { get; set; }

		public string FName { get; set; }

		private string _name;

		public string Name
		{
			get 
			{
				if (LName == null && FName == null)
				{
					_name = "Autre";
				}
				else if (LName == "" && FName == "")
				{
					_name = "Aucune";
				}
				else
				{
					_name = LName.ToUpper() + " " + FName;					
				}
				return _name;
			}
			set { _name = value; }
		}
	}
}
