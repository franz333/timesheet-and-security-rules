﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using WebApp.AppTools.Attributes;

namespace WebApp.Models.RegisterViewModels
{
	public class SetPerson
	{
		[Key]
		[Display(Name = "Vos nom et prénom")]
		public int? Id { get; set; }

		public List<SelectListItem> OptionsPersons { get; set; }

		public List<PersonListVM> Persons { get; set; }

		[Display(Name = "Nom de famille")]
		[RequiredIfOtherFieldIsZero("Id", ErrorMessage = "Le nom de famille est requis")]
		public string LName { get; set; }

		[Display(Name = "Prénom")]
		[RequiredIfOtherFieldIsZero("Id", ErrorMessage = "Le prénom est requis")]
		public string FName { get; set; }

		public DateTime? DateBegin { get; set; }

		public DateTime? DateEnd { get; set; }

		[Display(Name = "Personne visitée")]
		public int? PersonVisitedId { get; set; }

		public List<SelectListItem> OptionsPVisited { get; set; }

		public List<PersonListVM> PersonsVisited { get; set; }
	}
}
