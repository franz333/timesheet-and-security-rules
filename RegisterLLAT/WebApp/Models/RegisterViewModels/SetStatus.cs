﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace WebApp.Models.RegisterViewModels
{
	public class SetStatus
	{
		[Key]
		[HiddenInput(DisplayValue = false)]
		public int Id { get; set; }

		public IEnumerable<CompanyStatus> CompanyStatus { get; set; }
	}
}
