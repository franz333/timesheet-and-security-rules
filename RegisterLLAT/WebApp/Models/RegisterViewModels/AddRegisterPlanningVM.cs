﻿using DAL.Services;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApp.AppTools;

namespace WebApp.Models.RegisterViewModels
{
	// #TODO : delete this VM and associated mapper elements
	public class AddRegisterPlanningVM
	{
		private SetStatus _setStatus;

		public SetStatus SetStatus
		{
			get 
			{
				if (_setStatus == null)
				{
					_setStatus = new SetStatus();
				}
				return _setStatus; 
			}
			set { _setStatus = value; }
		}

		private SetCompany _setCompany;

		public SetCompany SetCompany
		{			
			get 
			{
				if (_setCompany == null)
				{
					_setCompany = new SetCompany();
				}
				return _setCompany; 
			}
			set { _setCompany = value; }
		}



		//private SetStatus _setStatus;

		//public SetStatus SetStatus
		//{
		//	get 
		//	{
		//		if (_setStatus == null)
		//		{
		//			CompanyStatusService serv = new CompanyStatusService();
		//			_setStatus.CompanyStatus = serv.GetAll().Select(cs => cs.ToApp()).ToList();
		//		}
		//		return _setStatus; 
		//	}
		//	set { _setStatus = value; }
		//}

		//private List<SelectListItem> _options;

		//public List<SelectListItem> Options
		//{
		//	get
		//	{
		//		if (_setStatus.CompanyStatus.Count() > 0)
		//		{
		//			_options = SetStatus.CompanyStatus.Select(a => new SelectListItem
		//			{
		//				Value = a.Id.ToString(),
		//				Text = a.StatusName
		//			}).ToList();
		//		}
		//		return _options;
		//	}
		//	set { _options = value; }
		//}
	}
}
