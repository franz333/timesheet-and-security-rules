﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using WebApp.AppTools;

namespace WebApp.Models.RegisterViewModels
{
	public class RegisterPlanningVM : RegisterVM
	{
		[Display(Name = "Etat")]
		public string State { get; set; }

		[DisplayName("Plannifié")]
		public bool Planned { get; set; }
	}
}

