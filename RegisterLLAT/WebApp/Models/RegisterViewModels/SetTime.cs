﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models.RegisterViewModels
{
	public class SetTime
	{
		public int PlanningTypeId { get; set; } = 0;
		
		public List<PlanningType> Planning = new List<PlanningType>()
		{
			new PlanningType { Id = 1, Type = "un jour" },
			new PlanningType { Id = 2, Type = "une période même horaire" },
			new PlanningType { Id = 3, Type = "personnalisé" },
		};

		public string SameDayDate { get; set; }

		public string SameDayTimeIn { get; set; }

		public string SameDayTimeOut { get; set; }

		public string DifferentDayTimeIn { get; set; }

		public string DifferentDayTimeOut { get; set; }

		public class PlanningType
		{
			public int Id { get; set; }

			public string Type { get; set; }
		}
	}


}
