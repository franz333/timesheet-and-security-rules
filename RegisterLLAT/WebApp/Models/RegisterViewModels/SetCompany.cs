﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using WebApp.AppTools.Attributes;

namespace WebApp.Models.RegisterViewModels
{
	public class SetCompany
	{
		[Key]
		[Display(Name = "Nom de la société")]
		[EmptyOrPositive(ErrorMessage = "Ce champ doit être un nombre positif ou sans valeur")]
		public int? Id { get; set; }

		public List<SelectListItem> Options { get; set; } // TODO : potential useless

		public List<Company> Companies { get; set; }

		[Display(Name = "Indiquez le nom de la société")]
		[RequiredIfOtherFieldIsZero("Id", ErrorMessage = "Le nom de la société est requis")]
		public string Name { get; set; }

		public int IdAutoComplete { get; set; }

		[Display(Name = "Département")]
		[EmptyOrPositive(ErrorMessage = "Ce champ doit être un nombre positif ou sans valeur")]
		public int? DepartmentId { get; set; } // TODO : potential useless

		public List<Department> Departments { get; set; } 

		[Display(Name = "Nom du département")]
		[RequiredIfOtherFieldIsZero("DepartmentId", ErrorMessage = "Le nom du département est requis")]
		public string DepartmentName { get; set; } // TODO : potential useless

		public int CompanyStatusId { get; set; } // TODO : potential useless
	}
}
