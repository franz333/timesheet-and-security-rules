﻿using DAL.Services;
using DALE = DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Models.RegisterSharedViewModels;
using WebApp.Models.PersonTestViewModels;
using WebApp.AppTools;
using WebApp.Interfaces;

namespace WebApp.Models.RegisterViewModels
{
	public class FormRegisterVM : AbstractRegisterVM, IRegistrable<FormRegisterVM>
	{
		public bool IsCheckingOut { get; set; }

		public bool UpdateRegister { get; set; }

		public bool InsertNewRegister { get; set; }

		public int IsInOrder { get; set; }

		#region Methods

		/// <summary>
		/// Populate model with SetCompany Session data 
		/// </summary>
		/// <param name="user"></param>
		/// <returns></returns>
		public FormRegisterVM UserToSetCompanyProps(User user)
		{
			FormRegisterVM register = new FormRegisterVM();
			register.IsCheckingOut = user.isCheckinOut;
			register.StatusId = user.StatusId;
			register.DepartmentId = user.DepartmentId;
			register.DepartmentName = user.DepartmentName;
			register.CompanyId = user.CompanyId;
			register.CompanyName = user.CompanyName;
			register.FName = user.FName;
			register.LName = user.LName;
			return register;		
		}

		// TODO : place in an interface later
		/// <summary>
		/// Prepare Model for company and departement form's dropdowns
		/// </summary>
		/// <param name="register"></param>
		/// <returns></returns>
		public FormRegisterVM SetCompany(FormRegisterVM register)
		{
			// If status is "Intern"
			if (register.StatusId == 1)
			{
				register.Companies = null;
				register.CompanyId = 1;

				// Get SNCB's departments
				register.Departments = GetDepartmentsForDropDown((int)register.CompanyId);

				if (register.Departments.Count() == 0)
				{
					register.DepartmentId = 0;
				}
			}
			else
			{
				// Get companies by status 
				register.Companies = GetCompaniesForDropDown(register.StatusId);

				// Provide the company's dropdownlist
				if (register.Companies.Count() == 0)
				{
					register.CompanyId = 0;
				}
			}
			return register;
		}

		/// <summary>
		/// Validate SetCompany wizard step by checking company and department exist or avoiding potential duplicates
		/// </summary>
		/// <param name="register"></param>
		/// <returns></returns>
		public override string ValidateSetCompany(AbstractRegisterVM register)
		{
			if (register.StatusId == 1 && register.CompanyId == 1)
			{
				if (register.DepartmentId == null)
				{
					return "Veuillez sélectionner un département ou l'ajouter en cliquant sur \"Autre\".";
				}
				else
				{
					return register.ValidateDepartment((int)register.CompanyId, (int)register.DepartmentId, register.DepartmentName);
				}
			}
			else
			{
				if (register.CompanyId == null)
				{
					return "Veuillez sélectionner une société ou l'ajouter en cliquant sur \"Autre\".";
				}
				else
				{
					return register.ValidateCompany((int)register.CompanyId, register.StatusId, register.CompanyName);
				}
			}
		}

		// TODO : place in an interface later
		/// <summary>
		/// Prepare model for dropdown forms
		/// </summary>
		/// <param name="register"></param>
		/// <returns></returns>
		public FormRegisterVM SetPerson(FormRegisterVM register)
		{
			// Prepare dropdown for a user intern (SNCB) or extern
			if (register.StatusId == 1 && register.CompanyId == 1)
			{
				register.Persons = register.GetPersonsForDropDown((int)register.CompanyId, (int)register.DepartmentId);
			}
			else
			{
				register.Persons = register.GetPersonsForDropDown((int)register.CompanyId);
			}

			// Set Person Name input in case the list is empty
			if (register.Persons.Count() == 0)
			{
				register.PersonId = 0;
			}

			// Prepare dropdown for the visited person
			if (register.DepartmentId != 2)
			{
				register.PersonsVisited = register.GetPersonsForDropDown(1, 2);

				if (register.PersonsVisited.Count() == 0)
				{
					register.PersonVisitedId = 0;
				}
			}
			//else
			//{
			//    register.PersonsVisited = null;
			//}
			return register;
		}

		/// <summary>
		/// Validate the person checking if it exists or avoiding duplicate entry and validate the visited person checking if it exists.
		/// </summary>
		/// <param name="register"></param>
		/// <returns></returns>
		public override string ValidateSetPerson(AbstractRegisterVM register)
		{
			string result = "Cette personne n'existe pas.";
			
			// Check provided person
			if (register.PersonId == null)
			{
				return "Veuillez sélectionner un personne dans la liste ou l'ajouter en cliquant sur \"Autre\"";
			}				
			
			// Check provided person
			if (register.PersonId > 0)
			{
				if (register.StatusId == 1 && register.CompanyId == 1)
				{
					return register.ValidatePerson((int)register.CompanyId, (int)register.DepartmentId, (int)register.PersonId);
				}
				else
				{
					return register.ValidatePerson((int)register.CompanyId, (int)register.PersonId);
				}
			}

			// Avoid duplicate FName and LName
			if (register.PersonId == 0)
			{
				if (register.StatusId == 1 && register.CompanyId == 1)
				{
					return register.AvoidDuplicatePerson(register.LName, register.FName, (int)register.CompanyId, (int)register.DepartmentId);
				}
				else
				{
					return register.AvoidDuplicatePerson(register.LName, register.FName, (int)register.CompanyId);
				}
			}

			// Validate the visited person
			if (register.CompanyId != 1 && register.DepartmentId != 2)
			{
				if (register.PersonVisitedId == null)
				{
					return "Veuillez sélectionner dans la liste la personne que vous venez visiter";
				}
				else
				{
					return register.ValidateVisitedPerson((int)register.PersonVisitedId);
				}
			}

			return result;
		}

		/// <summary>
		/// Save potential new department
		/// </summary>
		/// <param name="register"></param>
		public override int? SaveNewDepartment(AbstractRegisterVM register)
		{
			int? result = register.DepartmentId;

			// Insert potential new department
			if (register.StatusId == 1 && register.DepartmentId == 0 && register.DepartmentName != "")
			{
				DepartmentService serviceD = new DepartmentService();
				try
				{
					register.DepartmentId = serviceD.Insert(new DALE.Department
					{
						CompanyId = (int)register.CompanyId,
						DepartmentName = register.DepartmentName,
					});
				}
				catch (Exception e)
				{
					throw e;
				}
			}
			return result;
		}

		/// <summary>
		/// Save potential new company
		/// </summary>
		/// <param name="register"></param>
		public override int SaveNewCompany(AbstractRegisterVM register)
		{
			int result = (int)register.CompanyId;
			
			if (register.CompanyId == 0 && register.CompanyName != "")
			{
				CompanyService serviceC = new CompanyService();
				try
				{
					result = serviceC.Insert(new DALE.Company
					{
						CompanyStatusId = register.StatusId,
						CompanyName = register.CompanyName,
						TestId = 1
					});
				}
				catch (Exception e)
				{
					throw e;
				}
			}
			return result;
		}

		/// <summary>
		/// Save potential new person
		/// </summary>
		/// <param name="register"></param>
		/// <returns></returns>
		public override int SaveNewPerson(AbstractRegisterVM register)
		{
			int result = (int)register.PersonId;

			if (register.PersonId == 0 && register.FName != "" && register.LName != "")
			{
				PersonService serviceP = new PersonService();

				try
				{
					result = serviceP.Insert(new DALE.Person
					{
						FName = register.FName,
						LName = register.LName.ToUpper(),
						CompanyId = (int)register.CompanyId,
						DepartmentId = register.DepartmentId,
						AdministratorId = null
					});
				}
				catch (Exception e)
				{
					throw e;
				}
			}
			return result;		
		}

		/// <summary>
		/// Save new created department, company and user if necessary
		/// </summary>
		/// <param name="register"></param>
		public void SaveNewUserData(AbstractRegisterVM register)
		{
			try
			{
				register.DepartmentId = register.SaveNewDepartment(register);
			}
			catch (Exception e)
			{
				throw e;
			}

			// Save potential new Company
			try
			{
				register.CompanyId = register.SaveNewCompany(register);
			}
			catch (Exception e)
			{
				throw e;
			}

			// Save potential new Person
			try
			{
				register.PersonId = register.SaveNewPerson(register);
			}
			catch (Exception e)
			{
				throw e;
			}
		}
		
		public FormRegisterVM AvoidInternToTakeTest(FormRegisterVM register)
		{
			// the person is an intern and belongs to SNCB
			if (register.StatusId == 1 && register.CompanyId == 1)
			{
				register.IsInOrder = 1;
			}
			return register;
		}

		// TODO : place in an interface later
		/// <summary>
		/// Sign a user in.
		/// </summary>
		/// <param name="register"></param>
		/// <returns></returns>
		public int SignInUser(FormRegisterVM register)
		{
			int result;
			
			RegisterService service = new RegisterService();

			DALE.Register final = new DALE.Register();

			final.TimeIn = DateTime.Now;
			final.PersonId = (int)register.PersonId;
			final.PersonVisited = register.PersonVisitedId;

			try
			{
				result = service.Insert(final);
			}
			catch (Exception e)
			{
				throw e;
			}

			return result;
		}

		/// <summary>
		/// Sign a user in and out in one go.
		/// </summary>
		/// <param name="register"></param>
		/// <returns></returns>
		public int SignInSignOutUser(FormRegisterVM register)
		{
			int result;

			RegisterService service = new RegisterService();

			DALE.Register final = new DALE.Register();

			final.TimeIn = DateTime.Now;
			final.TimeOut = DateTime.Now;
			final.PersonId = (int)register.PersonId;
			final.PersonVisited = register.PersonVisitedId;

			try
			{
				result = service.Insert(final);
			}
			catch (Exception e)
			{
				throw e;
			}

			return result;
		}

		public void CheckOutRegister(FormRegisterVM register)
		{
			RegisterService service = new RegisterService();
			DALE.Register final = new DALE.Register();

			final.TimeOut = DateTime.Now;
			final.Id = register.Id;

			try
			{
				service.CheckOut(final);
			}
			catch (Exception e)
			{
				throw e;
			}

		}
		
		public int FinaliseRegistration(FormRegisterVM register)
		{
			int result = 0;

			if (register.InsertNewRegister == true)
			{
				try
				{
					result = register.SignInUser(register);
				}
				catch (Exception e)
				{
					throw e;
				}
			}

			if (register.UpdateRegister == true)
			{
				try
				{
					register.CheckOutRegister(register);					
				}
				catch (Exception e)
				{
					throw e;
				}
			}
			return result;
		}

		#endregion
	}
}
