﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models
{
	public class CompanyStatus
	{
		[Key]
		[HiddenInput]
		public int Id { get; set; }

		public string StatusName { get; set; }
	}
}
