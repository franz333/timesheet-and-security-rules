﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using WebApp.AppTools;

namespace WebApp.Models
{
	public class User 
	{
		/// <summary>
		/// Class combined with FjDSessionExtensions to store session data
		/// </summary>

		// Person
		[Key]
		public int Id { get; set; }

		public bool isCheckinOut { get; set; }

		public bool hasActuallyCheckedIn { get; set; }

		public string LName { get; set; }

		public string FName { get; set; }

		public int? PersonVisited { get; set; }

		// Status
		public int StatusId { get; set; }

		//public string StatusType { get; set; }

		// Company
		public int? CompanyId { get; set; }

		public string CompanyName { get; set; }

		public int? DepartmentId { get; set; }

		public string DepartmentName { get; set; }

		public int RegisterId { get; set; }

		public int PersonTestId { get; set; }

		public List<int> RegisterIds { get; set; }

	}
}
