﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models
{
	public class PersonTest
	{
		[Key]
		public int Id { get; set; }

		public int PersonId { get; set; }

		public int TestId { get; set; }

		public float? Result { get; set; }

		public DateTime TakeDate { get; set; }

	}
}
