﻿using DAL.Services;
using DALE = DAL.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using WebApp.AppTools;
using WebApp.AppTools.Attributes;
using WebApp.Models.PersonViewModels;
using WebApp.Models.RegisterViewModels;

namespace WebApp.Models.RegisterSharedViewModels
{
	public abstract class AbstractRegisterVM
	{
		public int Id { get; set; }

		// Status
		[Display(Name = "Statut")]
		public int StatusId { get; set; }

		private IEnumerable<CompanyStatus> _companyStatuses;

		public IEnumerable<CompanyStatus> CompanyStatuses
		{
			get
			{
				if (_companyStatuses == null)
				{
					CompanyStatusService serv = new CompanyStatusService();
					_companyStatuses = serv.GetAll().Select(cs => cs.DalToCompanyStatusApp()).ToList();
				}
				return _companyStatuses;
			}
			set { _companyStatuses = value; }
		}

		// Company
		[Display(Name = "Nom de la société")]
		[EmptyOrPositive(ErrorMessage = "Veuillez choisir une société de la liste")]
		public int? CompanyId { get; set; }

		public int IdAutoComplete { get; set; }

		public IEnumerable<Company> Companies { get; set; }


		[Display(Name = "Indiquez le nom de la société")]
		[RequiredIfOtherFieldIsZero("CompanyId", ErrorMessage = "Le nom de la société est requis")]
		public string CompanyName { get; set; }

		// Department
		[Display(Name = "Département")]
		[EmptyOrPositive(ErrorMessage = "Veuillez choisir un département de la liste")]
		public int? DepartmentId { get; set; }

		public IEnumerable<Department> Departments { get; set; }

		[Display(Name = "Nom du département")]
		[RequiredIfOtherFieldIsZero("DepartmentId", ErrorMessage = "Le nom du département est requis")]
		public string DepartmentName { get; set; }

		// Person
		[Display(Name = "Vos nom et prénom")]
		public int? PersonId { get; set; }

		public IEnumerable<PersonDropDownVM> Persons { get; set; }

		[Display(Name = "Nom de famille")]
		[RequiredIfOtherFieldIsZero("PersonId", ErrorMessage = "Le nom de famille est requis")]
		public string LName { get; set; }

		[Display(Name = "Prénom")]
		[RequiredIfOtherFieldIsZero("PersonId", ErrorMessage = "Le prénom est requis")]
		public string FName { get; set; }

		
		[Display(Name = "Personne visitée")]
		public int? PersonVisitedId { get; set; }

		public IEnumerable<PersonDropDownVM> PersonsVisited { get; set; }
		
		#region Methods

		/// <summary>
		/// Get companies by status
		/// </summary>
		/// <param name="statusId"></param>
		/// <returns></returns>
		public IEnumerable<Company> GetCompaniesForDropDown(int statusId)
		{
			CompanyService service = new CompanyService();

			IEnumerable<Company> companies = service.GetAllCompanyByStatusWithoutDep(statusId).Select(c => c.DalToCompanyApp());
			return companies;
		}

		/// <summary>
		/// Get departements by companyId
		/// </summary>
		/// <param name="companyId"></param>
		/// <returns></returns>
		public IEnumerable<Department> GetDepartmentsForDropDown(int companyId)
		{
			DepartmentService service = new DepartmentService();
			IEnumerable<Department> departments = service.GetAllDepartmentByCompany(companyId).Select(d => d.DalToDepartmentApp());
			return departments;
		}

		//public abstract FormRegisterVM SetCompany(FormRegisterVM register);

		// it could be deleted id the methods in controllermethods remains valid for several controller
		/// <summary>
		/// Check that the company exist under a given statusId
		/// </summary>
		/// <param name="companyId"></param>
		/// <param name="statusId"></param>
		/// <returns></returns>
		public string CheckCompanyExist(int companyId, int statusId)
		{
			string result = null;

			CompanyService service = new CompanyService();
			DALE.Company company = service.Get(companyId, statusId);

			if (company == null)
			{
				result = "Cette société n'existe pas.";
			}

			return result;
		}

		/// <summary>
		/// Avoid a duplicate company name
		/// </summary>
		/// <param name="companyName"></param>
		/// <param name="statusId"></param>
		/// <returns></returns>
		public string AvoidDuplicateCompany(string companyName, int statusId)
		{
			string result = null;

			CompanyService service = new CompanyService();
			DALE.Company company = service.Get(companyName).FirstOrDefault();

			if (company != null)
			{
				if (company.CompanyStatusId != statusId)
				{
					result = String.Format("Cette société existe déjà sous le status {0}, veuillez recommencer le processus depuis le début en choissant ce statut", company.StatusName);
				}
				else
				{
					result = "Cette société existe déjà, veuillez la sélectionner dans la liste.";
				}
			}

			return result;
		}

		// see if can be reused later, if so, add it in validateDepartment method
		public string CheckDepartmentExist(int companyId, int departmentId)
		{
			string result = null;

			DepartmentService service = new DepartmentService();
			DALE.Department dep = service.Get(departmentId, companyId);

			if (dep == null)
			{
				return "Ce département n'existe pas.";
			}
			return result;
		}

		// see if can be reused later, if so, add it in validateDepartment method
		public string AvoidDuplicateDepartment(int companyId, string departmentName)
		{
			string result = null;

			DepartmentService service = new DepartmentService();
			DALE.Department dep = service.Get(companyId, departmentName);
			if (dep != null)
			{
				return "Ce département existe déjà, veuillez le sélectionner dans la liste.";
			}
			return result;
		}

		/// <summary>
		/// Check that department exist and avoid duplicate department name
		/// </summary>
		/// <param name="companyId"></param>
		/// <param name="departmentId"></param>
		/// <param name="departmentName"></param>
		/// <returns></returns>
		public string ValidateDepartment(int companyId, int departmentId, string departmentName)
		{
			string result = null;

			DepartmentService service = new DepartmentService();

			if (departmentId > 0)
			{
				// Check that department exists
				DALE.Department dep = service.Get(departmentId, companyId);

				if (dep == null)
				{
					return "Ce département n'existe pas.";
				}
			}
			else if (departmentId == 0)
			{
				// Avoid duplicate department name
				DALE.Department dep = service.Get(companyId, departmentName);
				if (dep != null)
				{
					return "Ce département existe déjà, veuillez le sélectionner dans la liste.";
				}
			}
			else
			{
				return "Veuillez sélectionner un département ou l'ajouter s'il n'est pas dans (\"Autre\").";
			}

			return result;
		}

		/// <summary>
		/// Validate model for SetCompany wizard step
		/// </summary>
		/// <param name="companyId"></param>
		/// <param name="statusId"></param>
		/// <param name="companyName"></param>
		/// <returns></returns>
		public string ValidateCompany(int companyId, int statusId, string companyName)
		{
			string result;

			if (companyId > 0)
			{
				result = ControllerMethods.CheckCompanyExist(companyId, statusId);
				
				if (result != null)
				{
					return result;
				}
			}
			else if (companyId == 0)
			{
				result = AvoidDuplicateCompany(companyName, statusId);

				if (result != null)
				{
					return result;
				}
			}
			else
			{
				return "Veuillez sélectionner une société dans la liste ou l'ajouter en cliquant sur \"Autre\".";
			}

			return result;
		}

		public abstract string ValidateSetCompany(AbstractRegisterVM register);

		/// <summary>
		/// Get a list of person within a company and a department
		/// </summary>
		/// <param name="companyId"></param>
		/// <param name="departmentId"></param>
		/// <returns></returns>
		public IEnumerable<PersonDropDownVM> GetPersonsForDropDown(int companyId, int departmentId)
		{
			PersonService service = new PersonService();
			IEnumerable<PersonDropDownVM> persons = service.GetPersonsByCompanyAndDepartment(companyId, departmentId).Select(p => p.DalToPersonDropDownVM()).ToList();
			return persons;
		}

		/// <summary>
		/// Get a list of person within a company
		/// </summary>
		/// <param name="companyId"></param>
		/// <returns></returns>
		public IEnumerable<PersonDropDownVM> GetPersonsForDropDown(int companyId)
		{
			PersonService service = new PersonService();
			IEnumerable<PersonDropDownVM> persons = service.GetPersonsByCompany(companyId).Select(p => p.DalToPersonDropDownVM());
			return persons;
		}

		/// <summary>
		/// Check that a person exist within a company and a department
		/// </summary>
		/// <param name="companyId"></param>
		/// <param name="departmentId"></param>
		/// <param name="personId"></param>
		/// <returns></returns>
		public string ValidatePerson(int companyId, int departmentId, int personId)
		{
			string result = null;

			PersonService service = new PersonService();

			DALE.Person person = service.Get(personId, companyId, departmentId);

			if (person == null)
			{
				return "Cette personne n'existe pas";
			}
			return result;		
		}

		/// <summary>
		/// Check that a person exist within a company
		/// </summary>
		/// <param name="companyId"></param>
		/// <param name="personId"></param>
		/// <returns></returns>
		public string ValidatePerson(int companyId, int personId)
		{
			string result = null;

			PersonService service = new PersonService();

			DALE.Person person = service.Get(personId, companyId);

			if (person == null)
			{
				return "Cette personne n'existe pas";
			}
			return result;
		}

		/// <summary>
		/// Check that the visited person exists
		/// </summary>
		/// <param name="visitedPersonId"></param>
		/// <returns></returns>
		public string ValidateVisitedPerson(int visitedPersonId)
		{
			string result = null;
			
			PersonService service = new PersonService();
			DALE.Person checkVisitee = service.GetPersonVisited(visitedPersonId);

			if (checkVisitee == null)
			{
				return "Cette personne n'existe pas, veuillez sélectionner un élément de la liste.";
			}
			return result;
		}

		/// <summary>
		/// Avoid a duplicate entry for a person within a company
		/// </summary>
		/// <param name="lName"></param>
		/// <param name="fName"></param>
		/// <param name="companyId"></param>
		/// <returns></returns>
		public string AvoidDuplicatePerson(string lName, string fName, int companyId)
		{
			string result = null;

			PersonService service = new PersonService();
			DALE.Person person = service.GetAll(lName, fName)
								.Where(p => p.CompanyId == companyId).SingleOrDefault();

			if (person != null)
			{
				return "Une personne avec les mêmes nom et prénom existe déjà. Veuillez indiquer une différenciation (ex: deuxième nom de famille).";
			}
			return result;
		}

		/// <summary>
		/// Avoid a duplicate entry for a person within a company and a department
		/// </summary>
		/// <param name="lName"></param>
		/// <param name="fName"></param>
		/// <param name="companyId"></param>
		/// <param name="departmentId"></param>
		/// <returns></returns>
		public string AvoidDuplicatePerson(string lName, string fName, int companyId, int departmentId)
		{
			string result = null;

			PersonService service = new PersonService();
			DALE.Person person = service.GetAll(lName, fName)
								.Where(p => p.CompanyId == companyId && p.DepartmentId == departmentId).SingleOrDefault();

			if (person != null)
			{
				return "Une personne avec les mêmes nom et prénom existe déjà. Veuillez indiquer une différenciation (ex: deuxième nom de famille).";
			}
			return result;
		}

		public abstract string ValidateSetPerson(AbstractRegisterVM register);

		public abstract int? SaveNewDepartment(AbstractRegisterVM register);

		public abstract int SaveNewCompany(AbstractRegisterVM register);

		public abstract int SaveNewPerson(AbstractRegisterVM register);

		//public abstract int SaveRegister(AbstractRegisterVM register);
		#endregion
	}
}
