﻿using DAL.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using WebApp.AppTools;
using WebApp.Models.QuestionViewModels;

namespace WebApp.Models.TestViewModels
{
	public class TestVM
	{
		[Key]
		public int Id { get; set; }

		[DisplayName("Titre")]
		[Required(ErrorMessage = "Le titre du test est requis.")]
		public string Title { get; set; }

		[Url(ErrorMessage = "L'URL n'est pas valide")]
		[DisplayName("URL de la vidéo")]
		[Required(ErrorMessage = "L'URL est requise.")]
		public string VideoURL { get; set; }

		[DisplayName("Actif")]
		public bool IsActive { get; set; }

		[DisplayName("Paramètres du test")]
		[Required(ErrorMessage = "Un test doit être lié à un jeu de paramètres.")]
		public int TestSettingsId { get; set; }

		private float _testSettingsTreshold;

		public float TestSettingsTreshold
		{
			get 
			{
				TestSettingsService service = new TestSettingsService();
				_testSettingsTreshold = service.Get(TestSettingsId).Treshold;
				return _testSettingsTreshold; 
			}
			set { _testSettingsTreshold = value; }
		}


		private IEnumerable<TestSettings> _testSettings;

		public IEnumerable<TestSettings> TestSettings
		{
			get 
			{
				if (_testSettings == null)
				{
					TestSettingsService service = new TestSettingsService();
					_testSettings = service.GetAll().Select(t => t.DalToTestSettingsApp());
				}
				return _testSettings; 
			}
			set { _testSettings = value; }
		}

		[DisplayName("Paramètres")]
		public string TestSettingsTitle { get; set; }

		private IEnumerable<QuestionVM> _questions;

		[DisplayName("Questions présentes dans ce test")]
		public IEnumerable<QuestionVM> Questions
		{
			get 
			{
				if (_questions == null)
				{
					QuestionService service = new QuestionService();
					_questions = service.GetAll(Id).Select(q => q.DalToQuestionVM()).OrderByDescending(o => o.IsActive).ToList();
				}
				return _questions; 
			}
			set { _questions = value; }
		}


		#region Methods

		#endregion
	}
}
