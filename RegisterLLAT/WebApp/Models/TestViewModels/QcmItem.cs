﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models.TestViewModels
{
	public class QcmItem
	{
		public int Id { get; set; }

		public string Item { get; set; }

		public bool IsAnswer { get; set; }
	}
}
