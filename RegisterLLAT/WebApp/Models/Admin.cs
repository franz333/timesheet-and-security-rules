﻿using DAL.Services;
using Microsoft.AspNetCore.Http;
using System;
using System.ComponentModel.DataAnnotations;
using WebApp.AppTools;

namespace WebApp.Models
{
	public class Admin
	{
		[Key]
		public int Id { get; set; }

		public string Email { get; set; }

		public string Pwd { get; set; }

		public string LName { get; set; }

		public string FName { get; set; }

		public bool IsActive { get; set; }

		public bool Deleted { get; set; }

		public int RoleTypeId { get; set; }

		public string Role { get; set; }

		public string RoleDescription { get; set; }
	}
}
