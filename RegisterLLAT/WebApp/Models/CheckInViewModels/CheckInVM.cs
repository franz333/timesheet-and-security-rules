﻿using DAL.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using WebApp.AppTools;
using WebApp.AppTools.Attributes;
using WebApp.Models.RegisterViewModels;

namespace WebApp.Models.CheckInViewModels
{
	public class CheckInVM
	{
		public bool IsCheckingOut { get; set; }

		#region SetStatus

		[HiddenInput(DisplayValue = false)]
		public int StatusId { get; set; }

		private IEnumerable<CompanyStatus> _companyStatuses;

		public IEnumerable<CompanyStatus> CompanyStatuses
		{
			get
			{
				if (_companyStatuses == null)
				{
					CompanyStatusService serv = new CompanyStatusService();
					_companyStatuses = serv.GetAll().Select(cs => cs.DalToCompanyStatusApp()).ToList();
				}
				return _companyStatuses;
			}
			set { _companyStatuses = value; }
		}
		
		#endregion

		#region SetCompany

		[Display(Name = "Nom de la société")]
		[EmptyOrPositive(ErrorMessage = "Veuillez choisir une société de la liste")]
		public int? CompanyId { get; set; }

		public List<Company> Companies { get; set; }

		[Display(Name = "Indiquez le nom de la société")]
		[RequiredIfOtherFieldIsZero("CompanyId", ErrorMessage = "Le nom de la société est requis")]
		public string CompanyName { get; set; }

		[Display(Name = "Département")]
		[EmptyOrPositive(ErrorMessage = "Veuillez choisir un département de la liste")]
		public int? DepartmentId { get; set; } 

		public List<Department> Departments { get; set; }

		[Display(Name = "Nom du département")]
		[RequiredIfOtherFieldIsZero("DepartmentId", ErrorMessage = "Le nom du département est requis")]
		public string DepartmentName { get; set; }

		public int IdAutoComplete { get; set; }

		#endregion

		#region SetPerson

		[Display(Name = "Vos nom et prénom")]
		public int? PersonId { get; set; }

		[Display(Name = "Nom de famille")]
		[RequiredIfOtherFieldIsZero("PersonId", ErrorMessage = "Le nom de famille est requis")]
		public string LName { get; set; }

		[Display(Name = "Prénom")]
		[RequiredIfOtherFieldIsZero("PersonId", ErrorMessage = "Le prénom est requis")]
		public string FName { get; set; }

		public List<PersonListVM> Persons { get; set; }

		public DateTime? DateBegin { get; set; } // TODO : delete

		public DateTime? DateEnd { get; set; } // TODO : delete

		[Display(Name = "Personne visitée")]
		public int? PersonVisitedId { get; set; }

		public List<PersonListVM> PersonsVisited { get; set; }

		#endregion
	}
}
