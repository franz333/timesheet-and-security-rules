﻿using DAL.Services;
using DALE = DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApp.AppTools;
using WebApp.Models.TestViewModels;

namespace WebApp.Models.PersonTestViewModels
{
	public class PersonTestVM
	{
		public int Id { get; set; }

		public int PersonId { get; set; }

		public int TestId { get; set; }

		public float? Result { get; set; }

		public DateTime TakeDate { get; set; }

		private TestVM _testVM;

		public TestVM TestVM
		{
			get 
			{
				TestService service = new TestService();
				_testVM = service.Get(TestId).DalToTestVM();
				return _testVM; 
			}
			set { _testVM = value; }
		}

		public List<AnswersVM> PersonAnswers { get; set; }

		#region Methods

		public int CreateBlankPersonTest(int personId, int testId)
		{
			int personTestId = 0;

			TestService testService = new TestService();
			DALE.Test test = testService.Get(testId);

			if (test.IsActive == true)
			{
				PersonTestVM personTest = new PersonTestVM()
				{
					PersonId = personId,
					TestId = testId,
					Result = null,
					TakeDate = DateTime.Now
				};

				PersonTestService service = new PersonTestService();

				try
				{
					personTestId = service.Insert(personTest.PersonTestVMToDal());
				}
				catch (Exception e)
				{
					throw e;
				}
			}			
			
			return personTestId;

		}

		/// <summary>
		/// Check if a blank test exists in DB and if so present it to the user
		/// </summary>
		/// <param name="person"></param>
		/// <returns></returns>
		//public PersonTestVM CheckForBlankTest(DALE.Person person)
		//{	
		//	// Get person testId
		//	int testId;
		//	try
		//	{
		//		testId = ControllerMethods.FindUserTestId(person.Id);

		//	}
		//	catch (Exception e)
		//	{
		//		throw e;
		//	}

		//	// Search for a PersonTest with the testId and personId where result is null
		//	PersonTestService service = new PersonTestService();
		//	PersonTestVM result;
		//	try
		//	{
		//		result = service.GetBlankTest(person.Id, testId).DalToPersonTestVM();
		//	}
		//	catch (Exception e)
		//	{
		//		throw e;
		//	}

		//	return result;
		//}

		/// <summary>
		/// Check if a user has a test to take. If so, check for incompleted test to reuse or create a new one.
		/// </summary>
		/// <param name="person"></param>
		/// <returns></returns>
		public PersonTestVM MustTakeTest(DALE.Person person)
		{
			PersonTestVM result = new PersonTestVM();

			// 0 means the person has never taken any test, 2 means she must take a test
			if (person.IsInOrder == 0 || person.IsInOrder == 2)
			{
				// Get user's testId 
				int testId;
				try
				{
					testId = ControllerMethods.FindUserTestId(person.Id);					
				}
				catch (Exception e)
				{
					throw e;
				}

				// Check if a blank test could be reused 
				PersonTestService service = new PersonTestService();
				PersonTestVM existingTest;
				try
				{
					existingTest = service.GetIncompletedTest(person.Id).DalToPersonTestVM();
				}
				catch (Exception e)
				{
					throw e;
				}

				if (existingTest != null)
				{
					return existingTest;
				}
				else
				{
					// Create a blank test if the test is active (will allow to identify users who skipped the test)			
					int id;
					try
					{
						id = result.CreateBlankPersonTest(person.Id, testId);
					}
					catch (Exception e)
					{
						throw e;
					}

					result = new PersonTestVM() { Id = id, TestId = testId, PersonId = person.Id };
				}
			}
			return result;		
		}

		#endregion
	}
}
