﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models.PersonTestViewModels
{
	public class AnswersVM
	{
		public int QuestionId { get; set; }
		public int QcmItemId { get; set; }

		public bool IsAnswer { get; set; }

		public string Item { get; set; }
	}
}
