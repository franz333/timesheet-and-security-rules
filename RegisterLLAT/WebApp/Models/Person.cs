﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models
{
	public class Person
	{
		[Key]
		public int Id { get; set; }

		public string LName { get; set; }

		public string FName { get; set; }

		public bool Deleted { get; set; }

		public int CompanyId { get; set; }

		public string CompanyName { get; set; }

		public int CompanyStatus { get; set; }

		public string StatusType { get; set; }

		public bool CompanyDeleted { get; set; }

		public int? DepartmentId { get; set; }

		public string DepartmentName { get; set; }

		public int? AdminId { get; set; }

		public string AdminLName { get; set; }

		public string AdminFName { get; set; }
	}
}
