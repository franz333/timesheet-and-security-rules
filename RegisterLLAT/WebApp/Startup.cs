using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using DAL;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc.DataAnnotations;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using WebApp.AppMiddleWare;
using WebApp.Models.RegisterViewModels;

namespace WebApp
{
	public class Startup
	{
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			services.AddControllersWithViews();

			// FjD : Configuration
			// Binding configuration (InvariantName & ConnectionString) to a static class in DAL library
			// Sensitive data are stored in secrets (dev) and in server environment variables (prod) 
			AppSettings.ConnectionString = Configuration.GetSection("AppSettings").GetSection("ConnectionString").Value;
			AppSettings.InvariantName = Configuration.GetSection("AppSettings").GetSection("InvariantName").Value;

			// FjD : Middleware service with the AddAuthentication and AddCookie methods
			services.AddAuthentication("CookieAuthentication").AddCookie("CookieAuthentication", options =>
			 {
				 options.Cookie.SameSite = SameSiteMode.None;
				 options.Cookie.HttpOnly = true;
				 options.Cookie.SecurePolicy = CookieSecurePolicy.Always;
				 options.Cookie.Name = "UserLoginCookie";
				 //options.ExpireTimeSpan = TimeSpan.FromDays(14);

				 // Specify where to redirect un-authenticated users
				 options.LoginPath = "/Account/Login";
				 options.LogoutPath = "/Account/Logout";
				 options.AccessDeniedPath = "/Account/AccessDenied";
			 });

			services.AddAuthorization(options =>
			{
				// #TODO map roles with non manually given role names
				options.AddPolicy("SuperAdmin", policy => policy.RequireClaim(ClaimTypes.Role, "Super Admin"));
				options.AddPolicy("FullAdmin", policy => policy.RequireClaim(ClaimTypes.Role, "Full Admin"));
				options.AddPolicy("HRAdmin", policy => policy.RequireClaim(ClaimTypes.Role, "HR Admin"));
				options.AddPolicy("PreventionAdmin", policy => policy.RequireClaim(ClaimTypes.Role, "Prevention Admin"));
			});

			// FjD : Use Session state
			services.AddSession(options =>
			{
				options.Cookie.SameSite = SameSiteMode.None;
				options.Cookie.HttpOnly = true;
				options.Cookie.Name = ".FjDRegisterLLAT.Session";
				//options.IdleTimeout = TimeSpan.FromSeconds(900);				
				options.Cookie.IsEssential = true;
			});

			// FjD : Data Annotation - Registering the Generic Attribute Adapter
			services.AddSingleton<IValidationAttributeAdapterProvider, CustomAttributeAdapterProviderRegistration>();

			// FjD : DI for HttpContent
			//services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
			//services.ConfigureApplicationCookie(options =>
			//{
			//	options.Cookie.SameSite = SameSiteMode.None;
			//});
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}
			else
			{
				app.UseExceptionHandler("/Home/Error");
				// The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
				app.UseHsts();
			}
			app.UseHttpsRedirection();
			app.UseStaticFiles();

			// FjD : Use Session state
			app.UseSession();

			// FjD : MysqlProvider (.Net connector)
			app.UseMysqlProviderRegistration();

			app.UseRouting();

			// FjD : Authentication & Authorization  
			app.UseAuthentication();
			app.UseAuthorization();


			app.UseEndpoints(endpoints =>
			{
				endpoints.MapControllerRoute(
					name: "default",
					pattern: "{controller=Home}/{action=Index}/{id?}");
			});
		}
	}
}
