﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using DAL.Services;
using DALE = DAL.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using WebApp.AppTools;
using WebApp.Models;
using WebApp.Models.AdminViewModels;
using WebApp.Models.RegisterViewModels;

namespace WebApp.Controllers
{
    [Authorize]
    public class AdminController : Controller
    {
        // Display Admin Dashboard
        public IActionResult Index()
        {
            return View();
        }

        // List of Administrators 
        [HttpGet]
        [Authorize(Roles = "Super Admin")]
        public IActionResult Administrators()
        {
            AdminService serv = new AdminService();
            IEnumerable<AdminVM> admins = serv.GetAll().Select(a => a.DalToAdminVM());

            // Prepare ViewBag for modal display in view in case of redirectToAction from Edit
            if (TempData["title"] != null && TempData["message"] != null)
            {
                ViewBag.ModalTitle = TempData["title"];
                TempData.Remove("title");

                ViewBag.ModalMessage = TempData["message"];
                TempData.Remove("message");
            }

            return View(admins);
        }

        [HttpGet]
        public IActionResult Details(int id)
        {
            // Check that the user is trying to access its own data or that the user is SA
            bool isAllowed = IsAdminAllowed(id);

            if (isAllowed == false)
            {
                return RedirectToAction("AccessDenied", "Account");
            }
            
            // Get the admin data 
            AdminService serv = new AdminService();
            AdminForm admin = new AdminForm();
            admin = serv.Get(id).ToAppAdminForm();
            
            return View(admin);
        }

        [HttpGet]
        [Authorize(Roles = "Super Admin")]
        public IActionResult Create()
        {
            AdminForm form = new AdminForm();
            return View(form);
        }

        [HttpPost]
        [Authorize(Roles = "Super Admin")]
        [ValidateAntiForgeryToken]
        public IActionResult Create(AdminForm admin)
        {
            // Model binding validation
            if (!ModelState.IsValid)
            {
                return View(admin);
            }

            // Server side validation
            // Check that roleType exist in DB and that it's different from SA, avoid duplicate name and check email is unique 
            string validate = ValidateAdmin(admin);
            if (validate != null)
            {
                ViewBag.Error = validate;
                return View(admin);
            }

            // Insert admin
            AdminService adminService = new AdminService();
			try
			{
                admin.Id = adminService.Insert(admin.AdminFormToDal());
            }
			catch (Exception e)
			{
				throw e;
			}

            return RedirectToAction("Details", "Admin", new { id = admin.Id });
        }

        [HttpGet]
        public IActionResult Edit(int id)
        {
            // Check wether the user is trying to edit its own data or whether the user is SA
            bool isAllowed = IsAdminAllowed(id);

            if (isAllowed == false)
            {
                return RedirectToAction("AccessDenied", "Account");
            }

            // Check that admin exists in DB
            DALE.Admin admin = CheckAdminExists(id);
			if (admin == null)
			{
                TempData["title"] = "Erreur";
                TempData["message"] = "L'administrateur que vous souhaitez éditer n'existe pas.";
                return RedirectToAction("Administrators");
            }
           
            AdminVM adminVM = admin.DalToAdminVM();
            return View(adminVM);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(AdminVM admin)
        {
            // Model binding validation
            if (!ModelState.IsValid)
            {
                return View(admin);
            }

            // Server side validation
            // Check admin exists
            DALE.Admin checkAdmin = CheckAdminExists(admin.Id);
			if (checkAdmin == null)
			{
                TempData["title"] = "Erreur";
                TempData["message"] = "L'administrateur que vous souhaitez éditer n'existe pas.";
                return RedirectToAction("Administrators");
            }

            // Check that roleType exist in DB and that it's different from SA, avoid duplicate name and check email is unique 
            string validate = ValidateAdmin(admin);
			if (validate != null)
			{
                ViewBag.Error = validate;
                return View(admin);
            }

            // Update Admin
            AdminService service = new AdminService();
			try
			{
                service.Update(admin.AdminVMToDal());
            }
			catch (Exception e)
			{
				throw e;
			}            

            return RedirectToAction("Details", "Admin", new { id = admin.Id });
        }

        [HttpGet]
        [Authorize(Roles = "Super Admin")]
        public IActionResult Delete(int id)
        {
            AdminService service = new AdminService();
            service.Delete(id);
            AdminVM admin = service.Get(id).DalToAdminVM();
            return PartialView("_AdminTable", admin);
        }

        [HttpGet]
        [Authorize(Roles = "Super Admin")]
        public IActionResult Activate(int id)
        {
            // TODO : check Admin exist application side (it's done in DB at the moment)
            AdminService service = new AdminService();
			try
			{
                service.Activate(id);
            }
			catch (Exception e)
			{
				throw e;
			}
            
            AdminVM admin = service.Get(id).DalToAdminVM();
            return PartialView("_AdminTable", admin);
        }

        [HttpGet]
        [Authorize(Roles = "Super Admin")]
        public IActionResult Deactivate(int id)
        {
            // TODO : check Admin exist application side (it's done in DB at the moment)
            AdminService service = new AdminService();
			try
			{
                service.Deactivate(id);
            }
			catch (Exception e)
			{
				throw e;
			}
            
            AdminVM admin = service.Get(id).DalToAdminVM();
            return PartialView("_AdminTable", admin);
        }

        [HttpGet]
        [Authorize(Roles = "Super Admin")]
        public IActionResult Restore(int id)
        {
            // TODO : check Admin exist application side (it's done in DB at the moment)
            AdminService service = new AdminService();
			try
			{
                service.Restore(id);
            }
			catch (Exception e)
			{
				throw e;
			}
            
            AdminVM admin = service.Get(id).DalToAdminVM();
            return PartialView("_AdminTable", admin);
        }


        [HttpGet]
        public IActionResult GetRoleType(int id)
        {
            try
            {
                RoleTypeService serv = new RoleTypeService();
                RoleType roleType = new RoleType();
                roleType = serv.Get(id).DalToRoleTypeApp();
                return View(roleType);
            }
            catch (Exception)
            {

                return View();
            }
        }

        [HttpGet]
        public IActionResult UpdateRoleType(int id)
        {
            try
            {
                RoleTypeService serv = new RoleTypeService();
                RoleType roleType = new RoleType();
                roleType = serv.Get(id).DalToRoleTypeApp();
                return View(roleType);
            }
            catch (Exception)
            {
                return View();
            }
        }

        /// <summary>
        /// Get role description for dropdown in edit and create views (js)
        /// </summary>
        /// <param name="roleId"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetRoleDescription(int roleId)
        {
            RoleTypeService serv = new RoleTypeService();
            RoleType role = serv.Get(roleId).DalToRoleTypeApp();

            string roleType = "";

			if (role != null)
			{
                roleType = role.RoleTypeDescription;
            }

            return Json(roleType);
        }

        [HttpGet]
        public IActionResult ModifyPassword(int userId)
        {
            ModifyPassword form = new ModifyPassword();
            form.UserId = userId;

            return PartialView("_ModifyPwd", form);
        }

        [HttpPost]
        public IActionResult ModifyPassword(ModifyPassword form)
        {
            // Client side validation
            if (!ModelState.IsValid)
            {
                return PartialView("_ModifyPwd", form);
            }

            // Server site validation
            if (!String.IsNullOrWhiteSpace(form.Password) && !String.IsNullOrWhiteSpace(form.PasswordCheck) && !String.IsNullOrWhiteSpace(form.CurrentPassword))
            {
                if (String.Compare(form.Password, form.PasswordCheck) == 0)
                { 
                    // if the user is the SA, check its own password in any case, otherwise check user password
                    int userId = Int32.Parse(HttpContext.User.FindFirst("Id").Value);

                    AdminService serv = new AdminService();

                    int pwdMatch = 0;

                    // If the session user equals the user whose password is to be modified
                    if (userId == form.UserId)
                    {
                        pwdMatch = serv.PasswordMatch(form.CurrentPassword.ApplyHash(), form.UserId);
                    }
                    // if the session user is SA
                    else if (userId == 1)
                    {
                        pwdMatch = serv.PasswordMatch(form.CurrentPassword.ApplyHash(), 1);
                    }
                    else
                    {
                        ViewBag.ModalError = "Vous n'êtes pas autorisé-e à modifier ce mot de passe";
                        return PartialView("_ModifyPwd", form);
                    }

                    bool success = true;

                    if (pwdMatch > 0)
                    {
                        DAL.Entities.Admin admin = new DAL.Entities.Admin() 
                        { 
                            Pwd = form.Password.ApplyHash(), 
                            Id = form.UserId 
                        };

                        success = serv.ModifyPwd(admin);
                    }
                    else
                    {
                       ViewBag.ModalError = "Votre mot de passe actuel ne correspont à aucun compte. Veuillez réessager...";
                    }

                    if (!success)
                    {
                        ViewBag.ModalError = "La modification du mot de passe n'a pas été réalisée. Veuillez réessager...";
                    }
                }
            }
            return PartialView("_ModifyPwd", form);
        }

        [HttpGet]
        public IActionResult GetCompanies(int statusId)
        {
            SetCompany company = new SetCompany();

            // Get companies by status 
            CompanyService serv = new CompanyService();
            company.Companies = serv.GetAllCompanyByStatusWithoutDep(statusId).Select(c => c.DalToCompanyApp()).ToList();

            IEnumerable<SelectListItem> options;

            // Provide the company's dropdownlist
            if (company.Companies.Count() > 0)
            {
                // Add "Autre" in the dropdownlist
                Company other = new Company { Id = 0, CompanyName = "Autre" };
                company.Companies.Insert(0, other);

                options = company.Companies.Select(a => new SelectListItem
                {
                    Value = a.Id.ToString(),
                    Text = a.CompanyName
                }).ToList();
            }
            else
            {
                // If no dropdown list with companies is provided, force user to enter the company's name
                company.Id = 0;
                options = null;
            }
            return Json(options);
        }

		#region Methods
		/// <summary>
		/// Check that the connected user is trying to access his own data or that he is the SuperAdmin
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		[NonAction]
        public bool IsAdminAllowed(int id)
        {
            // Get Id of the current user
            int userId = Int32.Parse(HttpContext.User.FindFirst("Id").Value);

            bool isAllowed = false;

            if (userId == id || userId == 1)
            {
                isAllowed = true;
            }

            return isAllowed;
        }

        /// <summary>
        /// Check that admin exists in DB
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [NonAction]
        private DALE.Admin CheckAdminExists(int id)
        {
            AdminService service = new AdminService();
            DALE.Admin admin = service.Get(id);
            return admin;
        }

        /// <summary>
        /// Check that roleType exists in DB and that it's different from SA
        /// </summary>
        /// <param name="roletypeId"></param>
        /// <returns></returns>
        [NonAction]
        private string CheckRoleTypeExist(int roletypeId)
        {
            string result = null;

            if (roletypeId == 1)
            {
                return "Vous ne pouvez pas attribuer ce rôle. Un seul super administrateur est autorisé.";
            }
            else if (roletypeId == 0)
            {
                return "Veuillez sélectionner un rôle dans la liste.";
            }
            else
            {
                RoleTypeService service = new RoleTypeService();
                DALE.RoleType role = service.Get(roletypeId);
                if (role == null)
                {
                    return "Ce role n'existe pas.";
                }
            }
            return result;
        }

        /// <summary>
        /// Avoid a duplicate admin firstname and lastname in DB
        /// </summary>
        /// <param name="admin"></param>
        /// <returns></returns>
        [NonAction]
        private string AvoidDuplicateAdminName(AdminVM admin)
        {
            string result = null;

            AdminService serv = new AdminService();
            IEnumerable<Admin> admins = serv.GetAll().Select(a => a.DalToAdminApp());
            var isInDB = false;

            // If a new admin is created, Id will be 0
            if (admin.Id == 0)
            {
                isInDB = admins.Any(a => a.FName.Equals(admin.FName, StringComparison.OrdinalIgnoreCase) && a.LName.Equals(admin.LName, StringComparison.OrdinalIgnoreCase));
            }
            else
            {
                isInDB = admins.Any(a => a.FName.Equals(admin.FName, StringComparison.OrdinalIgnoreCase) && a.LName.Equals(admin.LName, StringComparison.OrdinalIgnoreCase) && a.Id != admin.Id);
            }

            if (isInDB == true)
            {
                result = "cet administrateur existe déjà. Deux administrateurs avec les même noms et prénoms ne peuvent être coexister.";
            }

            return result;
        }

        /// <summary>
        /// Avoid duplicate emails in DB
        /// </summary>
        /// <param name="admin"></param>
        /// <returns></returns>
        [NonAction]
        private string AvoidDuplicateAdminEmail(AdminVM admin)
        {
            string result = null;
            bool exist = false;

            AdminService service = new AdminService();

			// If a new admin is being created, Id = 0
			if (admin.Id == 0)
			{
                exist = service.GetAll().Any(a => a.Email.Equals(admin.Email, StringComparison.OrdinalIgnoreCase));
            }
			else
			{
                exist = service.GetAll().Any(a => a.Email.Equals(admin.Email, StringComparison.OrdinalIgnoreCase) && a.Id != admin.Id);
            }            

			if (exist)
			{
                return "un administrateur avec le même e-mail existe déjà. L'e-mail doit être unique.";
			}

            return result;
        }

        /// <summary>
        /// Validate admin checking roletype, names and email.
        /// </summary>
        /// <param name="admin"></param>
        /// <returns></returns>
        [NonAction]
        private string ValidateAdmin(AdminVM admin)
        {
            // Check role
            string result = CheckRoleTypeExist(admin.RoleId);

			if (result != null)
			{
                return result;
			}

            // Check email
            result = AvoidDuplicateAdminEmail(admin);

            if (result != null)
            {
                return result;
            }

            // Avoid duplicate Admin
            result = AvoidDuplicateAdminName(admin);

			if (result != null)
			{
                return result;
            }

            return result;
        }
        #endregion
    }
}