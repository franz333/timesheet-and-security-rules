﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL.Services;
using DALE = DAL.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApp.AppTools;
using WebApp.Facades.Register;
using WebApp.Models;
using WebApp.Models.RegisterPlanningViewModels;
using Microsoft.AspNetCore.Authorization;

namespace WebApp.Controllers
{
    [Authorize(Roles = "Super Admin, Full Admin, HR Admin")]
    public class RegisterPlanningController : Controller
	{
		//public IActionResult Index()
		//{
		//	return View();
		//}

        [HttpGet]
        public IActionResult SetStatus()
        {
            //User user = GetUser();

            FormRegisterPlanningVM register = new FormRegisterPlanningVM();

            //SetStatus status = new SetStatus();
            //CompanyStatusService serv = new CompanyStatusService();
            //status.CompanyStatus = serv.GetAll().Select(cs => cs.DalToCompanyStatusApp()).ToList();

            // Save statuses
            //statusesGlobal = status.CompanyStatus;
            
            // If next && previous
            //status.Id = user.StatusId;

            return View(register);
        }

        [HttpPost]
		public IActionResult SetStatus(string nextBtn, FormRegisterPlanningVM register)
		{
			User session = GetUser();

			if (nextBtn != null)
			{
                // Check if a choice is made, if so check that the status exists
				ViewBag.Error = ControllerMethods.CheckCompanyStatus(register.Id);
				if (ViewBag.Error != null)
				{
                    return View(register);
                }

                // if status is valid
                session.StatusId = register.Id;
				HttpContext.Session.SetComplexData("user", session);

				return RedirectToAction("SetCompany");

			}
			return View(register);
		}

        //[HttpGet]
        //public IActionResult SetCompany()
        //{
        //    User user = GetUser();

        //    RegisterFacade facade = new RegisterFacade();

        //    SetCompany company = facade.SessionToSetCompany(user);
        //    company = facade.PrepareCompany(company);

        //    return View(company);
        //}

        [HttpGet]
        public IActionResult SetCompany()
        {
            User user = GetUser();

            FormRegisterPlanningVM register = new FormRegisterPlanningVM();
            register = register.UserToSetCompanyProps(user);

            register = register.SetCompany(register);

            return View(register);
        }

        //[HttpPost]
        //public IActionResult AddRegisterPlanningCompany(string nextBtn, string prevBtn, SetCompany company)
        //{
        //    User user = GetUser();
        //    RegisterFacade facade = new RegisterFacade();

        //    if (prevBtn != null)
        //    {
        //        return RedirectToAction("SetStatus");
        //    }

        //    if (nextBtn != null)
        //    {
        //        // Client side validation
        //        if (!ModelState.IsValid)
        //        {
        //            company = facade.PrepareCompany(company);
        //            return View(company);
        //        }
        //        // Server side validation
        //        else
        //        {
        //            ViewBag.Error = facade.CheckCompany(company);
        //        }
        //    }

        //    // An error occured, return to view
        //    if (ViewBag.Error != null)
        //    {
        //        company = facade.PrepareCompany(company);
        //        return View(company);
        //    }
        //    else
        //    {
        //        // No error save new company and new department if exists
        //        if (company.Name != null)
        //            user.CompanyId = facade.SaveCompany(company);
        //        user.CompanyId = (int)company.Id;

        //        if (company.DepartmentName != null)
        //            user.DepartmentId = facade.SaveDepartment(company);
        //        user.DepartmentId = company.DepartmentId;
        //    }

        //    // Save session
        //    HttpContext.Session.SetComplexData("user", user);

        //    return RedirectToAction("AddRegisterPlanningPerson");
        //}

        //[HttpGet]
        //public IActionResult AddRegisterPlanningPerson()
        //{
        //    User user = GetUser();
        //    SetPerson person = SetPersonToView(user);
        //    return View(person);
        //}

        //[HttpPost]
        //public IActionResult AddRegisterPlanningPerson(string nextBtn, string prevBtn, SetPerson person)
        //{
        //    User user = GetUser();

        //    PersonService serv = new PersonService();

        //    if (prevBtn != null)
        //    {
        //        return RedirectToAction("AddRegisterPlanningCompany");
        //    }

        //    if (nextBtn != null)
        //    {
        //        if (!ModelState.IsValid)
        //        {
        //            SetPerson personToView = SetPersonToView(user);
        //            return View(personToView);
        //        }
        //        else
        //        {
        //            // A user is selected from the list
        //            if (person.Id > 0)
        //            {
        //                // Check that the person is in DB
        //                DALE.Person personExist = serv.Get((int)person.Id, (int)user.CompanyId);

        //                if (personExist != null)
        //                {
        //                    user.Id = (int)person.Id;
        //                }
        //                else
        //                {
        //                    ViewBag.Error = "Cette personne n'existe pas";
        //                }
        //            }
        //            else if (person.Id == 0)
        //            {
        //                string nameAndLastName = ControllerMethods.NameAndLastNameValid(person.FName, person.LName);

        //                if (nameAndLastName == null)
        //                {
        //                    // Prevent duplicate in the DB, search by LName & FName or FName & LName
        //                    Person personExists = CheckIfPersonExistInACompany(user, person);

        //                    // If we find a duplicate alert the user
        //                    if (personExists != null)
        //                    {
        //                        ViewBag.NameError = "Cette personne existe déjà, sélectionnez la dans la liste. Si vous souhaitez ajouter une personne portant le même nom et le même prénom, ajoutez une différenciation (ex: troisième prénom).";
        //                    }
        //                    else
        //                    {
        //                        user.Id = 0;
        //                        user.LName = person.LName;
        //                        user.FName = person.FName;
        //                    }
        //                }
        //                // In case of error, set viewbag
        //                else
        //                {
        //                    ViewBag.NameError = nameAndLastName;
        //                }
        //            }
        //            else
        //            {
        //                ViewBag.Error = "Veuillez choisir une personne dans la liste ou inscrire son nom manuellement";
        //            }

        //            // The user is a visitor and must provide a visitee
        //            if (user.StatusId == 3)
        //            {
        //                if (person.PersonVisitedId > 0)
        //                {
        //                    // Check that the visitee is in DB
        //                    DALE.Person personVisitedExists = serv.GetPersonVisited((int)person.PersonVisitedId);

        //                    if (personVisitedExists != null)
        //                    {
        //                        user.PersonVisited = person.PersonVisitedId;
        //                    }
        //                    else
        //                    {
        //                        ViewBag.VisitedError = "La personne visitée n'existe pas.";
        //                    }
        //                }
        //                else
        //                {
        //                    ViewBag.VisitedError = "Veuillez sélectionner la personne que vous venez visiter.";
        //                }
        //            }
        //        }
        //    }

        //    if (ViewBag.Error != null || ViewBag.NameError != null || ViewBag.VisitedError != null)
        //    {
        //        SetPerson personToView = SetPersonToView(user);
        //        return View(personToView);
        //    }
        //    else
        //    {
        //        // Save new user if he/she exists
        //        if (user.LName != null && user.FName != null)
        //        {
        //            DALE.Person newPerson = new DALE.Person();
        //            newPerson.LName = user.LName;
        //            newPerson.FName = user.FName;
        //            newPerson.AdministratorId = 1;
        //            newPerson.CompanyId = (int)user.CompanyId;
        //            newPerson.DepartmentId = user.DepartmentId;
        //            newPerson.Id = serv.Insert(newPerson);
        //            user.Id = newPerson.Id;

        //            // reset values as a new user was created
        //            user.FName = null;
        //            user.LName = null;
        //        }

        //        // Save session
        //        HttpContext.Session.SetComplexData("user", user);

        //        return RedirectToAction("AddRegisterPlanningTime");
        //    }
        //}

        //[HttpGet]
        //public IActionResult AddRegisterPlanningTime()
        //{
        //    SetTime setTime = new SetTime();
        //    return View(setTime);
        //}

        //[HttpPost]
        //public IActionResult AddRegisterPlanningTime(string nextBtn, string prevBtn, IFormCollection form)
        //{
        //    if (prevBtn != null)
        //    {
        //        return RedirectToAction("AddRegisterPlanningPerson");
        //    }

        //    if (nextBtn != null)
        //    {
        //        if (!ModelState.IsValid)
        //        {
        //            return View();
        //        }
        //        else
        //        {
        //            User user = GetUser();

        //            RegisterPlanningVM register = new RegisterPlanningVM();

        //            string validation;
        //            string integrity;

        //            if (form["PlanningTypeId"] == "1")
        //            {
        //                validation = FormTimeValid(form, "SameDayDate", "SameDayTimeIn", "SameDayTimeOut");

        //                if (validation == null)
        //                {
        //                    DateTime timeIn = SameDayToDateTime(form, "SameDayDate", "SameDayTimeIn");
        //                    DateTime timeOut = SameDayToDateTime(form, "SameDayDate", "SameDayTimeOut");

        //                    integrity = TimeInTimeOutIntegrity(timeIn, timeOut);
        //                    if (integrity == null)
        //                    {
        //                        register.TimeIn = timeIn;
        //                        register.TimeOut = timeOut;

        //                        RegisterPlanningService serv = new RegisterPlanningService();
        //                        DALE.RegisterPlanning registerExist = serv.GetExisting(user.Id, register.TimeIn);

        //                        // if a registerPlanning exist
        //                        if (registerExist != null)
        //                        {
        //                            // check if it was planned
        //                            if (registerExist.Planned == false)
        //                            {
        //                                bool succeed = serv.SetPlanned(registerExist.Id);
        //                                if (succeed == true)
        //                                {
        //                                    TempData["Alert"] = "true";
        //                                }
        //                                else
        //                                {
        //                                    // TODO : handle error and redirect to error page
        //                                }
        //                            }
        //                            else
        //                            {
        //                                TempData["Alert"] = "false";
        //                            }
        //                            return RedirectToAction("GetRegisterPlanning");
        //                        }
        //                    }
        //                    else
        //                    {
        //                        ViewBag.Error = integrity;
        //                    }
        //                }
        //                else
        //                {
        //                    ViewBag.Error = FormTimeValidViewBag(validation, 1);
        //                }

        //            }
        //            else if (form["PlanningTypeId"] == "2")
        //            {
        //                validation = FormTimeValid(form, "DifferentDayTimeIn", "DifferentDayTimeOut");

        //                if (validation == null)
        //                {
        //                    DateTime DateTimeIn = DateTime.Parse(form["DifferentDayTimeIn"]);
        //                    DateTime DateTimeOut = DateTime.Parse(form["DifferentDayTimeOut"]);

        //                    integrity = TimeInTimeOutIntegrity(DateTimeIn, DateTimeOut);
        //                    if (integrity == null)
        //                    {
        //                        //dt.ToString("H:mm");



        //                        register.TimeIn = DateTimeIn;
        //                        register.TimeOut = DateTimeOut;





        //                    }
        //                    else
        //                    {
        //                        ViewBag.Error = integrity;
        //                    }
        //                }
        //                else
        //                {
        //                    ViewBag.Error = FormTimeValidViewBag(validation, 2);
        //                }
        //            }
        //            else if (form["PlanningTypeId"] == "3")
        //            {

        //            }
        //            else
        //            {
        //                ViewBag.Error = "Veuillez sélectionner un type d'horaire.";
        //            }
        //        }
        //    }

        //    SetTime setTimeError = FormToSetTime(form);
        //    return View(setTimeError);
        //}

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult SetCompany(string nextBtn, string prevBtn, FormRegisterPlanningVM register)
        {
            if (prevBtn != null)
            {
                return RedirectToAction("SetStatus");
            }

            if (nextBtn != null)
            {
                if (!ModelState.IsValid)
                {
                    register = register.SetCompany(register);
                    return View("SetCompany", register);
                }

                User user = GetUser();

                // Force statusId value to be like session value (to prevent manual DOM changes)
                register.StatusId = user.StatusId;

                // Validation
                // TODO : Manage autocomplete possibility
                ViewBag.Error = register.ValidateSetCompany(register);
                if (ViewBag.Error != null)
                {
                    register = register.SetCompany(register);
                    return View("SetCompany", register);
                }

                // If validation is OK
                user.CompanyId = register.CompanyId;
                user.CompanyName = register.CompanyName;
                user.DepartmentId = register.DepartmentId;
                user.DepartmentName = register.DepartmentName;
                HttpContext.Session.SetComplexData("user", user);
            }
            return RedirectToAction("SetPerson");
        }

        [HttpGet]
        public IActionResult SetPerson()
        {
            User user = GetUser();

            FormRegisterPlanningVM register = new FormRegisterPlanningVM();
            register = register.UserToSetCompanyProps(user);

            // Prepare model with dropdowns
            register = register.SetPerson(register);

            return View("SetPerson", register);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult SetPerson(string nextBtn, string prevBtn, FormRegisterPlanningVM register)
        {
            if (prevBtn != null)
            {
                return RedirectToAction("SetCompany");
            }

            if (nextBtn != null)
            {
                if (!ModelState.IsValid)
                {
                    register = register.SetPerson(register);
                    return View("SetPerson", register);
                }

                // Force statusId, companyId, departmentId values to be like session value (to prevent manual DOM changes)             
                User user = GetUser();
                register.StatusId = user.StatusId;
                register.CompanyId = user.CompanyId;
                register.DepartmentId = user.DepartmentId;

                // Check provided person
                ViewBag.Error = register.ValidateSetPerson(register);

                if (ViewBag.Error != null)
                {
                    register = register.SetPerson(register);
                    return View("SetPerson", register);
                }

                // If validation is OK
                user.Id = (int)register.PersonId;
                user.FName = register.FName;
                user.LName = register.LName;
                user.PersonVisited = register.PersonVisitedId;
                HttpContext.Session.SetComplexData("user", user);

                // Save potential new user data
                register.CompanyName = user.CompanyName;
                register.DepartmentName = user.DepartmentName;

                try
                {
                    register.SaveNewUserData(register);
                }
                catch (Exception e)
                {
                    throw e;
                }

                // Save data in session
                user.Id = (int)register.PersonId;
                user.CompanyId = register.CompanyId;
                user.DepartmentId = register.DepartmentId;
                HttpContext.Session.SetComplexData("user", user);
            }

            return RedirectToAction("SetTime");
        }

        [HttpGet]
        public IActionResult SetTime()
        {
            FormRegisterPlanningVM register = new FormRegisterPlanningVM();
            return View(register);
        }

        [HttpPost]
        public IActionResult SetTime(string nextBtn, string prevBtn, IFormCollection form)
        {
            if (prevBtn != null)
            {
                return RedirectToAction("SetPerson");
            }            

            if (nextBtn != null)
            {
                FormRegisterPlanningVM register = new FormRegisterPlanningVM();
                
                // Get data from form and check for null inputs
                register = register.FormToSetTime(form, out int inValid);

				if (register.Times.Count() == 0)
				{
                    ViewBag.Error = "Le formulaire est vide, veuillez ajouter au moins une plage horaire.";
                    return View(register);
                }

                if (!ModelState.IsValid || inValid > 0)
                {
                    if (inValid > 0)
                    {
                        ViewBag.Error = "Veuillez remplir les dates et heures correctement.";
                    }
                    return View(register);
                }

                // Check Time integrity (no duplicate, timeIn < timeOut)
                ViewBag.Error = register.ValidateTimeIntegrity(register);

                if (ViewBag.Error != null)
                {
                    return View(register);
                }

                // Prepare registers for insert or update
                User user = GetUser();
                int AdministratorId = int.Parse(HttpContext.User.FindFirst("Id").Value);
                List<DALE.RegisterPlanning> registers = register.PrepareRegisters(register, user, AdministratorId);

                // Update or insert in DB
                RegisterPlanningService service = new RegisterPlanningService();

                int update = 0;
                int insert = 0;
                int duplicate = 0;

                user.RegisterIds = new List<int>(); // TODO : did not manage to add list to tempdata and retrieve it

                foreach (var item in registers)
                {
                    DALE.RegisterPlanning registerExist = service.GetExisting(item.PersonId, item.TimeIn);

                    if (registerExist != null)
                    {
                        // check if it was planned and updated it
                        if (registerExist.Planned == false)
                        {
                            try
                            {
                                service.SetPlanned(registerExist.Id);
                                item.Id = registerExist.Id;
                                user.RegisterIds.Add(item.Id);
                                update++;
                            }
                            catch (Exception e)
                            {
                                throw e;
                            }
                        }
						else
						{
                            user.RegisterIds.Add(registerExist.Id);
                            duplicate++;
						}
                    }
                    else
                    {
                        try
                        {
                            item.Id = service.Insert(item);
                            user.RegisterIds.Add(item.Id);
                            insert++;
                        }
                        catch (Exception e)
                        {
                            throw e;
                        }
                    }
                }
                TempData["RegisterUpdates"] = update;
                TempData["RegisterInserts"] = insert;
                TempData["RegisterDuplicates"] = duplicate;
                HttpContext.Session.SetComplexData("user", user);
            }
            return RedirectToAction("Summary");
        }

        #region ARchives à deleter
        [AllowAnonymous]
        [HttpGet]
        public IActionResult Test()
        {
            FormRegisterPlanningVM register = new FormRegisterPlanningVM();
            return View(register);
        }

        [AllowAnonymous]
        [HttpPost]
        public IActionResult Test(string nextBtn, IFormCollection form)
        {
            FormRegisterPlanningVM register = new FormRegisterPlanningVM();

            if (nextBtn != null)
            {
                // Get data from form and check for null inputs
                register = register.FormToSetTime(form, out int inValid);

                if (!ModelState.IsValid || inValid > 0)
                {
                    if (inValid > 0)
                    {
                        ViewBag.Error = "Veuillez remplir les dates et heures correctement.";
                    }
                    return View(register);
                }

                // Check Time integrity (no duplicate, timeIn < timeOut)
                ViewBag.Error = register.ValidateTimeIntegrity(register);

                if (ViewBag.Error != null)
                {
                    return View(register);
                }

                // Prepare registers
                User user = GetUser();
                int AdministratorId = int.Parse(HttpContext.User.FindFirst("Id").Value);
                List<DALE.RegisterPlanning> registers = register.PrepareRegisters(register, user, AdministratorId);

                // Update or insert in DB
                RegisterPlanningService service = new RegisterPlanningService();

                int update = 0;
                int insert = 0;
                List<int> registersIds = new List<int>();

                foreach (var item in registers)
                {
                    DALE.RegisterPlanning registerExist = service.GetExisting(item.PersonId, item.TimeIn);

                    if (registerExist != null)
                    {
                        // check if it was planned and updated it
                        if (registerExist.Planned == false)
                        {
                            try
                            {
                                service.SetPlanned(registerExist.Id);
                                item.Id = registerExist.Id;
                                registersIds.Add(item.Id);
                                update++;
                            }
                            catch (Exception e)
                            {
                                throw e;
                            }
                        }
                    }
                    else
                    {
                        try
                        {
                            item.Id = service.Insert(item);
                            registersIds.Add(item.Id);
                            insert++;
                        }
                        catch (Exception e)
                        {
                            throw e;
                        }
                    }
                }
                TempData["RegisterUpdates"] = update;
                TempData["RegisterInserts"] = insert;
                TempData["RegisterIds"] = registersIds;
            }
            return RedirectToAction("Summary");
        }
        #endregion

        [HttpGet]
        public IActionResult Summary()
        {
            User user = GetUser();

            // Someone typed the URL directly
			if (user.RegisterIds == null)
			{
                return RedirectToAction("Index", "Admin");
            }
			else
			{
                RegisterPlanningService service = new RegisterPlanningService();
                List<RegisterPlanningVM> registers = new List<RegisterPlanningVM>();

                foreach (var item in user.RegisterIds)
                {
                    try
                    {
                        RegisterPlanningVM temp = service.Get(item).DalToRegisterPlanningVM();
                        registers.Add(temp);
                    }
                    catch (Exception e)
                    {
                        throw e;
                    }
                }

                if (TempData["RegisterUpdates"] != null)
                {
                    ViewBag.Updates = (int)TempData["RegisterUpdates"];
                    TempData.Remove("RegisterUpdates");
                }

                if (TempData["RegisterInserts"] != null)
                {
                    ViewBag.Inserts = (int)TempData["RegisterInserts"];
                    TempData.Remove("RegisterInserts");
                }

				if (TempData["RegisterDuplicates"] != null)
				{
                    ViewBag.Duplicates = (int)TempData["RegisterDuplicates"];
                    TempData.Remove("RegisterDuplicates");
                }

                PersonService serviceP = new PersonService();
                DALE.Person person = serviceP.Get(user.Id);                
                ViewBag.FName = person.FName;
                ViewBag.LName = person.LName;
                ViewBag.Status = person.StatusName;
                ViewBag.Company = person.CompanyName;

                RemoveUser();

                return View(registers);
            }            
        }

        [HttpGet]
        public IActionResult GetRegisters(int sortOrder = 0, string searchString = null)
        {
            RegisterPlanningService service = new RegisterPlanningService();

            // Get registersPlanning from DAL and map them in POCO
            IEnumerable<RegisterPlanningVM> registersPlanning = service.GetAll()
                .Select(r => r.DalToRegisterPlanningVM())
                .OrderByDescending(r => r.TimeIn)
                .ToList();

            // if searchString exists
            if (searchString != null)
            {
                // apply filter to registerPlanning and registers
                registersPlanning = registersPlanning
                    .Where(r => r.FName.ToLower().Contains(searchString.ToLower()) || r.LName.ToLower().Contains(searchString.ToLower()) || r.CompanyName.ToLower().Contains(searchString.ToLower()));
                ViewData["nameFilter"] = searchString;
            }

            // Today DateTime
            DateTime today = DateTime.Now;

            // Initialise TimeSpan
            TimeSpan timeSpan = new TimeSpan(0, 0, 0);

            // if filter asks for all records, there is no linq operation done on the list
            if (sortOrder != 4)
            {
                switch (sortOrder)
                {
                    case 1: // yesterday
                        timeSpan = new TimeSpan(today.Hour + 24, today.Minute, 0);
                        registersPlanning = registersPlanning
                            .Where(r => r.TimeIn >= today.Subtract(timeSpan) && r.TimeIn.Day < today.Day)
                            .OrderByDescending(r => r.TimeIn)
                            .ToList();
                        break;

                    case 2: // this week
                        // Today day of the week
                        int toMonday = (int)today.DayOfWeek-1;
                        int Todaysmonth = (int)today.Month;                    

                        timeSpan = new TimeSpan(today.Hour + toMonday * 24, today.Minute, 0);
                        DateTime lastDayOfWeek = today.Subtract(timeSpan);                        

                        registersPlanning = registersPlanning
                            .Where(r => r.TimeIn >= today.Subtract(timeSpan) && (r.TimeIn.Month == Todaysmonth && r.TimeIn.Day <= lastDayOfWeek.Day+6))
                            .OrderByDescending(r => r.TimeIn)
                            .ToList();
                        break;

                    case 3: // this month

                        int daysInMonth = DateTime.DaysInMonth(today.Year, today.Month);
                        int toFirstDayOfMonth = today.Day;

                        timeSpan = new TimeSpan(today.Hour + toFirstDayOfMonth * 24, today.Minute, 0);
                        registersPlanning = registersPlanning
                            .Where(r => r.TimeIn >= today.Subtract(timeSpan) && r.TimeIn.Day <= daysInMonth)
                            .OrderByDescending(r => r.TimeIn)
                            .ToList();
                        break;

                    default: // today : today 
                        timeSpan = new TimeSpan(today.Hour, today.Minute, 0);
                        registersPlanning = registersPlanning
                            .Where(r => r.TimeIn >= today.Subtract(timeSpan) && r.TimeIn.Day <= today.Day)
                            .OrderByDescending(r => r.TimeIn)
                            .ToList();
                        break;
                }
            }
            return View(registersPlanning);
        }

        [HttpGet]
        public IActionResult Edit(int id)
        {
            RegisterPlanningService service = new RegisterPlanningService();
            RegisterPlanningVM register = service.Get(id).DalToRegisterPlanningVM();
            
            if (register == null)
            {
                ViewBag.ModalTitle = "Erreur";
                ViewBag.ModalMessage = "Ce registre n'existe pas.";
                return PartialView("_DisplayModal");
            }
            
            if (register.Planned == false && register.State != "Absent(e)")
            {
                ViewBag.ModalTitle = "Erreur";
                ViewBag.ModalMessage = "Vous ne pouvez pas modifier ce registre.";
                return PartialView("_DisplayModal");
            }

            FormRegisterPlanningVM model = new FormRegisterPlanningVM();
            model.Id = id;
            model.Time = new SetTime();
            model.Time.SameDayDate = register.TimeIn.ToString("yyyy-MM-dd");            
            model.Time.SameDayTimeIn = register.TimeIn.ToString("HH:mm");
            model.Time.SameDayTimeOut = register.TimeOut.HasValue ? register.TimeOut.Value.ToString("HH:mm") : "--:--";

            if (register.State == "Présente(e)")
            {
                ViewBag.State = true;
            }
            else 
            {
                ViewBag.State = false;
            }
            return PartialView("_EditRegisterPlanningModal", model);
        }

        [HttpPost]
        public IActionResult Edit(IFormCollection form)
        {
            FormRegisterPlanningVM register = new FormRegisterPlanningVM();

            // Get data from form and check for null inputs
            register = register.FormToSingleSetTime(form, out int inValid);

            if (register.Time == null)
            {
                ViewBag.Error = "Aucune plage horaire n'a été fournie.";
                return PartialView("_EditRegisterPlanningModal", register);
            }

            if (inValid > 0)
            {
                ViewBag.Error = "Veuillez remplir les dates et heures correctement.";
                return PartialView("_EditRegisterPlanningModal", register);
            }

            // Check Time integrity (no duplicate, timeIn < timeOut)
            string dateIn = register.Time.SameDayDate + " " + register.Time.SameDayTimeIn + ":00";
            DateTime dateTimeIn = DateTime.Parse(dateIn);

            string dateOut = register.Time.SameDayDate + " " + register.Time.SameDayTimeOut + ":00";
            DateTime dateTimeOut = DateTime.Parse(dateOut);

			if (dateTimeIn > dateTimeOut)
			{
                ViewBag.Error = "L'heure d'entrée doit être inférieure à l'heure de sortie.";
                return PartialView("_EditRegisterPlanningModal", register);
            }

            // Prepare register for update         
            int registerId = int.Parse(form["Id"]);

            RegisterPlanningService service = new RegisterPlanningService();
            DALE.RegisterPlanning updateRegister = service.Get(registerId);

			if (updateRegister == null)
			{
                ViewBag.Error = "Le registre que vous souhaitez éditer n'existe pas.";
                return PartialView("_EditRegisterPlanningModal", register);
            }

            int AdministratorId = int.Parse(HttpContext.User.FindFirst("Id").Value);
            bool isPresent = Convert.ToBoolean(form["State"].ToString().Split(',')[0]);            

            if (isPresent == true)
            {
                updateRegister.State = "Présente(e)";
                updateRegister.Mistake = "Check In & Out";
            }

            updateRegister.Id = registerId;
            updateRegister.AdministratorId = AdministratorId;
            updateRegister.TimeIn = dateTimeIn;
            updateRegister.TimeOut = dateTimeOut;

			try
			{
                service.Update(updateRegister);
			}
			catch (Exception e)
			{
				throw e;
			}

            RegisterPlanningVM updated = updateRegister.DalToRegisterPlanningVM();
            return PartialView("_RegisterPlanningItem", updated);
        }

        [HttpGet]
        public IActionResult SetAsPlanned(int id)
        {
            RegisterPlanningService service = new RegisterPlanningService(); 
            DALE.RegisterPlanning updateRegister = service.Get(id);

            if (updateRegister == null)
            {
                ViewBag.ModalTitle = "Erreur";
                ViewBag.ModalMessage = "Le registre que vous souhaitez éditer n'existe pas.";
                return PartialView("_DisplayModal");
            }

			try
			{
                service.SetPlanned(id);
                updateRegister.Planned = true;
            }
			catch (Exception e)
			{
				throw e;
			}

            RegisterPlanningVM updated = updateRegister.DalToRegisterPlanningVM();
            return PartialView("_RegisterPlanningItem", updated);
        }
        
        [NonAction]
        private User GetUser()
        {
            var session = HttpContext.Session.GetComplexData<User>("user");

            if (session == null)
            {
                User user = new User();
                HttpContext.Session.SetComplexData("user", user);
            }
            return (User)HttpContext.Session.GetComplexData<User>("user");
        }

        [NonAction]
        private void RemoveUser()
        {
            HttpContext.Session.Clear();
        }

        //[NonAction]
        //private SetPerson SetPersonToView(User user)
        //{
        //    PersonService serv = new PersonService();
        //    SetPerson person = new SetPerson();

        //    if (user.Id == 0)
        //    {
        //        person.Id = null;
        //    }
        //    else
        //    {
        //        person.Id = user.Id;
        //    }

        //    List<PersonListVM> personList = new List<PersonListVM>();

        //    if (user.DepartmentId > 0)
        //    {
        //        personList = serv.GetAll((int)user.CompanyId, user.DepartmentId).Select(p => p.ToAppPersonListVM()).ToList();
        //    }
        //    else
        //    {
        //        personList = serv.GetAll((int)user.CompanyId).Select(p => p.ToAppPersonListVM()).ToList();
        //    }

        //    // If the company has recorded employees, show a list
        //    if (personList.Count() > 0)
        //    {
        //        person.Persons = personList;

        //        // Add "Autre" to Dropdown list
        //        PersonListVM personOther = new PersonListVM { Id = 0, LName = null, FName = null };
        //        person.Persons.Insert(0, personOther);
        //    }
        //    else
        //    {
        //        person.Persons = null;
        //        // Force inputs (name and lastname) to be provided
        //        //person.Id = 0;
        //    }

        //    // If the person is a visitor, the visted person is required
        //    if (user.StatusId == 3)
        //    {
        //        // Populate the list of person for the user to find the name of the person he wants to visit
        //        // The parameter 1 of the underneath method correspond to companyId = 1 (SNCB) && departmentId = 2 (Arlon)
        //        List<PersonListVM> personToVist = serv.GetAll(1, 2).Select(p => p.ToAppPersonListVM()).ToList();

        //        if (personToVist.Count() > 0)
        //        {
        //            person.PersonsVisited = personToVist;
        //            person.PersonVisitedId = user.PersonVisited;
        //        }
        //        else
        //        {
        //            person.PersonsVisited = null;
        //        }
        //    }
        //    return person;
        //}

        //[NonAction]
        //private Person CheckIfPersonExistInACompany(User user, SetPerson setPerson)
        //{
        //    PersonService servP = new PersonService();

        //    IEnumerable<Person> people = servP.GetAll(setPerson.LName, setPerson.FName).Select(p => p.DalToPersonApp()).ToList();

        //    // If one or serveral person match
        //    // check if one person is in the same company and department

        //    Person person = null;

        //    if (people.Count() > 0)
        //    {
        //        foreach (var i in people)
        //        {
        //            if (user.DepartmentId > 0)
        //            {
        //                if (i.CompanyId == user.CompanyId && i.DepartmentId == user.DepartmentId)
        //                {
        //                    person = i;
        //                }
        //            }
        //            else
        //            {
        //                if (i.CompanyId == user.CompanyId)
        //                {
        //                    person = i;
        //                }
        //            }
        //        }
        //    }
        //    return person;
        //}


        [NonAction]
        private string FormTimeValid(IFormCollection form, params string[] keys)
        {
            string result = null;
            string value;

            for (int i = 0; i < keys.Length; i++)
            {
                value = form[keys[i]];

                if (value == "")
                {
                    result += i.ToString();
                }
            }
            return result;
        }

        [NonAction]
        private string FormTimeValidViewBag(string validation, int formType)
        {
            string viewbag = "";

            if (formType == 1)
            {
                switch (validation)
                {
                    case "0":
                        viewbag = "Veuillez spécifier une date.";
                        break;
                    case "01":
                        viewbag = "Veuillez spécifier une date et une heure d'arrivée";
                        break;
                    case "012":
                        viewbag = "Veuillez spécifier une date, une heure d'arrivée et de départ";
                        break;
                    case "12":
                        viewbag = "Veuillez spécifier une heure d'arrivée et de départ";
                        break;
                    case "1":
                        viewbag = "Veuillez spécifier une heure d'arrivée";
                        break;
                    case "2":
                        viewbag = "Veuillez spécifier une heure de départ";
                        break;
                }
            }
            else if (formType == 2)
            {
                switch (validation)
                {
                    case "0":
                        viewbag = "Veuillez spécifier la date et l'heure de début.";
                        break;
                    case "1":
                        viewbag = "Veuillez spécifier la date et l'heure de fin.";
                        break;
                    case "01":
                        viewbag = "Veuillez spécifier les dates et les heures de début et de fin.";
                        break;
                }
            }
            return viewbag;
        }

        [NonAction]
        private DateTime SameDayToDateTime(IFormCollection form, string dateKey, string hourKey)
        {
            string date = form[dateKey] + " " + form[hourKey] + ":00";
            DateTime dateTime = DateTime.Parse(date);
            return dateTime;
        }

        [NonAction]
        private string TimeInTimeOutIntegrity(DateTime timeIn, DateTime timeOut)
        {
            string result = null;

            if (timeIn >= timeOut)
            {
                result = "L'horaire de début doit être inférieur à l'horaire de fin.";
            }
            return result;
        }

        //[NonAction]
        //private SetTime FormToSetTime(IFormCollection form)
        //{
        //    SetTime setTime = new SetTime();
        //    setTime.PlanningTypeId = int.Parse(form["PlanningTypeId"]);
        //    setTime.SameDayDate = form["SameDayDate"];
        //    setTime.SameDayTimeIn = form["SameDayTimeIn"];
        //    setTime.SameDayTimeOut = form["SameDayTimeOut"];
        //    return setTime;
        //}
    }
}
