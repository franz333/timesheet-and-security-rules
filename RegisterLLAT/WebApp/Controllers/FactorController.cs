﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL.Services;
using Microsoft.AspNetCore.Mvc;
using WebApp.AppTools;
using WebApp.Models;
using WebApp.Models.RegisterViewModels;

namespace WebApp.Controllers
{
	public class FactorController : Controller
	{
		public IActionResult Index()
		{
			return View();
		}

        [NonAction]
        private SetCompany ReturnSetCompanyToView(SetCompany form)
        {
            SetCompany company = form;

            CompanyService servC = new CompanyService();
            DepartmentService servD = new DepartmentService();

            // Get all companies and departments
            List<Company> companies = servC.GetAll().Select(c => c.DalToCompanyApp()).ToList();
            List<Department> departments = servD.GetAll().Select(d => d.DalToDepartmentApp()).ToList();

            // If the user is an intern, display only departments
            // otherwise both departments and companies
            if (form.CompanyStatusId != 1)
            {
                company.Companies = companies
                    .Where(c => c.CompanyStatusId == form.CompanyStatusId)
                    .ToList();

                company.Companies.Insert(0, new Company { Id = 0, CompanyName = "Autre" });
            }

            // if the company exists and have a department list
            int companyExists = companies.Where(d => d.Id == form.Id).Count();

            if (companyExists > 0)
            {
                int departmentExists = departments
                    .Where(d => d.CompanyId == form.Id).Count();

                if (departmentExists > 0)
                {
                    company.Departments = departments
                        .Where(d => d.CompanyId == form.Id)
                        .ToList();
                    company.Departments.Insert(0, new Department { Id = 0, Name = "Autre" });
                }
            }
            return company;
        }

        //[HttpGet]
        //public IActionResult AddRegisterPlanningCompany()
        //{
        //    User user = GetUser();

        //    // Populate company with session data
        //    SetCompany company = UserToCompany(user);

        //    company = SetCompanyToView(company);

        //    //// If the user is part of SNCB
        //    //if (company.CompanyStatusId == 1)
        //    //{
        //    //    company.Departments = SetCompanyDepartments((int)company.Id);
        //    //}
        //    //else
        //    //{
        //    //    company.Companies = SetCompanyCompanies(company.CompanyStatusId);

        //    //    if (company.Companies != null)
        //    //    {
        //    //        // For navigation (Next & Previous) purposes
        //    //        if (company.DepartmentId > 0)
        //    //        {
        //    //            company.Departments = SetCompanyDepartments((int)company.Id);
        //    //        }
        //    //    }
        //    //    else
        //    //    {
        //    //        // If no dropdown list with companies is provided, force user to enter the company's name
        //    //        company.Id = 0;
        //    //    }
        //    //}
        //    return View(company);
        //}
    }
}
