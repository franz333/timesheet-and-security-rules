﻿using System;
using System.Collections.Generic;
using System.Linq;
using DAL.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using WebApp.AppTools;
using WebApp.Models;
using WebApp.Models.RegisterViewModels;
using Dal = DAL.Entities;

//namespace WebApp.Controllers
//{
//    public class RegisterController : Controller
//    {
//        #region Register Wizard Form

//        // GET : Sign in or Sign out
//        public IActionResult Index()
//        {
//            // TODO : Test the browser detection meth
//            // User Agent to detect the client's browser
//            //Request.Headers["User-Agent"].ToString();
//            return View("Index");
//        }

//        // GET: Register/SetStatus
//        [HttpGet]
//        public IActionResult SetStatus(bool isCheckingOut)
//        {
//            SetStatus status = new SetStatus();

//            User user = GetUser();

//            user.isCheckinOut = isCheckingOut;
//            HttpContext.Session.SetComplexData("user", user);
//            TempData["IsUserCheckinOut"] = isCheckingOut;

//            CompanyStatusService serv = new CompanyStatusService();
//            status.CompanyStatus = serv.GetAll().Select(cs => cs.ToApp()).ToList();

//            return View(status);
//        }

//        // POST: Register/SetStatus
//        [HttpPost]
//        public IActionResult SetStatus(SetStatus status)
//        {
//            CompanyStatusService serv = new CompanyStatusService();

//            if (!ModelState.IsValid)
//            {
//                // TODO : Log error                
//                status.CompanyStatus = serv.GetAll().Select(cs => cs.ToApp()).ToList();
//                ViewBag.Error = "ce status n'existe pas";
//            }
//            else
//            {
//                Dal.CompanyStatus C = serv.Get(status.Id);

//                // Check whether the status exist in the DB
//                if (C != null)
//                {
//                    User user = GetUser();
//                    user.StatusId = status.Id;
//                    HttpContext.Session.SetComplexData("user", user);

//                    return RedirectToAction("SetCompany", "Register");
//                }
//                else
//                {
//                    status.CompanyStatus = serv.GetAll().Select(cs => cs.ToApp()).ToList();
//                }
//            }
//            return View(status);
//        }

//        // GET: Register/SetCompany
//        [HttpGet]
//        public IActionResult SetCompany()
//        {
//            // Get the user from the session
//            User user = GetUser();

//            SetCompany company = new SetCompany();

//            // Set companyStatusId for comparison with autocomplete values
//            company.CompanyStatusId = user.StatusId;

//            // Get companies by status 
//            CompanyService serv = new CompanyService();
//            company.Companies = serv.GetAllCompanyByStatusWithoutDep(user.StatusId).Select(c => c.ToApp()).ToList();

//            // Provide the company's dropdownlist
//            if (company.Companies.Count() > 0)
//            {
//                // Add "Autre" in the dropdownlist
//                Company other = new Company { Id = 0, CompanyName = "Autre" };
//                company.Companies.Insert(0, other);

//                // Populate the dropdown with the Id and the Company's name
//                company.Options = company.Companies.Select(a => new SelectListItem
//                {
//                    Value = a.Id.ToString(),
//                    Text = a.CompanyName
//                }).ToList();
//            }
//            else
//            {
//                // If no dropdown list with companies is provided, force user to enter the company's name
//                company.Id = 0;
//            }
//            return View(company);
//        }

//        // POST: Register/SetCompany
//        [HttpPost]
//        [ValidateAntiForgeryToken]
//        public IActionResult SetCompany(SetCompany company)
//        {
//            // Get the user from the session
//            User user = GetUser();
//            CompanyService servC = new CompanyService();
//            DepartmentService servD = new DepartmentService();

//            if (!ModelState.IsValid)
//            {
//                // Get companies by status 
//                company.Companies = servC.GetAllCompanyByStatusWithoutDep(user.StatusId).Select(c => c.ToApp()).ToList();

//                if (company.Companies.Count() > 0)
//                {
//                    // Add "Autre" in the dropdownlist
//                    Company other = new Company { Id = 0, CompanyName = "Autre" };
//                    company.Companies.Insert(0, other);

//                    company.Options = company.Companies.Select(a => new SelectListItem
//                    {
//                        Value = a.Id.ToString(),
//                        Text = a.CompanyName
//                    }).ToList();
//                }
//                return View(company);
//            }
//            else
//            {
//                // Check if the field IdAutoComplete exists..if so this value is given to companyId
//                if (company.IdAutoComplete > 0)
//                {
//                    company.Id = company.IdAutoComplete;
//                }

//                // A company is selected from the list
//                if (company.Id > 0)
//                {
//                    // In case the user makes use of the input field to type in the company name
//                    // The user can then pick up a company from the autocomplete list
//                    // We need to ...

//                    // ... verify the user.StatusId equals the CompanyStatusId
//                    // the suggestions of the autocomplete display all compagnies regardless of companyStatus
//                    if (company.CompanyStatusId != user.StatusId)
//                    {
//                        user.StatusId = company.CompanyStatusId;
//                    }

//                    // Check whether the companyId exists in the DB and corresponds to the status                   
//                    Company companyCheck = servC.Get((int)company.Id, user.StatusId).ToApp();

//                    if (companyCheck == null)
//                    {
//                        // TODO : log the error
//                        ViewBag.Error = "Cette société n'existe pas.";
//                        SetCompany companyError = ReturnCompanyError(user.StatusId);
//                        return View(companyError);
//                    }
//                    else
//                    {
//                        // Set the user session's companyId
//                        user.CompanyId = (int)company.Id;

//                        // Check whether the company requires an associated department
//                        int hasDepartment = servD.GetAllDepartmentByCompany((int)company.Id).Count();

//                        // A department is needed
//                        if (hasDepartment >= 1)
//                        {
//                            // A department is selected from the list
//                            if (company.DepartmentId > 0)
//                            {
//                                // Check whether the department exists in the DB
//                                Department departmentCheck = servD.Get((int)company.DepartmentId, (int)company.Id).ToApp();

//                                if (departmentCheck != null)
//                                {
//                                    // Avoid that a person selects SNCB and Atelier de traction d'Arlon
//                                    if (company.Id == 1 & company.DepartmentId == 2)
//                                    {
//                                        SetCompany companyError = ReturnCompanyError(user.StatusId);
//                                        ViewBag.Error = "Le département de cette société n'est pas correct.";
//                                        return View(companyError);
//                                    }
//                                    else
//                                    {
//                                        user.DepartmentId = company.DepartmentId;
//                                    }
//                                }
//                                else
//                                {
//                                    SetCompany companyError = ReturnCompanyError(user.StatusId);
//                                    ViewBag.Error = "Ce département n'existe pas, veuillez recommencer.";
//                                    return View(companyError);
//                                }
//                            }
//                            // User typed the name of the department with input text
//                            else if (company.DepartmentId == 0)
//                            {
//                                // Check whether the input is valid
//                                if (String.IsNullOrWhiteSpace(company.DepartmentName))
//                                {
//                                    ViewBag.Error = "Le nom du département n'est pas correct, veuillez recommencer.";

//                                    SetCompany companyError = ReturnCompanyError(user.StatusId);
//                                    return View(companyError);
//                                }
//                                else
//                                {
//                                    // Check whether the department is not already in DB
//                                    Dal.Department departmentExist = servD.Get((int)company.Id, company.DepartmentName);
//                                    if (departmentExist != null)
//                                    {
//                                        company.DepartmentId = departmentExist.Id;
//                                        company.DepartmentName = departmentExist.DepartmentName;
//                                        user.DepartmentId = departmentExist.Id;
//                                    }
//                                }
//                                user.DepartmentName = company.DepartmentName;
//                                // TODO : Save the new department
//                            }
//                            else
//                            {
//                                // The user chose a compagny from the autocomplete and a department must be provided
//                                if (company.IdAutoComplete > 0)
//                                {
//                                    ViewBag.Error = "Un département doit être renseigné pour cette société, veuillez recommencer.";
//                                }
//                                else
//                                {
//                                    ViewBag.Error = "Un département valide doit être renseigné, veuillez recommencer.";
//                                }
//                                SetCompany companyError = ReturnCompanyError(user.StatusId);
//                                return View(companyError);
//                            }
//                        }
//                        else
//                        {
//                            if (company.DepartmentId != null)
//                            {
//                                ViewBag.Error = "Aucun département n'est nécessaire.";
//                                SetCompany companyError = new SetCompany();
//                                companyError.Companies = servC.GetAllCompanyByStatusWithoutDep(user.StatusId).Select(c => c.ToApp()).ToList();

//                                companyError.Options = new List<SelectListItem>();

//                                foreach (var item in companyError.Companies)
//                                {
//                                    if (item.Id == company.Id)
//                                    {
//                                        companyError.Options.Add(new SelectListItem
//                                        {
//                                            Value = item.Id.ToString(),
//                                            Text = item.CompanyName,
//                                            Selected = true
//                                        });
//                                    }
//                                    else
//                                    {
//                                        companyError.Options.Add(new SelectListItem
//                                        {
//                                            Value = item.Id.ToString(),
//                                            Text = item.CompanyName
//                                        });
//                                    }
//                                }
//                                return View(companyError);
//                            }
//                            else
//                            {
//                                user.DepartmentId = company.DepartmentId;
//                            }
//                        }
//                    }
//                }
//                // The user typed the name of the company with input text
//                else if (company.Id == 0)
//                {
//                    // Check whether the input is not null or whitespace
//                    if (String.IsNullOrWhiteSpace(company.Name))
//                    {
//                        ViewBag.Error = "Le nom de votre société doit être renseigné, veuillez recommencer.";
//                        SetCompany companyError = ReturnCompanyError(user.StatusId);
//                        return View(companyError);
//                    }

//                    // Check whether the company's name is already in DB
//                    Dal.Company companyCheck = servC.Get(company.Name).FirstOrDefault();

//                    // If the company is found in the DB
//                    if (companyCheck != null)
//                    {
//                        int hasDepartment = servD.GetAllDepartmentByCompany((int)company.Id).Count();

//                        if (hasDepartment > 0)
//                        {
//                            ViewBag.Error = "Un département doit être renseigné pour cette société, veuillez recommencer.";
//                            SetCompany companyError = ReturnCompanyError(user.StatusId);
//                            return View(companyError);
//                        }
//                        else
//                        {
//                            // We set the company's values to the session
//                            user.CompanyId = companyCheck.Id;
//                            user.CompanyName = companyCheck.CompanyName;
//                            user.StatusId = companyCheck.CompanyStatusId;
//                        }
//                    }
//                    else
//                    {
//                        // If the name exists already we just assign the value
//                        user.CompanyName = companyCheck.CompanyName;

//                        // #TODO tell the user about it and let him decide what do to...
//                        // The company could belong to another status that the one selected by the user
//                    }
//                }
//                else
//                {
//                    ViewBag.Error = "Le nom de votre société n'est pas correct.";
//                    SetCompany companyError = ReturnCompanyError(user.StatusId);
//                    return View(companyError);
//                }
//                // Everything successed. Register the user session and redirect to the next form of the wizard
//                HttpContext.Session.SetComplexData("user", user);

//                // If the user is checking out
//                if (user.isCheckinOut)
//                {
//                    return RedirectToAction("SearchRegister", "Register");
//                }
//                return RedirectToAction("SetPerson", "Register");
//            }
//        }

//        // GET : Register/SetPerson
//        [HttpGet]
//        public IActionResult SetPerson()
//        {
//            // Get the user from the session
//            User user = GetUser();

//            SetPerson person = new SetPerson();

//            PersonService serv = new PersonService();

//            // Check whether a person list can be retrieved or not. There won't be any person list...
//            // ... if the company was added by the user
//            // ... if the company has no recorded person 

//            // If the person used the input to provide the company's name
//            if (user.CompanyId == 0)
//            {
//                person.Id = 0;
//            }
//            // Populate the list of persons for the user to find its name.
//            else if (user.CompanyId > 0)
//            {
//                // The person is linked to a departement
//                if (user.DepartmentId != null)
//                {
//                    person.Persons = serv.GetAll(user.CompanyId, user.DepartmentId).Select(p => p.ToAppPersonListVM()).ToList();
//                }
//                // The person is not linked to any department.
//                else
//                {
//                    person.Persons = serv.GetAll(user.CompanyId).Select(p => p.ToAppPersonListVM()).ToList();
//                }

//                // If there is a list of person, populate the options otherwise set person.Id = 0
//                if (person.Persons.Count() > 0)
//                {
//                    // Add "Autre" to Dropdown list
//                    PersonListVM personOther = new PersonListVM { Id = 0, LName = null, FName = null };
//                    person.Persons.Insert(0, personOther);

//                    person.OptionsPersons = person.Persons.Select(a => new SelectListItem
//                    {
//                        Value = a.Id.ToString(),
//                        Text = a.Name
//                    }).ToList();
//                }
//                // If the list is empty we set person.Id = 0 to display input fields
//                else
//                {
//                    person.Id = 0;
//                }
//            }

//            // If the person is a visitor, the visted person is required
//            if (user.StatusId == 3)
//            {
//                // Populate the list of person for the user to find the name of the person he wants to visit
//                // The parameter 1 of the underneath method correspond to companyId = 1 (SNCB) && departmentId = 2 (Arlon)
//                person.PersonsVisited = serv.GetAll(1, 2).Select(p => p.ToAppPersonListVM()).ToList();

//                person.OptionsPVisited = person.PersonsVisited.Select(a => new SelectListItem
//                {
//                    Value = a.Id.ToString(),
//                    Text = a.Name
//                }).ToList();
//            }
//            return View(person);
//        }

//        // POST : Register/SetPerson
//        [HttpPost]
//        [ValidateAntiForgeryToken]
//        public IActionResult SetPerson(SetPerson person)
//        {
//            // Get the user from the session
//            User user = GetUser();

//            #region Save session

//            PersonService servP = new PersonService();

//            Dal.Person personCheck = new Dal.Person();
//            SetPerson setPersonError = null;

//            // If the person's id is provided (from dropdown list) check that the person exists in the DB 
//            if (person.Id > 0)
//            {
//                // The company has a department
//                if (user.CompanyId > 0 && user.DepartmentId > 0)
//                {
//                    personCheck = servP.Get((int)person.Id, user.CompanyId, (int)user.DepartmentId);
//                }
//                else
//                {
//                    personCheck = servP.Get((int)person.Id, user.CompanyId);
//                }

//                // If the person does not exist
//                if (personCheck == null)
//                {
//                    ViewBag.Error = "Cette personne n'existe pas.";
//                    setPersonError = new SetPerson() { PersonVisitedId = 404 };
//                }
//                else
//                {
//                    user.Id = (int)person.Id;
//                }

//            }
//            // If data come from the inputs
//            else if (person.Id == 0)
//            {
//                if (String.IsNullOrWhiteSpace(person.LName) ^ String.IsNullOrWhiteSpace(person.FName))
//                {
//                    ViewBag.NameError = "Veuillez indiquer vos nom et prénom.";
//                }
//                else
//                {
//                    // Prevent duplicate in the DB, search for by LName & FName or FName & LName
//                    Person person2 = CheckIfPersonExistInACompany(user, person);

//                    // If we find a duplicate alert the user
//                    if (person2 != null)
//                    {
//                        if (user.DepartmentId > 0)
//                        {
//                            user.DepartmentId = person2.DepartmentId;
//                        }
//                        user.Id = person2.CompanyId;
//                        user.FName = person2.FName;
//                        user.LName = person2.LName;
//                    }
//                    else
//                    {
//                        user.Id = 0;
//                        user.LName = person.LName;
//                        user.FName = person.FName;
//                    }
//                }
//            }
//            else
//            {
//                ViewBag.Error = "Cette personne n'est pas correcte.";
//            }

//            // if the person is a visitor, the visited person is required
//            if (user.StatusId == 3)
//            {
//                if (person.PersonVisitedId > 0)
//                {
//                    Dal.Person personVisited = servP.GetPersonVisited((int)person.PersonVisitedId);

//                    // Check that visited person exists in DB
//                    if (personVisited != null)
//                    {
//                        user.PersonVisited = person.PersonVisitedId;
//                    }
//                    else
//                    {
//                        ViewBag.VisitedError = "La personne visitée n'existe pas.";
//                        setPersonError = new SetPerson() { PersonVisitedId = 404 };
//                    }
//                }
//                else
//                {
//                    ViewBag.VisitedError = "La personne visitée n'existe pas.";
//                    setPersonError = new SetPerson() { PersonVisitedId = 404 };
//                }
//            }

//            // If an error occured 
//            if (setPersonError != null)
//            {
//                // Determine which dropdown list display on Error Post Back
//                if (user.CompanyId > 0 && user.DepartmentId > 0)
//                {
//                    setPersonError = ReturnPersonError(user.CompanyId, (int)user.DepartmentId, user.Id);
//                }
//                else
//                {
//                    setPersonError = ReturnPersonError(user.CompanyId, user.Id);
//                }

//                // Prepare dropdown list
//                setPersonError.PersonsVisited = servP.GetPersonList(1).Select(p => p.ToAppPersonListVM()).ToList();

//                // If the person visited was not valid
//                if (setPersonError.PersonVisitedId == 404)
//                {
//                    setPersonError.OptionsPVisited = setPersonError.PersonsVisited.Select(a => new SelectListItem
//                    {
//                        Value = a.Id.ToString(),
//                        Text = a.Name
//                    }).ToList();
//                }
//                else
//                {
//                    setPersonError.OptionsPVisited = new List<SelectListItem>();

//                    foreach (var item in setPersonError.PersonsVisited)
//                    {
//                        if (item.Id == user.PersonVisited)
//                        {
//                            setPersonError.OptionsPVisited.Add(new SelectListItem
//                            {
//                                Value = item.Id.ToString(),
//                                Text = item.Name,
//                                Selected = true
//                            });
//                        }
//                        else
//                        {
//                            setPersonError.OptionsPVisited.Add(new SelectListItem
//                            {
//                                Value = item.Id.ToString(),
//                                Text = item.Name,
//                            });
//                        }
//                    }
//                }
//                return View(setPersonError);
//            }
//            else
//            {
//                // Save the data into the session
//                HttpContext.Session.SetComplexData("user", user);
//            }
//            #endregion -- End Save Session


//            #region Save Register                

//            Dal.Person person1 = new Dal.Person();

//            // If the company is selected from registered companies            
//            if (user.CompanyId > 0)
//            {
//                person1.CompanyId = user.CompanyId;

//                // If the department is selected from registered departments
//                if (user.DepartmentId > 0)
//                {
//                    person1.DepartmentId = (int)user.DepartmentId;
//                }
//                // No department is selected
//                else
//                {
//                    // Check if department name is provided
//                    if (user.DepartmentName != null)
//                    {
//                        DepartmentService departmentS = new DepartmentService();

//                        // Register a Dal.Department as no specific logic is implied
//                        // TODO if time : implement a string format in Sql 
//                        Dal.Department d = new Dal.Department();
//                        d.CompanyId = user.CompanyId;
//                        d.DepartmentName = user.DepartmentName;
//                        d.Id = departmentS.Insert(d);
//                        user.DepartmentId = d.Id;
//                        person1.DepartmentId = d.Id;
//                    }
//                }
//            }
//            // if the person's company data comes from the input fields then a new company is created
//            else
//            {
//                CompanyService companyServ = new CompanyService();

//                Dal.Company company = new Dal.Company();
//                company.CompanyName = user.CompanyName;
//                company.CompanyStatusId = user.StatusId;
//                company.Id = companyServ.Insert(company);
//                user.CompanyId = company.Id;
//                person1.CompanyId = company.Id;
//            }

//            // Person 
//            if (user.Id > 0)
//            {
//                person1.Id = user.Id;
//            }
//            else
//            {
//                PersonService personService = new PersonService();
//                person1.LName = user.LName;
//                person1.FName = user.FName;
//                person1.AdministratorId = 1;
//                person1.CompanyId = user.CompanyId;
//                person1.DepartmentId = user.DepartmentId;
//                person1.Id = personService.Insert(person1);
//                user.Id = person1.Id;
//            }

//            // Register the user session into DB 
//            RegisterService registerServ = new RegisterService();
//            Register register = new Register();
//            register.PersonId = person1.Id;
//            register.TimeIn = DateTime.Now;

//            // The user is checkin out
//            if (user.isCheckinOut == true)
//            {
//                register.TimeOut = DateTime.Now;
//            }

//            if (user.PersonVisited > 0)
//            {
//                register.PersonVisited = user.PersonVisited;
//            }
//            registerServ.Insert(register.ToDal());

//            TempData.Remove("IsUserCheckinOut");

//            return RedirectToAction("Index", "Register");
//            #endregion
//        }

//        [HttpPost]
//        // GET: Ajax get the departements of the company
//        public JsonResult GetDepartmentsByCompany(int companyId)
//        {
//            DepartmentService serv = new DepartmentService();
//            List<Department> departments = new List<Department>();
//            Department other = new Department
//            {
//                Id = 0,
//                Name = "Autre",
//            };

//            // If Company is SNCB, provide departments without Arlon
//            if (companyId == 1)
//            {
//                departments = serv.GetAllDepartmentByCompany(companyId).Where(s => s.Id != 2).Select(d => d.ToApp()).ToList();
//                departments.Insert(0, other);
//            }
//            else
//            {
//                departments = serv.GetAllDepartmentByCompany(companyId).Select(d => d.ToApp()).ToList();
//                if (departments.Count() > 0)
//                {
//                    departments.Insert(0, other);
//                }
//            }

//            //if (departments.Count() == 1 & departments[0].Id == 0 )
//            //{
//            //    departments = null;
//            //}
//            return Json(departments);
//        }

//        [HttpPost]
//        public JsonResult GetCompaniesByStatus(int statusId)
//        {
//            // Get companies by status 
//            CompanyService serv = new CompanyService();

//            List<Company> companies = serv.GetAllCompanyByStatusWithoutDep(statusId).Select(c => c.ToApp()).ToList();

//            // Provide the company's dropdownlist
//            if (companies.Count() > 0)
//            {
//                // Add "Autre" in the dropdownlist
//                Company other = new Company { Id = 0, CompanyName = "Autre" };
//                companies.Insert(0, other);
//            }
//            else
//            {
//                companies = null;
//            }

//            return Json(companies);
//        }

//        [HttpPost]
//        public JsonResult AutoComplete(string Prefix)
//        {
//            CompanyService service = new CompanyService();
//            List<Dal.Company> companies = new List<Dal.Company>();

//            // Get the list of all companies
//            companies = service.GetAllAutoComplete().Select(c => c).ToList();

//            //Searching records from list using LINQ query  
//            var List = (from N in companies
//                        where N.CompanyName.Contains(Prefix, StringComparison.CurrentCultureIgnoreCase)
//                        select new { N.Id, N.CompanyName, N.CompanyStatusId });

//            return Json(List);
//        }

//        [HttpGet]
//        public IActionResult SearchRegister()
//        {
//            // We assume the user has actually checked-in
//            User user = GetUser();
//            user.hasActuallyCheckedIn = true;
//            HttpContext.Session.SetComplexData("user", user);

//            ViewBag.RegisterError = null;

//            RegisterService serv = new RegisterService();
//            SetPerson person = new SetPerson();

//            // Get persons checked in for the current day
//            List<Register> registers = serv.GetAllCurrentDay()
//                .Where(p => p.TimeOut == null && p.TimeIn != null && p.CompanyId == user.CompanyId)
//                .Select(p => p.ToApp())
//                .ToList();

//            // if a list of checked in people exist prepare dropdown
//            if (registers.Count() != 0)
//            {
//                foreach (var item in registers)
//                {
//                    person.Persons = new List<PersonListVM>();
//                    person.Persons.Add(new PersonListVM()
//                    {
//                        Id = item.PersonId,
//                        FName = item.FName,
//                        LName = item.LName
//                    });
//                }

//                person.OptionsPersons = person.Persons.Select(a => new SelectListItem
//                {
//                    Value = a.Id.ToString(),
//                    Text = a.Name
//                }).ToList();
//            }
//            // if no checked in people are found, prepare dropdown with employees by company
//            else
//            {
//                user.hasActuallyCheckedIn = false;
//                HttpContext.Session.SetComplexData("user", user);

//                ViewBag.RegisterError = "Aucune personne de votre entreprise n'est présente dans le registre aujourd'hui. Vous avez dû oublié de signer en arrivant...";

//                // Prepare dropdownList for employees'name of the user's company
//                PersonService personServ = new PersonService();
//                person.Persons = new List<PersonListVM>();

//                IEnumerable<Dal.Person> persons = personServ.GetPersonsByCompany(user.CompanyId);

//                if (persons.Count() > 0)
//                {
//                    person.Persons = persons.Select(p => new PersonListVM()
//                    {
//                        Id = p.Id,
//                        FName = p.FName,
//                        LName = p.LName
//                    }).ToList();

//                    person.OptionsPersons = person.Persons.Select(a => new SelectListItem
//                    {
//                        Value = a.Id.ToString(),
//                        Text = a.Name
//                    }).ToList();
//                }
//                else
//                {
//                    // We should never reach this as there should always be records
//                    // During preceding steps, the user can add his/her company if it does not exist in DB
//                    return RedirectToAction("Index", "Error");
//                }

//                // If the person is a visitor, the visted person is required
//                if (user.StatusId == 3)
//                {
//                    // Populate the list of person for the user to find the name of the person he wants to visit
//                    // The parameter 1 of the underneath method correspond to companyId = 1 (SNCB) && departmentId = 2 (Arlon)
//                    person.PersonsVisited = personServ.GetAll(1, 2).Select(p => p.ToAppPersonListVM()).ToList();

//                    person.OptionsPVisited = person.PersonsVisited.Select(a => new SelectListItem
//                    {
//                        Value = a.Id.ToString(),
//                        Text = a.Name
//                    }).ToList();
//                }

//            }
//            return View(person);
//        }

//        [HttpPost]
//        public IActionResult SearchRegister(SetPerson person)
//        {
//            // Client side validation
//            if (!ModelState.IsValid)
//            {
//                return View(person);
//            }

//            // Prepare registration
//            RegisterService registerServ = new RegisterService();
//            Register register = new Register();

//            ViewBag.Error = null;
//            ViewBag.RegisterError = null;

//            User user = GetUser();

//            if (person.Id < 0)
//            {
//                ViewBag.Error = "La personne sélectionnée n'est pas correcte. Veuillez recommencer...";
//                return View(person);
//            }

//            // the user found its name in the list
//            if (person.Id > 0 && user.hasActuallyCheckedIn == true)
//            {
//                // save session just in case
//                user.Id = (int)person.Id;

//                var check = registerServ.GetAllCurrentDay();

//                // Check server side if that user is well in DB for today's register
//                var isInDB = registerServ.GetAllCurrentDay()
//                    .Where(u => u.PersonId == person.Id && u.TimeIn != null)
//                    .FirstOrDefault();

//                if (isInDB != null)
//                {
//                    register.PersonId = (int)person.Id;
//                    register.TimeOut = DateTime.Now;
//                    registerServ.CheckOut(register.ToDal());
//                }
//            }
//            // the user has not checked in beforehand
//            else
//            {
//                user.hasActuallyCheckedIn = false;
//                return SetPerson(person);
//            }

//            TempData.Remove("IsUserCheckinOut");

//            return RedirectToAction("Index", "Register");
//        }

//        #endregion -- End Wizard


//        //[Authorize("SuperAdmin")]
//        //[Authorize(Policy = "SuperAdmin")]
//        //[HttpGet]
//        //public IActionResult GetRegisters2()
//        //{
//        //    RegisterService service = new RegisterService();

//        //    // Get registers from Dal and map them in POCO
//        //    IEnumerable<Register> registers = service.GetAllCurrentDay().Select(r => r.ToApp()).ToList();

//        //    // Map POCO in ViewModels
//        //    IEnumerable<RegisterVM> registerVMs = registers.Select(r => r.ToVM()).ToList();
//        //    return View(registerVMs);
//        //}

//        //[Authorize("SuperAdmin")]

//        [Authorize(Policy = "SuperAdmin")]
//        [HttpGet]
//        public IActionResult GetRegisters(int sortOrder = 0, string searchString = null)
//        {
//            RegisterService service = new RegisterService();

//            // Get registers from Dal and map them in POCO
//            IEnumerable<Register> registers = service.GetAll().Select(r => r.ToApp()).ToList();

//            // if searchString exists
//            if (searchString != null)
//            {
//                registers = registers
//                    .Where(r => r.FName.ToLower().Contains(searchString.ToLower()) || r.LName.ToLower().Contains(searchString.ToLower()) || r.CompanyName.ToLower().Contains(searchString.ToLower()));
//                ViewData["nameFilter"] = searchString;
//            }

//            // Today DateTime
//            DateTime today = DateTime.Now;

//            // Initialise TimeSpan
//            TimeSpan timeSpan = new TimeSpan(0, 0, 0);

//            // if filter asks for all records, there is no linq operation done on the list
//            if (sortOrder != 4)
//            {
//                switch (sortOrder)
//                {
//                    case 1: // yesterday
//                        timeSpan = new TimeSpan(today.Hour + 8 + 24, today.Minute, 0);
//                        break;

//                    case 2: // this week

//                        // Today day of the week
//                        int toMonday = (int)today.DayOfWeek;
//                        timeSpan = new TimeSpan(today.Hour + 8 + toMonday * 24, today.Minute, 0);
//                        break;

//                    case 3: // this month

//                        int toFirstDayOfMonth = today.Day;
//                        timeSpan = new TimeSpan(today.Hour + 8 + toFirstDayOfMonth * 24, today.Minute, 0);
//                        break;

//                    default: // today : today midnight - 8 hours

//                        timeSpan = new TimeSpan(today.Hour + 8, today.Minute, 0);
//                        break;
//                }

//                registers = registers
//                    .Where(r => r.TimeIn >= today.Subtract(timeSpan) && r.TimeIn <= today)
//                    .ToList();
//            }

//            // Map POCO in ViewModels
//            IEnumerable<RegisterVM> registerVMs = registers
//                .OrderByDescending(r => r.TimeIn)
//                .Select(r => r.ToVM())
//                .ToList();

//            return View(registerVMs);
//        }

//        [HttpGet]
//        public IActionResult GetRegisterPlanning(int sortOrder = 0, string searchString = null)
//        {
//            RegisterPlanningService serviceRPlanning = new RegisterPlanningService();
//            RegisterService serviceRegister = new RegisterService();

//            // Get registersPlanning from DAL and map them in POCO
//            IEnumerable<RegisterPlanning> registersPlanning = serviceRPlanning.GetAll().Select(r => r.ToApp()).ToList();

//            // Get registers from DAL and map them to POCO
//            IEnumerable<Register> registers = serviceRegister.GetAll().Select(r => r.ToApp()).ToList();

//            // if searchString exists
//            if (searchString != null)
//            {
//                // apply filter to registerPlanning and registers
//                registersPlanning = registersPlanning
//                    .Where(r => r.FName.ToLower().Contains(searchString.ToLower()) || r.LName.ToLower().Contains(searchString.ToLower()) || r.CompanyName.ToLower().Contains(searchString.ToLower()));
//                ViewData["nameFilter"] = searchString;

//                registers = registers.Where(r => r.FName.ToLower().Contains(searchString.ToLower()) || r.LName.ToLower().Contains(searchString.ToLower()) || r.CompanyName.ToLower().Contains(searchString.ToLower()));
//            }

//            // Today DateTime
//            DateTime today = DateTime.Now;

//            // Initialise TimeSpan
//            TimeSpan timeSpan = new TimeSpan(0, 0, 0);

//            // if filter asks for all records, there is no linq operation done on the list
//            if (sortOrder != 4)
//            {
//                switch (sortOrder)
//                {
//                    case 1: // yesterday
//                        timeSpan = new TimeSpan(today.Hour + 8 + 24, today.Minute, 0);
//                        break;

//                    case 2: // this week

//                        // Today day of the week
//                        int toMonday = (int)today.DayOfWeek;
//                        timeSpan = new TimeSpan(today.Hour + 8 + toMonday * 24, today.Minute, 0);
//                        break;

//                    case 3: // this month

//                        int toFirstDayOfMonth = today.Day;
//                        timeSpan = new TimeSpan(today.Hour + 8 + toFirstDayOfMonth * 24, today.Minute, 0);
//                        break;

//                    default: // today : today midnight - 8 hours

//                        timeSpan = new TimeSpan(today.Hour + 8, today.Minute, 0);
//                        break;
//                }

//                registersPlanning = registersPlanning
//                    .Where(r => r.TimeIn >= today.Subtract(timeSpan) && r.TimeIn <= today)
//                    .ToList();

//                registers = registers.Where(r => r.TimeIn >= today.Subtract(timeSpan)).ToList();
//            }

//            // Compare two lists
//            foreach (var i in registersPlanning)
//            {
//                foreach (var j in registers)
//                {
//                    if (i.PersonId == j.PersonId && i.TimeIn.Date == j.TimeIn.Date)
//                    {

//                    }
//                }
//            }

//            // Map POCO in ViewModels
//            var registersVM = registersPlanning
//                .OrderByDescending(r => r.TimeIn)
//                .Select(r => r.ToVM())
//                .ToList();

//            return View(registersVM);
//        }


//        #region AddRegisterPlanning Wizard form

//        // Save statuses
//        private IEnumerable<CompanyStatus> statusesGlobal;

//        [HttpGet]
//        public IActionResult AddRegisterPlanningStatus()
//        {
//            SetStatus status = new SetStatus();

//            User user = GetUser();

//            CompanyStatusService serv = new CompanyStatusService();
//            status.CompanyStatus = serv.GetAll().Select(cs => cs.ToApp()).ToList();

//            // Save statuses
//            statusesGlobal = status.CompanyStatus;

//            status.Id = user.StatusId;

//            return View(status);
//        }

//        [HttpPost]
//        public IActionResult AddRegisterPlanningStatus(string nextBtn, SetStatus status)
//        {
//            CompanyStatusService serv = new CompanyStatusService();

//            if (nextBtn != null)
//            {
//                if (!ModelState.IsValid)
//                {
//                    // TODO : Log error       
//                    ViewBag.Error = "ce status n'existe pas";
//                }
//                else
//                {
//                    Dal.CompanyStatus C = serv.Get(status.Id);

//                    // Check whether the status exist in the DB
//                    if (C != null)
//                    {
//                        User user = GetUser();
//                        user.StatusId = status.Id;

//                        // If the user is an intern, its company is SNCB
//                        if (status.Id == 1)
//                        {
//                            user.CompanyId = 1;
//                        }

//                        HttpContext.Session.SetComplexData("user", user);

//                        return RedirectToAction("AddRegisterPlanningCompany", "Register");
//                    }
//                }

//                status.CompanyStatus = serv.GetAll().Select(cs => cs.ToApp()).ToList();
//            }

//            return View(status);
//        }
//        //    public IActionResult AddRegisterPlanningStatus(string nextBtn, SetStatus status)
//        //    {
//        //        User user = GetUser();

//        //        if (nextBtn != null)
//        //        {
//        //            // Reset session if things changed after navigation (Next && Previous)
//        //            if (user.StatusId > 0 && user.StatusId != status.Id)
//        //            {
//        //                user.CompanyId = 0;
//        //                user.DepartmentId = 0;
//        //                user.PersonVisited = null;
//        //                //HttpContext.Session.SetComplexData("user", user);
//        //            }

//        //            if (status.Id == 0)
//        //            {
//        //                ViewBag.Error = "Veuillez sélectionner un status.";
//        //}
//        //else
//        //{
//        //                CompanyStatusService serv = new CompanyStatusService();

//        //                Dal.CompanyStatus statusExists = serv.Get(status.Id);

//        //                if (statusExists != null)
//        //                {
//        //                    user.StatusId = status.Id;

//        //                    // If the user is an intern, its company is SNCB
//        //                    if (status.Id == 1)
//        //                    {
//        //                        user.CompanyId = 1;
//        //                    }
//        //                }
//        //                else
//        //                {
//        //                    user.StatusId = 0;
//        //                    status.Id = 0;
//        //                    ViewBag.Error = "ce status n'existe pas";
//        //                }
//        //            }

//        //            HttpContext.Session.SetComplexData("user", user);

//        //            if (ViewBag.Error == null)
//        //            {
//        //                return RedirectToAction("AddRegisterPlanningCompany");
//        //            }
//        //            else
//        //            {
//        //                status.CompanyStatus = statusesGlobal;
//        //            }                
//        //        }
//        //        return View(status);
//        //    }

//        #region archives

//        [NonAction]
//        //private SetStatus CheckStatus(SetStatus checkstatus)
//        //{
//        //    SetStatus status = new SetStatus();

//        //    if (checkstatus.Id == 0)
//        //    {
//        //        ViewBag.Error = "Veuillez sélectionner un status.";
//        //    }
//        //    else
//        //    {
//        //        Dal.CompanyStatus statusExists = serv.Get(status.Id);

//        //        // Check whether the status exist in the DB
//        //        if (statusExists != null)
//        //        {
//        //            user.StatusId = status.Id;

//        //            // If the user is an intern, its company is SNCB
//        //            if (status.Id == 1)
//        //            {
//        //                user.CompanyId = 1;
//        //            }
//        //        }
//        //        else
//        //        {
//        //            user.StatusId = 0;
//        //            status.Id = 0;
//        //            ViewBag.Error = "ce status n'existe pas";
//        //        }
//        //    }

//        //    return status;
//        //}

//        //[HttpGet]
//        //public IActionResult AddRegisterPlanningCompany2()
//        //{
//        //    User user = GetUser();

//        //    // Populate company with session data
//        //    SetCompany company = SessionToSetCompany(user);

//        //    DepartmentService servD = new DepartmentService();

//        //    // If the user is part of SNCB
//        //    if (company.CompanyStatusId == 1)
//        //    {
//        //        company.Departments = servD.GetAllDepartmentByCompany(company.CompanyStatusId)
//        //            .Where(s => s.Id != 2)
//        //            .Select(d => d.ToApp())
//        //            .ToList();
//        //        company.Departments.Insert(0, new Department { Id = 0, Name = "Autre" });
//        //    }
//        //    else
//        //    {
//        //        // Get companies by status 
//        //        CompanyService serv = new CompanyService();
//        //        company.Companies = serv.GetAllCompanyByStatusWithoutDep(user.StatusId)
//        //            .Select(c => c.ToApp())
//        //            .ToList();

//        //        // Provide the company's dropdownlist
//        //        if (company.Companies.Count() > 0)
//        //        {
//        //            // Add "Autre" in the dropdownlist
//        //            Company other = new Company { Id = 0, CompanyName = "Autre" };
//        //            company.Companies.Insert(0, other);

//        //            // For navigation (Next & Previous) purposes
//        //            if (user.DepartmentId > 0)
//        //            {
//        //                List<Department> deps = servD.GetAllDepartmentByCompany((int)user.CompanyId)
//        //                    .Select(d => d.ToApp())
//        //                    .ToList();

//        //                if (deps.Count() > 0)
//        //                {
//        //                    company.Departments = deps;
//        //                    company.Departments.Insert(0, new Department { Id = 0, Name = "Autre" });
//        //                }
//        //                else
//        //                {
//        //                    company.Departments = null;
//        //                }

//        //                company.DepartmentId = user.DepartmentId;
//        //            }
//        //        }
//        //        else
//        //        {
//        //            // If no dropdown list with companies is provided, force user to enter the company's name
//        //            company.Id = 0;
//        //        }
//        //    }
//        //    return View(company);
//        //}
//        #endregion

//        [HttpGet]
//        public IActionResult AddRegisterPlanningCompany()
//        {
//            User user = GetUser();

//            // Populate company with session data
//            SetCompany company = SessionToSetCompany(user);

//            // Prepare companies and departments dropdown lists
//            company = SetCompanyToView(company);

//            return View(company);
//        }

//        [HttpPost]
//        public IActionResult AddRegisterPlanningCompany(string nextBtn, string prevBtn, SetCompany company)
//        {
//            // Get the user from the session
//            User user = GetUser();
//            CompanyService servC = new CompanyService();
//            DepartmentService servD = new DepartmentService();

//            if (prevBtn != null)
//            {
//                return RedirectToAction("AddRegisterPlanningStatus");
//            }

//            // Get all companies and departments
//            List<Company> companies = servC.GetAll().Select(c => c.ToApp()).ToList();
//            List<Department> departments = servD.GetAll().Select(d => d.ToApp()).ToList();

//            if (nextBtn != null)
//            {
//                // Client side validation
//                if (!ModelState.IsValid)
//                {
//                    if (company.DepartmentId == 0 && String.IsNullOrWhiteSpace(company.DepartmentName))
//                    {
//                        ViewBag.Error = "Le nom du département est requis";
//                    }

//                    if (company.Id == 0 && String.IsNullOrWhiteSpace(company.Name))
//                    {
//                        ViewBag.Error = "Le nom de la société est requis";
//                    }

//                    // Prepare companies and departments dropdown lists
//                    company = SetCompanyToView(company);

//                    return View(company);
//                }
//                // Server side validation
//                else
//                {
//                    // User provides companyId
//                    if (company.Id > 0)
//                    {
//                        // Check this Id exists in DB
//                        int companyExists = companies
//                            .Where(c => c.CompanyStatusId == company.CompanyStatusId && c.Id == company.Id)
//                            .Count();

//                        //bool companyExists0 = companies
//                        //    .Any(c => c.CompanyStatusId == company.CompanyStatusId && c.Id == company.Id);

//                        if (companyExists > 0)
//                        {
//                            user.CompanyId = (int)company.Id;

//                            // Check wether company requires department
//                            IEnumerable<int> hasDepartments = departments
//                                .Where(d => d.CompanyId == company.Id)
//                                .Select(d => d.Id)
//                                .ToList();

//                            // Company needs a department
//                            if (hasDepartments.Count() > 0)
//                            {
//                                if (company.DepartmentId > 0)
//                                {
//                                    // Check that provided department is in DB
//                                    int isDepartmentValid = hasDepartments
//                                        .Where(i => i == company.DepartmentId)
//                                        .SingleOrDefault();

//                                    if (isDepartmentValid > 0)
//                                    {
//                                        user.DepartmentId = company.DepartmentId;
//                                    }
//                                    else
//                                    {
//                                        ViewBag.Error = "Ce départment n'existe pas.";
//                                    }
//                                }
//                                // department provided by input
//                                else if (company.DepartmentId == 0)
//                                {
//                                    if (!String.IsNullOrWhiteSpace(company.DepartmentName))
//                                    {
//                                        // check if department does not already exist
//                                        int departmentNameExists = departments
//                                            .Where(d => d.Name.ToLower() == company.DepartmentName.ToLower())
//                                            .Select(d => d.Id)
//                                            .SingleOrDefault();

//                                        if (departmentNameExists > 0)
//                                        {
//                                            ViewBag.Error = "Cette société se trouve déjà dans la liste, veuillez la sélectionner.";
//                                        }
//                                        else
//                                        {
//                                            user.DepartmentId = company.DepartmentId;
//                                            user.DepartmentName = company.DepartmentName;
//                                        }
//                                    }
//                                    else
//                                    {
//                                        ViewBag.Error = "Veuillez fournir un nom correct pour le département.";
//                                    }
//                                }
//                                // No department was provided, return to view
//                                else
//                                {
//                                    ViewBag.Error = "Un département doit être renseigné pour cette société, veuillez recommencer.";
//                                }
//                            }
//                            // No need for department, save data
//                            else
//                            {
//                                // Save companyId to session
//                                user.CompanyId = (int)company.Id;
//                            }

//                        }
//                        // the company does not exist, return view
//                        else
//                        {
//                            ViewBag.Error = "Cette société n'existe pas";
//                        }
//                    }
//                    // User provides company Name
//                    else if (company.Id == 0)
//                    {
//                        // Input is valid
//                        if (!String.IsNullOrWhiteSpace(company.Name))
//                        {
//                            // Check that the company name is not already taken
//                            Company companyExist = companies
//                                .Where(c => c.CompanyName.ToLower() == company.Name.ToLower())
//                                .SingleOrDefault();

//                            if (companyExist != null)
//                            {
//                                // check that the company belongs to the same status
//                                if (companyExist.CompanyStatusId == user.StatusId)
//                                {
//                                    ViewBag.Error = "Cette société existe déjà, sélectionnez-là dans la liste.";
//                                }
//                                else
//                                {
//                                    ViewBag.Error = String.Format("Cette société existe déjà sous le status \"{0}\", veuillez revenir en arrière et sélectionnez le bon status", companyExist.StatusName);
//                                }
//                            }
//                            else
//                            {
//                                user.CompanyId = (int)company.Id;
//                                user.CompanyName = company.Name;
//                            }
//                        }
//                        // Input is not valid
//                        else
//                        {
//                            ViewBag.Error = "Veuillez spécifier un nom pour la société.";
//                            return View(company);
//                        }
//                    }
//                    // User did not make any choice
//                    else
//                    {
//                        ViewBag.Error = "Veuillez choisir une société.";
//                    }
//                }
//            }

//            // An error occured, return to view
//            if (ViewBag.Error != null)
//            {
//                company = SetCompanyToView(company);
//                return View(company);
//            }

//            // No error save new company if exists and new department if exists
//            if (company.Name != null)
//            {
//                Company newCompany = new Company()
//                {
//                    CompanyStatusId = company.CompanyStatusId,
//                    CompanyName = company.Name
//                };

//                // TODO Try Catch with error handling
//                user.CompanyId = servC.Insert(newCompany.ToDal());

//                // Remove companyName from session as new company was added
//                user.CompanyName = null;
//            }

//            if (company.DepartmentName != null)
//            {
//                Department newDepartment = new Department()
//                {
//                    CompanyId = (int)company.Id,
//                    Name = company.DepartmentName,
//                };

//                // TODO Try Catch with error handling
//                user.DepartmentId = servD.Insert(newDepartment.ToDal());

//                // Remove DepartmentName from session as new department was added
//                user.DepartmentName = null;
//            }

//            // Save session
//            HttpContext.Session.SetComplexData("user", user);

//            return RedirectToAction("AddRegisterPlanningPerson");
//        }

//        [HttpGet]
//        public IActionResult AddRegisterPlanningPerson()
//        {
//            User user = GetUser();
//            SetPerson person = SetPersonToView(user);
//            return View(person);
//        }

//        [HttpPost]
//        public IActionResult AddRegisterPlanningPerson(string nextBtn, string prevBtn, SetPerson person)
//        {
//            User user = GetUser();

//            PersonService serv = new PersonService();

//            if (prevBtn != null)
//            {
//                return RedirectToAction("AddRegisterPlanningCompany");
//            }

//            if (nextBtn != null)
//            {
//                if (!ModelState.IsValid)
//                {
//                    SetPerson personToView = SetPersonToView(user);
//                    return View(personToView);
//                }
//                else
//                {
//                    // A user is selected from the list
//                    if (person.Id > 0)
//                    {
//                        // Check that the person is in DB
//                        Dal.Person personExist = serv.Get((int)person.Id, user.CompanyId);

//                        if (personExist != null)
//                        {
//                            user.Id = (int)person.Id;
//                        }
//                        else
//                        {
//                            ViewBag.Error = "Cette personne n'existe pas";
//                        }
//                    }
//                    else if (person.Id == 0)
//                    {
//                        string nameAndLastName = ControllerMethods.NameAndLastNameValid(person.FName, person.LName);

//                        if (nameAndLastName == null)
//                        {
//                            // Prevent duplicate in the DB, search by LName & FName or FName & LName
//                            Person personExists = CheckIfPersonExistInACompany(user, person);

//                            // If we find a duplicate alert the user
//                            if (personExists != null)
//                            {
//                                ViewBag.NameError = "Cette personne existe déjà, sélectionnez la dans la liste. Si vous souhaitez ajouter une personne portant le même nom et le même prénom, ajoutez une différenciation (ex: troisième prénom).";
//                            }
//                            else
//                            {
//                                user.Id = 0;
//                                user.LName = person.LName;
//                                user.FName = person.FName;
//                            }
//                        }
//                        // In case of error, set viewbag
//                        else
//                        {
//                            ViewBag.NameError = nameAndLastName;
//                        }
//                    }
//                    else
//                    {
//                        ViewBag.Error = "Veuillez choisir une personne dans la liste ou inscrire son nom manuellement";
//                    }

//                    // The user is a visitor and must provide a visitee
//                    if (user.StatusId == 3)
//                    {
//                        if (person.PersonVisitedId > 0)
//                        {
//                            // Check that the visitee is in DB
//                            Dal.Person personVisitedExists = serv.GetPersonVisited((int)person.PersonVisitedId);

//                            if (personVisitedExists != null)
//                            {
//                                user.PersonVisited = person.PersonVisitedId;
//                            }
//                            else
//                            {
//                                ViewBag.VisitedError = "La personne visitée n'existe pas.";
//                            }
//                        }
//                        else
//                        {
//                            ViewBag.VisitedError = "Veuillez sélectionner la personne que vous venez visiter.";
//                        }
//                    }
//                }
//            }

//            if (ViewBag.Error != null || ViewBag.NameError != null || ViewBag.VisitedError != null)
//            {
//                SetPerson personToView = SetPersonToView(user);
//                return View(personToView);
//            }
//            else
//            {
//                // Save new user if he/she exists
//                if (user.LName != null && user.FName != null)
//                {
//                    Dal.Person newPerson = new Dal.Person();
//                    newPerson.LName = user.LName;
//                    newPerson.FName = user.FName;
//                    newPerson.AdministratorId = 1;
//                    newPerson.CompanyId = user.CompanyId;
//                    newPerson.DepartmentId = user.DepartmentId;
//                    newPerson.Id = serv.Insert(newPerson);
//                    user.Id = newPerson.Id;

//                    // reset values as a new user was created
//                    user.FName = null;
//                    user.LName = null;
//                }

//                // Save session
//                HttpContext.Session.SetComplexData("user", user);

//                return RedirectToAction("AddRegisterPlanningTime");
//            }
//        }

//        [HttpGet]
//        public IActionResult AddRegisterPlanningTime()
//        {
//            SetTime setTime = new SetTime();
//            return View(setTime);
//        }

//        [HttpPost]
//        public IActionResult AddRegisterPlanningTime(string nextBtn, string prevBtn, IFormCollection form)
//        {
//            if (prevBtn != null)
//            {
//                return RedirectToAction("AddRegisterPlanningPerson");
//            }

//            if (nextBtn != null)
//            {
//                if (!ModelState.IsValid)
//                {
//                    return View();
//                }
//                else
//                {
//                    User user = GetUser();

//                    RegisterPlanningVM register = new RegisterPlanningVM();

//                    string validation;
//                    string integrity;

//                    if (form["PlanningTypeId"] == "1")
//                    {
//                        validation = FormTimeValid(form, "SameDayDate", "SameDayTimeIn", "SameDayTimeOut");

//                        if (validation == null)
//                        {
//                            DateTime timeIn = SameDayToDateTime(form, "SameDayDate", "SameDayTimeIn");
//                            DateTime timeOut = SameDayToDateTime(form, "SameDayDate", "SameDayTimeOut");

//                            integrity = TimeInTimeOutIntegrity(timeIn, timeOut);
//                            if (integrity == null)
//                            {
//                                register.TimeIn = timeIn;
//                                register.TimeOut = timeOut;

//                                RegisterPlanningService serv = new RegisterPlanningService();
//                                Dal.RegisterPlanning registerExist = serv.GetExisting(user.Id, register.TimeIn);

//                                // if a registerPlanning exist
//                                if (registerExist != null)
//                                {
//                                    // check if it was planned
//                                    if (registerExist.Planned == false)
//                                    {
//                                        bool succeed = serv.SetPlanned(registerExist.Id);
//                                        if (succeed == true)
//                                        {
//                                            TempData["Alert"] = "true";
//                                        }
//                                        else
//                                        {
//                                            // TODO : handle error and redirect to error page
//                                        }
//                                    }
//                                    else
//                                    {
//                                        TempData["Alert"] = "false";
//                                    }
//                                    return RedirectToAction("GetRegisterPlanning");
//                                }
//                            }
//                            else
//                            {
//                                ViewBag.Error = integrity;
//                            }
//                        }
//                        else
//                        {
//                            ViewBag.Error = FormTimeValidViewBag(validation, 1);
//                        }

//                    }
//                    else if (form["PlanningTypeId"] == "2")
//                    {
//                        validation = FormTimeValid(form, "DifferentDayTimeIn", "DifferentDayTimeOut");

//                        if (validation == null)
//                        {
//                            DateTime DateTimeIn = DateTime.Parse(form["DifferentDayTimeIn"]);
//                            DateTime DateTimeOut = DateTime.Parse(form["DifferentDayTimeOut"]);

//                            integrity = TimeInTimeOutIntegrity(DateTimeIn, DateTimeOut);
//                            if (integrity == null)
//                            {
//                                //dt.ToString("H:mm");



//                                register.TimeIn = DateTimeIn;
//                                register.TimeOut = DateTimeOut;





//                            }
//                            else
//                            {
//                                ViewBag.Error = integrity;
//                            }
//                        }
//                        else
//                        {
//                            ViewBag.Error = FormTimeValidViewBag(validation, 2);
//                        }
//                    }
//                    else if (form["PlanningTypeId"] == "3")
//                    {

//                    }
//                    else
//                    {
//                        ViewBag.Error = "Veuillez sélectionner un type d'horaire.";
//                    }
//                }
//            }

//            SetTime setTimeError = FormToSetTime(form);
//            return View(setTimeError);
//        }


//        #endregion AddRegisterPlanning Wizard

//        [HttpPost]
//        public IActionResult Success(string title, string message)
//        {
//            ViewBag.ModalTitle = title;
//            ViewBag.Message = message;
//            return PartialView("_Success");
//        }

//        #region Methods

//        [NonAction]
//        private User GetUser()
//        {
//            var session = HttpContext.Session.GetComplexData<User>("user");

//            if (session == null)
//            {
//                User user = new User();
//                HttpContext.Session.SetComplexData("user", user);
//            }
//            return (User)HttpContext.Session.GetComplexData<User>("user");
//        }

//        [NonAction]
//        private void RemoveUser()
//        {
//            HttpContext.Session.Clear();
//        }

//        // Function used to populate a view model with the user choices in case of postback error
//        [NonAction]
//        private SetCompany ReturnCompanyError(int userStatusId)
//        {
//            CompanyService service = new CompanyService();
//            SetCompany companyError = new SetCompany();
//            companyError.Companies = service.GetAllCompanyByStatusWithoutDep(userStatusId).Select(c => c.ToApp()).ToList();

//            // Add "Autre" in the dropdownlist
//            Company other = new Company { Id = 0, CompanyName = "Autre" };
//            companyError.Companies.Insert(0, other);
//            companyError.Options = companyError.Companies.Select(a => new SelectListItem
//            {
//                Value = a.Id.ToString(),
//                Text = a.CompanyName
//            }).ToList();
//            return companyError;
//        }

//        [NonAction]
//        private SetPerson ReturnPersonError(int companyId, int departmentId = -1, int personId = 0)
//        {
//            PersonService service = new PersonService();

//            SetPerson setPersonError = new SetPerson();

//            if (departmentId != -1)
//            {
//                setPersonError.Persons = service.GetAll(companyId, departmentId).Select(p => p.ToAppPersonListVM()).ToList();
//            }
//            else
//            {
//                setPersonError.Persons = service.GetAll(companyId).Select(p => p.ToAppPersonListVM()).ToList();
//            }

//            // Add "Autre" to Dropdown list
//            PersonListVM personOther = new PersonListVM { Id = 0, LName = null, FName = null };
//            setPersonError.Persons.Insert(0, personOther);

//            if (personId != 0)
//            {
//                setPersonError.OptionsPersons = new List<SelectListItem>();

//                foreach (var item in setPersonError.Persons)
//                {
//                    if (item.Id == personId)
//                    {
//                        setPersonError.OptionsPersons.Add(new SelectListItem
//                        {
//                            Value = item.Id.ToString(),
//                            Text = item.Name,
//                            Selected = true
//                        });
//                    }
//                    else
//                    {
//                        setPersonError.OptionsPersons.Add(new SelectListItem
//                        {
//                            Value = item.Id.ToString(),
//                            Text = item.Name,
//                        });
//                    }
//                }
//            }
//            else
//            {
//                setPersonError.OptionsPersons = setPersonError.Persons.Select(a => new SelectListItem
//                {
//                    Value = a.Id.ToString(),
//                    Text = a.Name
//                }).ToList();
//            }
//            return setPersonError;
//        }

//        [NonAction]
//        private Person CheckIfPersonExistInACompany(User user, SetPerson setPerson)
//        {
//            PersonService servP = new PersonService();

//            IEnumerable<Person> people = servP.GetAll(setPerson.LName, setPerson.FName).Select(p => p.ToApp()).ToList();

//            // If one or serveral person match
//            // check if one person is in the same company and department

//            Person person = null;

//            if (people.Count() > 0)
//            {
//                foreach (var i in people)
//                {
//                    if (user.DepartmentId > 0)
//                    {
//                        if (i.CompanyId == user.CompanyId && i.DepartmentId == user.DepartmentId)
//                        {
//                            person = i;
//                        }
//                    }
//                    else
//                    {
//                        if (i.CompanyId == user.CompanyId)
//                        {
//                            person = i;
//                        }
//                    }
//                }
//            }
//            return person;
//        }

//        [NonAction]
//        private SetCompany SessionToSetCompany(User user)
//        {
//            SetCompany company = new SetCompany();

//            company.CompanyStatusId = user.StatusId;

//            // Handel company's id null value to prevent 0 as a selected list item
//            if (user.CompanyId == 0 && user.CompanyName == null)
//            {
//                company.Id = null;
//            }
//            else
//            {
//                company.Id = user.CompanyId;
//                company.Name = user.CompanyName;
//            }

//            //// Handel department's id null value to prevent 0 as a selected list item
//            //if (user.DepartmentId == 0 && user.DepartmentName == null)
//            //{
//            //    company.DepartmentId = null;
//            //}
//            //else
//            //{
//            //    company.DepartmentId = user.DepartmentId;
//            //    company.DepartmentName = user.DepartmentName;
//            //}

//            return company;
//        }

//        [NonAction]
//        private SetCompany SetCompanyToView(SetCompany form)
//        {
//            SetCompany company = form;

//            // If the user is part of SNCB
//            if (company.CompanyStatusId == 1)
//            {
//                company.Departments = SetCompanyDepartments((int)company.Id);
//            }
//            else
//            {
//                company.Companies = SetCompanyCompanies(company.CompanyStatusId);

//                if (company.Companies != null)
//                {
//                    // For navigation (Next & Previous) purposes
//                    if (company.DepartmentId > 0)
//                    {
//                        company.Departments = SetCompanyDepartments((int)company.Id);
//                    }
//                }
//                else
//                {
//                    // If no dropdown list with companies is provided, force user to enter the company's name
//                    company.Id = 0;
//                }
//            }
//            return company;
//        }

//        [NonAction]
//        private SetPerson SetPersonToView(User user)
//        {
//            PersonService serv = new PersonService();
//            SetPerson person = new SetPerson();

//            if (user.Id == 0)
//            {
//                person.Id = null;
//            }
//            else
//            {
//                person.Id = user.Id;
//            }

//            List<PersonListVM> personList = new List<PersonListVM>();

//            if (user.DepartmentId > 0)
//            {
//                personList = serv.GetAll(user.CompanyId, user.DepartmentId).Select(p => p.ToAppPersonListVM()).ToList();
//            }
//            else
//            {
//                personList = serv.GetAll(user.CompanyId).Select(p => p.ToAppPersonListVM()).ToList();
//            }

//            // If the company has recorded employees, show a list
//            if (personList.Count() > 0)
//            {
//                person.Persons = personList;

//                // Add "Autre" to Dropdown list
//                PersonListVM personOther = new PersonListVM { Id = 0, LName = null, FName = null };
//                person.Persons.Insert(0, personOther);
//            }
//            else
//            {
//                person.Persons = null;
//                // Force inputs (name and lastname) to be provided
//                //person.Id = 0;
//            }

//            // If the person is a visitor, the visted person is required
//            if (user.StatusId == 3)
//            {
//                // Populate the list of person for the user to find the name of the person he wants to visit
//                // The parameter 1 of the underneath method correspond to companyId = 1 (SNCB) && departmentId = 2 (Arlon)
//                List<PersonListVM> personToVist = serv.GetAll(1, 2).Select(p => p.ToAppPersonListVM()).ToList();

//                if (personToVist.Count() > 0)
//                {
//                    person.PersonsVisited = personToVist;
//                    person.PersonVisitedId = user.PersonVisited;
//                }
//                else
//                {
//                    person.PersonsVisited = null;
//                }
//            }
//            return person;
//        }

//        [NonAction]
//        private string NameAndLastNameValid(string name, string lastname)
//        {
//            string result;

//            if (!String.IsNullOrWhiteSpace(name) && !String.IsNullOrWhiteSpace(lastname))
//            {
//                result = null;
//            }
//            // FName or LName  or both are invalid
//            else
//            {
//                if (String.IsNullOrWhiteSpace(lastname))
//                {
//                    result = "Le nom de famille est requis.";
//                }
//                else if (String.IsNullOrWhiteSpace(name))
//                {
//                    result = "Le prénom est requis.";
//                }
//                else
//                {
//                    result = "Le nom de famille  et le prénom sont requis.";
//                }
//            }
//            return result;
//        }

//        [NonAction]
//        private List<Company> SetCompanyCompanies(int statusId)
//        {
//            CompanyService service = new CompanyService();
//            List<Company> companies = new List<Company>();

//            companies = service.GetAllCompanyByStatusWithoutDep(statusId)
//                    .Select(c => c.ToApp())
//                    .ToList();

//            if (companies.Count() > 0)
//            {
//                // Add "Autre" in the dropdownlist
//                Company other = new Company { Id = 0, CompanyName = "Autre" };
//                companies.Insert(0, other);
//            }
//            else
//            {
//                companies = null;
//            }

//            return companies;
//        }

//        [NonAction]
//        private List<Department> SetCompanyDepartments(int companyId)
//        {
//            DepartmentService service = new DepartmentService();
//            List<Department> departments = new List<Department>();

//            if (companyId == 1)
//            {
//                departments = service.GetAllDepartmentByCompany(companyId)
//                    .Where(s => s.Id != 2)
//                    .Select(d => d.ToApp())
//                    .ToList();
//            }
//            else
//            {
//                departments = service.GetAllDepartmentByCompany(companyId)
//                            .Select(d => d.ToApp())
//                            .ToList();
//            }

//            if (departments.Count() > 0)
//            {
//                departments.Insert(0, new Department { Id = 0, Name = "Autre" });
//            }
//            else
//            {
//                departments = null;
//            }

//            return departments;
//        }

//        [NonAction]
//        private string TimeInTimeOutIntegrity(DateTime timeIn, DateTime timeOut)
//        {
//            string result = null;

//            if (timeIn >= timeOut)
//            {
//                result = "L'horaire de début doit être inférieur à l'horaire de fin.";
//            }
//            return result;
//        }

//        [NonAction]
//        private string FormTimeValid(IFormCollection form, params string[] keys)
//        {
//            string result = null;
//            string value;

//            for (int i = 0; i < keys.Length; i++)
//            {
//                value = form[keys[i]];

//                if (value == "")
//                {
//                    result += i.ToString();
//                }
//            }
//            return result;
//        }

//        [NonAction]
//        private string FormTimeValidViewBag(string validation, int formType)
//        {
//            string viewbag = "";

//            if (formType == 1)
//            {
//                switch (validation)
//                {
//                    case "0":
//                        viewbag = "Veuillez spécifier une date.";
//                        break;
//                    case "01":
//                        viewbag = "Veuillez spécifier une date et une heure d'arrivée";
//                        break;
//                    case "012":
//                        viewbag = "Veuillez spécifier une date, une heure d'arrivée et de départ";
//                        break;
//                    case "12":
//                        viewbag = "Veuillez spécifier une heure d'arrivée et de départ";
//                        break;
//                    case "1":
//                        viewbag = "Veuillez spécifier une heure d'arrivée";
//                        break;
//                    case "2":
//                        viewbag = "Veuillez spécifier une heure de départ";
//                        break;
//                }
//            }
//            else if (formType == 2)
//            {
//                switch (validation)
//                {
//                    case "0":
//                        viewbag = "Veuillez spécifier la date et l'heure de début.";
//                        break;
//                    case "1":
//                        viewbag = "Veuillez spécifier la date et l'heure de fin.";
//                        break;
//                    case "01":
//                        viewbag = "Veuillez spécifier les dates et les heures de début et de fin.";
//                        break;
//                }
//            }
//            return viewbag;
//        }

//        [NonAction]
//        private DateTime SameDayToDateTime(IFormCollection form, string dateKey, string hourKey)
//        {
//            string date = form[dateKey] + " " + form[hourKey] + ":00";
//            DateTime dateTime = DateTime.Parse(date);
//            return dateTime;
//        }

//        [NonAction]
//        private SetTime FormToSetTime(IFormCollection form)
//        {
//            SetTime setTime = new SetTime();
//            setTime.PlanningTypeId = int.Parse(form["PlanningTypeId"]);
//            setTime.SameDayDate = form["SameDayDate"];
//            setTime.SameDayTimeIn = form["SameDayTimeIn"];
//            setTime.SameDayTimeOut = form["SameDayTimeOut"];
//            return setTime;
//        }
//        #endregion
//    }
//}
