﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using DAL;
using DAL.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WebApp.AppTools;
using WebApp.Models;
using WebApp.Models.PersonTestViewModels;
using WebApp.Models.RegisterViewModels;
using AppRegisterVM = WebApp.Models.RegisterViewModels;

namespace WebApp.Controllers
{
	public class HomeController : Controller
	{
		public IActionResult Index()
		{
			//string con = AppSettings.ConnectionString;
			//string inv = AppSettings.InvariantName;
			//ViewBag.AppSettings = string.Format("{0} : {1} :", con, inv);
			return View();
		}

		[HttpGet]
		public IActionResult TestF()
		{
			List<int> ids = new List<int>() { 1, 2, 3 };

			string test = "";
			foreach (int item in ids)
			{
				test += item.ToString();
			}

			TempData["Test"] = test;

			return RedirectToAction("TestG");
		}

		[HttpGet]
		public IActionResult TestG()
		{
			List<int> ids = new List<int>();

			if (TempData["Test"] != null)
			{
				string test = TempData["Test"].ToString();

				foreach (char item in test)
				{
					ids.Add((int)item);
				}
			}
			return View();
		}

		//private void TestViewBag()
		//{
		//	TempData["Alert"] = "Vous avez indiqué vouloir signer le registre de sortie";
		//}

		[HttpGet]
		public IActionResult CreatePerson()
		{
			SetPerson person = new SetPerson();
			return View(person);
		}

		[HttpPost]
		public IActionResult CreatePerson(SetPerson person)
		{
			string result = ControllerMethods.NameAndLastNameValid(person.FName, person.LName);

			if (result == null)
			{
				ViewBag.NameError = "Nom et prénom fourni, bravo";
				return View();
			}
			else
			{
				ViewBag.NameError = result;
			}
			return View();

		}



		[HttpGet]
		public IActionResult TestFormCollection()
		{
			SetPerson person = new SetPerson();
			return View(person);
		}

		[HttpPost]
		public IActionResult TestFormCollection(IFormCollection form)
		{
			foreach (string key in form.Keys)
			{
				Response.WriteAsync("Key = " + key + "  ");
				Response.WriteAsync("Value = " + form[key]);
				Response.WriteAsync("<br/>");
			}
			return View();
		}


		// TODO : To delete later on 
		[HttpGet]
		// GET: Ajax get the departements of the company
		public JsonResult GetDepartmentsByCompany(int companyId)
		{
			DepartmentService serv = new DepartmentService();
			List<Department> departments = new List<Department>();
			Department other = new Department
			{
				Id = 0,
				Name = "Autre",
			};

			// If Company is SNCB, provide departments without Arlon
			if (companyId == 1)
			{
				departments = serv.GetAllDepartmentByCompany(companyId).Where(s => s.Id != 2).Select(d => d.DalToDepartmentApp()).ToList();
				departments.Insert(0, other);
			}
			else
			{
				departments = serv.GetAllDepartmentByCompany(companyId).Select(d => d.DalToDepartmentApp()).ToList();
				if (departments.Count() > 0)
				{
					departments.Insert(0, other);
				}
			}

			//if (departments.Count() == 1 & departments[0].Id == 0 )
			//{
			//    departments = null;
			//}
			return Json(departments);
		}

		public ActionResult Test()
		{
			PersonService servP = new PersonService();
			IEnumerable<DAL.Entities.Person> people = servP.GetAll("Rina", "Toto").ToList();

			List<string> st = new List<string>();

			if (people != null)
			{
				foreach (var item in people)
				{
					st.Add(item.FName);
				}
			}
			ViewBag.Test = st;

			return View();
		}

		[HttpGet]
		public IActionResult AutoComplete()
		{
			return View();
		}

		public IActionResult Privacy()
		{
			return View();
		}

		#region Admin
		// GET : Home/Get
		[HttpGet]
		public IActionResult Get(int id)
		{
			AdminService serv = new AdminService();
			Admin admin = serv.Get(id).DalToAdminApp();
			return View(admin);
		}

		// GET : Home/GetAll
		[HttpGet]
		public IActionResult GetAll()
		{
			
			AdminService serv = new AdminService();
			IEnumerable<Admin> admins = serv.GetAll().Select(a => a.DalToAdminApp()).ToList();
			return View(admins);
		}

		// GET: Home/Insert
		[HttpGet]
		public IActionResult Insert()
		{
			return View();
		}

		//// POST: Home/Insert
		//[HttpPost]
		//[ValidateAntiForgeryToken]
		//public IActionResult Insert(Admin admin)
		//{
		//	try
		//	{
		//		AdminService serv = new AdminService();
		//		admin.SuperAdminId = 1;
		//		admin.Id = serv.Insert(admin.ToDal());
		//		return RedirectToAction("Get", "Home", new { @id = admin.Id });
		//	}
		//	catch
		//	{
		//		throw new NotImplementedException();
		//	}			
		//}

		// GET: Home/Update
		[HttpGet]
		public IActionResult Update(int id)
		{
			try
			{
				AdminService serv = new AdminService();
				Admin admin = serv.Get(id).DalToAdminApp();
				return View(admin);
			}
			catch (Exception)
			{

				throw new NotImplementedException();
			}
		}

		// POST: Home/Update
		//[HttpPost]
		//[ValidateAntiForgeryToken]
		//public IActionResult Update(Admin admin)
		//{
		//	try
		//	{
		//		AdminService serv = new AdminService();
		//		serv.Update(admin.ToDal());
		//		return RedirectToAction("Get", "Home", new { @id = admin.Id });
		//	}
		//	catch (Exception)
		//	{

		//		throw new NotImplementedException();
		//	}
		//}

		// GET: Home/Delete
		[HttpGet]
		public IActionResult Delete(int id)
		{
			AdminService serv = new AdminService();
			serv.Delete(id);
			return RedirectToAction("GetAll", "Home");
		}
		#endregion

		#region CompanyStatus
		// GET: Home/Get
		[HttpGet]
		public IActionResult GetCompanyStatus(int id)
		{
			try
			{
				CompanyStatusService serv = new CompanyStatusService();
				CompanyStatus companyStatus = serv.Get(id).DalToCompanyStatusApp();
				return View(companyStatus);
			}
			catch (Exception)
			{

				throw new NotImplementedException();
			}
		}

		// GET: Home/GetAll
		[HttpGet]
		public IActionResult GetAllCompanyStatus()
		{
			try
			{
				CompanyStatusService serv = new CompanyStatusService();
				IEnumerable<CompanyStatus> companyStatus = serv.GetAll().Select(p => p.DalToCompanyStatusApp()).ToList();
				return View(companyStatus);

			}
			catch (Exception)
			{

				throw new NotImplementedException();
			}
		}

		// GET: Home/Insert
		[HttpGet]
		public IActionResult InsertCompanyStatus()
		{
			return View();
		}

		// POST: Home/Insert
		[HttpPost]
		public IActionResult InsertCompanyStatus(CompanyStatus status)
		{
			try
			{
				CompanyStatusService serv = new CompanyStatusService();
				status.Id = serv.Insert(status.AppCompanyStatusToDal());
				//return View();
				return RedirectToAction("GetCompanyStatus", "Home", new { @id = status.Id });
			}
			catch (Exception)
			{

				throw new NotImplementedException();
			}			
		}

		// GET: Home/Update
		[HttpGet]
		public IActionResult UpdateCompanyStatus(int id)
		{
			try
			{
				CompanyStatusService serv = new CompanyStatusService();
				CompanyStatus companyStatus = serv.Get(id).DalToCompanyStatusApp();
				return View(companyStatus);
			}
			catch (Exception)
			{

				throw new NotImplementedException();
			}
		}

		// POST: Home/Update
		[HttpPost]
		public IActionResult UpdateCompanyStatus(CompanyStatus status)
		{
			try
			{
				CompanyStatusService serv = new CompanyStatusService();
				serv.Update(status.AppCompanyStatusToDal());
				return RedirectToAction("GetCompanyStatus", "Home", new { @id = status.Id });

			}
			catch (Exception)
			{

				throw new NotImplementedException();
			}
		}

		// GET: Home/Delete
		[HttpGet]
		public IActionResult DeleteCompanyStatus(int id)
		{
			try
			{
				CompanyStatusService serv = new CompanyStatusService();
				serv.Delete(id);
				return RedirectToAction("GetAllCompanyStatus", "Home");

			}
			catch (Exception)
			{

				throw;
			}
		}
		#endregion

		#region Company
		// GET: Home/GetCompany
		[HttpGet]
		public IActionResult GetCompany(int id)
		{
			try
			{
				CompanyService serv = new CompanyService();
				Company company = serv.Get(id).DalToCompanyApp();
				return View(company);
			}
			catch (Exception)
			{

				throw new NotImplementedException();
			}
		}

		// GET: Home/GetCompagnies
		[HttpGet]
		public IActionResult GetCompanies()
		{
			try
			{
				CompanyService serv = new CompanyService();
				IEnumerable<Company> companies = serv.GetAll().Select(c => c.DalToCompanyApp()).ToList();
				return View(companies);
			}
			catch (Exception)
			{

				throw;
			}		
		}

		// GET: Home/InsertCompany
		[HttpGet]
		public IActionResult InsertCompany()
		{
			return View();
		}

		// POST: Home/InsertCompany
		[HttpPost]
		public IActionResult InsertCompany(Company company)
		{
			try
			{
				CompanyService serv = new CompanyService();
				company.Id = serv.Insert(company.AppCompanyToDal());
				return RedirectToAction("GetCompany", "Home", new { @id = company.Id});
			}
			catch (Exception)
			{

				throw new NotImplementedException();
			}
		}

		// GET: Home/UpdateCompany
		[HttpGet]
		public IActionResult UpdateCompany(int id)
		{
			try
			{
				CompanyService serv = new CompanyService();
				Company company = serv.Get(id).DalToCompanyApp();
				return View(company);
			}
			catch (Exception)
			{

				throw new NotImplementedException();
			}
		}

		// POST: Home/UpdateCompany
		[HttpPost]
		public IActionResult UpdateCompany(Company company)
		{
			try
			{
				CompanyService serv = new CompanyService();
				serv.Update(company.AppCompanyToDal());
				return RedirectToAction("GetCompany", "Home", new { @id = company.Id });
				
			}
			catch (Exception)
			{

				throw new NotImplementedException();
			}
		}

		// GET: Home/DeleteCompany
		[HttpGet]
		public IActionResult DeleteCompany(int id)
		{
			try
			{
				CompanyService serv = new CompanyService();
				serv.Delete(id);
				return RedirectToAction("GetCompanies", "Home");
			}
			catch (Exception)
			{

				throw new NotImplementedException();
			}
		}
		#endregion

		#region Person

		// GET: Home/GetPersonById
		[HttpGet]
		public IActionResult GetPersonById(int id)
		{
			try
			{
				PersonService serv = new PersonService();
				Person person = serv.Get(id).DalToPersonApp();
				return View(person);
			}
			catch (Exception)
			{

				throw;
			}
		}

		// GET: Home/GetPerson
		[HttpGet]
		public IActionResult GetPerson()
		{
			try
			{
				PersonService serv = new PersonService();
				IEnumerable<Person> persons = serv.GetAll().Select(p => p.DalToPersonApp()).ToList();
				return View(persons);
			}
			catch (Exception)
			{

				throw new NotImplementedException();
			}
		}

		// GET: Home/InsertPerson
		[HttpGet]
		public IActionResult InsertPerson()
		{
			return View();
		}

		// POST: Home/InsertPerson
		[HttpPost]
		public IActionResult InsertPerson(Person person)
		{
			try
			{
				PersonService serv = new PersonService();
				person.Id = serv.Insert(person.PersonAppToDal());
				return RedirectToAction("GetPersonById", "Home", new { @id = person.Id });
			}
			catch (Exception)
			{

				throw;
			}
		}

		// GET: Home/UpdatePerson
		[HttpGet]
		public IActionResult UpdatePerson(int id)
		{
			try
			{
				PersonService serv = new PersonService();
				Person person = serv.Get(id).DalToPersonApp();
				return View(person);
			}
			catch (Exception)
			{

				throw;
			}
		}

		// POST: Home/UpdatePerson
		[HttpPost]
		public IActionResult UpdatePerson(Person person)
		{
			try
			{
				PersonService serv = new PersonService();
				serv.Update(person.PersonAppToDal());
				return RedirectToAction("GetPersonById", "Home", new { @id = person.Id });
			}
			catch (Exception)
			{

				throw;
			}
		}

		// GET: Home/DeletePerson
		[HttpGet]
		public IActionResult DeletePerson(int id)
		{
			try
			{
				PersonService serv = new PersonService();
				serv.Delete(id);
				return RedirectToAction("GetPerson", "Home");
			}
			catch (Exception)
			{

				throw;
			}
		}

		#endregion

		#region Register

		// GET: Home/InsertRegister
		[HttpGet]
		public IActionResult InsertRegister()
		{
			return View();
		}

		// POST: Home/InsertRegister
		[HttpPost]
		public IActionResult InsertRegister(AppRegisterVM.RegisterVM register)
		{
			try
			{
				RegisterService serv = new RegisterService();
				register.Id = serv.Insert(register.RegisterVMToDal());
				return RedirectToAction("GetRegisterById", "Home", new { @id = register.Id });
			}
			catch (Exception)
			{

				throw;
			}
		}

		// GET: Home/GetRegisterById
		[HttpGet]
		public IActionResult GetRegisterById(int id)
		{
			try
			{
				RegisterService serv = new RegisterService();
				Register register = serv.Get(id).DalToRegisterApp();
				return View(register);
			}
			catch (Exception)
			{

				throw;
			}
		}

		// GET: Home/UpdateRegsiter
		[HttpGet]
		public IActionResult UpdateRegister(int id)
		{
			try
			{
				RegisterService serv = new RegisterService();
				AppRegisterVM.RegisterVM register = serv.Get(id).DalToRegisterVM();
				return View(register);
				
			}
			catch (Exception)
			{

				throw;
			}
		}

		// POST: Home/UpdateRegister
		[HttpPost]
		public IActionResult UpdateRegister(AppRegisterVM.RegisterVM register)
		{
			try
			{
				RegisterService serv = new RegisterService();
				serv.Update(register.RegisterVMToDal());
				return RedirectToAction("GetRegisterById", "Home", new { @id = register.Id });
			}
			catch (Exception)
			{

				throw new NotImplementedException();
			}		
		}

		#endregion

		[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
		public IActionResult Error()
		{
			return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
		}
	}
}
