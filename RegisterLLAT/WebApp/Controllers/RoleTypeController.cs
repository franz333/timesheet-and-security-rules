﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApp.AppTools;
using WebApp.Models;
using WebApp.Models.RoleTypeViewModels;

namespace WebApp.Controllers
{
    public class RoleTypeController : Controller
    {
        // GET: RoleType
        public IActionResult Index()
        {
            RoleTypeService service = new RoleTypeService();
            List<RoleTypeVM> roles = service.GetAll().Select(r => r.DalToRoleTypeVM()).ToList();
            return View(roles);
        }

    }
}