﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL.Services;
using DALE = DAL.Entities;
using Microsoft.AspNetCore.Mvc;
using WebApp.AppTools;
using WebApp.Models;
using WebApp.Models.TestSettingsViewModels;

namespace WebApp.Controllers
{
	public class TestSettingsController : Controller
	{
		public IActionResult Index()
		{
			TestSettingsService service = new TestSettingsService();

			IEnumerable<TestSettingsVM> testSettingsVM = service.GetAll()
				.Select(t => t.DalToTestSettingsVM())
				.OrderBy(t => t.Treshold);

			return View(testSettingsVM);
		}

		[HttpGet]
		public IActionResult Edit(int id)
		{
			TestSettingsService service = new TestSettingsService();
			TestSettingsVM testVM = service.Get(id).DalToTestSettingsVM();

			// If the testSettings does not exist
			if (testVM == null)
			{
				ViewBag.ModalTitle = "Erreur";
				ViewBag.ModalMessage = "Ce paramètre de test n'existe pas.";
				return PartialView("_DisplayModal");
			}

			return PartialView("_TestSettings", testVM);
		}

		[HttpPost]
		public IActionResult Edit(TestSettingsVM test)
		{
			if (!ModelState.IsValid)
			{
				return PartialView("_TestSettings", test);
			}

			// Check that the test to update exists			
			string testSettingsExist = ControllerMethods.CheckTestSettingsExist(test.Id);

			if (testSettingsExist != null)
			{
				ViewBag.ModalTitle = "Erreur";
				ViewBag.ModalMessage = testSettingsExist;
				return PartialView("_DisplayModal");
			}

			// Update test
			TestSettingsService service = new TestSettingsService();
			try
			{
				service.Update(test.TestSettingsVMToDal());
			}
			catch (Exception e)
			{
				throw e;
			}

			// Return updated test for DOM insertion
			return PartialView("_TestSettingsTable", test);
		}

		[HttpGet]
		public IActionResult Create()
		{
			TestSettingsVM test = new TestSettingsVM();
			return PartialView("_AddTestSettings", test);
		}

		[HttpPost]
		public IActionResult Create(TestSettingsVM test)
		{
			if (!ModelState.IsValid)
			{
				return PartialView("_TestSettings", test);
			}

			TestSettingsService service = new TestSettingsService();
			try
			{
				test.Id = service.Insert(test.TestSettingsVMToDal());
			}
			catch (Exception e)
			{
				throw e;
			}

			return PartialView("_TestSettingsTable", test);
		}

		[HttpGet]
		public IActionResult AlertDelete(int id)
		{
			ViewBag.TestSettingsId = id;
			return PartialView("_AlertDelete");
		}
		
		[HttpPost]
		public IActionResult Delete(int id)
		{
			ViewBag.ModalTitle = "Suppression d'un jeu de paramètres de test";
			
			// Check that test exist and that it's not the default test
			ViewBag.ModalMessage = ValidateTestSettingsToDelete(id);

			if (ViewBag.ModalMessage != null)
			{
				return PartialView("_DisplayModal");
			}

			// Delete TestSettings
			TestSettingsService service = new TestSettingsService();
			try
			{
				service.Delete(id);
			}
			catch (Exception e)
			{
				throw e;
			}

			ViewBag.ModalMessage = "Le jeu de paramètres a été supprimé avec succès.";
			return PartialView("_DisplayModal");
		}

		/// <summary>
		/// Check that TestSettingsExists and that it's not the default test
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		[NonAction]
		private string ValidateTestSettingsToDelete(int id)
		{
			string result = null;			

			// Check that testSettingsExist
			string testCheck = ControllerMethods.CheckTestSettingsExist(id);

			if (testCheck != null)
			{
				return testCheck;
			}
			else
			{
				if (id == 1)
				{
					return "Le jeu de paramètres par défaut ne peut être supprimé.";
				}
			}

			return result;
		}

		/// <summary>
		/// TestSettings server side validation
		/// </summary>
		/// <param name="test"></param>
		/// <returns></returns>
		// Not necessary as validation taken care by data annotations
		//[NonAction]
		//private string ValidateTestSettings(TestSettingsVM test)
		//{
		//	string result = null;

		//	if (String.IsNullOrWhiteSpace(test.Title))
		//	{
		//		result = "Veuillez fournir un titre.";
		//	}

		//	if (test.Treshold < 5 || test.Treshold > 10)
		//	{
		//		if (result != null)
		//		{
		//			result += "<br>Le seuil doit être compris entre 5 et 10.";
		//		}
		//		result += "Le seuil doit être compris entre 5 et 10";
		//	}

		//	if (test.ValidityPeriod < 30 || test.ValidityPeriod > 360)
		//	{
		//		if (result != null)
		//		{
		//			result += "<br>La période de validité en cas de réussite doit être comprise entre 30 et 360.";
		//		}
		//		result += "La période de validité en cas de réussite doit être comprise entre 30 et 360.";
		//	}

		//	if (test.FailurePeriod < 15 || test.FailurePeriod > 45)
		//	{
		//		if (result != null)
		//		{
		//			result += "<br>La période de validité en cas d'échec doit être comprise entre 15 et 45.";
		//		}
		//		result += "La période de validité en cas d'échec doit être comprise entre 15 et 45.";
		//	}

		//	return result;
		//}
	}
}
