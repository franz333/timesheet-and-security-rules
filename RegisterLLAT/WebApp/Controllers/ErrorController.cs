﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace WebApp.Controllers
{
    public class ErrorController : Controller
    {
        public IActionResult Index(string message, string returnUrl)
        {
            ViewBag.Message = message;

            if (!string.IsNullOrWhiteSpace(returnUrl))
            {
                ViewBag.Url = returnUrl;
            }
			else
			{
                ViewBag.DashboardUrl = "/Admin";
			}
            return View();
        }

        [HttpGet]
        public IActionResult ErrorModal(string title, string message)
        {
            ViewBag.ModalTitle = title;
            ViewBag.Message = message;
            return PartialView("_ErrorModal");
        }
    }
}