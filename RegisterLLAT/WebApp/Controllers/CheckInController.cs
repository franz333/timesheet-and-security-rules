﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL.Services;
using DALE = DAL.Entities;
using Microsoft.AspNetCore.Mvc;
using WebApp.AppTools;
using WebApp.Models.CheckInViewModels;
using WebApp.Models.RegisterViewModels;
using WebApp.Models;
using Microsoft.AspNetCore.Authorization;

namespace WebApp.Controllers
{
    [AllowAnonymous]
	public class CheckInController : Controller
	{
        [HttpGet]
        public IActionResult RegisterType()
		{
			return View("RegisterType");
		}

        [HttpPost]
        public IActionResult RegisterType(bool isCheckingOut)
        {
            User user = GetUser();
            user.isCheckinOut = isCheckingOut;
            HttpContext.Session.SetComplexData("user", user);

            // Set Model with new value
            //CheckInVM register = new CheckInVM();
            //register.IsCheckingOut = isCheckingOut;

            return RedirectToAction("SetStatus");
        }

        [HttpGet]
        public IActionResult SetStatus()
        {
            CheckInVM register = new CheckInVM();
            return View("SetStatus", register);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult SetStatus(CheckInVM register)
        {
            User user = GetUser();
            
            // In case of next/previous check that status has not changed
            if (user.StatusId > 0 && user.StatusId != register.StatusId)
			{
                ResetUser(user);
			}

            // Check whether the status exist in the DB
            ViewBag.Error = ControllerMethods.CheckCompanyStatus(register.StatusId);
            if (ViewBag.Error != null)
            {
                return View("SetStatus", register);
            }

            // Save valid statusId in session
            user.StatusId = register.StatusId;
			if (user.StatusId == 1)
			{
                user.CompanyId = 1;
			}
            HttpContext.Session.SetComplexData("user", user);

            return RedirectToAction("SetCompany");
        }

        [HttpGet]
        public IActionResult SetCompany()
        {
            User user = GetUser();

            // extract to method
            CheckInVM register = new CheckInVM();
            register.IsCheckingOut = user.isCheckinOut;
            register.StatusId = user.StatusId;
            register.DepartmentId = user.DepartmentId;
            register.DepartmentName = user.DepartmentName;
            register.CompanyId = user.CompanyId;
            register.CompanyName = user.CompanyName;

            // If status is "Intern"
            if (register.StatusId == 1)
            {
                register.Companies = null;
                register.CompanyId = 1;

                // Get SNCB's departments
                register.Departments = GetDepartmentsForDropDown((int)register.CompanyId);

                if (register.Departments.Count() == 0)
                {
                    register.DepartmentId = 0;
                }
            }
            else
            {
                // Get companies by status 
                register.Companies = GetCompaniesForDropDown(register.StatusId);

                // Provide the company's dropdownlist
                if (register.Companies.Count() == 0)
                {
                    register.CompanyId = 0;
                }
            }
            return View("SetCompany", register);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult SetCompany(CheckInVM register, string nextBtn, string prevBtn)
        {
            User user = GetUser();

            if (prevBtn != null)
            {
                return RedirectToAction("SetStatus");
            }

            if (ModelState.IsValid)
            {
                if (nextBtn != null)
                {
                    // Server Side validation
                    ViewBag.Error = null;

                    // Get the statusId from session (just in case)                    
                    register.StatusId = user.StatusId;

                    if (register.StatusId == 1)
                    {
                        ViewBag.Error = SetCompanyCheckDepartment(register, user);
                    }
                    else if (register.StatusId > 1 && register.StatusId <= 3)
                    {
                        ViewBag.Error = SetCompanyCheckCompany(register, user);                       
                    }
					else
					{
                        ViewBag.Error = "Cette société n'est pas valide.";
					}
                }

                // Server side valtion error, return to view
                if (ViewBag.Error != null)
                {
                    register = ReturnSetCompanyToView(register);
                    return View("SetCompany", register);
                }
            }
			else
			{
                register = ReturnSetCompanyToView(register);
                return View("SetCompany", register);
            }

            // Save session
            HttpContext.Session.SetComplexData("user", user);

            return RedirectToAction("SetPerson");
        }

		[HttpGet]
        public IActionResult SetPerson()
        {
            User user = GetUser();
            CheckInVM register = new CheckInVM();
            register.IsCheckingOut = user.isCheckinOut;
            register.StatusId = user.StatusId;
            register.CompanyId = user.CompanyId;
            register.DepartmentId = user.DepartmentId;

            PersonService service = new PersonService();

			// The user is an intern (SNCB)
			if (register.StatusId == 1)
			{
                register.Persons = GetPersonsForDropDown(register);
            }
			else if (register.StatusId > 1 && register.StatusId <= 3)
			{
                register.Persons = GetPersonsForDropDown(register);
            }

            // Add "Autre" selected item if the list exist
            if (register.Persons.Count() > 0)
            {
                register.Persons.Insert(0, new PersonListVM { Id = 0, FName = null, LName = null });
            }
			else
			{
                register.PersonId = 0;
            }

            // Prepare dropdown for the visitee if the person is not an intern (SNCB Atelier d'Arlon)
            if (register.DepartmentId != 2)
			{
                register.PersonsVisited = GetPersonsForVisiteeDropDown(register);

                if (register.PersonsVisited.Count() > 0) 
                {
                    register.PersonsVisited.Insert(0, new PersonListVM { Id = 0, FName = "", LName = "" });
                }
				else
				{
                    register.PersonVisitedId = 0;
                } 
            }
			else
			{
                register.PersonsVisited = null;
			}

            return View("SetPerson", register);
        }

        [HttpPost]
        public IActionResult SetPerson(CheckInVM register, string prevBtn)
        {
			if (prevBtn != null)
			{
                return RedirectToAction("SetCompany");
            }

            User user = GetUser();

            // Get the statusId from session (just in case) + CompanyId                
            register.StatusId = user.StatusId;
            register.CompanyId = user.CompanyId;
            register.DepartmentId = user.DepartmentId;

            if (!ModelState.IsValid)
			{
                register = ReturnSetPersonToView(register);
                return View("SetPerson", register);
            }

            ViewBag.Error = null;
            ViewBag.NameError = null;
            ViewBag.VisitedError = null;

            // Check provided person
            PersonService service = new PersonService();

			if (register.PersonId > 0)
			{
                DALE.Person person = null; 

                if (register.StatusId == 1)
				{
                    person = service.Get((int)register.PersonId, (int)register.CompanyId, (int)register.DepartmentId);
                }
				else
				{
                    person = service.Get((int)register.PersonId, (int)register.CompanyId);
                }                

				if (person == null)
				{
                    ViewBag.Error = "Cette personne n'existe pas. Veuillez choisir une personne de la liste ou l'ajouter en cliquant sur \"Autre\"";
                }
			}
			else if (register.PersonId == 0)
			{
                // TODO : Loris
                if (String.IsNullOrWhiteSpace(register.FName) | String.IsNullOrWhiteSpace(register.LName))
				{
                    ViewBag.NameError = "Veuillez indiquer vos nom et prénom.";
                }
				else
				{
                    // Check that the person is not already in DB
                    DALE.Person checkPerson = null;

					if (register.StatusId == 1)
					{
                        // TODO : Loris
                        checkPerson = service.GetAll(register.LName, register.FName)
                            .Where(p => p.CompanyId == (int)register.CompanyId && p.DepartmentId == (int)register.DepartmentId)
                            .SingleOrDefault();
                    }
					else
					{
                        // TODO : Loris
                        checkPerson = service.GetAll(register.LName, register.FName)
                            .Where(p => p.CompanyId == (int)register.CompanyId)
                            .SingleOrDefault();
                    }

					if (checkPerson != null)
					{
                        ViewBag.NameError = "Une personne avec les mêmes nom et prénom existe déjà. Veuillez vous différencier (ex: deuxième nom de famille).";
                    }
				}
			}
			else
			{
                ViewBag.Error = "Veuillez choisir une personne de la liste ou l'ajouter en cliquant sur \"Autre\""; 
            }

			// Check provided visited person
			if (register.CompanyId != 1 && register.DepartmentId != 2)
			{
				if (register.PersonVisitedId != 0)
				{
                    ViewBag.VisitedError = SetPersonCheckVisitee(register);
                }
				else 
				{
                    register.PersonVisitedId = null;
                }
			}
			else
			{
                register.PersonVisitedId = null;
			}

            // Return view in case of error
            if (ViewBag.Error != null || ViewBag.NameError != null || ViewBag.VisitedError != null)
			{
                register = ReturnSetPersonToView(register);
                return View("SetPerson", register);
			}

            // Save potential new values
            register = SetPersonSaveNewValues(register);

            // Save session
            user.Id = (int)register.PersonId;
            //user.CompanyId = register.CompanyId;
            //user.DepartmentId = register.DepartmentId;
            //user.PersonVisited = register.PersonVisitedId;
            HttpContext.Session.SetComplexData("user", user);

            // Register the person
            RegisterService serviceR = new RegisterService();
            DALE.Register registerUser = SetPersonPrepareRegister(register);

            // TODO : Loris
			try
			{
                user.RegisterId = serviceR.Insert(registerUser);
            }
			catch (Exception)
			{
                // Redirect to error page, send data to admin for manual check (using session?)
				throw new NotImplementedException();
			}

            HttpContext.Session.SetComplexData("user", user);

            return RedirectToAction("RegisterSummary");
        }

        [HttpGet]
        public IActionResult RegisterSummary()
        {
            User user = GetUser();

            RegisterService serviceR = new RegisterService();

            RegisterVM register = serviceR.Get(user.RegisterId).DalToRegisterVM();

            ViewBag.IsCheckInOut = user.isCheckinOut;
            
            // Check if person has a test to take
            PersonService serviceP = new PersonService();
            DALE.Person person = serviceP.Get(user.Id);

			if (person.IsInOrder == 0 || person.IsInOrder == 2)
			{
                ViewBag.MustTakeTest = true;
			}
            
            return View("RegisterSummary", register);
        }

        [HttpGet]
        public IActionResult SecurityTest()
        {

            return View();
        }

		#region Methods


        [NonAction]
        private User GetUser()
        {
            var session = HttpContext.Session.GetComplexData<User>("user");

            if (session == null)
            {
                User user = new User();
                HttpContext.Session.SetComplexData("user", user);
            }
            return (User)HttpContext.Session.GetComplexData<User>("user");
        }

        /// <summary>
        /// Reset user cookie data to null
        /// </summary>
        /// <param name="user"></param>
        [NonAction]
        private void ResetUser(User user)
        {
            user.CompanyId = null;
            user.CompanyName = null;
            user.DepartmentId = null;
            user.DepartmentName = null;
            user.PersonVisited = null;
            user.Id = 0;
            HttpContext.Session.SetComplexData("user", user);
        }

        [NonAction]
        private List<Department> GetDepartmentsForDropDown(int companyId)
        {
            List<Department> departments = new List<Department>();

            DepartmentService service = new DepartmentService();

            departments = service.GetAllDepartmentByCompany(companyId).Select(d => d.DalToDepartmentApp()).ToList();

			if (departments.Count > 0)
			{
                departments.Insert(0, new Department { Id = 0, Name = "Autre" });
            }

            return departments;        
        }

        [NonAction]
        private List<Company> GetCompaniesForDropDown(int statusId)
        {
            List<Company> companies = new List<Company>();
            CompanyService service = new CompanyService();

            companies = service.GetAllCompanyByStatusWithoutDep(statusId).Select(c => c.DalToCompanyApp()).ToList();

			if (companies.Count() > 0)
			{
                companies.Insert(0, new Company { Id = 0, CompanyName = "Autre" });
            }
            return companies;
        }

        [NonAction]
        private List<PersonListVM> GetPersonsForDropDown(CheckInVM register)
        {
            PersonService service = new PersonService();

            List<PersonListVM> persons = new List<PersonListVM>();

            if (register.StatusId == 1)
            {
                if (register.DepartmentId > 0)
                {
                    // Prepare dropdown list of persons for that department
                    persons = service.GetPersonsByCompanyAndDepartment((int)register.CompanyId, (int)register.DepartmentId)
                        .Select(p => p.DalToPersonListVM())
                        .ToList();
                }
            }
            else
            {
                if (register.CompanyId > 0)
                {
                    // Prepare dropdown list of persons for that company
                    persons = service.GetAll((int)register.CompanyId)
                        .Select(p => p.DalToPersonListVM())
                        .ToList();
                }
            }
            return persons;
        }

        [NonAction]
        private List<PersonListVM> GetPersonsForVisiteeDropDown(CheckInVM register)
        {
            PersonService service = new PersonService();

            List<PersonListVM> persons = new List<PersonListVM>();

            persons = service.GetPersonsByCompanyAndDepartment(1, 2)
                .Select(p => p.DalToPersonListVM())
                .ToList();

            return persons;
        }

        [NonAction]
		private string SetCompanyCheckDepartment(CheckInVM register, User user)
		{
			string result = null;

			DepartmentService DServ = new DepartmentService();

			if (register.DepartmentId > 0)
			{
				// Check that department exists
				DALE.Department dep = DServ.Get((int)register.DepartmentId, (int)register.CompanyId);

                if (dep == null)
                {
                    result = "Ce département n'existe pas.";
                }
                    
			}
			else if (register.DepartmentId == 0)
			{
				// Check department's name
				if (String.IsNullOrWhiteSpace(register.DepartmentName))
				{
                    result = "Le nom du département n'est pas correct, veuillez recommencer.";
				}
				else
				{
					// Check whether the department is not already in DB
					DALE.Department dep = DServ.Get((int)register.CompanyId, register.DepartmentName);
                    if (dep != null)
                    {
                        result = "Ce département existe déjà, veuillez le sélectionner dans la liste.";
                    }
				}
			}
			else
			{
				ViewBag.Error = "Veuillez sélectionner un département ou l'ajouter s'il n'est pas dans (\"Autre\").";
			}

            if (result == null)
            {
                user.DepartmentId = register.DepartmentId;
                user.DepartmentName = register.DepartmentName;

                // Save session
                HttpContext.Session.SetComplexData("user", user);
            }

            return result;
		}

        [NonAction]
        private string SetCompanyCheckCompany(CheckInVM register, User user)
        {
            string result = null;

            CompanyService service = new CompanyService();
            DALE.Company company = new DALE.Company();

            // Check that provided company exists in DB
            if (register.CompanyId > 0)
			{
                company = service.Get((int)register.CompanyId, register.StatusId);

				if (company == null)
				{
                    result = "Cette société n'existe pas."; 
				}
			}

            // Check that provided company name does not exists in DB
			else if (register.CompanyId == 0)
			{
				if (String.IsNullOrWhiteSpace(register.CompanyName))
				{
                    result = "Le nom de votre société doit être renseigné, veuillez recommencer.";
                }
				else
				{
                    company = service.Get(register.CompanyName).FirstOrDefault();

					if (company != null)
					{
						if (company.CompanyStatusId != register.StatusId)
						{
                            result = String.Format("Cette société existe déjà sous le status {0}, veuillez recommencer le processus depuis le début en choissant ce status", company.StatusName);
                        }
						else
						{
                            result = "Cette société existe déjà, veuillez la sélectionner dans la liste.";
                        }                        
                    }
                }
			}
			else
			{
                result = "Veuillez sélectionner une société dans la liste ou l'ajouter en cliquant sur \"Autre\".";
            }

			if (result == null)
			{
                user.CompanyId = register.CompanyId;
                user.CompanyName = register.CompanyName;

                // Save session
                HttpContext.Session.SetComplexData("user", user);
            }
            return result;
        }

        [NonAction]
        private CheckInVM ReturnSetCompanyToView(CheckInVM register)
        {
            CheckInVM result = register;

            if (register.StatusId == 1)
            {
                // Prepare the list of Departments to return to View
                register.Departments = GetDepartmentsForDropDown((int)register.CompanyId);
                if (register.Departments.Count() == 0)
                {
                    register.DepartmentId = 0;
                }
            }
			else if (register.StatusId > 1 && register.StatusId <= 3)
			{
                // Prepare the list of companies to return to View
                register.Companies = GetCompaniesForDropDown(register.StatusId);
				if (register.Companies.Count() == 0)
				{
                    register.CompanyId = 0;
				}
            }

            return result;
        }

        [NonAction]
        private CheckInVM ReturnSetPersonToView(CheckInVM register)
        {
            // The user is an intern (SNCB)
            if (register.StatusId == 1)
            {
                register.Persons = GetPersonsForDropDown(register);
            }
            else if (register.StatusId > 1 && register.StatusId <= 3)
            {
                register.Persons = GetPersonsForDropDown(register);
            }

            // Add "Autre" selected item if the list exist
            if (register.Persons.Count() > 0)
            {
                register.Persons.Insert(0, new PersonListVM { Id = 0, FName = null, LName = null });
            }
            else
            {
                register.PersonId = 0;
            }

            // Prepare dropdown for the visitee if the person is not an intern (SNCB Atelier d'Arlon)
            if (register.DepartmentId != 2)
            {
                register.PersonsVisited = GetPersonsForVisiteeDropDown(register);

                if (register.PersonsVisited.Count() > 0)
                {
                    register.PersonsVisited.Insert(0, new PersonListVM { Id = 0, FName = "", LName = "" });
                }
                else
                {
                    register.PersonVisitedId = 0;
                }
            }
            else
            {
                register.PersonsVisited = null;
            }

            return register;
        }

        [NonAction]
        private CheckInVM SetPersonSaveNewValues(CheckInVM register)
        {
            // Save potential new values
            if (register.StatusId == 1 && register.DepartmentId == 0 && register.DepartmentName != "")
            {
                // TODO : Loris
                DepartmentService serviceD = new DepartmentService();
                register.DepartmentId = serviceD.Insert(new DALE.Department
                {
                    CompanyId = (int)register.CompanyId,
                    DepartmentName = register.DepartmentName,
                });
            }

            if (register.StatusId > 1 && register.StatusId <= 3 && register.CompanyId == 0 && register.CompanyName != "")
            {
                CompanyService serviceC = new CompanyService();
                register.CompanyId = serviceC.Insert(new DALE.Company
                {
                    CompanyStatusId = register.StatusId,
                    CompanyName = register.CompanyName,
                });
            }

            if (register.PersonId == 0 && register.FName != "" && register.LName != "")
            {
                PersonService service = new PersonService();

                register.PersonId = service.Insert(new DALE.Person
                {
                    FName = register.FName,
                    LName = register.LName,
                    CompanyId = (int)register.CompanyId,
                    DepartmentId = register.DepartmentId,
                    AdministratorId = 1 // TODO : change with a dummy administrator
                });
            }

            return register;
        }

        [NonAction]
        private DALE.Register SetPersonPrepareRegister(CheckInVM register)
        {
            DALE.Register registerUser = new DALE.Register();

            registerUser.PersonId = (int)register.PersonId;
            registerUser.TimeIn = DateTime.Now;

            if (register.IsCheckingOut == true)
            {
                registerUser.TimeOut = DateTime.Now;
            }

            if (registerUser.PersonVisited > 0)
            {
                registerUser.PersonVisited = register.PersonVisitedId;
            }
            else
            {
                registerUser.PersonVisited = null;
            }

            return registerUser;
        }

        [NonAction]
        private string SetPersonCheckVisitee(CheckInVM register)
        {
            string result = null;
            PersonService service = new PersonService();

            // Check that visited person exists in DB
            DALE.Person checkVisitee = service.GetPersonVisited((int)register.PersonVisitedId);

            if (checkVisitee == null)
            {
                ViewBag.VisitedError = "Cette personne n'existe pas, veuillez sélectionner un élément de la liste.";
            }

            return result;
        }
        #endregion
    }
}
