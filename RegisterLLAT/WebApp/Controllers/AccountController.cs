﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using DAL.Services;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApp.AppTools;
using WebApp.Models;
using WebApp.Models.AccountViewModels;

namespace WebApp.Controllers
{
    public class AccountController : Controller
    {
        //private readonly IMemoryCache _cache;

        //public AccountController(IMemoryCache cache)
        //{
        //    _cache = cache;
        //}
               
        // GET : Login
        
        [HttpGet]
        [AllowAnonymous]
        public ActionResult Login()
        {
            UserAccountLoginForm form = new UserAccountLoginForm();
            return View(form);            
        }

        // POST : Login
        [HttpPost]
        [AllowAnonymous]
        public ActionResult Login(UserAccountLoginForm form, string returnUrl)
        {
            // #TODO implement errors and viewbags 
            if (!ModelState.IsValid)
            {
                return View();
            }

            Admin loggedAdmin = form.LoginCheck();

            if (loggedAdmin != null)
            {
                if (!loggedAdmin.Deleted || !loggedAdmin.IsActive)
                {
                    string name = StringCustomMethods.GetInitialsFromFirstName(loggedAdmin.FName) + loggedAdmin.LName;

                    var userClaims = new List<Claim>()
                    {
                        new Claim(ClaimTypes.Name, name),
                        new Claim(ClaimTypes.Role, loggedAdmin.Role),
                        new Claim("Id", loggedAdmin.Id.ToString()),
                        new Claim("RoleType", loggedAdmin.Role),
                    };

                    // Passes list of claims and AuthenticationTypes
                    var claimsListAndAuthTypes = new ClaimsIdentity(userClaims, "User Identity");

                    // Accept an array of ClaimsIdentity.
                    var userPrincipal = new ClaimsPrincipal(new[] { claimsListAndAuthTypes });

                    var authProperties = new AuthenticationProperties
                    {
                        //AllowRefresh = true,    // Refreshing the authentication session should be allowed.
                        //ExpiresUtc = DateTimeOffset.UtcNow.AddDays(days),
                        IsPersistent = form.IsPersistent,
                        IssuedUtc = DateTimeOffset.UtcNow,
                        //RedirectUri = <string>
                    };

                    // Passes ClaimsPrinciple to it as paramater. This method will create a cookie into the browser
                    HttpContext.SignInAsync("CookieAuthentication", userPrincipal, authProperties);

                    if (!string.IsNullOrWhiteSpace(returnUrl) && Url.IsLocalUrl(returnUrl))
                    {
                        return Redirect(returnUrl);
                    }
                    return RedirectToAction("Index", "Admin", new { id = loggedAdmin.Id });
                }
                else
                {
                    ViewBag.Error = "L'utilisateur n'existe plus ou a été désactivé.";
                }
            }
            else
            {
                ViewBag.Error = "Aucun utilisateur ne correspond à ces identifiants.";
            }
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync("CookieAuthentication");
            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult AccessDenied()
        {           
            // Check if user is Logged in
   //         var userId = int.Parse(User.FindFirstValue(ClaimTypes.NameIdentifier));

			//if (userId > 0)
			//{
   //             ViewBag.Connected = true;
			//}

            return View();
        }
    }
}