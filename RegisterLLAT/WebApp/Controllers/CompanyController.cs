﻿using System;
using System.Collections.Generic;
using System.Linq;
using DAL.Services;
using DALE = DAL.Entities;
using Microsoft.AspNetCore.Mvc;
using WebApp.AppTools;
using WebApp.Models;
using WebApp.Models.CompanyViewModels;
using System.Net.NetworkInformation;
using Microsoft.AspNetCore.Authorization;

namespace WebApp.Controllers
{
    [Authorize(Roles = "Super Admin, Full Admin, HR Admin")]
    //[Authorize("SuperAdmin")] 
    public class CompanyController : Controller
    {
        public IActionResult Index()
        {
            CompanyService serv = new CompanyService();

            IEnumerable<CompanyForm> companies = serv.GetAll().OrderBy(c => c.CompanyName).Select(c => c.DalToCompanyForm()).ToList();

            // Prepare ViewBag for modal display in view in case of redirectToAction from Edit, Activate, Deactivate
            if (TempData["title"] != null && TempData["message"] != null)
            {
                ViewBag.ModalTitle = TempData["title"];
                TempData.Remove("title");

                ViewBag.ModalMessage = TempData["message"];
                TempData.Remove("message");
            }

            // Prepare filter dropdown list
            CompanyStatusService companyStatusService = new CompanyStatusService();
            ViewBag.StatusDropDown = companyStatusService.GetAll();

            return View(companies);
        }

        [HttpPost]
        public IActionResult Filter(int statusId, string search, bool isNew = false)
        {
            CompanyService service = new CompanyService();
            IEnumerable<CompanyForm> compagnies;

            if (String.IsNullOrWhiteSpace(search))
            {
                // If the user makes no choice
                if (statusId == 0)
                {
                    compagnies = service.GetAll().Select(q => q.DalToCompanyForm()).ToList();
                }
                else
                {
                    compagnies = service.GetAll(statusId).Select(q => q.DalToCompanyForm()).ToList();
                }
            }
            else
            {
                // If the user makes no choice
                if (statusId == 0)
				{
                    compagnies = service.GetAll(search).Select(q => q.DalToCompanyForm()).ToList();
                }
				else
				{
                    compagnies = service.GetAll(search, statusId).Select(q => q.DalToCompanyForm()).ToList();
                }                
            }

            // Filter companies added while user registration (administratorId is null)
			if (isNew == true)
			{
                compagnies = compagnies.Where(c => c.AdministratorId == null);
			}
            return PartialView("_CompanyFilter", compagnies);
        }

        [HttpGet]
        public IActionResult Create()
        {
            CompanyForm form = new CompanyForm();
            return View(form);
        }

        [HttpPost]
        public IActionResult Create(CompanyForm form)
        {
            // Client side validation
            if (!ModelState.IsValid)
            {
                return View(form);
            }

            // Check that status and test are valid and Prevent duplicate entries in the DB
            ViewBag.Error = ValidateCompany(form);            
            
            // If validation is OK
            if (ViewBag.Error == null)
			{
                CompanyService serv = new CompanyService();
                DALE.Company company = new DALE.Company()
                {
                    CompanyName = form.CompanyName,
                    CompanyStatusId = form.CompanyStatusId,
                    TestId = form.TestId,
                };

				try
				{
                    company.Id = serv.Insert(company);
                }
				catch (Exception e)
				{
					throw e;
				}

                TempData["title"] = "Création d'une société";
                TempData["message"] = "La société a été ajoutée avec succès.";

                return RedirectToAction("Details", new { id = company.Id });
            }     
            return View(form);
        }

        [HttpGet]
        public IActionResult Details(int id)
        {
            // Check that company exist in DB
            DALE.Company companyExist = ControllerMethods.CheckCompanyExist(id);
            if (companyExist == null)
            {
                TempData["title"] = "Erreur";
                TempData["message"] = "La société que vous souhaitez consulter n'existe pas.";
                return RedirectToAction("Index");
            }

            // In case of redirection from edit or create prepare modal display
            if (TempData["title"] != null && TempData["message"] != null)
            {
                ViewBag.ModalTitle = TempData["title"];
                TempData.Remove("title");

                ViewBag.ModalMessage = TempData["message"];
                TempData.Remove("message");
            }

            CompanyForm companyForm = companyExist.DalToCompanyForm();

            TestService testService = new TestService();
            companyForm.TestName = testService.Get(companyForm.TestId).Title;

            return View(companyForm);
        }

        [HttpGet]
        public IActionResult Edit(int id)
        {
            // Check that company exist in DB
            DALE.Company companyExist = ControllerMethods.CheckCompanyExist(id);
			if (companyExist == null)
			{
                TempData["title"] = "Erreur";
                TempData["message"] = "La société que vous souhaitez éditer n'existe pas.";
                return RedirectToAction("Index");
            }
            
            CompanyForm companyForm = companyExist.DalToCompanyForm();

            return View(companyForm);
        }

        [HttpPost]
        public IActionResult Edit(CompanyForm company)
        {
            // Client side validation
            if (!ModelState.IsValid)
            {
                return View(company);
            }

            // Check that company exist in DB
            DALE.Company companyExist = ControllerMethods.CheckCompanyExist(company.Id);
            
            if (companyExist == null)
            {
                TempData["title"] = "Erreur";
                TempData["message"] = "La société que vous souhaitez éditer n'existe pas.";
                return RedirectToAction("Index");
            }

            // Check that status and testId are valid + avoid duplicate names for company in DB
            ViewBag.Error = ValidateCompany(company);

            // Return view in case of error
			if (ViewBag.Error != null)
			{
                return View(company);
            }

            // Update company if all the tests are valid
            CompanyService companyService = new CompanyService();            
            DALE.Company companyToEdit = company.CompanyFormToDal();

			try
			{
                companyService.Update(companyToEdit);
            }
			catch (Exception e)
			{
				throw e;
			}

            TempData["title"] = "Edition de la société";
            TempData["message"] = "La société a été éditée avec succès.";
            
            return RedirectToAction("Details", new { id = companyToEdit.Id } );
        }

        /// <summary>
        /// Alert user that softdeleting a company will prevent its employees from accessing register and test
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult SoftDelete()
        {
            return PartialView("_SoftDelete");
        }

        /// <summary>
        /// Alert user that hardDeleting a company will hardDelete its employees along with their linked registers, registersPlanning, tests.
        /// The departments linked to the company will also be removed.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult HardDelete()
        {
            return PartialView("_HardDelete");
        }

        [HttpGet]
        public IActionResult Delete(int id)
        {
            // Check that company exist in DB
            DALE.Company companyExist = ControllerMethods.CheckCompanyExist(id);

            if (companyExist == null)
            {
                TempData["title"] = "Erreur";
                TempData["message"] = "La société que vous souhaitez supprimer n'existe pas.";
                return RedirectToAction("Index");
            }

            // Delete company
            CompanyService service = new CompanyService();
			try
			{
                service.Delete(id);
            }
			catch (Exception e)
			{
				throw e;
			}

            TempData["title"] = "Suppression de la société";
            TempData["message"] = "La société a été supprimée avec succès.";

            return RedirectToAction("Index");
        }


        /// <summary>
        /// Check if the name of the company does not already exist in DB
        /// </summary>
        /// <param name="company"></param>
        /// <returns></returns>
        [NonAction]
        private string AvoidDuplicateCompanyName(CompanyForm company)
        {
            string result = null;

            CompanyService serv = new CompanyService();
            IEnumerable<Company> companies = serv.GetAll().Select(c => c.DalToCompanyApp());
            var isInDB = companies.Where(c => c.CompanyName.Equals(company.CompanyName, StringComparison.OrdinalIgnoreCase) && c.Id != company.Id)
                .Select(c => c)
                .FirstOrDefault();

            if (isInDB != null)
            {
                result = "Le nom de cette société existe déjà; deux sociétés avec le même noms ne peuvent être enregistrées.";
            }

            return result;
        }

        /// <summary>
        /// Check that the test exists in DB
        /// </summary>
        /// <param name="testId"></param>
        /// <returns></returns>
        [NonAction]
        private string CheckTest(int testId)
        {
            string result = null;

			if (testId == 0)
			{
                return "Veuillez sélectionner un test dans la liste.";
			}

            TestService service = new TestService();
            DALE.Test test = service.Get(testId);

			if (test == null)
			{
                return "Le test n'existe pas.";
            }

            return result;        
        }

        /// <summary>
        /// Validate company checking if status and test exist in DB
        /// </summary>
        /// <param name="company"></param>
        /// <returns></returns>
        [NonAction]
        private string ValidateCompany(CompanyForm company)
        {
            string result = null;

            // Check that status exists
            result = ControllerMethods.CheckCompanyStatus(company.CompanyStatusId);
            if (result != null)
            {
                return result;
            }

            // Check that test exists
            result = CheckTest(company.TestId);
            if (result != null)
            {
                return result;
            }

            // Avoid duplicate company name in DB
            result = AvoidDuplicateCompanyName(company);
            if (result != null)
            {
                return result;
            }

            return result;
        }
    }
}