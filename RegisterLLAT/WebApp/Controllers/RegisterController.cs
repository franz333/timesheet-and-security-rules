﻿using System;
using System.Collections.Generic;
using System.Linq;
using DAL.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using WebApp.AppTools;
using WebApp.Facades.Register;
using WebApp.Models;
using WebApp.Models.PersonTestViewModels;
using WebApp.Models.RegisterViewModels;
using WebApp.Models.TestViewModels;
using DALE = DAL.Entities;


namespace WebApp.Controllers
{
    [AllowAnonymous]
    public class RegisterController : Controller
    {
        #region Register Wizard Form

        [HttpGet]
        public IActionResult Index()
        {   
            return View("Index");        
        }

        [HttpPost]
        public IActionResult Index(bool isCheckingOut)
        {
            User user = GetUser();
            user.isCheckinOut = isCheckingOut;
            HttpContext.Session.SetComplexData("user", user);

            // Set Model with new value
            //CheckInVM register = new CheckInVM();
            //register.IsCheckingOut = isCheckingOut;

            return RedirectToAction("SetStatus");
        }

        [HttpGet]
        public IActionResult SetStatus()
        {
            FormRegisterVM register = new FormRegisterVM();
            return View(register);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult SetStatus(FormRegisterVM register)
        {
            User user = GetUser();

            // In case of next/previous check that status has not changed
            if (user.StatusId > 0 && user.StatusId != register.StatusId)
            {
                ResetUser(user);
            }

            // Check whether the status exist in the DB
            ViewBag.Error = ControllerMethods.CheckCompanyStatus(register.StatusId);
            if (ViewBag.Error != null)
            {
                return View("SetStatus", register);
            }

            // If validation is OK, save valid statusId in session
            user.StatusId = register.StatusId;
            if (user.StatusId == 1)
            {
                user.CompanyId = 1;
            }
            HttpContext.Session.SetComplexData("user", user);

            return RedirectToAction("SetCompany");
        }

        [HttpGet]
        public IActionResult SetCompany()
        {
            User user = GetUser();

            FormRegisterVM register = new FormRegisterVM();
            register = register.UserToSetCompanyProps(user);

            register = register.SetCompany(register); 

            return View(register);
        }        

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult SetCompany(FormRegisterVM register, string nextBtn, string prevBtn)
        {
            if (prevBtn != null)
            {
                return RedirectToAction("SetStatus");
            }            

			if (nextBtn != null)
			{
                if (!ModelState.IsValid)
                {
                    register = register.SetCompany(register);
                    return View("SetCompany", register);
                }

                User user = GetUser();
                
                // Force statusId value to be like session value (to prevent manual DOM changes)
                register.StatusId = user.StatusId;

                // Validation
                // TODO : Manage autocomplete possibility
                ViewBag.Error = register.ValidateSetCompany(register);
                if (ViewBag.Error != null)
                {
                    register = register.SetCompany(register);
                    return View("SetCompany", register);
                }

                // If validation is OK
                user.CompanyId = register.CompanyId;
                user.CompanyName = register.CompanyName;
                user.DepartmentId = register.DepartmentId;
                user.DepartmentName = register.DepartmentName;
                HttpContext.Session.SetComplexData("user", user);
            }
            return RedirectToAction("SetPerson", "Register");

            #region archives
            //CompanyService servC = new CompanyService();
            //DepartmentService servD = new DepartmentService();

            //if (!ModelState.IsValid)
            //{
            //    // Get companies by status 
            //    company.Companies = servC.GetAllCompanyByStatusWithoutDep(user.StatusId).Select(c => c.DalToCompanyApp()).ToList();

            //    if (company.Companies.Count() > 0)
            //    {
            //        // Add "Autre" in the dropdownlist
            //        Company other = new Company { Id = 0, CompanyName = "Autre" };
            //        company.Companies.Insert(0, other);

            //        company.Options = company.Companies.Select(a => new SelectListItem
            //        {
            //            Value = a.Id.ToString(),
            //            Text = a.CompanyName
            //        }).ToList();
            //    }
            //    return View(company);
            //}
            //else
            //{
            //    // Check if the field IdAutoComplete exists..if so this value is given to companyId
            //    if (company.IdAutoComplete > 0)
            //    {
            //        company.Id = company.IdAutoComplete;
            //    }

            //    // A company is selected from the list
            //    if (company.Id > 0)
            //    {
            //        // In case the user makes use of the input field to type in the company name
            //        // The user can then pick up a company from the autocomplete list
            //        // We need to ...

            //        // ... verify the user.StatusId equals the CompanyStatusId
            //        // the suggestions of the autocomplete display all compagnies regardless of companyStatus
            //        if (company.CompanyStatusId != user.StatusId)
            //        {
            //            user.StatusId = company.CompanyStatusId;
            //        }

            //        // Check whether the companyId exists in the DB and corresponds to the status                   
            //        Company companyCheck = servC.Get((int)company.Id, user.StatusId).DalToCompanyApp();

            //        if (companyCheck == null)
            //        {
            //            // TODO : log the error
            //            ViewBag.Error = "Cette société n'existe pas.";
            //            SetCompany companyError = ReturnCompanyError(user.StatusId);       
            //            return View(companyError);
            //        }
            //        else
            //        {
            //            // Set the user session's companyId
            //            user.CompanyId = (int)company.Id;

            //            // Check whether the company requires an associated department
            //            int hasDepartment = servD.GetAllDepartmentByCompany((int)company.Id).Count();

            //            // A department is needed
            //            if (hasDepartment >= 1)
            //            {
            //                // A department is selected from the list
            //                if (company.DepartmentId > 0)
            //                {
            //                    // Check whether the department exists in the DB
            //                    Department departmentCheck = servD.Get((int)company.DepartmentId, (int)company.Id).ToApp();

            //                    if (departmentCheck != null)
            //                    {
            //                        // Avoid that a person selects SNCB and Atelier de traction d'Arlon
            //                        if (company.Id == 1 & company.DepartmentId == 2)
            //                        {
            //                            SetCompany companyError = ReturnCompanyError(user.StatusId);
            //                            ViewBag.Error = "Le département de cette société n'est pas correct.";                               
            //                            return View(companyError);
            //                        }
            //                        else
            //                        {
            //                            user.DepartmentId = company.DepartmentId;
            //                        }
            //                    }
            //                    else
            //                    {
            //                        SetCompany companyError = ReturnCompanyError(user.StatusId);
            //                        ViewBag.Error = "Ce département n'existe pas, veuillez recommencer.";
            //                        return View(companyError);
            //                    }
            //                }
            //                // User typed the name of the department with input text
            //                else if (company.DepartmentId == 0)
            //                {
            //                    // Check whether the input is valid
            //                    if (String.IsNullOrWhiteSpace(company.DepartmentName))
            //                    {
            //                        ViewBag.Error = "Le nom du département n'est pas correct, veuillez recommencer.";

            //                        SetCompany companyError = ReturnCompanyError(user.StatusId);
            //                        return View(companyError);
            //                    }
            //                    else
            //                    {
            //                        // Check whether the department is not already in DB
            //                        Dal.Department departmentExist = servD.Get((int)company.Id, company.DepartmentName);
            //                        if (departmentExist != null)
            //                        {
            //                            company.DepartmentId = departmentExist.Id;
            //                            company.DepartmentName = departmentExist.DepartmentName;
            //                            user.DepartmentId = departmentExist.Id;
            //                        }
            //                    }
            //                    user.DepartmentName = company.DepartmentName;
            //                    // TODO : Save the new department
            //                }
            //                else
            //                {             
            //                    // The user chose a compagny from the autocomplete and a department must be provided
            //                    if (company.IdAutoComplete > 0)
            //                    {
            //                        ViewBag.Error = "Un département doit être renseigné pour cette société, veuillez recommencer.";
            //                    }
            //                    else
            //                    {
            //                        ViewBag.Error = "Un département valide doit être renseigné, veuillez recommencer.";
            //                    }
            //                    SetCompany companyError = ReturnCompanyError(user.StatusId);
            //                    return View(companyError);
            //                }
            //            }
            //            else
            //            {
            //                if (company.DepartmentId != null)
            //                {
            //                    ViewBag.Error = "Aucun département n'est nécessaire.";
            //                    SetCompany companyError = new SetCompany();
            //                    companyError.Companies = servC.GetAllCompanyByStatusWithoutDep(user.StatusId).Select(c => c.DalToCompanyApp()).ToList();

            //                    companyError.Options = new List<SelectListItem>();

            //                    foreach (var item in companyError.Companies)
            //                    {
            //                        if (item.Id == company.Id)
            //                        {
            //                            companyError.Options.Add(new SelectListItem
            //                            {
            //                                Value = item.Id.ToString(),
            //                                Text = item.CompanyName,
            //                                Selected = true
            //                            });
            //                        }
            //                        else
            //                        {
            //                            companyError.Options.Add(new SelectListItem
            //                            {
            //                                Value = item.Id.ToString(),
            //                                Text = item.CompanyName
            //                            });
            //                        }
            //                    }
            //                    return View(companyError);
            //                }
            //                else
            //                {
            //                    user.DepartmentId = company.DepartmentId;
            //                }                            
            //            }
            //        }
            //    }
            //    // The user typed the name of the company with input text
            //    else if (company.Id == 0)
            //    {
            //        // Check whether the input is not null or whitespace
            //        if (String.IsNullOrWhiteSpace(company.Name))
            //        {
            //            ViewBag.Error = "Le nom de votre société doit être renseigné, veuillez recommencer.";
            //            SetCompany companyError = ReturnCompanyError(user.StatusId);
            //            return View(companyError);
            //        }

            //        // Check whether the company's name is already in DB
            //        Dal.Company companyCheck = servC.Get(company.Name).FirstOrDefault();

            //        // If the company is found in the DB
            //        if (companyCheck != null)
            //        {
            //            int hasDepartment = servD.GetAllDepartmentByCompany((int)company.Id).Count();

            //            if (hasDepartment > 0)
            //            {
            //                ViewBag.Error = "Un département doit être renseigné pour cette société, veuillez recommencer.";
            //                SetCompany companyError = ReturnCompanyError(user.StatusId);
            //                return View(companyError);
            //            }
            //            else
            //            {
            //                // We set the company's values to the session
            //                user.CompanyId = companyCheck.Id;
            //                user.CompanyName = companyCheck.CompanyName;
            //                user.StatusId = companyCheck.CompanyStatusId;
            //            }
            //        }
            //        else
            //        {
            //            // If the name exists already we just assign the value
            //            user.CompanyName = companyCheck.CompanyName;

            //            // #TODO tell the user about it and let him decide what do to...
            //            // The company could belong to another status that the one selected by the user
            //        }                    
            //    }
            //    else
            //    {
            //        ViewBag.Error = "Le nom de votre société n'est pas correct.";
            //        SetCompany companyError = ReturnCompanyError(user.StatusId);
            //        return View(companyError);
            //    }
            //    // Everything successed. Register the user session and redirect to the next form of the wizard
            //    HttpContext.Session.SetComplexData("user", user);

            //    // If the user is checking out
            //    if (user.isCheckinOut)
            //    {
            //        return RedirectToAction("SearchRegister", "Register");
            //    }
            //    return RedirectToAction("SetPerson", "Register");
            //}
            #endregion
        }

        [HttpGet]
        public IActionResult SetPerson()
        {
            User user = GetUser();

            FormRegisterVM register = new FormRegisterVM();
            register = register.UserToSetCompanyProps(user);

            // Prepare model with dropdowns
            register = register.SetPerson(register);

            #region Archives
            //// Prepare dropdown for a user intern (SNCB) or extern
            //if (register.StatusId == 1 && register.CompanyId == 1)
            //{
            //    register.Persons = register.GetPersonsForDropDown((int)register.CompanyId, (int)register.DepartmentId);
            //}
            //else
            //{
            //    register.Persons = register.GetPersonsForDropDown((int)register.CompanyId);
            //}

            //// Set Person Name input in case the list is empty
            //if (register.Persons.Count() == 0)
            //{
            //    register.PersonId = 0;
            //}

            //// Prepare dropdown for the visited person
            //if (register.DepartmentId != 2)
            //{
            //    register.PersonsVisited = register.GetPersonsForDropDown(1, 2);

            //    if (register.PersonsVisited.Count() == 0)
            //    {
            //        register.PersonVisitedId = 0;
            //    }      
            //}
            ////else
            ////{
            ////    register.PersonsVisited = null;
            ////}
            #endregion
            return View("SetPerson", register);
            #region Archives
            //SetPerson person = new SetPerson();

            //PersonService serv = new PersonService();

            //// Check whether a person list can be retrieved or not. There won't be any person list...
            //// ... if the company was added by the user
            //// ... if the company has no recorded person 

            //// If the person used the input to provide the company's name
            //if (user.CompanyId == 0)
            //{
            //    person.Id = 0;
            //}
            //// Populate the list of persons for the user to find its name.
            //else if (user.CompanyId > 0)
            //{                
            //    // The person is linked to a departement
            //    if (user.DepartmentId != null)
            //    {
            //        person.Persons = serv.GetPersonsByCompanyAndDepartment((int)user.CompanyId, (int)user.DepartmentId).Select(p => p.ToAppPersonListVM()).ToList();
            //    }
            //    // The person is not linked to any department.
            //    else
            //    {
            //        person.Persons = serv.GetAll((int)user.CompanyId).Select(p => p.ToAppPersonListVM()).ToList();
            //    }

            //    // If there is a list of person, populate the options otherwise set person.Id = 0
            //    if (person.Persons.Count() > 0)
            //    {
            //        // Add "Autre" to Dropdown list
            //        PersonListVM personOther = new PersonListVM { Id = 0, LName = null, FName = null};
            //        person.Persons.Insert(0, personOther);

            //        person.OptionsPersons = person.Persons.Select(a => new SelectListItem
            //        {
            //            Value = a.Id.ToString(),
            //            Text = a.Name
            //        }).ToList();
            //    }
            //    // If the list is empty we set person.Id = 0 to display input fields
            //    else
            //    {
            //        person.Id = 0;
            //    }
            //}

            //// If the person is a visitor, the visted person is required
            //if (user.StatusId == 3)
            //{
            //    // Populate the list of person for the user to find the name of the person he wants to visit
            //    // The parameter 1 of the underneath method correspond to companyId = 1 (SNCB) && departmentId = 2 (Arlon)
            //    person.PersonsVisited = serv.GetPersonsByCompanyAndDepartment(1, 2).Select(p => p.ToAppPersonListVM()).ToList();

            //    person.OptionsPVisited = person.PersonsVisited.Select(a => new SelectListItem
            //    {
            //        Value = a.Id.ToString(),
            //        Text = a.Name
            //    }).ToList();
            //}
            //return View(person);
            #endregion
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult SetPerson(FormRegisterVM register, string prevBtn)
        {
            if (prevBtn != null)
            {
                return RedirectToAction("SetCompany");
            }

            if (!ModelState.IsValid)
            {
                register = register.SetPerson(register);
                return View("SetPerson", register);
            }

            // Force statusId, companyId, departmentId values to be like session value (to prevent manual DOM changes)             
            User user = GetUser();
            register.StatusId = user.StatusId;
            register.CompanyId = user.CompanyId;
            register.DepartmentId = user.DepartmentId;

            // Check provided person
            ViewBag.Error = register.ValidateSetPerson(register);

            if (ViewBag.Error != null)
			{
                register = register.SetPerson(register);
                return View("SetPerson", register);
            }

            // If validation is OK
            user.Id = (int)register.PersonId;
            user.FName = register.FName;
            user.LName = register.LName;
            user.PersonVisited = register.PersonVisitedId;
            HttpContext.Session.SetComplexData("user", user);

            // If the user is an inter, no test is required.. 
            register = register.AvoidInternToTakeTest(register);

            // Save potential new user data
            register.CompanyName = user.CompanyName;
            register.DepartmentName = user.DepartmentName;

            try
            {
                register.SaveNewUserData(register);
            }
			catch (Exception e)
			{
				throw e;
			}            

			// Save data in session
            user.Id = (int)register.PersonId;
            user.CompanyId = register.CompanyId;
            user.DepartmentId = register.DepartmentId;
            register.IsCheckingOut = user.isCheckinOut;
            HttpContext.Session.SetComplexData("user", user);

            // Prevent orphan register in DB making sure about user's intention. Ask for confirmation if necessary with alert
            register = AvoidDuplicateRegister(register);         

            if (TempData["Alert"] != null)
			{
                return RedirectToAction("RegisterAlert");
			}

            // Sign a user in or sign a user out updating an existing record
            int finalise;
			try
			{
                finalise = register.FinaliseRegistration(register);
            }
			catch (Exception e)
			{
				throw e;
			}

            // Save registerId (new or updated one)
			if (finalise > 0)
			{
                user.RegisterId = finalise;               
            }
			else
			{
                user.RegisterId = register.Id;
			}
            HttpContext.Session.SetComplexData("user", user);

            #region Archive

            //PersonService servP = new PersonService();

            //Dal.Person personCheck = new Dal.Person();
            //SetPerson setPersonError = null;

            //// If the person's id is provided (from dropdown list) check that the person exists in the DB 
            //if (person.Id > 0)
            //{
            //    // The company has a department
            //    if (user.CompanyId > 0 && user.DepartmentId > 0)
            //    {
            //        personCheck = servP.Get((int)person.Id, (int)user.CompanyId, (int)user.DepartmentId);
            //    }
            //    else
            //    {
            //        personCheck = servP.Get((int)person.Id, (int)user.CompanyId);
            //    }

            //    // If the person does not exist
            //    if (personCheck == null)
            //    {
            //        ViewBag.Error = "Cette personne n'existe pas.";
            //        setPersonError = new SetPerson() { PersonVisitedId = 404 };
            //    }
            //    else
            //    {
            //        user.Id = (int)person.Id;
            //    }

            //}
            //// If data come from the inputs
            //else if (person.Id == 0)
            //{
            //    if (String.IsNullOrWhiteSpace(person.LName) ^ String.IsNullOrWhiteSpace(person.FName))
            //    {
            //        ViewBag.NameError = "Veuillez indiquer vos nom et prénom.";
            //    }
            //    else
            //    {
            //        // Prevent duplicate in the DB, search for by LName & FName or FName & LName
            //        Person person2 = CheckIfPersonExistInACompany(user, person);

            //        // If we find a duplicate alert the user
            //        if (person2 != null)
            //        {
            //            if (user.DepartmentId > 0)
            //            {
            //                user.DepartmentId = person2.DepartmentId;
            //            }
            //            user.Id = person2.CompanyId;
            //            user.FName = person2.FName;
            //            user.LName = person2.LName;
            //        }
            //        else
            //        {
            //            user.Id = 0;
            //            user.LName = person.LName;
            //            user.FName = person.FName;
            //        }
            //    }
            //}
            //else
            //{
            //    ViewBag.Error = "Cette personne n'est pas correcte.";
            //}

            //// if the person is a visitor, the visited person is required
            //if (user.StatusId == 3)
            //{
            //    if (person.PersonVisitedId > 0)
            //    {
            //        Dal.Person personVisited = servP.GetPersonVisited((int)person.PersonVisitedId);

            //        // Check that visited person exists in DB
            //        if (personVisited != null)
            //        {
            //            user.PersonVisited = person.PersonVisitedId;
            //        }
            //        else
            //        {
            //            ViewBag.VisitedError = "La personne visitée n'existe pas.";
            //            setPersonError = new SetPerson() { PersonVisitedId = 404 };
            //        }
            //    }
            //    else
            //    {
            //        ViewBag.VisitedError = "La personne visitée n'existe pas.";
            //        setPersonError = new SetPerson() { PersonVisitedId = 404 };
            //    }                
            //}

            //// If an error occured 
            //if (setPersonError != null)
            //{              
            //    // Determine which dropdown list display on Error Post Back
            //    if (user.CompanyId > 0 && user.DepartmentId > 0)
            //    {
            //        setPersonError = ReturnPersonError((int)user.CompanyId, (int)user.DepartmentId, user.Id);
            //    }
            //    else
            //    {
            //        setPersonError = ReturnPersonError((int)user.CompanyId, user.Id);
            //    }

            //    // Prepare dropdown list
            //    //setPersonError.PersonsVisited = servP.GetPersonList(1).Select(p => p.ToAppPersonListVM()).ToList();

            //    // If the person visited was not valid
            //    if (setPersonError.PersonVisitedId == 404)
            //    {
            //        setPersonError.OptionsPVisited = setPersonError.PersonsVisited.Select(a => new SelectListItem
            //        {
            //            Value = a.Id.ToString(),
            //            Text = a.Name
            //        }).ToList();
            //    }
            //    else
            //    {
            //        setPersonError.OptionsPVisited = new List<SelectListItem>();

            //        foreach (var item in setPersonError.PersonsVisited)
            //        {
            //            if (item.Id == user.PersonVisited)
            //            {
            //                setPersonError.OptionsPVisited.Add(new SelectListItem
            //                {
            //                    Value = item.Id.ToString(),
            //                    Text = item.Name,
            //                    Selected = true
            //                });
            //            }
            //            else
            //            {
            //                setPersonError.OptionsPVisited.Add(new SelectListItem
            //                {
            //                    Value = item.Id.ToString(),
            //                    Text = item.Name,
            //                });
            //            }
            //        }
            //    }      
            //    return View(setPersonError);
            //}
            //else
            //{
            //    // Save the data into the session
            //    HttpContext.Session.SetComplexData("user", user);
            //}
            //#endregion -- End Save Session


            //#region Save Register                

            //Dal.Person person1 = new Dal.Person();

            //// If the company is selected from registered companies            
            //if (user.CompanyId > 0)
            //{
            //    person1.CompanyId = (int)user.CompanyId;

            //    // If the department is selected from registered departments
            //    if (user.DepartmentId > 0)
            //    {
            //        person1.DepartmentId = (int)user.DepartmentId;
            //    }
            //    // No department is selected
            //    else
            //    {
            //        // Check if department name is provided
            //        if (user.DepartmentName != null)
            //        {
            //            DepartmentService departmentS = new DepartmentService();

            //            // Register a Dal.Department as no specific logic is implied
            //            // TODO if time : implement a string format in Sql 
            //            Dal.Department d = new Dal.Department();
            //            d.CompanyId = (int)user.CompanyId;
            //            d.DepartmentName = user.DepartmentName;
            //            d.Id = departmentS.Insert(d);
            //            user.DepartmentId = d.Id;
            //            person1.DepartmentId = d.Id;
            //        }
            //    }
            //}
            //// if the person's company data comes from the input fields then a new company is created
            //else
            //{
            //    CompanyService companyServ = new CompanyService();

            //    Dal.Company company = new Dal.Company();
            //    company.CompanyName = user.CompanyName;
            //    company.CompanyStatusId = user.StatusId;                
            //    company.Id = companyServ.Insert(company);
            //    user.CompanyId = company.Id;
            //    person1.CompanyId = company.Id;
            //}

            //// Person 
            //if (user.Id > 0)
            //{
            //    person1.Id = user.Id;
            //}
            //else
            //{
            //    PersonService personService = new PersonService();
            //    person1.LName = user.LName;
            //    person1.FName = user.FName;
            //    person1.AdministratorId = 1;
            //    person1.CompanyId = (int)user.CompanyId;
            //    person1.DepartmentId = user.DepartmentId;                
            //    person1.Id = personService.Insert(person1);
            //    user.Id = person1.Id;
            //}

            //// Register the user session into DB 
            //RegisterService registerServ = new RegisterService();
            //Register register = new Register();
            //register.PersonId = person1.Id;
            //register.TimeIn = DateTime.Now;

            //// The user is checkin out
            //if (user.isCheckinOut == true)
            //{
            //    register.TimeOut = DateTime.Now;
            //}

            //if (user.PersonVisited > 0)
            //{
            //    register.PersonVisited = user.PersonVisited;
            //}
            //registerServ.Insert(register.ToDal());

            //TempData.Remove("IsUserCheckinOut");
            #endregion

            return RedirectToAction("RegisterSummary");
        }

        [HttpGet]
        public IActionResult RegisterAlert()
        {
			if (TempData["Alert"] != null)
			{
                ViewBag.Alert = TempData["Alert"];
                TempData.Remove("Alert");

                if (TempData["TimeIn"] != null)
				{
                    ViewBag.TimeIn = TempData["TimeIn"];
                    TempData.Remove("TimeIn");
                }
			}
			else
			{
                throw new NotImplementedException();
			}
            return View("RegisterAlert");
        }

        [HttpPost]
        public IActionResult RegisterAlert(IFormCollection form)
        {
            // New record with sign-in
            bool signIn = false;
            bool checkOut = false;
            bool signInSignOut = false;

			// Get values from form
			foreach (var key in form.Keys)
			{
                if (key.Contains("sign-in-new"))
                {
                    signIn = true;
                }
                else if (key.Contains("sign-out-existing"))
                {
                    checkOut = true;
                }
                else if (key.Contains("sign-in-sign-out-new"))
                {
                    signInSignOut = true;
                }
			}

            // Get User
            User user = GetUser();
            
            FormRegisterVM register = new FormRegisterVM()
            {
                PersonId = user.Id,
                PersonVisitedId = (int)user.PersonVisited,
            };

            if (signIn == true)
			{               
				try
				{
                    register.Id = register.SignInUser(register);
                    user.RegisterId = register.Id;
                    HttpContext.Session.SetComplexData("user", user);
                }
				catch (Exception e)
				{
					throw e;
				}
			}

			if (checkOut == true)
			{
				try
				{
                    register.Id = user.RegisterId;
                    register.CheckOutRegister(register);
                }
				catch (Exception e)
				{
					throw e;
				}				
			}

			if (signInSignOut == true)
			{
                register.Id = register.SignInSignOutUser(register);
                user.RegisterId = register.Id;
                HttpContext.Session.SetComplexData("user", user);
            }

            return RedirectToAction("RegisterSummary");
        }

        [HttpGet]
        public IActionResult RegisterSummary()
        {
            User user = GetUser();

            RegisterService serviceR = new RegisterService();
            RegisterVM register = serviceR.Get(user.RegisterId).DalToRegisterVM();

            ViewBag.IsCheckInOut = register.IsUserCheckingOut(register);

            // Check if person has a test to take and create a blank test if it's true and if the test is active
            PersonService serviceP = new PersonService();
            DALE.Person person = serviceP.Get(user.Id);

            PersonTestVM personTest = new PersonTestVM();

            // Check if the user has a test to take (test active an person not in order)
            personTest = personTest.MustTakeTest(person);
            
            if (personTest.Id > 0)
            {
                ViewBag.TestActive = true;
                user.PersonTestId = personTest.Id;
                HttpContext.Session.SetComplexData("user", user);
            }
			else
			{
                RemoveUser();
            }
            return View("RegisterSummary", register);
        }

        [HttpPost]
        // GET: Ajax get the departements of the company
        public JsonResult GetDepartmentsByCompany(int companyId) 
        {
            DepartmentService serv = new DepartmentService();
            List<Department> departments = new List<Department>();
            Department other = new Department
            {
                Id = 0,
                Name = "Autre",
            };         

            // If Company is SNCB, provide departments without Arlon
            if (companyId == 1)
            {
                departments = serv.GetAllDepartmentByCompany(companyId).Where(s => s.Id != 2).Select(d => d.DalToDepartmentApp()).ToList();
                departments.Insert(0, other);
            }
            else
            {
                departments = serv.GetAllDepartmentByCompany(companyId).Select(d => d.DalToDepartmentApp()).ToList();
                if (departments.Count() > 0)
                {
                    departments.Insert(0, other);
                }                
            }
            
            //if (departments.Count() == 1 & departments[0].Id == 0 )
            //{
            //    departments = null;
            //}
            return Json(departments);
        }

        [HttpPost]
        public JsonResult GetCompaniesByStatus(int statusId)
        {
            // Get companies by status 
            CompanyService serv = new CompanyService();

            List<Company> companies = serv.GetAllCompanyByStatusWithoutDep(statusId).Select(c => c.DalToCompanyApp()).ToList();

            // Provide the company's dropdownlist
            if (companies.Count() > 0)
            {
                // Add "Autre" in the dropdownlist
                Company other = new Company { Id = 0, CompanyName = "Autre" };
                companies.Insert(0, other);
            }
            else
            {
                companies = null;
            }

            return Json(companies);
        }

        [HttpPost]
        public JsonResult AutoComplete(string Prefix)
        {
            CompanyService service = new CompanyService();
            List<DALE.Company> companies = new List<DALE.Company>();           

            // Get the list of all companies
            companies = service.GetAllAutoComplete().Select(c => c).ToList();

            //Searching records from list using LINQ query  
            var List = (from N in companies
                        where N.CompanyName.Contains(Prefix, StringComparison.CurrentCultureIgnoreCase)
                            select new { N.Id, N.CompanyName, N.CompanyStatusId });
            
            return Json(List);
        }

        [HttpGet]
        public IActionResult SearchRegister()
        {
            // We assume the user has actually checked-in
            User user = GetUser(); 
            user.hasActuallyCheckedIn = true;
            HttpContext.Session.SetComplexData("user", user);

            ViewBag.RegisterError = null;            

            RegisterService serv = new RegisterService();
            SetPerson person = new SetPerson();

            // Get persons checked in for the current day
            List<Register> registers = serv.GetAllCurrentDay()
                .Where(p => p.TimeOut == null && p.TimeIn != null && p.CompanyId == user.CompanyId)
                .Select(p => p.DalToRegisterApp())
                .ToList();

            // if a list of checked in people exist prepare dropdown
            if (registers.Count() != 0)
            {
                foreach (var item in registers)
                {
                    person.Persons = new List<PersonListVM>();
                    person.Persons.Add(new PersonListVM() 
                    {
                        Id = item.PersonId,
                        FName = item.FName,                         
                        LName = item.LName 
                    });
                }

                person.OptionsPersons = person.Persons.Select(a => new SelectListItem
                {
                    Value = a.Id.ToString(),
                    Text = a.Name
                }).ToList();
            }
            // if no checked in people are found, prepare dropdown with employees by company
            else
            {
                user.hasActuallyCheckedIn = false;                
                HttpContext.Session.SetComplexData("user", user);

                ViewBag.RegisterError = "Aucune personne de votre entreprise n'est présente dans le registre aujourd'hui. Vous avez dû oublié de signer en arrivant...";

                // Prepare dropdownList for employees'name of the user's company
                PersonService personServ = new PersonService();
                person.Persons = new List<PersonListVM>();

                IEnumerable<DALE.Person> persons = personServ.GetPersonsByCompany((int)user.CompanyId);

                if (persons.Count() > 0)
                {
                    person.Persons = persons.Select(p => new PersonListVM()
                    {
                        Id = p.Id,
                        FName = p.FName,
                        LName = p.LName
                    }).ToList();

                    person.OptionsPersons = person.Persons.Select(a => new SelectListItem
                    {
                        Value = a.Id.ToString(),
                        Text = a.Name
                    }).ToList();
                }
                else
                {   
                    // We should never reach this as there should always be records
                    // During preceding steps, the user can add his/her company if it does not exist in DB
                    return RedirectToAction("Index", "Error");
                }

                // If the person is a visitor, the visted person is required
                if (user.StatusId == 3)
                {
                    // Populate the list of person for the user to find the name of the person he wants to visit
                    // The parameter 1 of the underneath method correspond to companyId = 1 (SNCB) && departmentId = 2 (Arlon)
                    person.PersonsVisited = personServ.GetPersonsByCompanyAndDepartment(1, 2).Select(p => p.DalToPersonListVM()).ToList();

                    person.OptionsPVisited = person.PersonsVisited.Select(a => new SelectListItem
                    {
                        Value = a.Id.ToString(),
                        Text = a.Name
                    }).ToList();
                }

            }
            return View(person);
        }

        [HttpPost]
        public IActionResult SearchRegister(SetPerson person)
        {
            // Client side validation
            if (!ModelState.IsValid)
            {
                return View(person);
            }

            // Prepare registration
            RegisterService registerServ = new RegisterService();
            Register register = new Register();

            ViewBag.Error = null;
            ViewBag.RegisterError = null; 

            User user = GetUser();

            if (person.Id < 0)
            {
                ViewBag.Error = "La personne sélectionnée n'est pas correcte. Veuillez recommencer...";
                return View(person);
            }

            // the user found its name in the list
            if (person.Id > 0 && user.hasActuallyCheckedIn == true)
            {
                // save session just in case
                user.Id = (int)person.Id;

                var check = registerServ.GetAllCurrentDay();

                // Check server side if that user is well in DB for today's register
                var isInDB = registerServ.GetAllCurrentDay()
                    .Where(u => u.PersonId == person.Id && u.TimeIn != null)
                    .FirstOrDefault();

                if (isInDB != null)
                {
                    register.PersonId = (int)person.Id;
                    register.TimeOut = DateTime.Now;
                    registerServ.CheckOut(register.RegisterAppToDal());
                }
            }
            // the user has not checked in beforehand
            else 
            {
                user.hasActuallyCheckedIn = false;
                //return SetPerson(person);
            }

            TempData.Remove("IsUserCheckinOut");

            return RedirectToAction("Index", "Register");
        }

        /// <summary>
        /// Make sure about that the user's intention to preserve register's records in DB. Prepare alert message if necessary.
        /// </summary>
        /// <param name="register"></param>
        /// <returns></returns>
        [NonAction]
        private FormRegisterVM AvoidDuplicateRegister(FormRegisterVM register)
        {
            RegisterService service = new RegisterService();
            DALE.Register found = service.GetAllCurrentDay().Where(r => r.PersonId == (int)register.PersonId).FirstOrDefault();

            // No record found, or previous record is completed
            if (found == null || (found.TimeIn != null && found.TimeOut != null))
            {
                if (register.IsCheckingOut == true)
                {
                    TempData["Alert"] = "Vous avez indiqué vouloir signer le registre de sortie. Nous n'avons cependant trouvé aucun enregistrement d'entrée.";
                }
                else
                {
                    register.InsertNewRegister = true;
                }
            }
            // A record with a sign-in but no sign-out
            else if (found.TimeIn != null && found.TimeOut == null)
            {
                register.Id = found.Id;

                if (register.IsCheckingOut == false)
                {
                    TempData["Alert"] = "Vous avez indiqué vouloir signer le registre d'entrée. Nous avons cependant trouvé un enregistrement d'entrée existant non signé à la sortie:";
                    TempData["TimeIn"] = found.TimeIn;
                }
                else
                {                    
                    register.UpdateRegister = true;                    
                }
            }
            return register;
        }

        #endregion -- End Wizard


        [Authorize]
        [HttpGet]
        public IActionResult GetRegisters(int sortOrder = 0, string searchString = null)
        {
            RegisterService service = new RegisterService();

            // Get registers from Dal and map them in POCO
            IEnumerable<Register> registers = service.GetAll()
                .Select(r => r.DalToRegisterApp())
                .OrderByDescending(r => r.TimeIn)
                .ToList();

            // if searchString exists
            if (searchString != null)
            {
                registers = registers
                    .Where(r => r.FName.ToLower().Contains(searchString.ToLower()) || r.LName.ToLower().Contains(searchString.ToLower()) || r.CompanyName.ToLower().Contains(searchString.ToLower()));
                ViewData["nameFilter"] = searchString; 
            }

            // Today DateTime
            DateTime today = DateTime.Now;

            // Initialise TimeSpan
            TimeSpan timeSpan = new TimeSpan(0, 0, 0);

            // if filter asks for all records, there is no linq operation done on the list
            switch (sortOrder)
            {
                case 1: // yesterday
                    timeSpan = new TimeSpan(today.Hour + 24, today.Minute, 0);
                    registers = registers
                        .Where(r => r.TimeIn >= today.Subtract(timeSpan) && r.TimeIn.Day < today.Day)
                        .OrderByDescending(r => r.TimeIn)
                        .ToList();
                    break;

                case 2: // this week
                        // Today day of the week
                    int toMonday = (int)today.DayOfWeek - 1;
                    int Todaysmonth = (int)today.Month;

                    timeSpan = new TimeSpan(today.Hour + toMonday * 24, today.Minute, 0);
                    DateTime lastDayOfWeek = today.Subtract(timeSpan);

                    registers = registers
                        .Where(r => r.TimeIn >= today.Subtract(timeSpan) && (r.TimeIn.Month == Todaysmonth && r.TimeIn.Day <= lastDayOfWeek.Day + 6))
                        .OrderByDescending(r => r.TimeIn)
                        .ToList();
                    break;

                case 3: // this month

                    int daysInMonth = DateTime.DaysInMonth(today.Year, today.Month);
                    int toFirstDayOfMonth = today.Day;

                    timeSpan = new TimeSpan(today.Hour + toFirstDayOfMonth * 24, today.Minute, 0);
                    registers = registers
                        .Where(r => r.TimeIn >= today.Subtract(timeSpan) && r.TimeIn.Day <= daysInMonth)
                        .OrderByDescending(r => r.TimeIn)
                        .ToList();
                    break;

                default: // today : today 
                    timeSpan = new TimeSpan(today.Hour, today.Minute, 0);
                    registers = registers
                        .Where(r => r.TimeIn >= today.Subtract(timeSpan) && r.TimeIn.Day <= today.Day)
                        .OrderByDescending(r => r.TimeIn)
                        .ToList();
                    break;
            }
            //if (sortOrder != 4)
            //{
            //    switch (sortOrder)
            //    {                    
            //        case 1: // yesterday
            //            timeSpan = new TimeSpan(today.Hour + 8 + 24, today.Minute, 0);
            //            break;                    

            //        case 2: // this week

            //            // Today day of the week
            //            int toMonday = (int)today.DayOfWeek;
            //            timeSpan = new TimeSpan(today.Hour + 8 + toMonday * 24, today.Minute, 0);
            //            break;                    

            //        case 3: // this month

            //            int toFirstDayOfMonth = today.Day;
            //            timeSpan = new TimeSpan(today.Hour + 8 + toFirstDayOfMonth * 24, today.Minute, 0);
            //            break;                   

            //        default: // today : today midnight - 8 hours

            //            timeSpan = new TimeSpan(today.Hour + 8, today.Minute, 0);
            //            break;
            //    }

            //    registers = registers
            //        .Where(r => r.TimeIn >= today.Subtract(timeSpan) && r.TimeIn <= today)
            //        .ToList();
            //}

            // Map POCO in ViewModels
            IEnumerable<RegisterVM> registerVMs = registers
                .OrderByDescending(r => r.TimeIn)
                .Select(r => r.AppToRegisterVM())
                .ToList();

            return View(registerVMs);
        }

        //[HttpGet]
    //    public IActionResult GetRegisterPlanning(int sortOrder = 0, string searchString = null)
    //    {
    //        RegisterPlanningService serviceRPlanning = new RegisterPlanningService();
    //        RegisterService serviceRegister = new RegisterService();

    //        // Get registersPlanning from DAL and map them in POCO
    //        IEnumerable<RegisterPlanning> registersPlanning = serviceRPlanning.GetAll().Select(r => r.ToApp()).ToList();

    //        // Get registers from DAL and map them to POCO
    //        IEnumerable<Register> registers = serviceRegister.GetAll().Select(r => r.DalToRegisterApp()).ToList();

    //        // if searchString exists
    //        if (searchString != null)
    //        {
    //            // apply filter to registerPlanning and registers
    //            registersPlanning = registersPlanning
    //                .Where(r => r.FName.ToLower().Contains(searchString.ToLower()) || r.LName.ToLower().Contains(searchString.ToLower()) || r.CompanyName.ToLower().Contains(searchString.ToLower()));
    //            ViewData["nameFilter"] = searchString;

    //            registers = registers.Where(r => r.FName.ToLower().Contains(searchString.ToLower()) || r.LName.ToLower().Contains(searchString.ToLower()) || r.CompanyName.ToLower().Contains(searchString.ToLower()));
    //        }

    //        // Today DateTime
    //        DateTime today = DateTime.Now;

    //        // Initialise TimeSpan
    //        TimeSpan timeSpan = new TimeSpan(0, 0, 0);

    //        // if filter asks for all records, there is no linq operation done on the list
    //        if (sortOrder != 4)
    //        {
    //            switch (sortOrder)
    //            {
    //                case 1: // yesterday
    //                    timeSpan = new TimeSpan(today.Hour + 8 + 24, today.Minute, 0);
    //                    break;

    //                case 2: // this week

    //                    // Today day of the week
    //                    int toMonday = (int)today.DayOfWeek;
    //                    timeSpan = new TimeSpan(today.Hour + 8 + toMonday * 24, today.Minute, 0);
    //                    break;

    //                case 3: // this month

    //                    int toFirstDayOfMonth = today.Day;
    //                    timeSpan = new TimeSpan(today.Hour + 8 + toFirstDayOfMonth * 24, today.Minute, 0);
    //                    break;

    //                default: // today : today midnight - 8 hours

    //                    timeSpan = new TimeSpan(today.Hour + 8, today.Minute, 0);
    //                    break;
    //            }

    //            registersPlanning = registersPlanning
    //                .Where(r => r.TimeIn >= today.Subtract(timeSpan) && r.TimeIn <= today)
    //                .ToList();

    //            registers = registers.Where(r => r.TimeIn >= today.Subtract(timeSpan)).ToList();
    //        }

    //        // Compare two lists
    //        foreach (var i in registersPlanning)
    //        {
    //            foreach (var j in registers)
    //            {
				//	if (i.PersonId == j.PersonId && i.TimeIn.Date == j.TimeIn.Date)
				//	{

				//	}
				//}
    //        }           
            
    //        // Map POCO in ViewModels
    //        var registersVM = registersPlanning
    //            .OrderByDescending(r => r.TimeIn)
    //            .Select(r => r.ToVM())
    //            .ToList();

    //        return View(registersVM);
    //    }


        #region AddRegisterPlanning Wizard form

        // Save statuses
        private IEnumerable<CompanyStatus> statusesGlobal;

		[HttpGet]
        public IActionResult AddRegisterPlanningStatus()
        {
            User user = GetUser();
            
            SetStatus status = new SetStatus();
            CompanyStatusService serv = new CompanyStatusService();
            status.CompanyStatus = serv.GetAll().Select(cs => cs.DalToCompanyStatusApp()).ToList();
            
            // Save statuses
            statusesGlobal = status.CompanyStatus;
            
            status.Id = user.StatusId;            

            return View(status);
        }

        [HttpPost]
		public IActionResult AddRegisterPlanningStatus(string nextBtn, SetStatus status)
		{
			User session = GetUser();

			if (nextBtn != null)
			{
                RegisterFacade facade = new RegisterFacade();
                ViewBag.Error = facade.CheckStatus(session, status);               

				HttpContext.Session.SetComplexData("user", session);

				if (ViewBag.Error == null)
                    return RedirectToAction("AddRegisterPlanningCompany");
				status.CompanyStatus = statusesGlobal;
			}
			return View(status);
		}

		[HttpGet]
		public IActionResult AddRegisterPlanningCompany()
		{
			User user = GetUser();

            RegisterFacade facade = new RegisterFacade();

            SetCompany company = facade.SessionToSetCompany(user);
            company = facade.PrepareCompany(company);

            return View(company);
		}

		[HttpPost]
        public IActionResult AddRegisterPlanningCompany(string nextBtn, string prevBtn, SetCompany company)
        {
            User user = GetUser();
            RegisterFacade facade = new RegisterFacade();

            if (prevBtn != null)
            {
                return RedirectToAction("AddRegisterPlanningStatus");
            }

            if (nextBtn != null)
            {
                // Client side validation
                if (!ModelState.IsValid)
                {               
                    company = facade.PrepareCompany(company);
                    return View(company);
                }
                // Server side validation
                else
                {
                    ViewBag.Error = facade.CheckCompany(company);
                }
            }

            // An error occured, return to view
            if (ViewBag.Error != null)
            {
                company = facade.PrepareCompany(company);
                return View(company);
            }
			else
			{
                // No error save new company and new department if exists
                if (company.Name != null)
                    user.CompanyId = facade.SaveCompany(company);
                user.CompanyId = (int)company.Id;

                if (company.DepartmentName != null)
                    user.DepartmentId = facade.SaveDepartment(company);
                user.DepartmentId = company.DepartmentId;
            }

            // Save session
            HttpContext.Session.SetComplexData("user", user);

            return RedirectToAction("AddRegisterPlanningPerson");
        }

        [HttpGet]
        public IActionResult AddRegisterPlanningPerson()
        {
            User user = GetUser();
            SetPerson person = SetPersonToView(user);
            return View(person);
        }

        [HttpPost]
        public IActionResult AddRegisterPlanningPerson(string nextBtn, string prevBtn, SetPerson person)
        {
            User user = GetUser();

            PersonService serv = new PersonService();

            if (prevBtn != null)
            {
                return RedirectToAction("AddRegisterPlanningCompany");
            }

            if (nextBtn != null)
            {
                if (!ModelState.IsValid)
                {
                    SetPerson personToView = SetPersonToView(user);
                    return View(personToView);
                }
                else
                {
                    // A user is selected from the list
                    if (person.Id > 0)
                    {
                        // Check that the person is in DB
                        DALE.Person personExist = serv.Get((int)person.Id, (int)user.CompanyId);

                        if (personExist != null)
                        {
                            user.Id = (int)person.Id;
                        }
                        else
                        {
                            ViewBag.Error = "Cette personne n'existe pas";
                        }
                    }
                    else if (person.Id == 0)
                    {
                        string nameAndLastName = ControllerMethods.NameAndLastNameValid(person.FName, person.LName); 

                        if (nameAndLastName == null) 
                        {
                            // Prevent duplicate in the DB, search by LName & FName or FName & LName
                            Person personExists = CheckIfPersonExistInACompany(user, person);

                            // If we find a duplicate alert the user
                            if (personExists != null)
                            {
                                ViewBag.NameError = "Cette personne existe déjà, sélectionnez la dans la liste. Si vous souhaitez ajouter une personne portant le même nom et le même prénom, ajoutez une différenciation (ex: troisième prénom).";
                            }
                            else
                            {
                                user.Id = 0;
                                user.LName = person.LName;
                                user.FName = person.FName;
                            }
                        }
                        // In case of error, set viewbag
                        else
                        {
                            ViewBag.NameError = nameAndLastName;
                        }
                    }
                    else
                    {
                        ViewBag.Error = "Veuillez choisir une personne dans la liste ou inscrire son nom manuellement";
                    }

                    // The user is a visitor and must provide a visitee
                    if (user.StatusId == 3)
                    {
                        if (person.PersonVisitedId > 0)
                        {
                            // Check that the visitee is in DB
                            DALE.Person personVisitedExists = serv.GetPersonVisited((int)person.PersonVisitedId);

							if (personVisitedExists != null)
							{
                                user.PersonVisited = person.PersonVisitedId;
							}
							else
							{
                                ViewBag.VisitedError = "La personne visitée n'existe pas.";
                            }
                        }
                        else
                        {
                            ViewBag.VisitedError = "Veuillez sélectionner la personne que vous venez visiter.";
                        }
                    }
                }
            }

            if (ViewBag.Error != null || ViewBag.NameError != null || ViewBag.VisitedError != null)
            {
                SetPerson personToView = SetPersonToView(user);
                return View(personToView);
            }
            else
            {
				// Save new user if he/she exists
				if (user.LName != null && user.FName != null)
				{
                    DALE.Person newPerson = new DALE.Person();
                    newPerson.LName = user.LName;
                    newPerson.FName = user.FName;
                    newPerson.AdministratorId = 1;
                    newPerson.CompanyId = (int)user.CompanyId;
                    newPerson.DepartmentId = user.DepartmentId;
                    newPerson.Id = serv.Insert(newPerson);
                    user.Id = newPerson.Id;

                    // reset values as a new user was created
                    user.FName = null;
                    user.LName = null;
                }

                // Save session
                HttpContext.Session.SetComplexData("user", user);

                return RedirectToAction("AddRegisterPlanningTime");
            }            
        }

        [HttpGet]
        public IActionResult AddRegisterPlanningTime()
        {
            SetTime setTime = new SetTime();
            return View(setTime);
        }

        [HttpPost]
        public IActionResult AddRegisterPlanningTime(string nextBtn, string prevBtn, IFormCollection form)
        {
			if (prevBtn != null)
			{
                return RedirectToAction("AddRegisterPlanningPerson");
            }

			if (nextBtn != null)
			{
				if (!ModelState.IsValid)
				{
                    return View();
				}
				else
				{
                    User user = GetUser();
                    
                    RegisterPlanningVM register = new RegisterPlanningVM();

                    string validation;
                    string integrity;

                    if (form["PlanningTypeId"] == "1")
                    {
                        validation = FormTimeValid(form, "SameDayDate", "SameDayTimeIn", "SameDayTimeOut");

                        if (validation == null)
                        {
                            DateTime timeIn = SameDayToDateTime(form, "SameDayDate", "SameDayTimeIn");
                            DateTime timeOut = SameDayToDateTime(form, "SameDayDate", "SameDayTimeOut");

                            integrity = TimeInTimeOutIntegrity(timeIn, timeOut);
                            if (integrity == null)
							{
                                register.TimeIn = timeIn;
                                register.TimeOut = timeOut;

                                RegisterPlanningService serv = new RegisterPlanningService();
                                DALE.RegisterPlanning registerExist = serv.GetExisting(user.Id, (DateTime)register.TimeIn);

								// if a registerPlanning exist
								if (registerExist != null)
								{
									// check if it was planned
									if (registerExist.Planned == false)
									{
                                        bool succeed = serv.SetPlanned(registerExist.Id);
										if (succeed == true)
										{
                                            TempData["Alert"] = "true";                                         
										}
										else
										{
                                            // TODO : handle error and redirect to error page
										}
                                    }
									else
									{
                                        TempData["Alert"] = "false";
                                    }
                                    return RedirectToAction("GetRegisterPlanning");
                                }
							}
							else
							{
                                ViewBag.Error = integrity;
                            }
                        }
                        else
                        {
                            ViewBag.Error = FormTimeValidViewBag(validation, 1);
                        }

                    }
                    else if (form["PlanningTypeId"] == "2")
                    {
                        validation = FormTimeValid(form, "DifferentDayTimeIn", "DifferentDayTimeOut");

						if (validation == null)
						{
                            DateTime DateTimeIn = DateTime.Parse(form["DifferentDayTimeIn"]);
                            DateTime DateTimeOut = DateTime.Parse(form["DifferentDayTimeOut"]);

                            integrity = TimeInTimeOutIntegrity(DateTimeIn, DateTimeOut);
                            if (integrity == null)
                            {
                                //dt.ToString("H:mm");



                                register.TimeIn = DateTimeIn;
                                register.TimeOut = DateTimeOut;





                            }
                            else
                            {
                                ViewBag.Error = integrity;
                            }
                        }
						else
						{
                            ViewBag.Error = FormTimeValidViewBag(validation, 2);
                        }
                    }
					else if (form["PlanningTypeId"] == "3")
					{

					}
					else
					{
                        ViewBag.Error = "Veuillez sélectionner un type d'horaire.";
                    }
				}
			}

            SetTime setTimeError = FormToSetTime(form);
            return View(setTimeError);
        }


		#endregion AddRegisterPlanning Wizard

		[HttpPost]
		public IActionResult Success(string title, string message)
		{
			ViewBag.ModalTitle = title;
			ViewBag.Message = message;
			return PartialView("_Success");
		}

		#region Methods

		[NonAction]
        private User GetUser()
        {
            var session = HttpContext.Session.GetComplexData<User>("user");

            if (session == null)
            {
                User user = new User();
                HttpContext.Session.SetComplexData("user", user);
            }
            return (User)HttpContext.Session.GetComplexData<User>("user");
        }

        /// <summary>
        /// Reset user cookie data to null
        /// </summary>
        /// <param name="user"></param>
        [NonAction]
        private void ResetUser(User user)
        {
            user.CompanyId = null;
            user.CompanyName = null;
            user.DepartmentId = null;
            user.DepartmentName = null;
            user.PersonVisited = null;
            user.Id = 0;
            HttpContext.Session.SetComplexData("user", user);
        }

        [NonAction]
        private void RemoveUser()
        {
            HttpContext.Session.Clear();
        }

        // Function used to populate a view model with the user choices in case of postback error
        [NonAction]
        private SetCompany ReturnCompanyError(int userStatusId)
        {
            CompanyService service = new CompanyService();
            SetCompany companyError = new SetCompany();
            companyError.Companies = service.GetAllCompanyByStatusWithoutDep(userStatusId).Select(c => c.DalToCompanyApp()).ToList();

            // Add "Autre" in the dropdownlist
            Company other = new Company { Id = 0, CompanyName = "Autre" };
            companyError.Companies.Insert(0, other);
            companyError.Options = companyError.Companies.Select(a => new SelectListItem
            {
                Value = a.Id.ToString(),
                Text = a.CompanyName
            }).ToList();
            return companyError;
        }

        [NonAction]
        private SetPerson ReturnPersonError(int companyId, int departmentId = -1, int personId = 0)
        {
            PersonService service = new PersonService();

            SetPerson setPersonError = new SetPerson();

            if (departmentId != -1)
            {
                setPersonError.Persons = service.GetPersonsByCompanyAndDepartment(companyId, departmentId).Select(p => p.DalToPersonListVM()).ToList();
            }
            else
            {
                setPersonError.Persons = service.GetAll(companyId).Select(p => p.DalToPersonListVM()).ToList();
            }

            // Add "Autre" to Dropdown list
            PersonListVM personOther = new PersonListVM { Id = 0, LName = null, FName = null };
            setPersonError.Persons.Insert(0, personOther);

            if (personId != 0)
            {
                setPersonError.OptionsPersons = new List<SelectListItem>();

                foreach (var item in setPersonError.Persons)
                {
                    if (item.Id == personId)
                    {
                        setPersonError.OptionsPersons.Add(new SelectListItem
                        {
                            Value = item.Id.ToString(),
                            Text = item.Name,
                            Selected = true
                        });
                    }
                    else
                    {
                        setPersonError.OptionsPersons.Add(new SelectListItem
                        {
                            Value = item.Id.ToString(),
                            Text = item.Name,
                        });
                    }
                }
            }
            else
            {
                setPersonError.OptionsPersons = setPersonError.Persons.Select(a => new SelectListItem
                {
                    Value = a.Id.ToString(),
                    Text = a.Name
                }).ToList();
            }
            return setPersonError;
        }

        [NonAction]
        private Person CheckIfPersonExistInACompany(User user, SetPerson setPerson)
        {
            PersonService servP = new PersonService();

            IEnumerable<Person> people = servP.GetAll(setPerson.LName, setPerson.FName).Select(p => p.DalToPersonApp()).ToList();

            // If one or serveral person match
            // check if one person is in the same company and department

            Person person = null;

            if (people.Count() > 0)
            {
                foreach (var i in people)
                {
                    if (user.DepartmentId > 0)
                    {
                        if (i.CompanyId == user.CompanyId && i.DepartmentId == user.DepartmentId)
                        {
                            person = i;
                        }
                    }
                    else
                    {
                        if (i.CompanyId == user.CompanyId)
                        {
                            person = i;
                        }
                    }
                }
            }
            return person;
        }

        [NonAction]
        private SetCompany SetCompanyToView(SetCompany form)
        {
            SetCompany company = form;

            // If the user is part of SNCB
            if (company.CompanyStatusId == 1)
            {
                company.Departments = SetCompanyDepartments((int)company.Id);
            }
            else
            {
                company.Companies = SetCompanyCompanies(company.CompanyStatusId);

                if (company.Companies != null)
                {
                    // For navigation (Next & Previous) purposes
                    if (company.DepartmentId > 0)
                    {
                        company.Departments = SetCompanyDepartments((int)company.Id);
                    }
                }
                else
                {
                    // If no dropdown list with companies is provided, force user to enter the company's name
                    company.Id = 0;
                }
            }
            return company;
        }

        [NonAction]
        private SetPerson SetPersonToView(User user)
        {
            PersonService serv = new PersonService();
            SetPerson person = new SetPerson();

			if (user.Id == 0)
			{
                person.Id = null;
			}
			else
			{
                person.Id = user.Id;
			}

            List<PersonListVM> personList = new List<PersonListVM>();  

            if (user.DepartmentId > 0)
            {
                personList = serv.GetPersonsByCompanyAndDepartment((int)user.CompanyId, (int)user.DepartmentId).Select(p => p.DalToPersonListVM()).ToList();
            }
            else
            {
                personList = serv.GetAll((int)user.CompanyId).Select(p => p.DalToPersonListVM()).ToList();
            }           

            // If the company has recorded employees, show a list
            if (personList.Count() > 0)
            {
                person.Persons = personList;

                // Add "Autre" to Dropdown list
                PersonListVM personOther = new PersonListVM { Id = 0, LName = null, FName = null };                
                person.Persons.Insert(0, personOther);
            }
            else
            {
                person.Persons = null;
                // Force inputs (name and lastname) to be provided
                //person.Id = 0;
            }

            // If the person is a visitor, the visted person is required
            if (user.StatusId == 3)
            {
                // Populate the list of person for the user to find the name of the person he wants to visit
                // The parameter 1 of the underneath method correspond to companyId = 1 (SNCB) && departmentId = 2 (Arlon)
                List<PersonListVM> personToVist = serv.GetPersonsByCompanyAndDepartment(1, 2).Select(p => p.DalToPersonListVM()).ToList();              

                if (personToVist.Count() > 0)
                {
					person.PersonsVisited = personToVist;
                    person.PersonVisitedId = user.PersonVisited;
                }
                else
                {
                    person.PersonsVisited = null;
                }
            }
            return person;
        }

        [NonAction]
        private string NameAndLastNameValid(string name, string lastname)
        {
            string result;

            if (!String.IsNullOrWhiteSpace(name) && !String.IsNullOrWhiteSpace(lastname))
            {
                result = null;
            }
            // FName or LName  or both are invalid
            else
            {
                if (String.IsNullOrWhiteSpace(lastname))
                {
                    result = "Le nom de famille est requis.";
                }
                else if (String.IsNullOrWhiteSpace(name))
                {
                    result = "Le prénom est requis.";
                }
                else
                {
                    result = "Le nom de famille  et le prénom sont requis.";
                }
            }
            return result;
        }

        [NonAction]
        private List<Company> SetCompanyCompanies(int statusId)
        {
            CompanyService service = new CompanyService();
            List<Company> companies = new List<Company>();

            companies = service.GetAllCompanyByStatusWithoutDep(statusId)
                    .Select(c => c.DalToCompanyApp())
                    .ToList();

            if (companies.Count() > 0)
            {
                // Add "Autre" in the dropdownlist
                Company other = new Company { Id = 0, CompanyName = "Autre" };
                companies.Insert(0, other);
            }
            else
            {
                companies = null;
            }

            return companies;
        }

        [NonAction]
        private List<Department> SetCompanyDepartments(int companyId)
        {
            DepartmentService service = new DepartmentService();
            List<Department> departments = new List<Department>();

            if (companyId == 1)
            {
                departments = service.GetAllDepartmentByCompany(companyId)
                    .Where(s => s.Id != 2)
                    .Select(d => d.DalToDepartmentApp())
                    .ToList();
            }
            else
            {
                departments = service.GetAllDepartmentByCompany(companyId)
                            .Select(d => d.DalToDepartmentApp())
                            .ToList();
            }

            if (departments.Count() > 0)
            {
                departments.Insert(0, new Department { Id = 0, Name = "Autre" });
            }
            else
            {
                departments = null;
            }

            return departments;
        }

        [NonAction]
        private string TimeInTimeOutIntegrity(DateTime timeIn, DateTime timeOut)
        {
            string result = null;

            if (timeIn >= timeOut)
            {
                result = "L'horaire de début doit être inférieur à l'horaire de fin.";
            }
            return result;
        }

        [NonAction]
        private string FormTimeValid(IFormCollection form, params string[] keys)
        {
            string result = null;
            string value;

            for (int i = 0; i < keys.Length; i++)
            {
                value = form[keys[i]];

                if (value == "")
                {
                    result += i.ToString();
                }
            }
            return result;
        }

        [NonAction]
        private string FormTimeValidViewBag(string validation, int formType)
        {
            string viewbag = "";

            if (formType == 1)
            {
                switch (validation)
                {
                    case "0":
                        viewbag = "Veuillez spécifier une date.";
                        break;
                    case "01":
                        viewbag = "Veuillez spécifier une date et une heure d'arrivée";
                        break;
                    case "012":
                        viewbag = "Veuillez spécifier une date, une heure d'arrivée et de départ";
                        break;
                    case "12":
                        viewbag = "Veuillez spécifier une heure d'arrivée et de départ";
                        break;
                    case "1":
                        viewbag = "Veuillez spécifier une heure d'arrivée";
                        break;
                    case "2":
                        viewbag = "Veuillez spécifier une heure de départ";
                        break;
                }
            }
            else if (formType == 2)
            {
                switch (validation)
                {
                    case "0":
                        viewbag = "Veuillez spécifier la date et l'heure de début.";
                        break;
                    case "1":
                        viewbag = "Veuillez spécifier la date et l'heure de fin.";
                        break;
                    case "01":
                        viewbag = "Veuillez spécifier les dates et les heures de début et de fin.";
                        break;
                }
            }
            return viewbag;
        }

        [NonAction]
        private DateTime SameDayToDateTime(IFormCollection form, string dateKey, string hourKey)
        {
            string date = form[dateKey] + " " + form[hourKey] + ":00";
            DateTime dateTime = DateTime.Parse(date);
            return dateTime;
        }

        [NonAction]
        private SetTime FormToSetTime(IFormCollection form)
        {
            SetTime setTime = new SetTime();
            setTime.PlanningTypeId = int.Parse(form["PlanningTypeId"]);
            setTime.SameDayDate = form["SameDayDate"];
            setTime.SameDayTimeIn = form["SameDayTimeIn"];
            setTime.SameDayTimeOut = form["SameDayTimeOut"];
            return setTime;
        }
        #endregion
    }
}