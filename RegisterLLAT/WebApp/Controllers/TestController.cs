﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using DAL.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using WebApp.AppTools;
using WebApp.Models;
using WebApp.Models.PersonTestViewModels;
using WebApp.Models.TestViewModels;
using DALE = DAL.Entities;

namespace WebApp.Controllers
{
	[Authorize(Roles = "Super Admin, Full Admin, Prevention Admin")]
	public class TestController : Controller
	{
		[HttpGet]
		public IActionResult Index()
		{
			TestService service = new TestService();

			IEnumerable<TestVM> tests = service.GetAll().Select(t => t.DalToTestVM()).ToList();

			// Prepare ViewBag for modal display in view in case of redirectToAction from Edit, Activate, Deactivate
			if (TempData["title"] != null && TempData["message"] != null)
			{
				ViewBag.ModalTitle = TempData["title"];
				TempData.Remove("title");

				ViewBag.ModalMessage = TempData["message"];
				TempData.Remove("message");
			}

			return View("Index", tests);
		}

		[HttpGet]
		public IActionResult Edit(int id)
		{
			// Check that test exists
			DALE.Test testExist = CheckTestExist(id);

			if (testExist == null)
			{
				TempData["title"] = "Erreur";
				TempData["message"] = "Le test que vous souhaitez éditer n'existe pas";
				return RedirectToAction("Index");
			}

			TestVM test = testExist.DalToTestVM();

			return View(test);
		}

		[HttpPost]
		public IActionResult Edit(TestVM test)
		{
			// Model validation
			if (!ModelState.IsValid)
			{
				return View(test);
			}

			// Server side validation
			ViewBag.Error = ValidateTest(test);

			if (ViewBag.Error != null)
			{
				return View(test);
			}

			// Prepare modal display	
			TempData["title"] = "Edition du test";
			TempData["message"] = "Edition réalisée avec succès.";

			// Check if question can be activated	
			if (test.IsActive)
			{
				string activated = TestCanBeActivated(test);

				if (activated != null)
				{
					TempData["message"] += " Cependant, " + activated;
					test.IsActive = false;
				}				
			}

			// Update Test
			TestService service = new TestService();			
			try
			{
				service.Update(test.TestVMToDal());
			}
			catch (Exception e)
			{
				throw e;
			}			

			return RedirectToAction("Details", new { id = test.Id });
		}

		[HttpGet]
		public IActionResult Details(int id)
		{
			// Check that test exists
			DALE.Test testExist = CheckTestExist(id);

			if (testExist == null)
			{
				// Prepare modal display	
				TempData["title"] = "Erreur";
				TempData["message"] = "Ce test n'existe pas.";
				return RedirectToAction("Index");
			}

			// Prepare ViewBag for modal display in view in case of redirectToAction from Create or Edit
			if (TempData["title"] != null && TempData["message"] != null)
			{
				ViewBag.ModalTitle = TempData["title"];
				TempData.Remove("title");
				
				ViewBag.ModalMessage = TempData["message"];
				TempData.Remove("message");
			}

			TestVM test = testExist.DalToTestVM();

			return View(test);
		}

		[HttpGet]
		public IActionResult Create()
		{
			TestVM test = new TestVM();
			return View(test);
		}

		[HttpPost]
		public IActionResult Create(TestVM test)
		{
			// Model validation
			if (!ModelState.IsValid)
			{
				return View(test);
			}

			// Server side validation
			ViewBag.Error = ValidateTest(test);

			if (ViewBag.Error != null)
			{
				return View(test);
			}

			// Validation Ok, insert test into DB
			TestService service = new TestService();
			try
			{
				test.Id = service.Insert(test.TestVMToDal());
			}
			catch (Exception e)
			{
				throw e;
			}

			// Prepare modal display
			TempData["title"] = "Création du test";
			TempData["message"] = "Test créé avec succès.";

			return RedirectToAction("Details", new { id = test.Id });
		}

		[HttpGet]
		public IActionResult VisualiseVideo(string videoUrl)
		{
			ViewBag.Url = videoUrl;
			return PartialView("_VisualiseVideoModal");
		}

		[HttpPost]
		public IActionResult Activate(int id)
		{
			TestService service = new TestService();

			TestVM test = service.Get(id).DalToTestVM();

			string activate = TestCanBeActivated(test);

			if (activate != null)
			{
				ViewBag.ModalTitle = "Activation du test";
				ViewBag.ModalMessage = "Malheureusement " + activate;
				return PartialView("_DisplayModal");
			}

			// Validation OK, activate test and update it
			test.IsActive = true;

			try
			{
				service.Update(test.TestVMToDal());
			}
			catch (Exception e)
			{
				throw e;
			}

			//test = service.Get(id).DalToTestVM();

			return PartialView("_TestTable", test);
		}

		[HttpPost]
		public IActionResult Deactivate(int id)
		{
			TestService service = new TestService();

			DALE.Test test = service.Get(id);

			test.IsActive = false;

			try
			{
				service.Update(test);
			}
			catch (Exception e)
			{
				throw e;
			}

			TestVM testVM = service.Get(id).DalToTestVM();

			return PartialView("_TestTable", testVM);
		}

		/// <summary>
		/// Alert user that hardDeleting a test will hardDelete the test and all the employees's result corresponding to that test.
		/// All companies linked to this test will be automatically linked with the default test.
		/// </summary>
		/// <returns></returns>
		[HttpGet]
		public IActionResult HardDelete()
		{
			return PartialView("_HardDelete");
		}

		[HttpGet]
		public IActionResult Delete(int id)
		{
			// Check that Test existh
			DALE.Test testExist = CheckTestExist(id);

			TempData["title"] = "Suppression d'un test";

			if (testExist == null)
			{
				// If test does not exist prepare message
				TempData["message"] = "Ce test n'existe pas.";
			}
			else
			{
				TestService service = new TestService();
				try
				{
					service.Delete(id);
				}
				catch (Exception e)
				{
					throw e;
				}

				// If deletion is OK prepare message
				TempData["message"] = "La suppression du test a été réalisée avec succès";
			}
			return RedirectToAction("Index");
		}

		[AllowAnonymous]
		[HttpGet]
		public IActionResult TakeTest()
		{
			User user = GetUser();

			PersonTestService service = new PersonTestService();
			PersonTestVM personTest; 

			try
			{
				personTest = service.Get(user.PersonTestId).DalToPersonTestVM(); 
			}
			catch (Exception e)
			{
				throw e;
			}

			// Extract video ID
			string Url = personTest.TestVM.VideoURL;
			string[] Id = Url.Split('/');
			ViewBag.VideoURL = Id[3];

			return View(personTest);			
		}

		[AllowAnonymous]
		[HttpGet]
		public IActionResult Questions()
		{
			User user = GetUser();

			PersonTestService service = new PersonTestService();
			PersonTestVM personTest = new PersonTestVM();

			try
			{
				personTest = service.Get(user.PersonTestId).DalToPersonTestVM();
			}
			catch (Exception e)
			{
				throw e;
			}

			return View(personTest);
		}

		[AllowAnonymous]
		[HttpPost]
		public IActionResult Questions(IFormCollection form)
		{
			PersonTestVM Test = new PersonTestVM();
			Test.PersonAnswers = new List<AnswersVM>();
			
			// Get user's answers for questionIds
			foreach (var key in form.Keys)
			{
				if (key.Contains("question"))
				{
					int id = int.Parse(key.Replace("question", ""));
					int answerId = int.Parse(form["question" + id]);
					Test.PersonAnswers.Add(new AnswersVM() { QuestionId = id, QcmItemId = answerId });	
				}
			}

			// Get user's test result
			int numberQuestion = Test.PersonAnswers.Count();
			float questionWeigh = 10 / (float)numberQuestion;
			float result = 0;

			// Calculate result and prepare test summary
			QuestionQcmItemService questionQcmItemService = new QuestionQcmItemService();

			// Get qmcItems for each question
			foreach (var userAnswer in Test.PersonAnswers)
			{
				DALE.QuestionQcmItem check = questionQcmItemService.GetByQuestionIdQcmItemId(userAnswer.QuestionId, userAnswer.QcmItemId);
				if (check.QcmItemIsAnswer == true)
				{
					result += questionWeigh;
				}
				userAnswer.Item = check.QcmItemItem;
			}

			// Update user's test as he has just completed a test and set Person IsInOrder field 
			User user = GetUser();
			PersonTestService service = new PersonTestService();
			Test.Id = user.PersonTestId;
			Test.PersonId = user.Id;
			Test.TestId = int.Parse(form["TestId"]);
			Test.Result = result;
			Test.TakeDate = DateTime.Now;
			try
			{
				service.Update(Test.PersonTestVMToDal());
			}
			catch (Exception e)
			{
				throw e;
			}

			// Display final result to user
			if (Test.Result >= Test.TestVM.TestSettingsTreshold)
			{
				ViewBag.TestSuccess = true;
			}
			else
			{
				ViewBag.TestSuccess = false;
			}

			RemoveUser();
			return View("Results", Test); 
		}

		[NonAction]
		private User GetUser()
		{
			var session = HttpContext.Session.GetComplexData<User>("user");

			if (session == null)
			{
				User user = new User();
				HttpContext.Session.SetComplexData("user", user);
			}
			return (User)HttpContext.Session.GetComplexData<User>("user");
		}

		/// <summary>
		/// Reset user cookie data to null
		/// </summary>
		/// <param name="user"></param>
		[NonAction]
		private void ResetUser(User user)
		{
			user.CompanyId = null;
			user.CompanyName = null;
			user.DepartmentId = null;
			user.DepartmentName = null;
			user.PersonVisited = null;
			user.Id = 0;
			HttpContext.Session.SetComplexData("user", user);
		}

		[NonAction]
		private void RemoveUser()
		{
			HttpContext.Session.Clear();
		}

		/// <summary>
		/// Check that the test has 3 activated questions linked to it.
		/// </summary>
		/// <param name="test"></param>
		/// <returns></returns>
		[NonAction]
		private string TestCanBeActivated(TestVM test)
		{
			string result = null;

			QuestionService service = new QuestionService();

			List<DALE.Question> testQuestions = service.GetAll(test.Id).ToList();
			int total = 0;

			foreach (var item in testQuestions)
			{
				if (item.IsActive)
				{
					total++;
				}
			}

			if (total < 3)
			{
				result = "le test ne peut être activé car il ne comporte pas le minimum requis de 3 questions actives.";
			}

			return result;
		}

		/// <summary>
		/// Check that the test exist in DB
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		[NonAction]
		private DALE.Test CheckTestExist(int id)
		{
			TestService service = new TestService();

			DALE.Test test = service.Get(id);

			return test;
		}

		/// <summary>
		/// Avoid duplicate name for Test in DB
		/// </summary>
		/// <param name="test"></param>
		/// <returns></returns>
		[NonAction]
		private string AvoidDuplicateTestName(TestVM test)
		{
			string result = null;

			TestService service = new TestService();
			bool exist = service.GetAll().Any(t => t.Title.Equals(test.Title, StringComparison.OrdinalIgnoreCase) && t.Id != test.Id); 

			if (exist)
			{
				result = "Un test porte déjà ce nom. Deux tests ne peuvent porter le même nom.";
			}
			return result;
		}


		/// <summary>
		/// Validate the test checking if it exists and if its testSetting exist
		/// </summary>
		/// <param name="test"></param>
		/// <returns></returns>
		[NonAction]
		private string ValidateTest(TestVM test)
		{
			string result = null;

			// Check that test exist in case of edition (creation, id will be 0)
			if (test.Id > 0)
			{
				DALE.Test testExist = CheckTestExist(test.Id);

				if (testExist == null)
				{
					return "Le test que vous souhaitez éditer n'existe pas.";
				}
			}

			// Avoid duplicate test name in DB
			string avoidDuplicate = AvoidDuplicateTestName(test);

			if (avoidDuplicate != null)
			{
				return avoidDuplicate;
			}

			// No testSettings was chosen
			if (test.TestSettingsId == 0)
			{
				return "Veuillez choisir un jeu de paramètre via le menu déroulant.";
			}
			else
			{
				// Check that the test's testSettings exist
				string testSettingsExist = ControllerMethods.CheckTestSettingsExist(test.TestSettingsId);

				if (testSettingsExist != null)
				{
					return testSettingsExist;
				}
			}

			return result;
		}

		/// <summary>
		/// Display the total amount of questions and the number of active and inactive ones
		/// </summary>
		/// <param name="testQuestions"></param>
		/// <returns></returns>
		[NonAction]
		private string DisplayQuestionsInformation(int id)
		{
			QuestionService service = new QuestionService();
			List<Question> questions = service.GetAll(id).Select(q => q.DalToQuestionApp())
				.OrderBy(o => o.IsActive)
				.ToList();


			string result = null;

			if (questions.Count() > 0)
			{
				int total = questions.Count();
				int active = 0;
				int inactive = 0;

				foreach (var item in questions)
				{
					if (item.IsActive == true)
					{
						active += 1;
					}
					else
					{
						inactive += 1;
					}
				}

				if (active > 0 && inactive > 0)
				{
					result = String.Format("Il y a {0} question(s) dans ce test, dont {1} active(s) et {2} inactive(s).", total, active, inactive);
				}
				else if (active > 0 && inactive == 0)
				{
					result = String.Format("Il y a {0} question(s) active(s) dans ce test.", total);
				}
				else
				{
					result = String.Format("Il y a {0} question(s) inactive(s) dans ce test.", total);
				}
			}
			else
			{
				result = "Ce test ne comporte aucune question.";
			}

			return result;
		}

	}
}
