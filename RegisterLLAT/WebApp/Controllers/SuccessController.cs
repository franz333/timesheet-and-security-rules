﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace WebApp.Controllers
{
    public class SuccessController : Controller
    {
        [HttpGet]
        public IActionResult SuccessModal(string title, string message)
        {
            ViewBag.ModalTitle = title;
            ViewBag.Message = message;
            return PartialView("_SuccessModal");
        }
    }
}