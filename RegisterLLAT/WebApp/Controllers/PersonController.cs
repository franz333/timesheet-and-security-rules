﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL.Services;
using DALE = DAL.Entities;
using Microsoft.AspNetCore.Mvc;
using WebApp.AppTools;
using WebApp.Models.PersonViewModels;
using WebApp.Models;
using Microsoft.AspNetCore.Authorization;

namespace WebApp.Controllers
{
	[Authorize(Roles = "Super Admin, Full Admin, HR Admin")]
	public class PersonController : Controller
	{
		[HttpGet]
		public IActionResult Index()
		{
			PersonService service = new PersonService();

			List<PersonVM> persons = service.GetAll().OrderBy(o => o.LName).Select(p => p.DalToPersonVM()).ToList();

			// Prepare ViewBag for modal display in view in case of redirectToAction from Edit
			if (TempData["title"] != null && TempData["message"] != null)
			{
				ViewBag.ModalTitle = TempData["title"];
				TempData.Remove("title");

				ViewBag.ModalMessage = TempData["message"];
				TempData.Remove("message");
			}

			// Prepare filter dropdown list
			CompanyStatusService companyStatusService = new CompanyStatusService();
			ViewBag.StatusDropDown = companyStatusService.GetAll();

			return View(persons);
		}

		[HttpGet]
		public IActionResult Details(int id)
		{
			// Check that person exist in DB
			DALE.Person personExist = CheckPersonExist(id);
			if (personExist == null)
			{
				TempData["title"] = "Erreur";
				TempData["message"] = "L'employé que vous souhaitez éditer n'existe pas.";
				return RedirectToAction("Index");
			}

			// In case of redirection from create prepare modal display
			if (TempData["title"] != null && TempData["message"] != null)
			{
				ViewBag.ModalTitle = TempData["title"];
				TempData.Remove("title");

				ViewBag.ModalMessage = TempData["message"];
				TempData.Remove("message");
			}
			PersonVM person = personExist.DalToPersonVM();

			return View(person);
		}

		[HttpGet]
		public IActionResult Edit(int id)
		{
			// Check that company exist in DB
			DALE.Person personExist = CheckPersonExist(id);

			if (personExist == null)
			{
				ViewBag.ModalTitle = "Erreur";
				ViewBag.ModalMessage = "L'employé que vous souhaitez éditer n'existe pas.";
				return PartialView("_DisplayModal");
			}
			else
			{
				PersonVM person = personExist.DalToPersonVM();

				// Identify admin encoder
				person.AdminId = int.Parse(HttpContext.User.FindFirst("Id").Value);
				return PartialView("_EditPersonModal", person);
			}
		}

		[HttpPost]
		public IActionResult Edit(PersonVM person)
		{
			// Check that employee exists in DB before editing
			DALE.Person personExist = CheckPersonExist(person.Id);

			if (personExist == null)
			{
				ViewBag.ModalTitle = "Edition de l'employé";
				ViewBag.ModalMessage = "L'employé que vous souhaitez éditer n'existe pas.";
				return PartialView("_DisplayModal");
			}

			// Client side validation
			if (!ModelState.IsValid)
			{
				return PartialView("_EditPersonModal", person);
			}

			// Server side validation
			#region archive validate person
			//// Check that status Exists
			//ViewBag.Error = CheckCompanyStatus(person.CompanyStatusId);
			//if (ViewBag.Error != null)
			//{
			//	return PartialView("_EditPersonModal", person);
			//}

			//// Check that company exist with that status
			//ViewBag.Error = CheckCompany(person.CompanyStatusId, person.CompanyId);
			//if (ViewBag.Error != null)
			//{
			//	return PartialView("_EditPersonModal", person);
			//}

			//// if company is SNCB, check that department exists
			//if (person.CompanyId == 1)
			//{
			//	ViewBag.Error = CheckCompanyDepartment((int)person.DepartmentId, person.CompanyId);
			//	if (ViewBag.Error != null)
			//	{
			//		return PartialView("_EditPersonModal", person);
			//	}
			//}

			//// Check for duplicate person's LastName and FirstName within the same company
			//ViewBag.Error = CheckDuplicatePerson(person.LName, person.FName, person.CompanyId, person.Id);
			//if (ViewBag.Error != null)
			//{
			//	return PartialView("_EditPersonModal", person);
			//}
			#endregion
			
			ViewBag.Error = person.ValidatePerson(person);
			if (ViewBag.Error != null)
			{
				return PartialView("_EditPersonModal", person);
			}

			// If validation is OK, edit person
			PersonService personService = new PersonService();
			try
			{
				personService.Update(person.PersonVMToDal());
			}
			catch (Exception e)
			{
				throw e;
			}

			ViewBag.ModalTitle = "Edition de l'employé";
			ViewBag.ModalMessage = "Edition réalisée avec succès.";
			return PartialView("_SuccessModal");
		}

		[HttpGet]
		public IActionResult Create(int companyId = 0) 
		{
			PersonVM person = new PersonVM();
			person.AdminId = int.Parse(HttpContext.User.FindFirst("Id").Value);

			// If the user comes from the details page of a company where no employees are registered
			if (companyId != 0)
			{
				DALE.Company companyOk = ControllerMethods.CheckCompanyExist(companyId);

				if (companyOk != null)
				{
					person.CompanyStatusId = companyOk.CompanyStatusId;
					person.CompanyId = companyOk.Id;
				}
			}
			return View(person);
		}

		[HttpPost]
		public IActionResult Create(PersonVM person)
		{
			// Client side validation
			if (!ModelState.IsValid)
			{
				return View(person);
			}

			// Server side validation
			ViewBag.Error = person.ValidatePerson(person);

			if (ViewBag.Error != null)
			{
				return View(person);
			}

			// handle null value for department
			if (person.CompanyId != 1)
			{
				person.DepartmentId = null;
			}

			// If the user is an inter, no test is required.. 
			person = person.AvoidInternToTakeTest(person);

			// If validation is ok, insert person
			PersonService service = new PersonService();
			DALE.Person newPerson = person.PersonVMToDal();
			try
			{
				newPerson.Id = service.Insert(newPerson);
			}
			catch (Exception e)
			{
				throw e;
			}

			TempData["title"] = "Ajout d'un employé";
			TempData["message"] = "L'ajout de l'employé a été réalisé avec succès.";
			return RedirectToAction("Details", new { id = newPerson.Id });
		}

		[HttpPost]
		public IActionResult Filter(int statusId, string search, bool isNew = false)
		{
			List<PersonVM> persons = FilterPerson(statusId, search, isNew);
			return PartialView("_PersonFilter", persons);
		}

		[HttpGet]
		public IActionResult GetCompanies(int statusId)
		{
			CompanyService service = new CompanyService();
			List<Company> companies = service.GetAll(statusId).Select(c => c.DalToCompanyApp()).ToList();
			return Json(companies);
		}

		[HttpGet]
		public IActionResult GetDepartments(int companyId)
		{
			DepartmentService service = new DepartmentService();
			List<Department> departments = service.GetAllDepartmentByCompany(companyId).Select(d => d.DalToDepartmentApp()).ToList();
			return Json(departments);
		}

		#region Methods
		
		// TODO : move to controllerbase's methods?
		/// <summary>
		/// Check that the person exist in DB
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		private DALE.Person CheckPersonExist(int id)
		{
			PersonService service = new PersonService();
			DALE.Person person = service.Get(id);
			return person;
		}

		/// <summary>
		/// Returns a list of person depending on status and/or seach params and or isNew.
		/// </summary>
		/// <param name="statusId"></param>
		/// <param name="search"></param>
		/// <returns></returns>
		private List<PersonVM> FilterPerson(int statusId, string search, bool isNew = false)
		{
			PersonService service = new PersonService();
			
			List<PersonVM> persons = null;

			if (String.IsNullOrWhiteSpace(search))
			{
				// If the user makes no choice
				if (statusId == 0)
				{
					persons = service.GetAll().OrderBy(o => o.LName).Select(p => p.DalToPersonVM()).ToList();
				}
				else
				{
					// If "Intern" status is chosen, sort first by department
					if (statusId == 1)
					{
						persons = service.GetAllPersonByCompanyStatusId(statusId).OrderBy(o => o.DepartmentName).ThenBy(o => o.LName).Select(p => p.DalToPersonVM()).ToList();
					}
					else
					{
						persons = service.GetAllPersonByCompanyStatusId(statusId).OrderBy(o => o.LName).Select(p => p.DalToPersonVM()).ToList();
					}
				}
			}
			else
			{
				// If the user makes no choice
				if (statusId == 0)
				{
					//persons = service.GetAll(search).Select(q => q.DalToPersonVM()()).ToList();
					persons = service.GetAll(search).Select(p => p.DalToPersonVM()).ToList();
				}
				else
				{
					// If "Intern" status is chosen, sort first by department
					if (statusId == 1)
					{
						persons = service.GetAll(search, statusId).OrderBy(o => o.DepartmentName).ThenBy(o => o.LName).Select(p => p.DalToPersonVM()).ToList();
					}
					else
					{
						persons = service.GetAll(search, statusId).OrderBy(o => o.LName).Select(p => p.DalToPersonVM()).ToList();
					}
				}
			}

			// Filter companies added while user registration (administratorId is null)
			if (isNew == true)
			{
				persons = persons.Where(c => c.AdminId == null).ToList();
			}

			return persons;		
		}
		
		#endregion
	}
}
