﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL.Services;
using DALE = DAL.Entities;
using Microsoft.AspNetCore.Mvc;
using WebApp.AppTools;
using WebApp.Models.QuestionViewModels;
using DAL.Entities;
using Microsoft.AspNetCore.Http;
using WebApp.Models.TestViewModels;

namespace WebApp.Controllers
{
	public class QuestionController : Controller
	{
		[HttpGet]
		public IActionResult Index(int testId)
		{
			QuestionService serviceQ = new QuestionService();
			TestService serviceT = new TestService();
			
			IEnumerable<QuestionVM> questions = null;

			if (testId > 0)
			{
				questions = serviceQ.GetAll(testId).Select(q => q.DalToQuestionApp().AppToQuestionVM()).ToList();
				ViewBag.TestId = testId;
			}
			else
			{
				questions = serviceQ.GetAll().Select(q => q.DalToQuestionApp().AppToQuestionVM()).ToList();
			}

			// Prepare filter dropdown list
			ViewBag.TestDropDown = serviceT.GetAll();

			return View(questions);
		}

		[HttpPost]
		public IActionResult Filter(int testId, string search)
		{
			QuestionService service = new QuestionService();
			IEnumerable<QuestionVM> questions;

			if (String.IsNullOrWhiteSpace(search))
			{
				// If the user makes no choice
				if (testId == 0)
				{
					questions = service.GetAll().Select(q => q.DalToQuestionApp().AppToQuestionVM()).ToList();
				}
				else
				{
					questions = service.GetAll(testId).Select(q => q.DalToQuestionApp().AppToQuestionVM()).ToList();
				}
			}
			else
			{
				questions = service.GetAll(search, testId).Select(q => q.DalToQuestionApp().AppToQuestionVM()).ToList();				
			}
			return PartialView("_QuestionFilter", questions);
		}

		[HttpGet]
		public IActionResult Create()
		{
			QuestionVM questionVM = new QuestionVM();

			TestService serviceT = new TestService();
			questionVM.TestQuestion = serviceT.GetAll().Select(t => t.DalToTestQuestionVM()).ToList();

			return PartialView("_AddQuestionModal", questionVM);
		}

		[HttpPost]
		public IActionResult Create(IFormCollection form)
		{
			// Get question
			Question question = GetQuestionFromForm(form);

			// Get tests
			List<int> tests = GetTests(form);

			// Get ids
			List<int> newIds = GetNewQcmIds(form);

			// Get new qcmItems
			List<DALE.QcmItem> newQcmItems = GetNewQcm(form, newIds);

			// Get the number of answers
			int hasOneAnswer = GetNumberOfAnswers(newQcmItems);

			// Check that conditions are met to modify the question
			int totalQcmItem = newQcmItems.Count();

			// validation
			ViewBag.Error = ValidateIForm(form, totalQcmItem, hasOneAnswer, tests);

			if (ViewBag.Error != null)
			{
				QuestionVM returnQuestionVM = ReturnQuestionVM(question, tests, newQcmItems);
				return PartialView("_AddQuestionModal", returnQuestionVM);
			}

			// Insert new question
			QuestionService serviceQ = new QuestionService();
			int insertedQuestion = 0;

			try
			{
				insertedQuestion = serviceQ.Insert(question);
			}
			catch (Exception e)
			{
				throw e;
			}

			// Check in qcmitems if existing records could be used
			CheckToUseExistingQcmItems(newQcmItems, question.Id);

			// Save tests to which the question is linked
			TestQuestionService serviceTQ = new TestQuestionService();

			// TODO : Check if tests are existing in DB before inserting
			foreach (var item in tests)
			{
				serviceTQ.Insert(item, insertedQuestion);
			}

			// Return model when everything is completed
			QuestionVM questionVM = serviceQ.Get(insertedQuestion).DalToQuestionApp().AppToQuestionVM();

			return PartialView("_QuestionTable", questionVM);
		}

		[HttpGet]
		public IActionResult Edit(int Id)
		{
			QuestionService service = new QuestionService();
			QuestionVM question = service.Get(Id).DalToQuestionApp().AppToQuestionVM();

			// TODO : check if question exists

			// Get tests linked to the question
			question.TestQuestion = GetTestQuestion(Id);

			return PartialView("_EditQuestionModal", question);
		}

		[HttpPost]
		public IActionResult Edit(IFormCollection form)
		{
			QuestionService serviceQ = new QuestionService();

			// Check that QuestionExist
			Question questionExist = serviceQ.Get(int.Parse(form["Id"]));

			if (questionExist == null)
			{
				ViewBag.ModalTitle = "Modification de la question";
				ViewBag.Message = "Cette question n'existe pas.";
				return PartialView("_ErrorModal");		
			}

			// Get question
			Question question = GetQuestionFromForm(form);

			// Get tests
			List<int> tests = GetTests(form);

			// Get ids
			List<int> ExistingIds = GetExistingQcmIds(form);
			List<int> newIds = GetNewQcmIds(form);

			// Get existing qcmItems
			List<DALE.QcmItem> existingQcmItems = GetExistingQcm(form, ExistingIds);

			// Get new qcmItems
			List<DALE.QcmItem> newQcmItems = GetNewQcm(form, newIds);

			// Get the number of answers
			int hasOneAnswer = GetNumberOfAnswers(existingQcmItems, newQcmItems);

			// Check that conditions are met to modify the question
			int totalQcmItem = existingQcmItems.Count() + newQcmItems.Count();

			// validation
			ViewBag.Error = ValidateIForm(form, totalQcmItem, hasOneAnswer);
						
			if (ViewBag.Error != null)
			{
				QuestionVM returnQuestionVM = new QuestionVM();
				returnQuestionVM.Id = question.Id;
				returnQuestionVM.Title = question.Title;
				returnQuestionVM.IsActive = question.IsActive;
				returnQuestionVM.QcmItems = existingQcmItems.Select(a => a.DalToQcmItemApp()).Concat(newQcmItems.Select(a => a.DalToQcmItemApp()));

				return PartialView("_EditQuestionModal", returnQuestionVM);
			}

			// Update Existing QcmItems
			UpdateExistingQcmItems(existingQcmItems, question.Id);

			// Use existing QcmItems or create new ones
			CheckToUseExistingQcmItems(newQcmItems, question.Id);

			// Update Question
			try
			{
				serviceQ.Update(question);
			}
			catch (Exception e)
			{
				throw e;
			}

			// Update the test in which the question appear
			UpdateTestsQuestion(tests, question.Id);

			// Return updated data
			TestService serviceT = new TestService();

			QuestionVM newQuestionVm = serviceQ.Get(question.Id).DalToQuestionApp().AppToQuestionVM();
			newQuestionVm.Tests = serviceT.GetAll(question.Id).Select(t => t.DalToTestApp()).ToList();

			return PartialView("_QuestionTable", newQuestionVm);
		}

		/// <summary>
		/// Adds or removes the question that appear in one or several tests
		/// </summary>
		/// <param name="tests"></param>
		/// <param name="questionId"></param>
		[NonAction]
		private void UpdateTestsQuestion(List<int> tests, int questionId)
		{
			TestService serviceT = new TestService();			

			// Retrieve the Id of existing tests
			List<int> existingTestIds = serviceT.GetAll().Select(t => t.Id).ToList();

			// Retrieve the Id of the test in wich the question appears in DB
			List<int> dbTestQuestionsIds = serviceT.GetAll(questionId).Select(t => t.Id).ToList();

			// Create a list of TestQuestionVM with test checked in the form
			List<TestQuestionVM> formTestQuestions = new List<TestQuestionVM>();

			// Create a list of TestQuestionVM with test checked in DB
			List<TestQuestionVM> dbTestQuestions = new List<TestQuestionVM>();

			// Populate the two list with tests checked in DB and in the Form
			for (int i = 0; i < existingTestIds.Count(); i++)
			{
				TestQuestionVM testQuestionVMForm = new TestQuestionVM();

				for (int j = 0; j < tests.Count(); j++)
				{
					// If the Id of the existing test is found in the Test present in the form link the question to the test
					if (existingTestIds[i] == tests[j])
					{
						testQuestionVMForm.IsLinkedToQuestion = true;
					}
				}
				// Give the testQuestionVMForm the id of the existing test
				testQuestionVMForm.Id = existingTestIds[i];

				// Add the TestQuestionVM to the list
				formTestQuestions.Add(testQuestionVMForm);

				TestQuestionVM testQuestionVMDB = new TestQuestionVM();

				for (int k = 0; k < dbTestQuestionsIds.Count(); k++)
				{
					// If the Id of the existing test is found in the Test present in the form link the question to the test
					if (existingTestIds[i] == dbTestQuestionsIds[k])
					{
						testQuestionVMDB.IsLinkedToQuestion = true;
					}
				}

				// Give the testQuestionVMDB the id of the existing test
				testQuestionVMForm.Id = existingTestIds[i];

				// Add the TestQuestionVM to the list
				dbTestQuestions.Add(testQuestionVMDB);
			}


			TestQuestionService serviceTQ = new TestQuestionService();

			// Compare the two lists and save changes
			for (int i = 0; i < existingTestIds.Count(); i++)
			{
				if (formTestQuestions[i].IsLinkedToQuestion != dbTestQuestions[i].IsLinkedToQuestion)
				{
					if (formTestQuestions[i].IsLinkedToQuestion == true)
					{
						serviceTQ.Insert(new TestQuestion() 
						{ 
							TestId = existingTestIds[i],
							QuestionId = questionId
						});
					}
					else
					{
						serviceTQ.Delete(existingTestIds[i], questionId);
					}
				}
			}
		}

		[NonAction]
		private string ValidateIForm(IFormCollection form, int totalQcmItem, int hasOneAnswer)
		{
			string result = null;

			foreach (string key in form.Keys)
			{
				if (form[key] == "")
				{
					result = "Une ou plusieurs valeurs n'a pas été fournie. Vérifiez bien tous les champs textes.";
				}

				if (key.Contains("Title") || key.Contains("item") || key.Contains("Item"))
				{
					if (String.IsNullOrWhiteSpace(form[key]))
					{
						result = "Une ou plusieurs valeurs n'a pas été fournie. Vérifiez bien tous les champs textes.";
					}
				}
			}

			if (totalQcmItem < 2 || hasOneAnswer > 1 || hasOneAnswer < 1)
			{
				if (result != null)
				{
					result += "\n" + "Une question doit comporter au moins deux Qcm dont l'un est la réponse à la question.";
				}
				else
				{
					result = "Une question doit comporter au moins deux Qcm dont l'un est la réponse à la question.";
				}
			}

			return result;		
		}

		[NonAction]
		private string ValidateIForm(IFormCollection form, int totalQcmItem, int hasOneAnswer, List<int> tests)
		{
			string result = null;

			foreach (string key in form.Keys)
			{
				if (form[key] == "")
				{
					result = "Une ou plusieurs valeurs n'a pas été fournie. Vérifiez bien tous les champs textes.";
				}

				if (key.Contains("Title") || key.Contains("item") || key.Contains("Item"))
				{
					if (String.IsNullOrWhiteSpace(form[key]))
					{
						result = "Une ou plusieurs valeurs n'a pas été fournie. Vérifiez bien tous les champs textes.";
					}
				}
			}

			if (totalQcmItem < 2 || hasOneAnswer > 1 || hasOneAnswer < 1)
			{
				if (result != null)
				{
					result += "\n" + "Une question doit comporter au moins deux Qcm dont l'un est la réponse à la question.";
				}
				else
				{
					result = "Une question doit comporter au moins deux Qcm dont l'un est la réponse à la question.";
				}
			}

			if (tests.Count() == 0)
			{
				if (result != null)
				{
					result += "\n" + "Une question doit être liée à au moins un test.";
				}
				else
				{
					result = "Une question doit être liée à au moins un test.";
				}
			}

			return result;
		}

		/// <summary>
		/// Create a Question entity with form data
		/// </summary>
		/// <param name="form"></param>
		/// <returns></returns>
		[NonAction]
		private Question GetQuestionFromForm(IFormCollection form)
		{
			Question question = new Question();

			question.Id = int.Parse(form["Id"]);
			question.Title = form["Title"];
			question.IsActive = Convert.ToBoolean(form["IsActive"].ToString().Split(',')[0]);

			return question;
		}

		/// <summary>
		/// Get QcmItem's id that exist in DB from the form
		/// </summary>
		/// <param name="form"></param>
		/// <returns></returns>
		[NonAction]
		private List<int> GetExistingQcmIds(IFormCollection form)
		{
			List<int> ExistingIds = new List<int>();

			foreach (var key in form.Keys)
			{
				if (key.Contains("item"))
				{
					int id = int.Parse(key.Replace("item", ""));
					ExistingIds.Add(id);
				}
			}
			return ExistingIds;
		}

		/// <summary>
		/// Get manually created QcmItem's id from the form. They correspond to new QcmItem created by the user.
		/// </summary>
		/// <param name="form"></param>
		/// <returns></returns>
		[NonAction]
		private List<int> GetNewQcmIds(IFormCollection form)
		{
			List<int> newIds = new List<int>();

			foreach (var key in form.Keys)
			{
				if (key.Contains("addedItem"))
				{
					int id = int.Parse(key.Replace("addedItem", ""));
					newIds.Add(id);
				}
			}
			return newIds;
		}

		/// <summary>
		/// Get QcmItem registered in DB from the form.
		/// </summary>
		/// <param name="form"></param>
		/// <param name="ExistingIds"></param>
		/// <returns></returns>
		[NonAction]
		private List<DALE.QcmItem> GetExistingQcm(IFormCollection form, List<int> ExistingIds)
		{
			List<DALE.QcmItem> qcmItems = new List<DALE.QcmItem>();

			if (ExistingIds.Count() > 0)
			{
				for (int i = 0; i < ExistingIds.Count(); i++)
				{
					DALE.QcmItem qcmItem = new DALE.QcmItem();
					qcmItem.QcmItemId = ExistingIds[i];
					qcmItem.Item = form["item" + ExistingIds[i].ToString()];
					qcmItem.IsAnswer = bool.Parse(form["IsAnswer" + ExistingIds[i].ToString()]);

					qcmItems.Add(qcmItem);
				}
			}

			return qcmItems;
		}

		/// <summary>
		/// Get QcmItem added by the user with the form
		/// </summary>
		/// <param name="form"></param>
		/// <param name="newIds"></param>
		/// <returns></returns>
		[NonAction]
		private List<DALE.QcmItem> GetNewQcm(IFormCollection form, List<int> newIds)
		{
			List<DALE.QcmItem> newQcmItems = new List<DALE.QcmItem>();

			if (newIds.Count() > 0)
			{
				for (int i = 0; i < newIds.Count(); i++)
				{
					DALE.QcmItem qcmItem = new DALE.QcmItem();
					qcmItem.QcmItemId = newIds[i];
					qcmItem.Item = form["addedItem" + newIds[i].ToString()];
					qcmItem.IsAnswer = bool.Parse(form["addedIsAnswer" + newIds[i].ToString()]);

					newQcmItems.Add(qcmItem);
				}
			}

			return newQcmItems;
		}

		/// <summary>
		/// Get the number of QcmItem that are marked as being the answer to a question
		/// </summary>
		/// <param name="existingQcm"></param>
		/// <param name="newQcm"></param>
		/// <returns></returns>
		[NonAction]
		private int GetNumberOfAnswers(List<DALE.QcmItem> existingQcm, List<DALE.QcmItem> newQcm)
		{
			int hasAnswer = 0;

			if (existingQcm.Count() > 0)
			{
				foreach (var item in existingQcm)
				{
					if (item.IsAnswer == true)
					{
						hasAnswer++;
					}
				}
			}

			if (newQcm.Count() > 0)
			{
				foreach (var item in newQcm)
				{
					if (item.IsAnswer == true)
					{
						hasAnswer++;
					}
				}
			}

			return hasAnswer;
		}

		/// <summary>
		/// Get the number of QcmItem that are marked as being the answer to a question
		/// </summary>
		/// <param name="newQcm"></param>
		/// <returns></returns>
		[NonAction]
		private int GetNumberOfAnswers(List<DALE.QcmItem> newQcm)
		{
			int hasAnswer = 0;

			if (newQcm.Count() > 0)
			{
				foreach (var item in newQcm)
				{
					if (item.IsAnswer == true)
					{
						hasAnswer++;
					}
				}
			}

			return hasAnswer;
		}

		/// <summary>
		/// Update or remove QcmItems linked to a given question
		/// </summary>
		/// <param name="existingQcmItems"></param>
		/// <param name="questionId"></param>
		[NonAction]
		private void UpdateExistingQcmItems(List<DALE.QcmItem> existingQcmItems, int questionId)
		{
			QcmItemService serviceQcm = new QcmItemService();

			QuestionQcmItemService serviceQuesQcm = new QuestionQcmItemService();

			List<DALE.QcmItem> questionQcmItems = serviceQcm.GetAll(questionId).ToList();

			int counter = 0;

			// Loop through existingQmcItems to find for updates or removals
			if (existingQcmItems.Count() > 0)
			{
				for (int i = 0; i < questionQcmItems.Count(); i++)
				{
					bool found = false;

					while (found == false && counter < existingQcmItems.Count())
					{
						if (questionQcmItems[i].QcmItemId == existingQcmItems[i].QcmItemId)
						{
							found = true;
						}
						counter++;
					}

					// If the form contains the existing QcmItem, update it
					if (found == true)
					{
						serviceQcm.Update(existingQcmItems[i]);
					}
					// If not, delete it (remove the link between question and qcmitem)
					else
					{
						serviceQuesQcm.Delete(questionId, questionQcmItems[i].QcmItemId);
					}
				}
			}
			else
			{
				for (int i = 0; i < questionQcmItems.Count(); i++)
				{
					serviceQuesQcm.Delete(questionId, existingQcmItems[i].QcmItemId);
				}
			}
		}

		/// <summary>
		/// Get the test's id of the test to which the user wants the question to appear
		/// </summary>
		/// <param name="form"></param>
		/// <returns></returns>
		[NonAction]
		private List<int> GetTests(IFormCollection form)
		{
			List<int> tests = new List<int>();

			foreach (var key in form.Keys)
			{
				if (key.Contains("test"))
				{
					int id = int.Parse(key.Replace("test", ""));
					tests.Add(id);
				}
			}
			return tests;
		}

		/// <summary>
		/// Get tests in which a question appear for checkboxes display
		/// </summary>
		/// <param name="questionId"></param>
		/// <returns></returns>
		[NonAction]
		private List<TestQuestionVM> GetTestQuestion(int questionId)
		{
			TestService serviceT = new TestService();

			List<TestQuestionVM> testQuestionVMs = serviceT.GetAll().Select(t => t.DalToTestQuestionVM()).ToList();

			List<Test> existingTests = serviceT.GetAll(questionId).ToList();

			for (int i = 0; i < existingTests.Count(); i++)
			{
				for (int j = 0; j < testQuestionVMs.Count(); j++)
				{
					if (existingTests[i].Id == testQuestionVMs[j].Id)
					{
						testQuestionVMs[j].IsLinkedToQuestion = true;
					}
				}
			}
			return testQuestionVMs;
		}

		/// <summary>
		/// Check in DB if the QcmItem exists in DB (compare title and if the item is the answer or not)
		/// </summary>
		/// <param name="newQcmItems"></param>
		/// <param name="questionId"></param>
		[NonAction]
		private void CheckToUseExistingQcmItems(List<DALE.QcmItem> newQcmItems, int questionId)
		{
			// Check in qcmitems if existing records could be used
			QcmItemService serviceQcm = new QcmItemService();
			QuestionQcmItemService serviceQQcm = new QuestionQcmItemService();

			int existingQcmItem = 0;

			for (int i = 0; i < newQcmItems.Count(); i++)
			{
				existingQcmItem = serviceQcm.GetExistingQcmItem(newQcmItems[i]);

				// If the item is existing in DB, get the Id and link it to the question
				if (existingQcmItem > 0)
				{
					serviceQQcm.Insert(questionId, existingQcmItem);
				}
				// Insert Item (the link with the question is done in the SP)
				else
				{
					serviceQcm.Insert(newQcmItems[i], questionId);
				}
			}
		}

		[NonAction]
		private QuestionVM ReturnQuestionVM(Question question, List<int> tests, List<DALE.QcmItem> qcmItems)
		{
			QuestionVM returnQuestionVM = new QuestionVM();
			returnQuestionVM.Id = question.Id;
			returnQuestionVM.Title = question.Title;
			returnQuestionVM.IsActive = question.IsActive;
			returnQuestionVM.QcmItems = qcmItems.Select(a => a.DalToQcmItemApp());

			TestService testService = new TestService();
			returnQuestionVM.TestQuestion = testService.GetAll().Select(t => t.DalToTestQuestionVM()).ToList();

			for (int i = 0; i < tests.Count(); i++)
			{
				for (int j = 0; j < returnQuestionVM.TestQuestion.Count(); j++)
				{
					if (returnQuestionVM.TestQuestion[j].Id == tests[i])
					{
						returnQuestionVM.TestQuestion[j].IsLinkedToQuestion = true;
					}
				}
			}

			return returnQuestionVM;
		}
	}
}
