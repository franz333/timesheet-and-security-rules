﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace WebApp.Controllers
{
	public class ModalController : Controller
	{
		public IActionResult Index()
		{
			return View();
		}

		[HttpGet]
		public IActionResult DisplayModal(string title, string message)
		{
			ViewBag.ModalTitle = title;
			ViewBag.ModalMessage = message;
			return PartialView("_DisplayModal");
		}
	}
}
