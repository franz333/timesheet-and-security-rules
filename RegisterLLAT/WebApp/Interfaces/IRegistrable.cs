﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Models;

namespace WebApp.Interfaces
{
	interface IRegistrable<TEntity>
	{
		TEntity SetCompany(TEntity entity);

		TEntity SetPerson(TEntity entity);

		TEntity AvoidInternToTakeTest(TEntity entity);

		TEntity UserToSetCompanyProps(User user);
	}
}
