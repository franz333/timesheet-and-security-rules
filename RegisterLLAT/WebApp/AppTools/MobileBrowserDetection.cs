﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace WebApp.AppTools
{
	public static class MobileBrowserDetection
	{
		public static bool IsMobileDevice(string userAgent) 
		{
			bool ret = false;

			if (userAgent != null)
			{
				if (userAgent.Contains("Mobi"))
				{
					ret = true;
				}
			}
			return ret;

		}

	}
}
