﻿using DAL.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DALE = DAL.Entities;
using WebApp.Models;

namespace WebApp.AppTools
{
	public static class ControllerMethods
	{
		/// <summary>
		/// Check that FirstName and LastName are provided. Returns a null string if everything is Ok.
		/// Assign returned value to ViewBag if returned string is not null
		/// </summary>
		/// <param name="name"></param>
		/// <param name="lastname"></param>
		/// <returns></returns>
		public static string NameAndLastNameValid(string name, string lastname)
		{
			string result;

			if (!String.IsNullOrWhiteSpace(name) && !String.IsNullOrWhiteSpace(lastname))
			{
				result = null;
			}
			else
			{
				if (String.IsNullOrWhiteSpace(lastname) && String.IsNullOrWhiteSpace(name))
				{
					result = "Le nom de famille et le prénom sont requis.";
				}
				else if (String.IsNullOrWhiteSpace(lastname) && !String.IsNullOrWhiteSpace(name))
				{
					result = "Le nom de famille est requis.";
				}
				else
				{
					result = "Le prénom est requis.";
				}
			}
			return result;
		}

		/// <summary>
		/// Check that the company exists in DB
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		public static DALE.Company CheckCompanyExist(int id)
		{
			CompanyService service = new CompanyService();
			DALE.Company company = service.Get(id);
			return company;
		}

		/// <summary>
		/// Check that the company exist under a given statusId
		/// </summary>
		/// <param name="companyId"></param>
		/// <param name="statusId"></param>
		/// <returns></returns>
		public static string CheckCompanyExist(int companyId, int statusId)
		{
			string result = null;

			CompanyService service = new CompanyService();
			DALE.Company company = service.Get(companyId, statusId);

			if (company == null)
			{
				result = "Cette société n'existe pas.";
			}

			return result;
		}

		/// <summary>
		/// Check that the companyStatus exists in DB
		/// </summary>
		/// <param name="statusId"></param>
		/// <returns></returns>
		public static string CheckCompanyStatus(int statusId)
		{
			string result = null;

			// No choice was done by the user
			if (statusId == 0)
			{
				result = "Veuillez sélectionner un statut dans la liste.";
			}
			else
			{
				CompanyStatusService service = new CompanyStatusService();
				DALE.CompanyStatus status = service.Get(statusId);

				if (status == null)
				{
					result = "Ce statut n'existe pas.";
				}
			}
			return result;
		}

		/// <summary>
		/// Check that the testSetting exist in DB
		/// </summary>
		/// <param name="testSettingsId"></param>
		/// <returns></returns>
		public static string CheckTestSettingsExist(int testSettingsId)
		{
			string result = null;

			TestSettingsService service = new TestSettingsService();

			DALE.TestSettings testSettings = service.Get(testSettingsId);

			if (testSettings == null)
			{
				result = "Ce paramètre de test n'existe pas";
			}

			return result;
		}

		/// <summary>
		/// Find a user's testId
		/// </summary>
		/// <param name="personId"></param>
		/// <returns></returns>
		public static int FindUserTestId(int personId)
		{
			int companyId, testId;

			// Find person's companyId
			PersonService serviceP = new PersonService();
			try
			{
				companyId = serviceP.Get(personId).CompanyId;
			}
			catch (Exception e)
			{
				throw e;
			}

			// Find testId
			CompanyService serviceC = new CompanyService();
			try
			{
				testId = serviceC.Get(companyId).TestId;
			}
			catch (Exception e)
			{
				throw e;
			}
			return testId;
		}
	}


}
