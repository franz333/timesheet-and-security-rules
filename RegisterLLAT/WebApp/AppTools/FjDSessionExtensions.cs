﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Models.RegisterViewModels;

namespace WebApp.AppTools
{
    // Session is able to store only byte of an array. 
    // ASP.NET Core does not perform any operation such as serialization/ de-serialization on the values stored in session.
    // The JSON convertion has to be done manually. 
    public static class FjDSessionExtensions
	{
        public static T GetComplexData<T>(this ISession session, string key)
        {
            var data = session.GetString(key);
            if (data == null)
            {
                return default(T);
            }
            return JsonConvert.DeserializeObject<T>(data);
        }

        public static void SetComplexData(this ISession session, string key, object value)
        {
            session.SetString(key, JsonConvert.SerializeObject(value));
        }
    }
}
