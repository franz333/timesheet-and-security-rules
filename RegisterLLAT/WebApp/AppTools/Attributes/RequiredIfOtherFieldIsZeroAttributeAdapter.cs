﻿using Microsoft.AspNetCore.Mvc.DataAnnotations;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using Microsoft.Extensions.Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.AppTools.Attributes
{
    // An 'AttributeAdapter' is for the client side validation
    // It gets called while rendering the view and allows to add custom html attributes to the element to be checked when extending the jQuery Unobtrusive library. 

    public class RequiredIfOtherFieldIsZeroAttributeAdapter : AttributeAdapterBase<RequiredIfOtherFieldIsZeroAttribute>
    {
        public RequiredIfOtherFieldIsZeroAttributeAdapter(RequiredIfOtherFieldIsZeroAttribute attribute, IStringLocalizer stringLocalizer)
            : base(attribute, stringLocalizer)
        {
        }

        public override void AddValidation(ClientModelValidationContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }
            context.Attributes.Add("data-val", "true");
            context.Attributes.Add("data-val-required-if-otherfield-is-zero", GetErrorMessage(context));
            context.Attributes.Add("data-val-required-if-otherfield-is-zero-otherfield", Attribute.PropertyName);
        }

        public override string GetErrorMessage(ModelValidationContextBase validationContext)
        {
            if (validationContext == null)
            {
                throw new ArgumentNullException(nameof(validationContext));
            }

            return GetErrorMessage(validationContext.ModelMetadata, validationContext.ModelMetadata.GetDisplayName());
        }
    }
}
