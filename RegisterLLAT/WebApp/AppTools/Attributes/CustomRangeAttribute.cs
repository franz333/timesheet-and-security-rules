﻿using System.ComponentModel.DataAnnotations;

namespace WebApp.AppTools.Attributes
{
	public class CustomRangeAttribute : ValidationAttribute
	{
		public string Min { get; }

		public string Max { get; } 

		public CustomRangeAttribute(string min, string max)
		{
			this.Min = min;
			this.Max = max;
		}

		protected override ValidationResult IsValid(object value, ValidationContext validationContext)
		{
			// Creation of the object used in the validationContext : the other attribute used to validate the current value

			var minProperty = validationContext?
			.ObjectInstance?
			.GetType()?
			.GetProperty(Min)?
			.GetValue(validationContext.ObjectInstance);

			var maxProperty = validationContext?
			.ObjectInstance?
			.GetType()?
			.GetProperty(Max)?
			.GetValue(validationContext.ObjectInstance);

			if (Min != null && Max != null)
			{
				int minValue = (int)minProperty;
				int maxValue = (int)maxProperty;

				int currentValue = (int)value;

				if (currentValue < minValue || currentValue > maxValue)
				{
					return new ValidationResult(string.Format(ErrorMessage,	minValue, maxValue));
				}
			}

			return ValidationResult.Success;
		}
	}
}
