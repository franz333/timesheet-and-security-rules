﻿using Microsoft.AspNetCore.Mvc.DataAnnotations;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using Microsoft.Extensions.Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.AppTools.Attributes
{
    public class CustomRangeAttributeAdapter : AttributeAdapterBase<CustomRangeAttribute>
    {
        public CustomRangeAttributeAdapter(CustomRangeAttribute attribute, IStringLocalizer stringLocalizer) 
            : base(attribute, stringLocalizer)
        {

        }

        public override void AddValidation(ClientModelValidationContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }
            context.Attributes.Add("data-val", "true");
            context.Attributes.Add("data-val-custom-range", GetErrorMessage(context));
            context.Attributes.Add("data-val-custom-range-min", Attribute.Min);
            context.Attributes.Add("data-val-custom-range-max", Attribute.Max);
        }

        public override string GetErrorMessage(ModelValidationContextBase validationContext)
        {
            if (validationContext == null)
            {
                throw new ArgumentNullException(nameof(validationContext));
            }
            return GetErrorMessage(validationContext.ModelMetadata, validationContext.ModelMetadata.GetDisplayName());
        }
    }
}
