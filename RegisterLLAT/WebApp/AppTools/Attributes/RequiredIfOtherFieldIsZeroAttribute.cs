﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.AppTools.Attributes
{
	public class RequiredIfOtherFieldIsZeroAttribute : ValidationAttribute
	{
		// CTOR
		public string PropertyName { get; }

		public RequiredIfOtherFieldIsZeroAttribute(string PropertyName)
		{
			this.PropertyName = PropertyName;
		}

		protected override ValidationResult IsValid(object value, ValidationContext validationContext)
		{
			// Creation of the object used in the validationContext : the other attribute used to validate the current value
			object propertyIsZero = validationContext?
			.ObjectInstance?
			.GetType()?
			.GetProperty(PropertyName)?
			.GetValue(validationContext.ObjectInstance);

			// If the attribute is not null, proceed to check  
			if (propertyIsZero != null)
			{
				if (propertyIsZero.ToString() == "0" && value == null)
				{
					return new ValidationResult(FormatErrorMessage(validationContext.DisplayName));
				}
			}
			return ValidationResult.Success;

			#region Without AttributeAdapter
			// To implement custom data annotation without AttributeAdapter, we can uncomment all the code underneath  
			// Add the IClientModelValidator Interface as parent of this current class
			// Remove the RequiredIfOtherFieldIsZeroAttributeAdapter Class
			// Remove the CustomAttributeAdapterProviderRegistration
			// Remove the registration in Startup Class

			//public void AddValidation(ClientModelValidationContext context)
			//{
			//    if (context == null)
			//    {
			//        throw new ArgumentNullException(nameof(context));
			//    }
			//    context.Attributes.Add("data-val", "true");
			//    context.Attributes.Add("data-val-required-if-otherfield-is-zero", ErrorMessage);
			//    context.Attributes.Add("data-val-required-if-otherfield-is-zero-otherfield", PropertyName);
			//}

			//public void AddValidation(ClientModelValidationContext context)
			//      {
			//          MergeAttribute(context.Attributes, "data-val", "true");
			//          var errorMessage = FormatErrorMessage(context.ModelMetadata.GetDisplayName());
			//          MergeAttribute(context.Attributes, "data-val-requiredifotherfieldiszero", errorMessage);
			//          MergeAttribute(context.Attributes, "data-val-requiredifotherfieldiszero-otherfield", Attribute.);
			//      }

			//      private bool MergeAttribute(IDictionary<string, string> attributes, string key, string value)
			//      {
			//          if (attributes.ContainsKey(key))
			//          {
			//              return false;
			//          }
			//          attributes.Add(key, value);
			//          return true;
			//      }
			#endregion
		}
	}
}
