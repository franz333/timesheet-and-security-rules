﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Models.RegisterViewModels;

namespace WebApp.AppTools.Attributes
{
	public class EmptyOrPositiveAttribute : ValidationAttribute
	{
		protected override ValidationResult IsValid(object value, ValidationContext validationContext) 
		{
			if (value != null)
			{
				// Check if it's a valid number 
				if (!int.TryParse(value.ToString(), out int valueInt) || valueInt < 0)
				{
					return new ValidationResult(FormatErrorMessage(validationContext.DisplayName));
				}
			}
			return ValidationResult.Success;
		}
	}
}
