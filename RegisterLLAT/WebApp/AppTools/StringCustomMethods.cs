﻿using Microsoft.AspNetCore.Html;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.AppTools
{
	public static class StringCustomMethods
	{
		public static string GetInitialsFromFirstName(string name)
		{
			// Trim potential white space before and after the string
			name = name.Trim();

			// Initialise separator
			char sep = '0';

			if (name.Contains("-") || name.Contains("_"))
			{
				sep = '-';
			}
			else if (name.Contains(" "))
			{
				sep = ' ';
			}

			// Split string by separator (transform array into list)			
			List<string> list = name.Split(sep).ToList();

			// Initialise counter
			int counter = list.Count();

			// Remove any potential unwanted blank space between words
			for (int i = 0; i < counter; i++)
			{
				if (list[i] == "")
				{
					list.RemoveAt(i);
					counter--;
					i--;
				}
			}

			// Retrieve first letter toUpper
			counter = list.Count();
			string result = " ";

			for (int i = 0; i < counter; i++)
			{
				if ((sep == '-') && (i % 2 == 0))
				{
					result += list[i].Substring(0, 1).ToUpper() + sep;
				}
				else
				{
					result += list[i].Substring(0, 1).ToUpper();
				}
			}

			result = result + ".";
			return result;
		}


		/// <summary>
		/// FjD : Convert a boolean value into a htmlstring for rendering in a view
		/// </summary>
		/// <param name="value"></param>
		/// <param name="valIfYes"></param>
		/// <param name="valIfNo"></param>
		/// <returns></returns>
		public static HtmlString BoolToHtmlString(this bool value, string valIfYes, string valIfNo)
		{
			var text = "";

			if (value == true)
			{
				text = valIfYes;
			}
			else
			{
				text = valIfNo;
			}

			return new HtmlString(text);
		}

		public static HtmlString IntIsInOrderToString(this int value, string valIfToBeTaken, string valIfSuccess, string valIfFailure) 
		{
			var text = "";

			if (value == 0)
			{
				text = valIfToBeTaken;
			}
			else if (value == 1)
			{
				text = valIfSuccess;
			}
			else
			{
				text = valIfFailure;
			}

			return new HtmlString(text);

		}
	}
}
