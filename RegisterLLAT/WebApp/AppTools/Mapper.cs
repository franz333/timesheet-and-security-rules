﻿using Dal = DAL.Entities;
using App = WebApp.Models;
using WebApp.Models.RegisterViewModels;
using WebApp.Models;
using WebApp.Models.TestViewModels;
using System.Linq;
using WebApp.Models.QuestionViewModels;
using WebApp.Models.TestSettingsViewModels;
using WebApp.Models.CompanyViewModels;
using WebApp.Models.PersonViewModels;
using WebApp.Models.AdminViewModels;
using WebApp.Models.RoleTypeViewModels;
using WebApp.Models.PersonTestViewModels;

namespace WebApp.AppTools
{
	public static class Mapper
	{
		#region RoleType
		//public static Dal.RoleType ToDal(this App.RoleType roleType)
		//{
		//	return (roleType == null) ? null : new Dal.RoleType()
		//	{
		//		Id = roleType.Id ?? null,
		//		Title = roleType.Title,
		//		RoleTypeDescription = roleType.RoleTypeDescription
		//	};
		//}
		public static RoleType DalToRoleTypeApp(this Dal.RoleType roleType)
		{
			return (roleType == null) ? null : new RoleType()
			{
				Id = roleType.Id,
				Title = roleType.Title,
				RoleTypeDescription = roleType.RoleTypeDescription
			};
		}

		public static RoleTypeVM DalToRoleTypeVM(this Dal.RoleType roleType)
		{
			return (roleType == null) ? null : new RoleTypeVM()
			{
				Id = roleType.Id,
				Title = roleType.Title,
				RoleTypeDescription = roleType.RoleTypeDescription
			};
		}
		#endregion

		#region Admin

		public static Dal.Admin AdminFormToDal(this AdminForm admin)
		{
			return (admin == null) ? null : new Dal.Admin()
			{
				Id = admin.Id,
				Email = admin.Email,
				FName = admin.FName,
				LName = admin.LName,
				Deleted = admin.Deleted,
				IsActive = admin.IsActive,
				Pwd = admin.Pwd.ApplyHash(),
				RoleTypeId = admin.RoleId
			};
		}

		public static AdminForm ToAppAdminForm(this Dal.Admin admin)
		{
			return (admin == null) ? null : new AdminForm()
			{
				Id = admin.Id,
				Email = admin.Email,
				FName = admin.FName,
				LName = admin.LName,
				Deleted = admin.Deleted,
				IsActive = admin.IsActive,
				Pwd = "*************",
				RoleId = admin.RoleTypeId,
				Role = admin.Role,
				RoleDescription = admin.RoleDescription
			};
		}

		public static AdminVM DalToAdminVM(this Dal.Admin admin)
		{
			return (admin == null) ? null : new AdminVM()
			{
				Id = admin.Id,
				Email = admin.Email,
				FName = admin.FName,
				LName = admin.LName,
				Deleted = admin.Deleted,
				IsActive = admin.IsActive,
				RoleId = admin.RoleTypeId,
				Role = admin.Role,
				RoleDescription = admin.RoleDescription
			};
		}

		public static Dal.Admin AdminVMToDal(this AdminVM admin)
		{
			return (admin == null) ? null : new Dal.Admin()
			{
				Id = admin.Id,
				Email = admin.Email,
				FName = admin.FName,
				LName = admin.LName,
				Deleted = admin.Deleted,
				IsActive = admin.IsActive,
				RoleTypeId = admin.RoleId
			};
		}

		//public static Dal.Admin ToDal(this App.Admin admin)
		//{
		//	return (admin == null) ? null : new Dal.Admin()
		//	{
		//		Id = admin.Id,
		//		Email = admin.Email,
		//		FName = admin.FName,
		//		LName = admin.LName,
		//		Deleted = admin.Deleted,
		//		IsActive = admin.IsActive,
		//		Pwd = admin.Pwd.ApplyHash(),
		//		RoleTypeId = admin.RoleTypeId
		//	};
		//}

		public static Admin DalToAdminApp(this Dal.Admin admin)
		{
			return (admin == null) ? null : new Admin()
			{
				Id = admin.Id,
				Email = admin.Email,
				FName = admin.FName,
				LName = admin.LName,
				IsActive = admin.IsActive,
				Deleted = admin.Deleted,
				Pwd = "************",
				RoleTypeId = admin.RoleTypeId,
				Role = admin.Role,
			};
		}
		
		#endregion

		#region CompanyStatus
		public static CompanyStatus DalToCompanyStatusApp(this Dal.CompanyStatus companyStatus)
		{
			return (companyStatus == null) ? null : new CompanyStatus()
			{
				Id = companyStatus.Id,
				StatusName = companyStatus.StatusName,
			};
		}

		public static Dal.CompanyStatus AppCompanyStatusToDal(this CompanyStatus companyStatus)
		{
			return (companyStatus == null) ? null : new Dal.CompanyStatus()
			{
				Id = companyStatus.Id,
				StatusName = companyStatus.StatusName,
			};
		}
		#endregion

		#region Company	
		
		public static Company DalToCompanyApp(this Dal.Company company)
		{
			return (company == null) ? null : new Company()
			{
				Id = company.Id,
				CompanyName = company.CompanyName,
				Deleted = company.Deleted,
				CompanyStatusId = company.CompanyStatusId,
				StatusName = company.StatusName,
				AdministratorId = company.AdministratorId,
				AdminFName = company.AdminFName,
				AdminLName = company.AdminLName,
				TestId = company.TestId,
				Title = company.Title
			};
		}

		public static Dal.Company AppCompanyToDal(this Company company)
		{
			return (company == null) ? null : new Dal.Company()
			{
				Id = company.Id,
				CompanyName = company.CompanyName,
				Deleted = company.Deleted,
				CompanyStatusId = company.CompanyStatusId,
				AdministratorId = company.AdministratorId,
				AdminFName = company.AdminFName,
				AdminLName = company.AdminLName,
				StatusName = company.StatusName,
				TestId = company.TestId,
				Title = company.Title
			};
		}

		//public static CompanyVM ToCompanyVM(this Company company)
		//{
		//	return (company == null) ? null : new CompanyVM()
		//	{
		//		Id = company.Id,
		//		CompanyName = company.CompanyName,
		//		Deleted = company.Deleted,
		//		CompanyStatusId = company.CompanyStatusId,
		//		StatusName = company.StatusName,
		//	};
		//}

		public static CompanyForm DalToCompanyForm(this Dal.Company company)
		{
			return (company == null) ? null : new CompanyForm()
			{
				Id = company.Id,
				CompanyName = company.CompanyName,
				CompanyStatusId = company.CompanyStatusId,
				StatusName = company.StatusName,
				AdministratorId = company.AdministratorId,
				AdminFName = company.AdminFName,
				AdminLName = company.AdminLName,
				Deleted = company.Deleted,
				TestId = company.TestId,
				TestName = company.Title
			};
		}

		//public static CompanyForm ToCompanyForm(this Company company)
		//{
		//	return (company == null) ? null : new CompanyForm()
		//	{
		//		Id = company.Id,
		//		CompanyName = company.CompanyName,
		//		Deleted = company.Deleted,
		//		CompanyStatusId = company.CompanyStatusId
		//	};
		//}

		public static Dal.Company CompanyFormToDal(this CompanyForm company)
		{
			return (company == null) ? null : new Dal.Company()
			{
				Id = company.Id,
				CompanyName = company.CompanyName,
				CompanyStatusId = company.CompanyStatusId,
				StatusName = company.StatusName,
				Deleted = company.Deleted,
				TestId = company.TestId
			};
		}

		//public static Company FromCompanyFormToCompany(this CompanyForm company)
		//{
		//	return (company == null) ? null : new App.Company()
		//	{
		//		Id = company.Id,
		//		CompanyName = company.CompanyName,
		//		Deleted = company.Deleted,
		//		CompanyStatusId = company.CompanyStatusId,
		//		TestId = company.TestId
		//	};
		//}
		
		#endregion

		#region Department

		public static Department DalToDepartmentApp(this Dal.Department department)
		{
			return (department == null) ? null : new Department()
			{
				Id = department.Id,
				Name = department.DepartmentName,
				CompanyId = department.CompanyId,
			};
		}

		public static Dal.Department DepartmentAppToDal(this Department department)
		{
			return (department == null) ? null : new Dal.Department()
			{
				Id = department.Id,
				DepartmentName = department.Name,
				CompanyId = department.CompanyId,
			};
		}

		#endregion

		#region Person		
		public static Person DalToPersonApp(this Dal.Person person)
		{
			return (person == null) ? null : new Person()
			{
				Id = person.Id,
				LName = person.LName,
				FName = person.FName,
				Deleted = person.Deleted,
				CompanyId = person.CompanyId,
				CompanyName = person.CompanyName,
				CompanyStatus = person.CompanyStatusId,
				StatusType = person.StatusName,
				CompanyDeleted = person.CompanyDeleted,
				DepartmentId = person.DepartmentId,
				DepartmentName = person.DepartmentName,
				AdminId = person.AdministratorId,
				AdminFName = person.AdminFName,
				AdminLName = person.AdminLName,
			};
		}

		public static Dal.Person PersonAppToDal(this Person person)
		{
			return (person == null) ? null : new Dal.Person()
			{
				Id = person.Id,
				LName = person.LName,
				FName = person.FName,
				Deleted = person.Deleted,
				CompanyId = person.CompanyId,
				DepartmentId = person.DepartmentId,
				AdministratorId = person.AdminId,
			};
		}
		
		public static PersonVM DalToPersonVM(this Dal.Person person)
		{
			return (person == null) ? null : new PersonVM()
			{
				Id = person.Id,
				LName = person.LName,
				FName = person.FName,
				Deleted = person.Deleted,
				IsInOrder = person.IsInOrder,
				CompanyId = person.CompanyId,
				CompanyName = person.CompanyName,
				CompanyStatusId = person.CompanyStatusId,
				StatusType = person.StatusName,
				DepartmentId = person.DepartmentId,
				DepartmentName = person.DepartmentName,
				AdminId = person.AdministratorId ?? null,
				AdminFName = person.AdminFName,
				AdminLName = person.AdminLName,
			};
		}

		public static Dal.Person PersonVMToDal(this PersonVM person)
		{
			return (person == null) ? null : new Dal.Person()
			{
				Id = person.Id,
				LName = person.LName,
				FName = person.FName,
				Deleted = person.Deleted,
				IsInOrder = person.IsInOrder,
				CompanyId = person.CompanyId,
				CompanyStatusId = person.CompanyStatusId,
				DepartmentId = person.DepartmentId,
				AdministratorId = person.AdminId ?? null,
			};
		}
		
		public static PersonDropDownVM DalToPersonDropDownVM(this Dal.Person person)
		{
			return (person == null) ? null : new PersonDropDownVM()
			{
				Id = person.Id,
				LName = person.LName,
				FName = person.FName,
			};
		}
		
		public static PersonListVM DalToPersonListVM(this Dal.Person person)
		{
			return (person == null) ? null : new PersonListVM()
			{
				Id = person.Id,
				LName = person.LName,
				FName = person.FName,
			};
		}
		#endregion

		#region Register

		public static Register DalToRegisterApp(this Dal.Register register)
		{
			return (register == null) ? null : new Register()
			{
				TimeIn = register.TimeIn,
				TimeOut = register.TimeOut,
				Mistake = register.Mistake,
				PersonId = register.PersonId,
				PersonVisited = register.PersonVisited,
				FName = register.FName,
				LName = register.LName,
				CompanyName = register.CompanyName,
				StatusName = register.StatusName,
				VisitedFName = register.VisitedFName,
				VisitedLName = register.VisitedLName
			};
		}

		public static Dal.Register RegisterAppToDal(this Register register)
		{
			return (register == null) ? null : new Dal.Register()
			{
				Id = register.Id,
				TimeIn = register.TimeIn,
				TimeOut = register.TimeOut,
				Mistake = register.Mistake,
				PersonId = register.PersonId,
				PersonVisited = register.PersonVisited,
				FName = register.FName,
				LName = register.LName,
				CompanyName = register.CompanyName,
				StatusName = register.StatusName,
				VisitedFName = register.VisitedFName,
				VisitedLName = register.VisitedLName
			};
		}

		public static RegisterVM AppToRegisterVM(this Register register)
		{
			return (register == null) ? null : new RegisterVM()
			{
				Id = register.Id,
				TimeIn = register.TimeIn,
				TimeOut = register.TimeOut,
				Mistake = register.Mistake,
				PersonId = register.PersonId,
				FName = register.FName,
				LName = register.LName,
				CompanyName = register.CompanyName,
				StatusName = register.StatusName,
				VisitedFName = register.VisitedFName,
				VisitedLName = register.VisitedLName
			};
		}

		public static Dal.Register RegisterVMToDal(this RegisterVM register)
		{
			return (register == null) ? null : new Dal.Register()
			{
				Id = register.Id,
				TimeIn = register.TimeIn,
				TimeOut = register.TimeOut ?? null,
				PersonId = register.PersonId,
				PersonVisited = register.PersonVisited,
			};
		}

		public static RegisterVM DalToRegisterVM(this Dal.Register register)
		{
			return (register == null) ? null : new RegisterVM()
			{
				Id = register.Id,
				TimeIn = register.TimeIn,
				TimeOut = register.TimeOut ?? null,
				Mistake = register.Mistake,
				PersonId = register.PersonId,
				LName = register.LName,
				FName = register.FName,
				PersonVisited = register.PersonVisited,
			};
		}

		#endregion

		#region RegisterPlanning
		


		public static App.RegisterPlanning ToApp(this Dal.RegisterPlanning registerP)
		{
			return (registerP == null) ? null : new App.RegisterPlanning()
			{
				Id = registerP.Id,
				PersonId = registerP.PersonId,
				FName = registerP.FName,
				LName = registerP.LName,
				TimeIn = registerP.TimeIn,
				TimeOut = registerP.TimeOut ?? null,
				CompanyName = registerP.CompanyName,
				StatusName = registerP.StatusName,
				PersonVisited = registerP.PersonVisited,
				VisitedFName = registerP.VisitedFName,
				VisitedLName = registerP.VisitedLName,
				Mistake = registerP.Mistake,
				State = registerP.State,
				Planned = registerP.Planned,
				AdministratorId = registerP.AdministratorId
			};
		}

		public static Dal.RegisterPlanning ToDal(this App.RegisterPlanning registerP)
		{
			return (registerP == null) ? null : new Dal.RegisterPlanning()
			{
				PersonId = registerP.PersonId,
				FName = registerP.FName,
				LName = registerP.LName,
				TimeIn = registerP.TimeIn,
				TimeOut = registerP.TimeOut ?? null,
				CompanyName = registerP.CompanyName,
				StatusName = registerP.StatusName,
				PersonVisited = registerP.PersonVisited,
				VisitedFName = registerP.VisitedFName,
				VisitedLName = registerP.VisitedLName,
				Mistake = registerP.Mistake,
				State = registerP.State,
				Planned = registerP.Planned,
				AdministratorId = registerP.AdministratorId,
				CompanyId = registerP.CompanyId
			};
		}

		public static RegisterPlanningVM ToVM(this App.RegisterPlanning registerP)
		{
			return (registerP == null) ? null : new RegisterPlanningVM()
			{
				Id = registerP.Id,
				TimeIn = registerP.TimeIn,
				TimeOut = registerP.TimeOut ?? null,
				PersonId = registerP.PersonId,
				LName = registerP.LName,
				FName = registerP.FName,
				CompanyName = registerP.CompanyName,
				StatusName = registerP.StatusName,
				PersonVisited = registerP.PersonVisited,
				VisitedFName = registerP.VisitedFName,
				VisitedLName = registerP.VisitedLName,
				Mistake = registerP.Mistake,
				State = registerP.State,
				Planned = registerP.Planned,
				AdministratorId = registerP.AdministratorId,
				CompanyId = registerP.CompanyId
			};
		}

		public static App.RegisterPlanningViewModels.RegisterPlanningVM DalToRegisterPlanningVM(this Dal.RegisterPlanning register)
		{
			return (register == null) ? null : new App.RegisterPlanningViewModels.RegisterPlanningVM()
			{
				Id = register.Id,
				TimeIn = register.TimeIn,
				TimeOut = register.TimeOut ?? null,
				PersonId = register.PersonId,
				LName = register.LName,
				FName = register.FName,
				CompanyName = register.CompanyName,
				StatusName = register.StatusName,
				PersonVisited = register.PersonVisited,
				VisitedFName = register.VisitedFName,
				VisitedLName = register.VisitedLName,
				Mistake = register.Mistake,
				State = register.State,
				Planned = register.Planned,
				AdministratorId = register.AdministratorId,
				CompanyId = register.CompanyId
			};
		}
		#endregion

		#region Test	

		public static Test DalToTestApp(this Dal.Test test)
		{
			return (test == null) ? null : new Test()
			{
				Id = test.Id,
				Title = test.Title,
				VideoURL = test.VideoURL,
				IsActive = test.IsActive,
				TestSettingsId = test.TestSettingsId,
				TestSettingsTitle = test.TestSettingsTitle
			};
		}

		//public static TestVM AppToTestVM(this Test test)
		//{
		//	return (test == null) ? null : new TestVM()
		//	{
		//		Id = test.Id,
		//		Title = test.Title,
		//		VideoURL = test.VideoURL,
		//		IsActive = test.IsActive,
		//		TestSettingsId = test.TestSettingsId,
		//		TestSettingsTitle = test.TestSettingsTitle
		//	};
		//}

		public static TestVM DalToTestVM(this Dal.Test test)
		{
			return (test == null) ? null : new TestVM()
			{
				Id = test.Id,
				Title = test.Title,
				VideoURL = test.VideoURL,
				IsActive = test.IsActive,
				TestSettingsId = test.TestSettingsId,
				TestSettingsTitle = test.TestSettingsTitle
			};
		}

		public static Dal.Test TestVMToDal(this TestVM test)
		{
			return (test == null) ? null : new Dal.Test()
			{
				Id = test.Id,
				IsActive = test.IsActive,
				TestSettingsId = test.TestSettingsId,
				TestSettingsTitle = test.TestSettingsTitle,
				Title = test.Title,
				VideoURL = test.VideoURL
			};
		}

		#endregion

		#region TestSettings
		public static TestSettings DalToTestSettingsApp(this Dal.TestSettings testSettings)
		{
			return (testSettings == null) ? null : new TestSettings()
			{
				Id = testSettings.Id,
				Title = testSettings.Title,
				Treshold = testSettings.Treshold,
				ValidityPeriod = testSettings.ValidityPeriod,
				FailurePeriod = testSettings.FailurePeriod
			};
		}

		public static TestSettingsVM DalToTestSettingsVM(this Dal.TestSettings testSettings)
		{
			return (testSettings == null) ? null : new TestSettingsVM()
			{
				Id = testSettings.Id,
				Title = testSettings.Title,
				Treshold = testSettings.Treshold,
				ValidityPeriod = testSettings.ValidityPeriod,
				FailurePeriod = testSettings.FailurePeriod
			};
		}

		public static Dal.TestSettings TestSettingsVMToDal(this TestSettingsVM testSettings)
		{
			return (testSettings == null) ? null : new Dal.TestSettings()
			{
				Id = testSettings.Id,
				Title = testSettings.Title,
				Treshold = testSettings.Treshold,
				ValidityPeriod = testSettings.ValidityPeriod,
				FailurePeriod = testSettings.FailurePeriod
			};
		}
		#region TestSettings archive
		//public static Dal.TestSettings ToDal(this App.TestSettings testSettings)
		//{
		//	return (testSettings == null) ? null : new Dal.TestSettings()
		//	{
		//		Id = testSettings.Id,
		//		Title = testSettings.Title,
		//		Treshold = testSettings.Treshold,
		//		ValidityPeriod = testSettings.ValidityPeriod,
		//		FailurePeriod = testSettings.FailurePeriod
		//	};
		//}

		//public static TestSettingsVM AppToVM(this App.TestSettings testSettings)
		//{
		//	return (testSettings == null) ? null : new TestSettingsVM()
		//	{
		//		Id = testSettings.Id,
		//		Title = testSettings.Title,
		//		Treshold = testSettings.Treshold,
		//		ValidityPeriod = testSettings.ValidityPeriod,
		//		FailurePeriod = testSettings.FailurePeriod
		//	};
		//}

		//public static TestSettings VMToApp(this TestSettingsVM testSettingsVM)
		//{
		//	return (testSettingsVM == null) ? null : new TestSettings()
		//	{
		//		Id = testSettingsVM.Id,
		//		Title = testSettingsVM.Title,
		//		Treshold = testSettingsVM.Treshold,
		//		ValidityPeriod = testSettingsVM.ValidityPeriod,
		//		FailurePeriod = testSettingsVM.FailurePeriod
		//	};
		//}
		#endregion

		#endregion

		#region TestQuestionVM
		public static TestQuestionVM DalToTestQuestionVM(this Dal.Test test)
		{
			return (test == null) ? null : new TestQuestionVM()
			{
				Id = test.Id,
				Title = test.Title,
			};

		}
		#endregion

		#region Question		

		public static Question DalToQuestionApp(this Dal.Question question)
		{
			return (question == null) ? null : new Question()
			{
				Id = question.Id,
				IsActive = question.IsActive,
				Title = question.Title,
				//QcmItems = question.QcmItems.Select(q => q.ToApp()),
			};
		}

		public static QuestionVM DalToQuestionVM(this Dal.Question question)
		{
			return (question == null) ? null : new QuestionVM()
			{
				Id = question.Id,
				Title = question.Title,
				IsActive = question.IsActive				
			};
		}

		public static QuestionVM AppToQuestionVM(this App.Question question)
		{
			return (question == null) ? null : new QuestionVM()
			{
				Id = question.Id,
				Title = question.Title,
				IsActive = question.IsActive
			};
		}
		#endregion

		#region QcmItem
		public static App.QcmItem DalToQcmItemApp(this Dal.QcmItem qcmItem)
		{
			return (qcmItem == null) ? null : new App.QcmItem()
			{
				QcmItemId = qcmItem.QcmItemId,
				Item = qcmItem.Item,
				IsAnswer = qcmItem.IsAnswer
			};
		}
		#endregion

		#region QuestionQcmItem		
		public static AnswersVM DalQuestionQcmItemToAnswersVM(this Dal.QuestionQcmItem qcmItem)
		{
			return (qcmItem == null) ? null : new AnswersVM()
			{
				QcmItemId = qcmItem.QcmItemId,
				QuestionId = qcmItem.QuestionId,
				IsAnswer = qcmItem.QcmItemIsAnswer,
				Item = qcmItem.QcmItemItem,
			};
		}
		#endregion

		#region PersonTest

		public static Dal.PersonTest PersonTestVMToDal(this PersonTestVM personTest)
		{
			return (personTest == null) ? null : new Dal.PersonTest()
			{
				Id = personTest.Id,
				PersonId = personTest.PersonId,
				Result = personTest.Result,
				TakeDate = personTest.TakeDate,
				TestId = personTest.TestId,
			};
		}

		public static PersonTestVM DalToPersonTestVM(this Dal.PersonTest personTest)
		{
			return (personTest == null) ? null : new PersonTestVM()
			{
				Id = personTest.Id,
				PersonId = personTest.PersonId,
				Result = personTest.Result,
				TakeDate = personTest.TakeDate,
				TestId = personTest.TestId,
			};
		}
		#endregion

		//public static App.RegisterViewModels.SetPerson ToSetPerson(this App.Person person)
		//{ 
		//	return (person == null) ? null : new App.RegisterViewModels.SetPerson()
		//	{ 
		//		Id = person.Id,
		//		DateBegin = person.DateBegin,
		//		DateEnd = person.DateEnd,
		//		FName = person.FName,
		//		LName = person.LName,
		//		PersonVisitedId = person.
		//	}
		//}

		//public static Dal.Register FormRegisterVMToDal(this FormRegisterVM register)
		//{
		//	return (register == null) ? null : new Dal.Register()
		//	{
		//		TimeIn = register.TimeIn,
		//		TimeOut = register.TimeOut,
		//		Mistake = register.Mistake,
		//		PersonId = register.PersonId,
		//		PersonVisited = register.PersonVisited,
		//		FName = register.FName,
		//		LName = register.LName,
		//		CompanyName = register.CompanyName,
		//		StatusName = register.StatusName,
		//		VisitedFName = register.VisitedFName,
		//		VisitedLName = register.VisitedLName
		//	};
		//}
	}

}
