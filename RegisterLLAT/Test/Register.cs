﻿using System;
using System.Collections.Generic;
using System.Reflection.Metadata.Ecma335;
using System.Text;

namespace Test
{
	public class Register
	{
		public int Age { get; set; }

		public int CompanyId { get; set; }

		public bool Accepted { get; set; }

		public int Salary { get; set; }

		public Register Validate(Register register)
		{
			if (register.Age > 30)
			{
				if (register.CompanyId == 1)
				{
					register.Accepted = true;
				}
			}
			else
			{
				register.Accepted = false;
			}
			return register;
		}

	}
}
