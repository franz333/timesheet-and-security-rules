﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Interfaces
{
	interface IRepository<TEntity>
	{
		// protected Connection _Connection;
		//private readonly IOptions<DB_Connection.DBConfig> _dbConfig;

		//public BaseRepository(IOptions<DB_Connection.DBConfig> dbConfig)
		//{
		//	_Connection = new Connection(_dbConfig.Value.ConnectionString, _dbConfig.Value.InvariantName);
		//}

		TEntity Get(int id);

		IEnumerable<TEntity> GetAll();

		int Insert(TEntity entity);

		bool Update(TEntity entity);

		bool Delete(int id);
	}
}
