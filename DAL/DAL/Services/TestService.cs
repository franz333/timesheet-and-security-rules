﻿using DAL.Entities;
using DAL.Interfaces;
using DAL.Mapper;
using DBConnection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Services
{
	public class TestService : IRepository<Test>
	{
		public bool Delete(int id)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_DeleteTest", true);
			cmd.AddParameter("@in_Id", id);
			return con.ExecuteNonQuery(cmd) > 0;
		}

		public Test Get(int id)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_GetTest", true);
			cmd.AddParameter("@in_Id", id);
			return con.ExecuteReader(cmd, UniversalMapper.Mapper<Test>).SingleOrDefault();
		}

		public IEnumerable<Test> GetAll()
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_GetAllTest", true);
			return con.ExecuteReader(cmd, UniversalMapper.Mapper<Test>);
		}

		public IEnumerable<Test> GetAll(int questionId)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_GetAllTestByQuestion", true);
			cmd.AddParameter("@in_QuestionId", questionId);
			return con.ExecuteReader(cmd, UniversalMapper.Mapper<Test>);
		}		

		public int Insert(Test entity)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_InsertTest", true);
			cmd.AddParameter("@in_Title", entity.Title);
			cmd.AddParameter("@in_VideoURL", entity.VideoURL);
			cmd.AddParameter("@in_TestSettingsId", entity.TestSettingsId);
			int value;
			try
			{
				value = Convert.ToInt32(con.ExecuteScalar(cmd));
			}
			catch (Exception)
			{
				string message = con.ExecuteScalar(cmd).ToString();
				throw new Exception(message);
			}
			return value;
		}

		public bool Update(Test entity)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_UpdateTest", true);
			cmd.AddParameter("@in_Title", entity.Title);
			cmd.AddParameter("@in_VideoURL", entity.VideoURL);
			cmd.AddParameter("@in_IsActive", entity.IsActive);
			cmd.AddParameter("@in_TestSettingsId", entity.TestSettingsId);
			cmd.AddParameter("@in_Id", entity.Id);
			return con.ExecuteNonQuery(cmd) > 0;
		}
	}
}
