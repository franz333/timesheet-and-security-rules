﻿using DAL.Entities;
using DAL.Interfaces;
using DAL.Mapper;
using DBConnection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Services
{
	public class CompanyService : IRepository<Company>
	{
		public bool Delete(int id)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_HardDeleteCompany", true);
			cmd.AddParameter("@in_Id", id);
			return con.ExecuteNonQuery(cmd) > 0;
		}

		public bool SoftDelete(int id)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_SoftDeleteCompany", true);
			cmd.AddParameter("@in_Id", id);
			return con.ExecuteNonQuery(cmd) > 0;
		}

		public bool Restore(int id)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_RestoreCompany", true);
			cmd.AddParameter("@in_Id", id);
			return con.ExecuteNonQuery(cmd) > 0;
		}

		// OK
		public Company Get(int id)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_GetCompany", true);
			cmd.AddParameter("@in_Id", id);
			return con.ExecuteReader(cmd, UniversalMapper.Mapper<Company>).SingleOrDefault();
		}

		// OK
		public Company Get(int id, int StatusId)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_GetCompanyById_StatusId", true);
			cmd.AddParameter("@in_Id", id);
			cmd.AddParameter("@in_StatusId", StatusId);
			return con.ExecuteReader(cmd, UniversalMapper.Mapper<Company>).SingleOrDefault();
		}

		public IEnumerable<Company> Get(string companyName)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_GetCompanyByName", true);
			cmd.AddParameter("@in_CompanyName", companyName);
			return con.ExecuteReader(cmd, UniversalMapper.Mapper<Company>);
		}

		// OK
		public IEnumerable<Company> GetAll()
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_GetAllCompany", true);
			return con.ExecuteReader(cmd, UniversalMapper.Mapper<Company>);
		}

		// OK
		public IEnumerable<Company> GetAll(int statusId)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_GetAllCompanyByStatus", true);
			cmd.AddParameter("@in_StatusId", statusId);
			return con.ExecuteReader(cmd, UniversalMapper.Mapper<Company>);
		}

		// OK
		public IEnumerable<Company> GetAll(string search)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_GetAllCompanyBySearch", true);
			cmd.AddParameter("@in_Search", search);
			return con.ExecuteReader(cmd, UniversalMapper.Mapper<Company>);
		}

		// OK
		public IEnumerable<Company> GetAll(string search, int statusId)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_GetAllCompanyBySearchStatus", true);
			cmd.AddParameter("@in_Search", search);
			cmd.AddParameter("@in_StatusId", statusId);
			return con.ExecuteReader(cmd, UniversalMapper.Mapper<Company>);
		}

		public IEnumerable<Company> GetAllAutoComplete()
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_GetAllCompanyForSearch", true);
			return con.ExecuteReader(cmd, UniversalMapper.Mapper<Company>);
		}

		// OK
		public IEnumerable<Company> GetAllCompanyByStatus(int statusId)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_GetAllCompanyByStatus", true);
			cmd.AddParameter("@in_StatusId", statusId);
			return con.ExecuteReader(cmd, UniversalMapper.Mapper<Company>);
		}

		public IEnumerable<Company> GetAllCompanyByStatusWithoutDep(int statusId)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_GetAllCompanyByStatusWithoutDep", true);
			cmd.AddParameter("@in_StatusId", statusId);
			return con.ExecuteReader(cmd, UniversalMapper.Mapper<Company>);
		}

		public int Insert(Company entity)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_InsertCompany", true);
			cmd.AddParameter("@in_CompanyName", entity.CompanyName);
			cmd.AddParameter("@in_CompanyStatusId", entity.CompanyStatusId);
			cmd.AddParameter("@in_AdministratorId", (object)entity.AdministratorId ?? DBNull.Value);
			cmd.AddParameter("@in_TestId", entity.TestId);
			int value = Convert.ToInt32(con.ExecuteScalar(cmd));
			return value;
		}

		public bool Update(Company entity)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_UpdateCompany", true);
			cmd.AddParameter("@in_CompanyName", entity.CompanyName);
			cmd.AddParameter("@in_CompanyStatusId", entity.CompanyStatusId);
			cmd.AddParameter("@in_AdministratorId", (object)entity.AdministratorId ?? DBNull.Value);
			cmd.AddParameter("@in_Deleted", entity.Deleted);
			cmd.AddParameter("@in_TestId", entity.TestId);
			cmd.AddParameter("@in_Id", entity.Id);
			return con.ExecuteNonQuery(cmd) > 0;
		}
	}
}
