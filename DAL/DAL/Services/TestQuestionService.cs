﻿using DAL.Entities;
using DAL.Interfaces;
using DAL.Mapper;
using DBConnection;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Services
{
	public class TestQuestionService : IRepository<TestQuestion>
	{
		public bool Delete(int id)
		{
			throw new NotImplementedException();
		}

		public bool Delete(int testId, int questionId)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_DeleteTestQuestion", true);
			cmd.AddParameter("@in_TestId", testId);
			cmd.AddParameter("@in_QuestionId", questionId);
			return con.ExecuteNonQuery(cmd) > 0;
		}

		// TODO : check to delete or not
		public bool DeleteAllByQuestion(int questionId)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_DeleteAllByQuestion", true);
			cmd.AddParameter("@in_QuestionId", questionId);
			return con.ExecuteNonQuery(cmd) > 0;
		}

		public TestQuestion Get(int id)
		{
			throw new NotImplementedException();
		}

		public IEnumerable<TestQuestion> GetAll()
		{
			throw new NotImplementedException();
		}

		public IEnumerable<TestQuestion> GetAll(int questionId)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_GetAllTestByQuestion", true);
			cmd.AddParameter("@in_questionId", questionId);
			return con.ExecuteReader(cmd, UniversalMapper.Mapper<TestQuestion>);
		}

		public int Insert(TestQuestion entity)
		{
			throw new NotImplementedException();
		}

		public int Insert(int testId, int questionId)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_InsertTestQuestion", true);
			cmd.AddParameter("@in_testId", testId);
			cmd.AddParameter("@in_questionId", questionId);

			int value;
			try
			{
				value = Convert.ToInt32(con.ExecuteScalar(cmd));
			}
			catch (Exception)
			{
				string message = con.ExecuteScalar(cmd).ToString();
				throw new Exception(message);
			}
			return value;

		}

		public bool Update(TestQuestion entity)
		{
			throw new NotImplementedException();
		}
	}
}
