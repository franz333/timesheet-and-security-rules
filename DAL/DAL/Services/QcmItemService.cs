﻿using DAL.Entities;
using DAL.Interfaces;
using DAL.Mapper;
using DBConnection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Services
{
	public class QcmItemService : IRepository<QcmItem>
	{
		public bool Delete(int id)
		{
			throw new NotImplementedException();
		}

		public QcmItem Get(int id)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_GetQcmItem", true);
			cmd.AddParameter("@in_Id", id);
			return con.ExecuteReader(cmd, UniversalMapper.Mapper<QcmItem>).SingleOrDefault();
		}

		public int GetExistingQcmItem(QcmItem item)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_GetExistingQcmItem", true);
			cmd.AddParameter("@in_Item", item.Item);
			cmd.AddParameter("@in_IsAnswer", item.IsAnswer);

			int value;
			try
			{
				value = Convert.ToInt32(con.ExecuteScalar(cmd));
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return value;
		}

		public IEnumerable<QcmItem> GetAll()
		{
			throw new NotImplementedException();
		}

		public IEnumerable<QcmItem> GetAll(int questionId)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_GetAllQcmItemByQuestion", true);
			cmd.AddParameter("@in_QuestionId", questionId);
			return con.ExecuteReader(cmd, UniversalMapper.Mapper<QcmItem>);
		}

		public int Insert(QcmItem entity)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_InsertQcmItem", true);
			cmd.AddParameter("@in_Item", entity.Item);
			cmd.AddParameter("@in_IsAnswer", entity.IsAnswer);

			int value;
			try
			{
				value = Convert.ToInt32(con.ExecuteScalar(cmd));
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return value;
		}

		public int Insert(QcmItem entity, int questionId)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_InsertQcmItem", true);
			cmd.AddParameter("@in_Item", entity.Item);
			cmd.AddParameter("@in_IsAnswer", entity.IsAnswer);
			cmd.AddParameter("@in_QuestionId", questionId);

			int value;
			try
			{
				value = Convert.ToInt32(con.ExecuteScalar(cmd));
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return value;
		}

		public bool Update(QcmItem entity)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_UpdateQcmItem", true);
			cmd.AddParameter("@in_Item", entity.Item);
			cmd.AddParameter("@in_IsAnswer", entity.IsAnswer);
			cmd.AddParameter("@in_Id", entity.QcmItemId);
			return con.ExecuteNonQuery(cmd) > 0;
		}
	}
}
