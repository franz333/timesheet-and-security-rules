﻿using DAL.Entities;
using DAL.Interfaces;
using DAL.Mapper;
using DBConnection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Services
{
	public class QuestionQcmItemService : IRepository<QuestionQcmItem>
	{
		public bool Delete(int id)
		{
			throw new NotImplementedException();
		}

		public bool Delete(int questionId, int qcmItemId)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_DeleteQuestionQcmItem", true);
			cmd.AddParameter("@in_questionId", questionId);
			cmd.AddParameter("@in_qcmItemId", qcmItemId);
			return con.ExecuteNonQuery(cmd) > 0;
		}

		public QuestionQcmItem Get(int id)
		{
			throw new NotImplementedException();
		}

		public QuestionQcmItem GetByQuestionIdQcmItemId(int questionId, int qcmItemId)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("GetByQuestionIdQcmItemId", true);
			cmd.AddParameter("@in_questionId", questionId);
			cmd.AddParameter("@in_QcmItemId", qcmItemId);
			return con.ExecuteReader(cmd, UniversalMapper.Mapper<QuestionQcmItem>).SingleOrDefault();
		}

		public IEnumerable<QuestionQcmItem> GetAll()
		{
			throw new NotImplementedException();
		}

		// To be deleted perhas
		public IEnumerable<QuestionQcmItem> GetAll(int questionId)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_GetAllQuestionQcmItemByQuestion", true);
			cmd.AddParameter("@in_QuestionId", questionId);
			return con.ExecuteReader(cmd, UniversalMapper.Mapper<QuestionQcmItem>);
		}

		public int Insert(QuestionQcmItem entity)
		{
			throw new NotImplementedException();
		}

		public int Insert(int questionId, int qcmItemId)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_InsertQuestionQcmItem", true);
			cmd.AddParameter("@in_questionId", questionId);
			cmd.AddParameter("@in_qcmItemId", qcmItemId);			 

			int value;
			try
			{
				value = Convert.ToInt32(con.ExecuteScalar(cmd));
			}
			catch (Exception)
			{
				string message = con.ExecuteScalar(cmd).ToString();
				throw new Exception(message);
			}
			return value;
		}

		public bool Update(QuestionQcmItem entity)
		{
			throw new NotImplementedException();
		}
	}
}
