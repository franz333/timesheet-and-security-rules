﻿using DAL.Entities;
using DAL.Interfaces;
using DAL.Mapper;
using DBConnection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Services
{
	public class RegisterPlanningService : IRepository<RegisterPlanning>
	{
		public bool Delete(int id)
		{
			throw new NotImplementedException();
		}

		public RegisterPlanning Get(int id)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_GetRegisterPlanning", true);
			cmd.AddParameter("@in_Id", id);
			return con.ExecuteReader(cmd, UniversalMapper.Mapper<RegisterPlanning>).SingleOrDefault();
		}

		public RegisterPlanning GetExisting(int id, DateTime timeIn)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_GetExistingRegisterPlanning", true);
			cmd.AddParameter("@in_PersonId", id);
			cmd.AddParameter("@in_Day", timeIn);
			return con.ExecuteReader(cmd, UniversalMapper.Mapper<RegisterPlanning>).SingleOrDefault();
		}

		public IEnumerable<RegisterPlanning> GetAll()
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_GetAllRegisterPlanning", true);
			return con.ExecuteReader(cmd, UniversalMapper.Mapper<RegisterPlanning>);
		}

		public int Insert(RegisterPlanning entity)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_InsertRegisterPlanning", true);
			cmd.AddParameter("@in_TimeIn", entity.TimeIn);
			cmd.AddParameter("@in_TimeOut", entity.TimeOut);
			cmd.AddParameter("@in_PersonId", entity.PersonId);
			cmd.AddParameter("@in_PersonVisited", entity.PersonVisited);
			cmd.AddParameter("@in_Planned", entity.Planned);
			cmd.AddParameter("@in_AdministratorId", entity.AdministratorId);

			int value;
			try
			{
				value = Convert.ToInt32(con.ExecuteScalar(cmd));
			}
			catch (Exception)
			{
				string message = con.ExecuteScalar(cmd).ToString();
				throw new Exception(message);
			}
			return value;
		}

		public bool Update(RegisterPlanning entity)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_UpdateRegisterPlanning", true);
			cmd.AddParameter("@in_TimeIn", entity.TimeIn);
			cmd.AddParameter("@in_TimeOut", entity.TimeOut);
			cmd.AddParameter("@in_AdministratorId", (object)entity.AdministratorId ?? DBNull.Value);
			cmd.AddParameter("@in_State", entity.State);
			cmd.AddParameter("@in_Mistake", entity.Mistake);
			cmd.AddParameter("@in_Id", entity.Id);
			return con.ExecuteNonQuery(cmd) > 0;
		}

		public bool SetPlanned(int id)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_UpdateRegisterPlanning_SetPlanned", true);
			cmd.AddParameter("@in_Id", id);
			return con.ExecuteNonQuery(cmd) > 0;
		}
	}
}
