﻿using DAL.Entities;
using DAL.Interfaces;
using DAL.Mapper;
using DBConnection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Services
{
	public class PersonService : IRepository<Person>
	{
		public bool Delete(int id)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_DeletePerson", true);
			cmd.AddParameter("@Id", id);
			return con.ExecuteNonQuery(cmd) > 0;
		}

		// OK
		public Person Get(int id)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_GetPerson", true);
			cmd.AddParameter("@in_Id", id);
			return con.ExecuteReader(cmd, UniversalMapper.Mapper<Person>).SingleOrDefault();
		}

		// OK
		public Person GetPersonVisited(int id)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_GetPersonVisisted", true);
			cmd.AddParameter("@in_Id", id);
			return con.ExecuteReader(cmd, UniversalMapper.Mapper<Person>).SingleOrDefault();
		}

		public Person Get(int id, int companyId)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_GetPersonById_Company", true);
			cmd.AddParameter("@in_Id", id);
			cmd.AddParameter("@in_CompanyId", companyId);
			return con.ExecuteReader(cmd, UniversalMapper.Mapper<Person>).SingleOrDefault();
		}

		public Person Get(int id, int companyId, int departmentId)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_GetPersonById_Company_Department", true);
			cmd.AddParameter("@in_Id", id);
			cmd.AddParameter("@in_CompanyId", companyId);
			cmd.AddParameter("@in_DepartmentId", departmentId);
			return con.ExecuteReader(cmd, UniversalMapper.Mapper<Person>).SingleOrDefault();
		}

		// OK
		public IEnumerable<Person> GetAll()
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_GetAllPerson", true);
			return con.ExecuteReader(cmd, UniversalMapper.Mapper<Person>);
		}

		// OK
		public IEnumerable<Person> GetAll(string search)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_GetAllPersonBySearch", true);
			cmd.AddParameter("@in_Search", search);
			return con.ExecuteReader(cmd, UniversalMapper.Mapper<Person>);
		}

		// OK
		public IEnumerable<Person> GetAll(string search, int statusId)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_GetAllPersonBySearchStatus", true);
			cmd.AddParameter("@in_Search", search);
			cmd.AddParameter("@in_StatusId", statusId);
			return con.ExecuteReader(cmd, UniversalMapper.Mapper<Person>);
		}

		// OK
		public IEnumerable<Person> GetAllPersonByCompanyStatusId(int statusId)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_GetAllPersonByCompanyStatusId", true);
			cmd.AddParameter("@in_StatusId", statusId);
			return con.ExecuteReader(cmd, UniversalMapper.Mapper<Person>);
		}

		public IEnumerable<Person> GetAll(int companyId)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_GetAllPersonByCompany", true);
			cmd.AddParameter("@in_CompanyId", companyId);
			return con.ExecuteReader(cmd, UniversalMapper.Mapper<Person>);
		}

		// OK
		public IEnumerable<Person> GetAll(string lName, string fName)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_GetPersonByLName_FName", true);
			cmd.AddParameter("@in_LName", lName);
			cmd.AddParameter("@in_FName", fName);
			return con.ExecuteReader(cmd, UniversalMapper.Mapper<Person>);
		}

		public IEnumerable<Person> GetPersonsByCompany(int companyId)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_GetAllPersonByCompany", true);
			cmd.AddParameter("@in_CompanyId", companyId);
			return con.ExecuteReader(cmd, UniversalMapper.Mapper<Person>);
		}

		public IEnumerable<Person> GetPersonsByCompanyAndDepartment(int companyId, int departmentId)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_GetAllPersonByCompany_Department", true);
			cmd.AddParameter("@in_CompanyId", companyId);
			cmd.AddParameter("@in_departmentId", departmentId);
			return con.ExecuteReader(cmd, UniversalMapper.Mapper<Person>);
		}

		//public IEnumerable<Person> GetPersonListWithoutArlon()
		//{
		//	Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
		//	Command cmd = new Command("sp_GetPersonListWithoutArlon", true);
		//	return con.ExecuteReader(cmd, UniversalMapper.Mapper<Person>);
		//}

		public int Insert(Person entity)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_InsertPerson", true);
			cmd.AddParameter("@in_LName", entity.LName);
			cmd.AddParameter("@in_FName", entity.FName);
			cmd.AddParameter("@in_Deleted", entity.Deleted);
			cmd.AddParameter("@in_CompanyId", entity.CompanyId);
			cmd.AddParameter("@in_DepartmentId", (object)entity.DepartmentId ?? DBNull.Value);
			cmd.AddParameter("@in_AdministratorId", (object)entity.AdministratorId ?? DBNull.Value);
			int value = Convert.ToInt32(con.ExecuteScalar(cmd));
			return value;
		}

		public bool Update(Person entity)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_UpdatePerson", true);
			cmd.AddParameter("@in_LName", entity.LName);
			cmd.AddParameter("@in_FName", entity.FName);
			cmd.AddParameter("@in_Deleted", entity.Deleted);
			cmd.AddParameter("@in_CompanyId", entity.CompanyId);
			cmd.AddParameter("@in_DepartmentId", (object)entity.DepartmentId ?? DBNull.Value);
			cmd.AddParameter("@in_AdministratorId", (object)entity.AdministratorId ?? DBNull.Value);
			cmd.AddParameter("@in_Id", entity.Id);
			return con.ExecuteNonQuery(cmd) > 0;
		}
	}
}
