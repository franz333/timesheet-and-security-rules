﻿using DAL.Entities;
using DAL.Interfaces;
using DAL.Mapper;
using DBConnection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace DAL.Services
{
	public class QuestionService : IRepository<Question>
	{
		public bool Delete(int id)
		{
			throw new NotImplementedException();
		}

		public Question Get(int id)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_GetQuestion", true);
			cmd.AddParameter("@in_Id", id);
			return con.ExecuteReader(cmd, UniversalMapper.Mapper<Question>).SingleOrDefault();
		}

		public IEnumerable<Question> GetAll()
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_GetAllQuestion", true);
			return con.ExecuteReader(cmd, UniversalMapper.Mapper<Question>);
		}

		public IEnumerable<Question> GetAll(int testId)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_GetAllQuestionByTest", true);
			cmd.AddParameter("@in_TestId", testId);
			return con.ExecuteReader(cmd, UniversalMapper.Mapper<Question>);
		}

		public IEnumerable<Question> GetAll(string search, int testId)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_GetAllQuestionBySearch", true);
			cmd.AddParameter("@in_Search", search);
			cmd.AddParameter("@in_TestId", testId);
			return con.ExecuteReader(cmd, UniversalMapper.Mapper<Question>);
		}

		public int Insert(Question entity)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_InsertQuestion", true);
			cmd.AddParameter("@in_Title", entity.Title);
			cmd.AddParameter("@in_IsActive", entity.IsActive);

			int value;
			try
			{
				value = Convert.ToInt32(con.ExecuteScalar(cmd));
			}
			catch (Exception)
			{
				string message = con.ExecuteScalar(cmd).ToString();
				throw new Exception(message);
			}
			return value;
		}

		public bool Update(Question entity)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_UpdateQuestion", true);
			cmd.AddParameter("@in_Title", entity.Title);
			cmd.AddParameter("@in_IsActive", entity.IsActive);
			cmd.AddParameter("@in_Id", entity.Id);
			return con.ExecuteNonQuery(cmd) > 0;
		}
	}
}
