﻿using DAL.Entities;
using DAL.Interfaces;
using DAL.Mapper;
using DBConnection;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace DAL.Services
{
	public class AdminService : IRepository<Admin>
	{
		public bool Delete(int id)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_DeleteAdministrator", true);
			cmd.AddParameter("@in_Id", id);
			return con.ExecuteNonQuery(cmd) > 0;
		}

		public Admin Get(int id)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_GetAdministrator", true);
			cmd.AddParameter("@in_Id", id);
			return con.ExecuteReader(cmd, UniversalMapper.Mapper<Admin>).SingleOrDefault();
		}

		public Admin Get(Admin admin)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_GetByEmail", true);
			cmd.AddParameter("@in_Email", admin.Email);
			return con.ExecuteReader(cmd, UniversalMapper.Mapper<Admin>).SingleOrDefault();
		}

		public Admin Login(Admin admin)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_Login", true);
			cmd.AddParameter("@in_Email", admin.Email);
			cmd.AddParameter("@in_Pwd", admin.Pwd, DbType.Binary);
			return con.ExecuteReader(cmd, UniversalMapper.Mapper<Admin>).SingleOrDefault();
		}

		public IEnumerable<Admin> GetAll()
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_GetAllAdministrator", true);
			return con.ExecuteReader(cmd, UniversalMapper.Mapper<Admin>);
		}

		public int Insert(Admin entity)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_InsertAdministrator", true);
			cmd.AddParameter("@in_Email", entity.Email);
			cmd.AddParameter("@in_LName", entity.LName);
			cmd.AddParameter("@in_FName", entity.FName);
			cmd.AddParameter("@in_Pwd", entity.Pwd, DbType.Binary);
			cmd.AddParameter("@in_IsActive", entity.IsActive);
			cmd.AddParameter("@in_Deleted", entity.Deleted);
			cmd.AddParameter("@in_RoleTypeId", entity.RoleTypeId);
			int value = Convert.ToInt32(con.ExecuteScalar(cmd));
			return value;
		}

		public bool Update(Admin entity)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_UpdateAdministrator", true);
			cmd.AddParameter("@in_Email", entity.Email);
			cmd.AddParameter("@in_LName", entity.LName);
			cmd.AddParameter("@in_FName", entity.FName);
			cmd.AddParameter("@in_IsActive", entity.IsActive);
			cmd.AddParameter("@in_Deleted", entity.Deleted);
			cmd.AddParameter("@in_RoleTypeId", entity.RoleTypeId);
			cmd.AddParameter("@in_Id", entity.Id);
			return con.ExecuteNonQuery(cmd) > 0;
		}

		// OK
		public bool Restore(int id)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_RestoreAdministrator", true);
			cmd.AddParameter("@in_Id", id);
			return con.ExecuteNonQuery(cmd) > 0;
		}

		// OK
		public bool Activate(int id)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_ActivateAdministrator", true);
			cmd.AddParameter("@in_Id", id);
			return con.ExecuteNonQuery(cmd) > 0;
		}

		// OK
		public bool Deactivate(int id)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_DeactivateAdministrator", true);
			cmd.AddParameter("@in_Id", id);
			return con.ExecuteNonQuery(cmd) > 0;
		}


		public bool ModifyPwd(Admin entity)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_ModifyPwd", true);
			cmd.AddParameter("@in_Pwd", entity.Pwd, DbType.Binary);
			cmd.AddParameter("@in_Id", entity.Id);
			return con.ExecuteNonQuery(cmd) > 0;
		}

		public int PasswordMatch(byte[] pwd, int Id)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_PasswordMatch", true);
			cmd.AddParameter("@in_Pwd", pwd, DbType.Binary);
			cmd.AddParameter("@in_Id", Id);
			int value = Convert.ToInt32(con.ExecuteScalar(cmd));
			return value;
		}
	}
}
