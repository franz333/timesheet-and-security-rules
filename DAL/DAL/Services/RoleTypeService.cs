﻿using DAL.Entities;
using DAL.Interfaces;
using DAL.Mapper;
using DBConnection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Services
{
	public class RoleTypeService : IRepository<RoleType>
	{
		public bool Delete(int id)
		{
			throw new NotImplementedException();
		}

		public RoleType Get(int id)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_GetRoleType", true);
			cmd.AddParameter("@in_Id", id);
			return con.ExecuteReader(cmd, UniversalMapper.Mapper<RoleType>).SingleOrDefault();
		}

		public IEnumerable<RoleType> GetAll()
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_GetAllRoleType", true);
			return con.ExecuteReader(cmd, UniversalMapper.Mapper<RoleType>);
		}

		public int Insert(RoleType entity)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_GetAllRoleType", true);
			cmd.AddParameter("@in_Title", entity.Title);
			cmd.AddParameter("@in_RoleTypeDescription", entity.RoleTypeDescription);
			int value = Convert.ToInt32(con.ExecuteScalar(cmd));
			return value;
		}

		public bool Update(RoleType entity)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_UpdateRoleType", true);
			cmd.AddParameter("@in_Title", entity.Title);
			cmd.AddParameter("@in_RoleTypeDescription", entity.RoleTypeDescription);
			cmd.AddParameter("@in_Id", entity.Id);
			return con.ExecuteNonQuery(cmd) > 0;
		}
	}
}
