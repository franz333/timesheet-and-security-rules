﻿using DAL.Entities;
using DAL.Interfaces;
using DAL.Mapper;
using DBConnection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Services
{
	public class RegisterService : IRepository<Register>
	{
		public bool Delete(int id)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_DeleteRegister", true);
			cmd.AddParameter("@in_Id", id);
			return con.ExecuteNonQuery(cmd) > 0;
		}

		public Register Get(int id)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_GetRegister", true);
			cmd.AddParameter("@in_Id", id);
			return con.ExecuteReader(cmd, UniversalMapper.Mapper<Register>).SingleOrDefault();
		}

		public IEnumerable<Register> GetAll()
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_GetAllRegister", true);
			return con.ExecuteReader(cmd, UniversalMapper.Mapper<Register>);
		}

		public IEnumerable<Register> GetAllCurrentDay()
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_GetAllRegisterOfTheDay", true);
			return con.ExecuteReader(cmd, UniversalMapper.Mapper<Register>);
		}

		public int Insert(Register entity)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_InsertRegister", true);
			cmd.AddParameter("@in_TimeIn", entity.TimeIn);
			cmd.AddParameter("@in_TimeOut", (object)entity.TimeOut ?? DBNull.Value);
			cmd.AddParameter("@in_PersonId", entity.PersonId);
			cmd.AddParameter("@in_PersonVisited", (object)entity.PersonVisited ?? DBNull.Value);

			int value;
			
			try
			{
				value = Convert.ToInt32(con.ExecuteScalar(cmd));
			}
			catch (Exception)
			{
				string message = con.ExecuteScalar(cmd).ToString();
				throw new Exception(message);
			}
			
			return value;
		}

		public bool Update(Register entity)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_UpdateRegister", true);
			cmd.AddParameter("@in_TimeIn", entity.TimeIn);
			cmd.AddParameter("@in_TimeOut", (object)entity.TimeOut ?? DBNull.Value);
			cmd.AddParameter("@in_PersonId", entity.PersonId);
			cmd.AddParameter("@in_PersonVisited", (object)entity.PersonVisited ?? DBNull.Value);
			cmd.AddParameter("@in_Id", entity.Id);
			return con.ExecuteNonQuery(cmd) > 0;
		}

		public bool CheckOut(Register entity)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_CheckOut", true);
			cmd.AddParameter("@in_TimeOut", entity.TimeOut);
			cmd.AddParameter("@in_Id", entity.Id);
			return con.ExecuteNonQuery(cmd) > 0;
		}
	}
}
