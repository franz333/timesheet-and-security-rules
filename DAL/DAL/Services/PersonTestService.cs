﻿using DAL.Entities;
using DAL.Interfaces;
using DAL.Mapper;
using DBConnection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Services
{
	public class PersonTestService : IRepository<PersonTest>
	{
		public bool Delete(int id)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("", true);
			cmd.AddParameter("@in_Id", id);
			return con.ExecuteNonQuery(cmd) > 0;
		}

		public PersonTest Get(int id)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_GetPersonTest", true);
			cmd.AddParameter("@in_Id", id);
			return con.ExecuteReader(cmd, UniversalMapper.Mapper<PersonTest>).SingleOrDefault();
		}

		public PersonTest GetIncompletedTest(int personId)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_GetBlankTest", true);
			cmd.AddParameter("@in_PersonId", personId);
			return con.ExecuteReader(cmd, UniversalMapper.Mapper<PersonTest>).FirstOrDefault();
		}


		public IEnumerable<PersonTest> GetAll()
		{
			throw new NotImplementedException();
		}

		public int Insert(PersonTest entity)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_InsertPersonTest", true);
			cmd.AddParameter("@in_PersonId", entity.PersonId);
			cmd.AddParameter("@in_TestId", entity.TestId);
			cmd.AddParameter("@in_Result", (object)entity.Result ?? DBNull.Value);
			cmd.AddParameter("@in_TakeDate", entity.TakeDate);

			int value;

			try
			{
				value = Convert.ToInt32(con.ExecuteScalar(cmd));
			}
			catch (Exception)
			{
				string message = con.ExecuteScalar(cmd).ToString();
				throw new Exception(message);
			}

			return value;
		}

		public bool Update(PersonTest entity)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_UpdatePersonTest", true);
			cmd.AddParameter("@in_PersonId", entity.PersonId);
			cmd.AddParameter("@in_TestId", entity.TestId);
			cmd.AddParameter("@in_Result", (object)entity.Result ?? DBNull.Value);
			cmd.AddParameter("@in_TakeDate", entity.TakeDate);
			cmd.AddParameter("@in_Id", entity.Id);
			return con.ExecuteNonQuery(cmd) > 0;
		}
	}
}
