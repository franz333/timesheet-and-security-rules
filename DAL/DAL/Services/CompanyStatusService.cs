﻿using DAL.Entities;
using DAL.Interfaces;
using DAL.Mapper;
using DBConnection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Services
{
	public class CompanyStatusService : IRepository<CompanyStatus>
	{
		public bool Delete(int id)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_DeleteCompanyStatus", true);
			cmd.AddParameter("in_Id", id);
			return con.ExecuteNonQuery(cmd) > 0;
		}

		public CompanyStatus Get(int id)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_GetCompanyStatus", true);
			cmd.AddParameter("@in_Id", id);
			return con.ExecuteReader(cmd, UniversalMapper.Mapper<CompanyStatus>).SingleOrDefault();
		}

		public IEnumerable<CompanyStatus> GetAll()
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);			
			Command cmd = new Command("sp_GetAllCompanyStatus", true);
			return con.ExecuteReader(cmd, UniversalMapper.Mapper<CompanyStatus>);
		}

		public int Insert(CompanyStatus entity)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_InsertCompanyStatus", true);
			cmd.AddParameter("@in_StatusName", entity.StatusName);
			int value = Convert.ToInt32(con.ExecuteScalar(cmd));
			return value;
		}

		public bool Update(CompanyStatus entity)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_UpdateCompanyStatus", true);
			cmd.AddParameter("@in_StatusName", entity.StatusName);
			cmd.AddParameter("@in_Id", entity.Id);
			return con.ExecuteNonQuery(cmd) > 0;
		}
	}
}
