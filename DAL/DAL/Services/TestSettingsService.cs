﻿using DAL.Entities;
using DAL.Interfaces;
using DAL.Mapper;
using DBConnection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Services
{
	public class TestSettingsService : IRepository<TestSettings>
	{
		public bool Delete(int id)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_DeleteTestSettings", true);
			cmd.AddParameter("@in_Id", id);
			return con.ExecuteNonQuery(cmd) > 0;
		}

		public TestSettings Get(int id)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_GetTestSettings", true);
			cmd.AddParameter("@in_Id", id);
			return con.ExecuteReader(cmd, UniversalMapper.Mapper<TestSettings>).SingleOrDefault();
		}

		public IEnumerable<TestSettings> GetAll()
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_GetAllTestSettings", true);
			return con.ExecuteReader(cmd, UniversalMapper.Mapper<TestSettings>);
		}

		public int Insert(TestSettings entity)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_InsertTestSettings", true);
			cmd.AddParameter("@in_Title", entity.Title);
			cmd.AddParameter("@in_Treshold", entity.Treshold);
			cmd.AddParameter("@in_ValidityPeriod", entity.ValidityPeriod);
			cmd.AddParameter("@in_FailurePeriod", entity.FailurePeriod);
			int value;
			try
			{
				value = Convert.ToInt32(con.ExecuteScalar(cmd));
			}
			catch (Exception)
			{
				string message = con.ExecuteScalar(cmd).ToString();
				throw new Exception(message);
			}
			return value;
		}

		public bool Update(TestSettings entity)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_UpdateTestSettings", true);
			cmd.AddParameter("@in_Title", entity.Title);
			cmd.AddParameter("@in_Treshold", entity.Treshold);
			cmd.AddParameter("@in_ValidityPeriod", entity.ValidityPeriod);
			cmd.AddParameter("@in_FailurePeriod", entity.FailurePeriod);
			cmd.AddParameter("@in_Id", entity.Id);
			return con.ExecuteNonQuery(cmd) > 0;
		}
	}
}
