﻿using DAL.Entities;
using DAL.Interfaces;
using DAL.Mapper;
using DBConnection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Services
{
	public class DepartmentService : IRepository<Department>
	{
		public bool Delete(int id)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_DeleteDepartment", true);
			cmd.AddParameter("@in_Id", id);
			return con.ExecuteNonQuery(cmd) > 0;
		}

		public Department Get(int id)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_GetDepartment", true);
			cmd.AddParameter("@in_Id", id);
			return con.ExecuteReader(cmd, UniversalMapper.Mapper<Department>).SingleOrDefault();
		}

		public Department Get(int id, int companyId)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_GetDepartmentByCompanyId_DepartmentId", true);
			cmd.AddParameter("@in_Id", id);
			cmd.AddParameter("@in_CompanyId", companyId);
			return con.ExecuteReader(cmd, UniversalMapper.Mapper<Department>).SingleOrDefault();
		}

		public Department Get(int companyId, string departmentName)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_GetDepartmentByCompanyId_DepartmentName", true);
			cmd.AddParameter("@in_CompanyId", companyId);
			cmd.AddParameter("@in_DepartmentName", departmentName);
			return con.ExecuteReader(cmd, UniversalMapper.Mapper<Department>).SingleOrDefault();
		}

		public IEnumerable<Department> GetAll()
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_GetAllDepartment", true);
			return con.ExecuteReader(cmd, UniversalMapper.Mapper<Department>);
		}

		public IEnumerable<Department> GetAllDepartmentByCompany(int companyId)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_GetAllDepartmentByCompany", true);
			cmd.AddParameter("@in_CompanyId", companyId);
			return con.ExecuteReader(cmd, UniversalMapper.Mapper<Department>);
		}

		public int Insert(Department entity)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_InsertDepartment", true);
			cmd.AddParameter("@in_DepartmentName", entity.DepartmentName);
			cmd.AddParameter("@in_CompanyId", entity.CompanyId);
			int value;
			try
			{
				value = Convert.ToInt32(con.ExecuteScalar(cmd));
			}
			catch (Exception)
			{
				string message = con.ExecuteScalar(cmd).ToString();
				throw new Exception(message);
			}
			return value;
		}

		public bool Update(Department entity)
		{
			Connection con = new Connection(AppSettings.ConnectionString, AppSettings.InvariantName);
			Command cmd = new Command("sp_UpdateDepartment", true);
			cmd.AddParameter("@in_DepartmentName", entity.DepartmentName);
			cmd.AddParameter("@in_CompanyId", entity.CompanyId);
			cmd.AddParameter("@in_Id", entity.Id);
			return con.ExecuteNonQuery(cmd) > 0;
		}
	}
}
