﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DAL.Entities
{
	public class Company
	{
		[Key]
		public int Id { get; set; }

		public string CompanyName { get; set; }

		public bool Deleted { get; set; }

		public int CompanyStatusId { get; set; }

		public string StatusName { get; set; }

		public int TestId { get; set; }

		public string Title { get; set; }

		public int? AdministratorId { get; set; }

		public string AdminLName { get; set; }

		public string AdminFName { get; set; }
	}
}
