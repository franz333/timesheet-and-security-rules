﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DAL.Entities
{
	public class Register 
	{
		[Key]
		public int Id { get; set; }

		public DateTime TimeIn { get; set; }

		public DateTime? TimeOut { get; set; }

		public string Mistake { get; set; }

		public int PersonId { get; set; }

		public string LName { get; set; }

		public string FName { get; set; }

		public int CompanyId { get; set; }

		public string CompanyName { get; set; }

		public string StatusName { get; set; }

		public int? PersonVisited { get; set; }

		public string VisitedLName { get; set; }

		public string VisitedFName { get; set; }
	}
}
