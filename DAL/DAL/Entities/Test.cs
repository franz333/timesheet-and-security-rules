﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Entities
{
	public class Test
	{
		public int Id { get; set; }

		public string Title { get; set; }

		public string VideoURL { get; set; }

		public bool IsActive { get; set; }

		public int TestSettingsId { get; set; }

		public string TestSettingsTitle { get; set; }
	}
}
