﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DAL.Entities
{
	public class TestQuestion
	{
		[Key]
		public int Id { get; set; }

		public int TestId { get; set; }

		public int QuestionId { get; set; }

	}
}
