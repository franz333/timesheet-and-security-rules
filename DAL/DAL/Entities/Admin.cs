﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Entities
{
	public class Admin
	{
		public int Id { get; set; }

		public string Email { get; set; }

		public byte[] Pwd { get; set; }

		public string LName { get; set; }

		public string FName { get; set; }

		public bool IsActive { get; set; }

		public bool Deleted { get; set; }

		public int RoleTypeId { get; set; }

		public string Role { get; set; }

		public string RoleDescription { get; set; }
	}
}
