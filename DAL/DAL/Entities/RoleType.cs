﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Entities
{
	public class RoleType
	{
		public int Id { get; set; }

		public string Title { get; set; }

		public string RoleTypeDescription { get; set; }
	}
}
