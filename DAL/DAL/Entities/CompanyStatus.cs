﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DAL.Entities
{
	public class CompanyStatus
	{
		[Key]
		public int Id { get; set; }

		public string StatusName { get; set; }
	}
}
