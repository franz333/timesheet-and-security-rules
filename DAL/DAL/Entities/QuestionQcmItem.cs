﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DAL.Entities
{
	public class QuestionQcmItem
	{
		[Key]
		public int Id { get; set; }

		public int QuestionId { get; set; }

		public int QcmItemId { get; set; }

		public bool QcmItemIsAnswer { get; set; }

		public string QcmItemItem { get; set; }
	}
}
