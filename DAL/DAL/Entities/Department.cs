﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Entities
{
	public class Department
	{
		public int Id { get; set; }

		public string DepartmentName { get; set; }

		public int CompanyId { get; set; }
	}
}
