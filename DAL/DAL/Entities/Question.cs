﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Entities
{
	public class Question
	{
		public int Id { get; set; }

		public string Title { get; set; }

		public bool IsActive { get; set; }
	}
}
