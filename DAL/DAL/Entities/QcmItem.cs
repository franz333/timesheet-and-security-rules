﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Entities
{
	public class QcmItem
	{
		public int QcmItemId { get; set; }

		public string Item { get; set; }

		public bool IsAnswer { get; set; }
	}
}
