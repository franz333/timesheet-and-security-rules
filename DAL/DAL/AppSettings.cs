﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL
{
	// Static class binded to Configuration in app project statup.cs
	public static class AppSettings
	{
		public static string ConnectionString { get; set; }

		public static string InvariantName { get; set; }
	}
}
