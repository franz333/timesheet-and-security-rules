


<!-- PROJECT LOGO -->
<br />
<div align="left" id="top">
  <a href="https://www.technobel.be/fr/">
    <img src="https://www.technobel.be/images/technobel_logo_hor.png" alt="Logo" width=300>
  </a>

  <h2 align="left">.Net Developer 9 months training</h2>
  <h3>Internship: SNCB - Mechanics workshop of Arlon</h3>
  <br />
</div>

<!-- TABLE OF CONTENTS -->
Table of contents
<ol>
  <li><a href="#internship">Internship</a></li>
  <li><a href="#needs">Business Needs</a></li>
  <li><a href="#context">Company's Infrastructure And Context</a></li>
  <li><a href="#tech">Technologies</a></li>
      <ol>
        <li><a href="#built-with">Built With</a></li>
    </ol>
  <li><a href="#contact">Contact</a></li>
  <li><a href="#acknowledgments">Acknowledgments</a></li>
</ol>
<br />

<!-- THE CONTEXT OF THE INTERNSHIP -->
<h2 id="internship">1. Internship</h2>

In order to validate the 9 months training I did with Technobel - Training center chosen by [Le Forem](https://www.leforem.be/centres-de-competence/techno-bel.html) - I had to do a 1 month internship in a company.

I had the opportunity to do this internship in the mechanics workshop of the [SNCB - The National Railway Company of Belgium](https://www.belgiantrain.be/) in Arlon.  

As 1 month is very short for a trainee to be able to build something, I continued working on the application during my free time after the official termination of the internship. 
<br />
<p align="center"><a href="#top">-- Back to top --</a></p>

<h2 id="needs">2. Business Needs</h2>

In order to comply with the [ISO4501 certification requirements](https://www.iso.org/standard/63787.html), the SNCB - Mechanics workshop of Arlon needed an application to allow its employees, external visitors and subcontractors to register their presence and verify their knowledge of the security measures to respect in situ.

<ol>
    <li>Attendance sheet: track and give information about who is present in the workshop in order to react and take measures in case of emergency
        <ul>
            <li>name and surname</li>
            <li>compagny</li>
            <li>reason of the presence</li>
            <li>who the person is visiting in the workshop</li>
            <li>clock-in and clock-out times</li>
        </ul>
    </li>
    <li>Verify knowledge of security rules
        <ul>
            <li>Remind security rules with videos and/or text</li>
            <li>Ask questions to assess the person's knowledge about security rules</li>
        </ul>
    </li>
    <li>Different users with permissions
        <ul>
            <li>Several admin roles and permissions</li>
            <li>Users accounts</li>
        </ul>
    </li>
    <li>Content management
        <ul>
            <li>Edition of information content: videos, information</li>
            <li>Edition of the quizzes and control of users knowledge about security</li>
        </ul>
    </li>
</ol>

<p align="center"><a href="#top">-- Back to top --</a></p>
<br />

<h2 id="context">3. Company's Infrastructure And Context</h2>

The SNCB is a big company with strict rules regarding the use of Internet. For example, the firewall did not allow me to surf on Stackoverflow or other useful websites to make researches. 

As the intention was to provide the mechanics workshop with a real tool to use, I started the project with Visual Studio Code (not Visual Studio), .Net Core and a MySQL Server and database. This was for me an extra layer of difficulty as my knowledge was limited to .Net Framework. Also I had to do without the comfort of working with Visual Studio. Eventually, we dropped the idea of using the tool and stayed inside the context of an acamedic project. 

I built the web application with Visual Studio but I kept MySQL and .Net Core as I had already begun with those techs.

Unfortunately, the internship supervisor who was responsible for my time within the mechanics workshop was not familiar with .Net, .Net Core, nor C#. I could not benefit from a lot of support from his part regarding those technologies and had to solve a lot of problems alone. 

<p align="center"><a href="#top">-- Back to top --</a></p>
<br />

<!-- TECHNOLOGIES -->
<h2 id="tech">4. Technologies</h2>

The app was hosted on a physical machine at the main entrance of the workshop: IIS from Windows 10 Enterprise with MySQL Community Server as the database server.

The Framework I used was .Net Core and I used the Asp.Net Core MVC pattern.

<h3 id="built-with">1. Built With</h3>

This section list major tools/frameworks/libraries used to bootstrap my project. 

* [![DBMain][DBMain]][DBMain-url]
* [![MySQL][MySQL]][MySQL-url]
* [![VisualStudio][VisualStudio]][VisualStudio-url]
* [![CSharp][CSharp]][CSharp-url]
* [![DotnetCore][DotNetCore]][DotNetCore-url]
* [![Bootstrap][Bootstrap.com]][Bootstrap-url]
* [![JQuery][JQuery.com]][JQuery-url]
* [![BitBucket][BitBucket]][BitBucket-url]

<p align="center"><a href="#top">-- Back to top --</a></p>
<br />

<!-- CONTACT -->
<h2 id="contact">4. Contact</h2>

[Linkedin](https://www.linkedin.com/in/francoisduroy/)

<br />
<br />

<!-- ACKNOWLEDGMENTS -->
<h2 id="acknowledgments">5. Acknowledgments</h2>

* [Img Shields](https://shields.io)
* [GitHub Pages](https://pages.github.com)
* [OthNeilDrew Readme Template](https://github.com/othneildrew/Best-README-Template)

<p align="center"><a href="#top">-- Back to top --</a></p>

<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[Bootstrap.com]: https://img.shields.io/badge/Bootstrap-563D7C?style=for-the-badge&logo=bootstrap&logoColor=white
[Bootstrap-url]: https://getbootstrap.com

[JQuery.com]: https://img.shields.io/badge/jQuery-0769AD?style=for-the-badge&logo=jquery&logoColor=white
[JQuery-url]: https://jquery.com 

[CSharp]: https://img.shields.io/badge/c%20sharp-239120?style=for-the-badge&logo=csharp&logoColor=white
[CSharp-url]: https://learn.microsoft.com/en-us/dotnet/csharp/

[DotNetCore]: https://img.shields.io/badge/.NET-512BD4?style=for-the-badge&logo=dotnet&logoColor=white
[DotNetCore-url]: https://dotnet.microsoft.com/

[MySQL]: https://img.shields.io/badge/MySQL-4479A1?style=for-the-badge&logo=mysql&logoColor=white
[MySQL-url]: https://www.mysql.com/

[Invision]:https://img.shields.io/badge/invision-FF3366?style=for-the-badge&logo=invision&logoColor=white
[Invision-url]: https://www.invisionapp.com/

[DBMain]: https://img.shields.io/badge/DB%20Main-DB%20Main-red
[DBMain-url]: https://www.db-main.eu/

[VisualStudio]: https://img.shields.io/badge/Visual%20Studio-5C2D91?style=for-the-badge&logo=visualstudio&logoColor=white
[VisualStudio-url]: https://visualstudio.microsoft.com/

[BitBucket]: https://img.shields.io/badge/Bitbucket-0052CC?style=for-the-badge&logo=bitbucket&logoColor=white
[BitBucket-url]: https://bitbucket.org/