﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace DBConnection
{
	public class Parameter
	{
		public object Value { get; set; }
		public DbType? Type { get; set; }
	}
}
